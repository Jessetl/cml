import { Action } from "@ngrx/store";

export const TOGGLE_MENU = "[Menu] TOGGLE";
export const REMOVE_MENU = "[Menu] REMOVE";

export class ToggleMenuAction implements Action {
  readonly type = TOGGLE_MENU;

  constructor(public payload: boolean) {}
}

export type ActionMenu = ToggleMenuAction;
