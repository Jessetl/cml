import { Action } from "@ngrx/store";
import { Shop } from "@models";

export const SET_SHOP = "[Shop] SET";
export const REMOVE_SHOP = "[Shop] REMOVE";

export class SetShopAction implements Action {
  readonly type = SET_SHOP;

  constructor(public payload: Shop[]) {}
}

export class RemoveShopAction implements Action {
  readonly type = REMOVE_SHOP;

  constructor(public payload: Shop[]) {}
}

export type ActionShop = SetShopAction | RemoveShopAction;
