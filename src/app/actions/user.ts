import { Action } from "@ngrx/store";
import { User, Person } from "@models";

export const SET_USER = "[User] SET";
export const UPDATE_PERSON = "[Person] UPDATE";
export const REMOVE_USER = "[User] REMOVE";

export class SetUserAction implements Action {
	readonly type = SET_USER;

	constructor(public payload: User) {}
}

export class UpdatePersonAction implements Action {
	readonly type = UPDATE_PERSON;

	constructor(public payload: Person) {}
}

export class RemoveUserAction implements Action {
	readonly type = REMOVE_USER;

	constructor(public payload: null) {}
}

export type ActionUser = SetUserAction | UpdatePersonAction | RemoveUserAction;
