import { Action } from "@ngrx/store";
import { Token } from "@models";

export const SET_TOKEN = "[Token] SET";
export const REMOVE_TOKEN = "[Token] REMOVE";

export class SetTokenAction implements Action {
  readonly type = SET_TOKEN;

  constructor(public payload: Token) {}
}

export class RemoveTokenAction implements Action {
  readonly type = REMOVE_TOKEN;

  constructor(public id: number) {}
}

export type ActionToken = SetTokenAction | RemoveTokenAction;
