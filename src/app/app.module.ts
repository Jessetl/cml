import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";

// traslate
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

// interceptor
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { TokenInterceptor, HelperService } from "@core/interceptor";

// ngrx
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { StoreRouterConnectingModule, RouterState } from "@ngrx/router-store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";

// config
import { AppRoutingModule } from "./app-routing.module";

// components
import { AppComponent } from "./app.component";

// redux
import { reducers, metaReducers } from "@reducers";
import { environment } from "environments/environment";

const NGRX_IMPORTS = [
  StoreModule.forRoot(reducers, {
    metaReducers,
    runtimeChecks: {
      strictActionImmutability: true, // disable it to avoid the error
    },
  }),
  StoreRouterConnectingModule.forRoot({
    stateKey: "router",
    routerState: RouterState.Minimal,
  }),
  EffectsModule.forRoot([]),
  StoreDevtoolsModule.instrument({
    name: "Angular CMLEXPORTS",
    logOnly: !environment.production,
  }),
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    AppRoutingModule,
    HttpClientModule,
    ...NGRX_IMPORTS,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    HelperService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
