import { Shop } from "@models";
import * as shop from "@actions";

export interface State {
  shop: Shop[];
}

export const initialShopState: State = {
  shop: [],
};

export function reducer(state = initialShopState, action: shop.ActionShop) {
  switch (action.type) {
    case shop.SET_SHOP:
      return {
        ...state,
        shop: action.payload,
      };
    case shop.REMOVE_SHOP:
      return {
        ...state,
        shop: [],
      };
    default:
      return state;
  }
}

export const getShop = (state: State) => state.shop;
