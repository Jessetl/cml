import { User } from "@models";
import * as user from "@actions";

export interface State {
	isAuthenticated: boolean;
	user: User | null;
}

export const initialUserState: State = {
	isAuthenticated: false,
	user: null
};

export function reducer(state = initialUserState, action: user.ActionUser) {
	switch (action.type) {
		case user.SET_USER:
			return {
				...state,
				isAuthenticated: true,
				user: action.payload
			};
		case user.UPDATE_PERSON:
			return {
				...state,
				user: {
					...state.user,
					person: action.payload
				}
			};
		case user.REMOVE_USER:
			return {
				...state,
				isAuthenticated: false,
				user: null
			};
		default:
			return state;
	}
}

export const getUser = (state: State) => state.user;
