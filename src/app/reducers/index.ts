import { environment } from "../../environments/environment";

import { createSelector, createFeatureSelector } from "@ngrx/store";
import { ActionReducerMap, ActionReducer, MetaReducer } from "@ngrx/store";

import { RouterStateUrl } from "../shared/utils/router";

import { storeFreeze } from "ngrx-store-freeze";
import { localStorageSync } from "ngrx-store-localstorage";

import * as fromRouter from "@ngrx/router-store";
import * as fromUser from "./user";
import * as fromToken from "./token";
import * as fromMenu from "./menu";
import * as fromShop from "./shop";

export interface State {
  menu: fromMenu.State;
  user: fromUser.State;
  token: fromToken.State;
  shop: fromShop.State;
  router: fromRouter.RouterReducerState<RouterStateUrl>;
}

export const reducers: ActionReducerMap<State | any> = {
  menu: fromMenu.reducer,
  user: fromUser.reducer,
  token: fromToken.reducer,
  shop: fromShop.reducer,
  router: fromRouter.routerReducer,
};

export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return function (state: State, action: any): State {
    console.log({ state: state, action: action });
    return reducer(state, action);
  };
}

export function localStorageSyncReducer(
  reducer: ActionReducer<State>
): ActionReducer<State> {
  return localStorageSync({
    keys: ["menu", "user", "token", "shop"],
    rehydrate: true,
  })(reducer);
}

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? [logger, storeFreeze, localStorageSyncReducer]
  : [localStorageSyncReducer];

export const getMenuState = createFeatureSelector<fromMenu.State>("menu");

export const getUserState = createFeatureSelector<fromUser.State>("user");

export const getTokenState = createFeatureSelector<fromToken.State>("token");

export const getShopState = createFeatureSelector<fromShop.State>("shop");

export const getMenu = createSelector(
  getMenuState,
  (state: fromMenu.State) => state.isToggled
);

export const getAuth = createSelector(
  getUserState,
  (state: fromUser.State) => state.isAuthenticated
);

export const getToken = createSelector(
  getTokenState,
  (state: fromToken.State) => state.token
);

export const getShop = createSelector(
  getShopState,
  (state: fromShop.State) => state.shop
);

export const getToggled = createSelector(getMenuState, fromMenu.getMenu);

export const getUser = createSelector(getUserState, fromUser.getUser);

export const getStrToken = createSelector(getTokenState, fromToken.getToken);

export const getStore = createSelector(getShopState, fromShop.getShop);

export const getTypeToken = createSelector(
  getTokenState,
  fromToken.getTypeToken
);
