import { User } from "@models";
import * as menu from "@actions";

export interface State {
  isToggled: boolean;
}

export const initialUserState: State = {
  isToggled: false,
};

export function reducer(state = initialUserState, action: menu.ActionMenu) {
  switch (action.type) {
    case menu.TOGGLE_MENU:
      return {
        isToggled: action.payload,
      };
    default:
      return state;
  }
}

export const getMenu = (state: State) => state.isToggled;
