import { Token } from "@models";
import * as token from "@actions";

export interface State {
	token: Token | null;
}

export const initialTokenState: State = {
	token: null
};

export function reducer(state = initialTokenState, action: token.ActionToken) {
	switch (action.type) {
		case token.SET_TOKEN:
			return {
				...state,
				token: action.payload
			};
		case token.REMOVE_TOKEN:
			return {
				...state,
				token: null
			};
		default:
			return state;
	}
}

export const getTypeToken = (state: State) => state.token.token_type;
export const getToken = (state: State) => state.token.token;
