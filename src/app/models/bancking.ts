import { User } from "./user";

export interface Bancking {
  id: number;
  user_id: number;
  headline: string;
  account_number: string;
  interbank: number;
  institution: string;
  tax_identification: string;
  user: User;
  created_at: Date;
  updated_at: Date;
}

export interface BanckingForm {
  id?: number;
  user_id: number;
  headline: string;
  account_number: string;
  interbank: number;
  institution: string;
  tax_identification: string;
}
