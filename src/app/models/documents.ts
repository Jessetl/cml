import { User } from "./user";

export interface Document {
  id: number;
  user_id: string;
  url: string;
  user: User;
  image_url?: string;
  created_at: Date;
  updated_at: Date;
}
