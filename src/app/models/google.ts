export interface RestCountry {
	name: string | null;
	alpha2Code: string;
}
