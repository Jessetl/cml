export interface Content {
  id: number;
  country_id: number;
  type: number;
  type_impost: number;
  impost: number;
  type_insurance: number;
  insurance: number;
  type_nationalization: number;
  nationalization: number;
  type_crossing: number;
  crossing: number;
  created_at?: string;
  update_at?: string;
}
