import { Package } from "./package";
import { User } from "./user";

export interface Charge {
  id: number;
  quote_id: number;
  package_id: number;
  url_file: string;
  file_url?: string;
  barcode: string;
  alphanumeric: string;
  name: string;
  formula: number;
  promotion: number;
  description: string;
  type: number;
  shipping_type: number;
  crossing: number;
  nationalization: number;
  declared: number;
  insured_value: number;
  volumetric_weight: number;
  ups_flat: number;
  cubic_foot: number;
  cubic_meter: number;
  kilograms: number;
  linear_weight: number;
  other_measures: number;
  extra_pound: number;
  weight: number;
  max_weight: number;
  inch: number;
  long: number;
  width: number;
  high: number;
  quantity: number;
  price: number;
  price_user: number;
  price_agency: number;
  cost: number;
  cost_user: number;
  utility: number;
  insurance: number;
  impost: number;
  content: number;
  package: Package;
  dimensions: string;
  status: number;
  location?: location[];
}

interface location {
  id: number;
  user_id: number;
  charge_id: number;
  status: number;
  latitude: string;
  longitude: string;
  updated_at?: string;
  created_at: string;
  user: User;
  es_date?: string;
  es_hour?: string;
}
