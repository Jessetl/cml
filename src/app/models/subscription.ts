export interface Subscription {
  id: number;
  user_id: number;
  customer: string;
  subscription: string;
  product: string;
  price: string;
  unit_amount: number;
  interval: string;
  type: string;
  status: number;
  period_start: null | string;
  period_end: null | string;
  created_at?: string;
  updated_at?: string;
}
