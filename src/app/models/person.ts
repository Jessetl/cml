import { Country } from "./country";

export interface Person {
  id: number;
  user_id: number;
  country_id: number;
  name: string;
  identification_id: string;
  address: string;
  phone?: number;
  cellphone?: number;
  image: string | null;
  zip_code: string;
  state: string;
  city: string;
  contact: string;
  country: Country;
  agency_settings: number | null;
  special_instructions: string | null;
  full_address: string | null;
  image_url?: string;
  format_phone?: string;
  format_cellphone?: string;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
}
