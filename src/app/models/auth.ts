import { Platform } from "./platform";
import { User } from "./user";

export interface Auth {
  email: string;
  password: string;
}

export interface AuthUser extends User {
  access_token: string;
  token_type: string;
  user: User;
  platforms: Platform;
  image_admin: string;
  admin_name?: string;
  expires_in: string;
}
