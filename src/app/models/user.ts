import { Person } from "./person";
import { Role } from "./role";
import { Subscription } from "./subscription";
import { Quote } from "@angular/compiler";
import { PayPalVerification } from "./paypal";
import { Consolidation } from "./consolidation";
import { Bancking } from "./bancking";
import { Productivity } from "./productivity";

export interface User {
  id: number;
  agencyName: string | null;
  name: string | null;
  alphanumeric?: string;
  email: string;
  email_verified_at: string;
  tier: number;
  status: number;
  activated: number;
  invitation: number;
  online: number;
  completed?: number;
  type_pay: number;
  image_admin?: string;
  admin_name?: string;
  es_date?: string;
  user_id: number | null;
  commission: number;
  app_ios: string;
  app_android: string;
  agencyId?: string;
  creator: User | null;
  paypal?: PayPalVerification | null;
  productivity_payment: Productivity[];
  profiles?: Role[];
  reseller?: User[];
  subscription?: Subscription;
  bancking: Bancking;
  consolidation?: Consolidation;
  person: Person;
  role: Role;
  files: Document[];
  agency?: User;
  employees?: User[];
  quotes?: Quote[];
  created_at: Date;
  updated_at: Date;
  deleted_at?: Date;
}

export interface UserForm {
  id?: number;
  email: string;
  tier: number;
  person: Person;
  referralLink?: string;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
}

export interface RemitterReceiverForm {
  id: number;
  alphanumeric: string;
  name: string;
  full_address: string;
  special_instructions: string;
  identification_id: string;
  phone: number;
  cellphone: number | null;
  email: string;
  zip_code: string;
  country_id: number;
  state: string;
  city: string;
  address: string;
  person: Person;
  type_pay: number;
  consolidation?: Consolidation;
}

export interface FormUser {
  id?: number;
  name: string;
  identification_id: string;
  email: string;
  address: string;
  phone: number;
  cellphone: number;
  zip_code: number;
  country_id: number;
  state: string;
  city: string;
  password?: string;
  tier: number;
  role: number;
}

export interface Users extends User {
  data: User[];
  current_page: number;
  last_page: number;
}
