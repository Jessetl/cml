import { Assignment } from "./assignment";
import { User as Agency } from "./user";

export interface Role {
  id: number;
  user_id: number;
  name: string;
  name_en?: string;
  type: number;
  assignments?: Assignment[];
  agency: Agency;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
}

export interface FormRole {
  id?: number;
  user_id: number;
  name: string;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
}

export interface Roles {
  data: Role[];
  current_page: number;
  last_page: number;
}
