import { Charge } from "./charge";
import { User, RemitterReceiverForm } from "./user";
import { ChargeForm } from "./package";
import { Country } from "./country";
import { Invoicing } from "./invoicing";

export interface Quote {
  id: number;
  alphanumeric: string;
  remitter_id: number;
  receiver_id: number;
  country_id: number;
  specials: string;
  quantity_products: number;
  subtotal: number;
  subtotal_user: number;
  subtotal_agency: number;
  impost: number;
  insurance: number;
  nationalization: number;
  crossing: number;
  total: number;
  total_user: number;
  utility: number;
  confirmed_address: boolean;
  status: number;
  invoiced: number;
  remitter: User;
  receiver: User;
  origin: string;
  origin_en: string;
  signaturepad: string | null;
  destiny: string;
  address: string;
  state: string;
  city: string;
  zip_code: string;
  full_address: string;
  es_date: string;
  latitude: string;
  longitude: string;
  user_id: number;
  user: User;
  agencyName?: string;
  charge: Charge[];
  country: Country;
  url?: string;
  invoicing: Invoicing;
  locker: number;
  commission: number;
  created_at?: Date;
  updated_at?: Date;
  deleted_at?: Date;
}

export interface QuoteForm {
  remitter: RemitterReceiverForm;
  receiver: RemitterReceiverForm;
  country_id: number;
  signature: string;
  signatureName: string;
  charges: ChargeForm[];
  latitude: string;
  longitude: string;
  isLocker: boolean;
}

export interface AddressForm {
  address: string;
  zip_code: number;
  country_id: number;
  state: string;
  city: string;
  policy: boolean;
  signature: string;
}
