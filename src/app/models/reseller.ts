export interface PaymentSetting {
  id: number;
  country_id: number;
  user_settings: number;
  payment: number;
  type: number;
  type_amount: number;
  amount: number;
  created_at: Date;
  updated_at: Date;
}

export interface PaymentSettingForm {
  id?: number;
  country_id: number;
  user_settings: number;
  payment: number;
  type: number;
  type_amount: number;
  amount: number;
}
