import { Chat } from "./chat";
import { User } from "./user";
export interface Message {
  id: number;
  user_id: number;
  chat_id: number;
  message: string;
  file?: string;
  file_url?: string;
  status: number;
  created_at?: string;
  updated_at?: string;
  chat?: Chat;
  user?: User;
  receiver_id?: number;
}

export interface Msg {
  data?: Message[];
  last_page?: number;
  to?: number;
}
