import { User } from "./user";

export interface Notification {
  id: number;
  user_id: number;
  user: User;
  tier: number;
  content: string;
  content_en: string;
  translation?: string;
  status: number;
  es_date: string;
  created_at?: string;
  updated_at?: string;
}
