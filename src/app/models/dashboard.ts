import { Package } from "./package";
import { Country } from "./country";
import { Notification } from "./notification";

export interface Dashboard {
  promotions?: Package[];
  shipping_current_month: number;
  shipping_last_month: number;
  order_current_month: number;
  order_last_month: number;
  agencies: number;
  employees: number;
  lockers: number;
  cml: number;
  messages?: number;
  inventory?: Package[];
  seller?: number;
  supervisor?: number;
  consolidation?: string;
  country_consolidation?: Country;
  notifications?: Notification[];
}
