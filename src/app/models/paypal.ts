export interface AccessTokenPayPal {
  token_type: string;
  expires_in: number;
  nonce: string;
  refresh_token: string;
  scope: string;
  access_token: string;
}

export interface CustomerPaypal {
  user_id: string;
  name: string;
  address: Address;
  verified_account?: boolean;
  emails: Email[];
}

interface Address {
  country: string;
  locality: string;
  postal_code: string;
  region: string;
  street_address: string;
}

interface Email {
  confirmed: boolean;
  primary: boolean;
  value: string;
}

export interface PayPalVerification {
  id: number;
  user_id: number;
  user: string;
  name: string;
  email: string;
  country: string;
  zip_code: string;
  confirmed: string;
  created_at?: string;
  updated_at?: string;
}
