export interface Product {
  id: number;
  country_id: number;
  product: string;
  price: string;
  name: string;
  description: string | null;
  status: number;
  unit_amount: number;
  interval: string;
  type: number;
  price_status: number;
  created_at?: string;
  updated_at?: string;
}
