import { SubModule } from "./module";

export interface Assignment {
  id: number;
  role_id: number;
  submodule_id: number;
  type: number;
  submodule: SubModule;
  created_at?: string;
  updated_at?: string;
}

export interface Access {
  id: number;
  type: number;
  checked?: boolean;
}

export interface AssignmentForm {
  profile_id: number;
  access: Access[];
}
