import { PackageImage } from "./package";
export interface Shop {
  id: number;
  name: string;
  sale_price: number;
  quantity: number;
  taxation: number;
  subtotal: number;
  stock: number;
  total: number;
  inStore: boolean;
  images?: any[];
}
