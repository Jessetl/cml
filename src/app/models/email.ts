export interface Email {
  id: number;
  name: string;
  status: number;
  html: string;
  type: number;
  created_at?: string;
  updated_at?: string;
}
