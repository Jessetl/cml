export interface Image {
  id: number;
  url: string;
  original_url: string;
  created_at: boolean;
  deleted_at: boolean;
}

export interface Uploaded {
  images: Image[];
  images_deleted: string[];
}
