import { Country } from "./country";
import { Inventory } from "./inventory";

export interface Package {
  id: number;
  country_id: number;
  url: string;
  type: number;
  formula: number;
  promotion: number;
  cost_pound: number;
  status: number;
  name: string;
  name_en: string;
  description: string;
  description_en: string;
  volumetric_weight: number;
  cubic_foot: number;
  long: number;
  high: number;
  width: number;
  base_price: number;
  agency_price: number;
  corporate_price: number;
  public_price: number;
  sale_price: number;
  minimum_weight: number;
  maximum_weight: number;
  minimum_insurance: number;
  maximum_insurance: number;
  commercial_surplus: number;
  insurance: number;
  country: Country;
  type_content: TypeContent[];
  inventory: Inventory[];
  images: PackageImage[];
  last_movement: Inventory;
  stock: number;
  image_url: string;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
}

export interface PackagePivot extends Package {
  pivot: {
    quantity: number;
    sale_prive: number;
  };
}

export interface TypeContent {
  id: number;
  name?: string;
  package_id: number;
  content: number;
  created_at?: string;
  updated_at?: string;
}

export interface PackageForm {
  id?: number;
  country_id: number;
  type: number;
  formula: number;
  promotion: number;
  cost_pound: number;
  name: string;
  name_en: string;
  description: string;
  description_en: string;
  long: number;
  high: number;
  width: number;
  base_price: number;
  agency_price: number;
  corporate_price: number;
  public_price: number;
  minimum_weight: number;
  maximum_weight: number;
  minimum_insurance: number;
  maximum_insurance: number;
  country: Country;
  status: number;
  created_at?: string;
  updated_at?: string;
  deleted_at?: string;
}

export interface Packages {
  data: Package[];
  current_page: number;
  last_page: number;
}

export interface PackageType {
  type: Package[];
}

export interface ChargeForm {
  id?: number;
  quote_id?: number;
  country_id: number;
  locker_id: number;
  url_file: string;
  file_url?: string;
  name: string;
  package_name: string;
  package_name_en: string;
  formula: number;
  description: number;
  type: string;
  type_content: number;
  shipping_type: string;
  crossing: number;
  nationalization: number;
  declared: number;
  insured_value: number;
  volumetric_weight: number;
  cubic_foot: number;
  ups_flat: string;
  cubic_meter: number;
  kilograms: number;
  linear_weight: number;
  cost: number;
  extra_pound: number;
  price_per_pound: number;
  weight: number;
  max_weight: number;
  max_package_weight: number;
  inch: number;
  long: number;
  width: number;
  high: number;
  price: number;
  price_public: number;
  price_user: number;
  price_agency: number;
  insurance: number;
  impost: number;
  content: string;
  dimensions: string;
}

export interface PackageImage {
  id: number;
  package_id: number;
  url: string;
  image_url: string;
  created_at: Date;
  updated_at: Date;
}
