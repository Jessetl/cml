export interface Consolidation {
  id: number;
  address: string;
  zip_code: string;
  state: string;
  city: string;
  created_at?: string;
  updated_at?: string;
}
