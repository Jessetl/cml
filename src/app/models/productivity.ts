import { Quote } from "./quote";
import { User } from "./user";

export interface Productivity {
  id: number;
  user_id: number;
  alphanumeric: string;
  barcode: string;
  signaturename: string;
  signaturepad: string;
  url: string;
  income: number;
  shipping: number;
  total: number;
  status: number;
  es_date: string;
  url_file: string;
  reseller: User;
  payment: ProductivityPay;
  created_at: Date;
  updated_at: Date;
}

export interface ProductivityPay {
  id: number;
  productivity_id: number;
  voucher: string;
  file_url: string;
  transaction?: string;
  type: number;
  bank_origin: string;
  last_origin: number;
  bank_destiny: string;
  last_destiny: number;
  reference: string;
  amount: number;
  productivity: Productivity;
  created_at: Date;
  updated_at: Date;
}

export interface UserProductivity {
  user_id: number;
  user: User;
  tier: string;
  email: string;
  payment_income: number;
  payment_shipping: number;
  quotes_id: number[];
  users_id: number[];
  quotes: Quote[];
  users: User[];
}
