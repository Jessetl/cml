import { User } from "./user";
import { Message } from "./messages";

export interface Chat {
  id: number;
  name?: string;
  role?: string;
  transmitter_id: number;
  receiver_id: number;
  status: number;
  badge?: number;
  chat_user?: Message[];
  current_page?: number;
  to?: any;
  created_at?: string;
  updated_at?: string;
  transmitter?: User;
  receiver?: User;
}
