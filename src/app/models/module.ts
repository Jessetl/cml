import { Assignment } from "./assignment";

export interface SubModule {
  id: number;
  module_id: number;
  name: string | null;
  nameModule: string | null;
  path: string;
  type: number;
  status: boolean;
  module: Module;
  created_at?: string;
  updated_at?: string;
}

export interface Module {
  id: number;
  name: string;
  path: string;
  icon: string;
  type: string;
  visible: number;
  active: boolean;
  status: boolean;
  sub_module: SubModule[];
  created_at?: string;
  updated_at?: string;
}

export interface ModulesAndAssignments {
  modules: Module[];
  assignments: Assignment[];
}
