import { User } from "./user";

export interface Commodity {
  id: number;
  user_id: number;
  quote_id: number | null;
  alphanumeric: string | null;
  description: string | null;
  amount: number | null;
  file: string | null;
  status: number;
  insured_value?: number;
  user?: User;
  date?: string;
  file_url?: string;
  created_at?: string;
  updated_at?: string;
}
