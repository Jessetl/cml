import { Shop } from "./store";

export interface Payment {
  id: number;
  invoice_id: number;
  voucher?: string;
  file_url?: string;
  transaction?: string;
  type: number;
  bank_origin?: string;
  last_origin?: number;
  bank_destiny?: string;
  last_destiny?: number;
  reference: string;
  amount: number;
  created_at?: string;
  updated_at?: string;
}

export interface TransferForm {
  shop: Shop[];
  bank_id?: number;
  issueDate: Date;
  transaction: string;
  bank_origin: string;
  last_origin: number;
  bank_destiny: string;
  last_destiny: number;
  reference: string;
  amount: number;
  voucher: string;
  latitude: string;
  longitude: string;
}

export interface DepositForm {
  shop: Shop[];
  bank_id?: number;
  issueDate: Date;
  origin: string;
  reference: string;
  amount: number;
  voucher: string;
  latitude: string;
  longitude: string;
}

export interface PaypalForm {
  issueDate: Date;
  destiny: string;
  reference: string;
  amount: number;
  voucher: string;
  latitude: string;
  longitude: string;
}

export interface StripeForm {
  shop: Shop[];
  token: string;
  amount: number;
  latitude: string;
  longitude: string;
}
