import { User } from "./user";

export interface Code {
  id: number;
  user_id: number;
  code: string;
  status: number;
  created_at: string;
  updated_at: string;
  user: User;
}
