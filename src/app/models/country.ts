import { Content } from "./content";
import { Package } from "./package";
import { Product } from "./product";
import { PaymentSetting } from "./reseller";

export interface Country {
  id: number;
  name: string;
  name_en: string;
  alpha2Code: string;
  type_impost: boolean;
  impost: number;
  type_insurance: boolean;
  insurance: number;
  type_nationalization: boolean;
  nationalization: number;
  type_crossing: boolean;
  crossing: number;
  membership_agency?: number;
  membership_corporate?: number;
  status: number;
  user_settings: number;
  type_taxation: number;
  taxation: number;
  type_extra: number;
  extra: number;
  membership_product?: Product[];
  content: Content[];
  packages: Package[];
  payment_setting: PaymentSetting[];
  created_at?: string;
  updated_at?: string;
}

export interface CountryForm {
  id?: number;
  name: string;
  name_en: string;
  alpha2Code: string;
  type_impost: boolean;
  impost: number;
  type_insurance: boolean;
  insurance: number;
  type_nationalization: boolean;
  nationalization: number;
  type_crossing: boolean;
  crossing: number;
  type_taxation: number;
  taxation: number;
  type_extra: number;
  extra: number;
}

export interface Countries {
  data: Country[];
  current_page: number;
  last_page: number;
}
