export interface PropsValue {
  name: string;
  value: string | number;
}
