import { Package } from "./package";

export interface Inventory {
  id: number;
  package_id: number;
  type: number;
  movement: number;
  current_stock: number;
  amount: number;
  status: number;
  packages: Package;
  created_at: Date;
  updated_at: Date;
}

export interface StockForm {
  id: number;
  amount: number;
  sale_price: number;
}
