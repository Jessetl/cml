import { User } from "./user";
import { Quote } from "./quote";
import { Payment } from "./payment";
import { PackagePivot } from "./package";
import { Subscription } from "./subscription";

export interface Invoicing {
  id: number;
  user_id: number;
  alphanumeric: string;
  type: number;
  url: string;
  cost: number;
  cost_user: number;
  charges: number;
  total: number;
  total_user: number;
  status: number;
  membership: null | number;
  subscription: null | Subscription;
  invoiced_by: number;
  quantity: number;
  locker: number;
  user: User;
  es_date?: string;
  quotes: Quote[];
  packages: PackagePivot[];
  payment?: Payment[];
  created_at?: string;
  updated_at?: string;
}
