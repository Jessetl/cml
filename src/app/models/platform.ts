export interface Platform {
  app_ios: string;
  app_android: string;
  created_at: Date;
  updated_at: Date;
}
