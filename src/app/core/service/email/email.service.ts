import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "environments/environment";
import { Email } from "@models";

@Injectable({
  providedIn: "root",
})
export class EmailService {
  constructor(private http: HttpClient) {}

  getEmails = () => {
    return this.http
      .get<Email[]>(`${environment.apiUrl}/emails`)
      .pipe(map((emails: Email[]) => emails));
  };

  updateEmail = (form: any) => {
    return this.http
      .post<Email>(`${environment.apiUrl}/emails/update`, {
        ...form,
      })
      .pipe(map((emails: Email) => emails));
  };
}
