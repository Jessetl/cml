import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Countries, CountryForm, Country, Product } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class CountryService {
  constructor(private http: HttpClient) {}

  getCountries = () => {
    return this.http
      .get<Country[]>(`${environment.apiUrl}/countries`)
      .pipe(map((countries: Country[]) => countries));
  };

  getCountriesByPage = (paginate: number) => {
    return this.http
      .get<Countries>(`${environment.apiUrl}/countries?paginate=${paginate}`)
      .pipe(map((countries: Countries) => countries));
  };

  getCountryUnitedStates = () => {
    return this.http
      .get<Country>(`${environment.apiUrl}/countries/us`)
      .pipe(map((countries: Country) => countries));
  };

  store = (country: CountryForm) => {
    return this.http
      .post<CountryForm>(`${environment.apiUrl}/countries`, country)
      .pipe(map((country: CountryForm) => country));
  };

  status = (id: {}) => {
    return this.http
      .post<Country>(`${environment.apiUrl}/countries/status`, id)
      .pipe(map((country: Country) => country));
  };

  search = (value: {}, paginate: number) => {
    return this.http
      .post<Country[]>(`${environment.apiUrl}/countries/search`, value)
      .pipe(map((countries: Country[]) => countries));
  };

  update = (country: CountryForm) => {
    return this.http
      .put<CountryForm>(
        `${environment.apiUrl}/countries/${country.id}`,
        country
      )
      .pipe(map((country: CountryForm) => country));
  };

  destroy = (countryId: number) => {
    return this.http
      .delete<Country>(`${environment.apiUrl}/countries/${countryId}`)
      .pipe(map((country: Country) => country));
  };

  getProduct = () => {
    return this.http
      .get<Product[]>(`${environment.apiUrl}/country/product`)
      .pipe(map((product: Product[]) => product));
  };
}
