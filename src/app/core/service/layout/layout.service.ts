import { Injectable } from "@angular/core";
import { Router, NavigationEnd } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

import { User, FormUser } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class LayoutService {
  private previousUrl: string;
  private currentUrl: string;

  constructor(private router: Router, private http: HttpClient) {
    this.currentUrl = this.router.url;

    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
      }
    });
  }

  store = (user: FormUser) => {
    return this.http
      .post<User>(`${environment.apiUrl}/layout/users`, {
        ...user,
      })
      .pipe(map((user: User) => user));
  };

  public getPreviousUrl() {
    return this.previousUrl;
  }
}
