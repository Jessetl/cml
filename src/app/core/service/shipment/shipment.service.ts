import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Quote } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class ShipmentService {
  constructor(private http: HttpClient) {}

  getShipmentByAlphanumeric = (alphanumeric: string) => {
    return this.http
      .get<Quote>(`${environment.apiUrl}/shipments/summary/${alphanumeric}`)
      .pipe(map((shipment: Quote) => shipment));
  };
}
