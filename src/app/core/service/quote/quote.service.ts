import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Quote, QuoteForm, Charge, User } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class QuoteService {
  constructor(private http: HttpClient) {}

  getCharge = () => {
    return this.http
      .get<Quote[]>(`${environment.apiUrl}/quotes`)
      .pipe(map((charge: Quote[]) => charge));
  };

  searchQuote = (value: {}) => {
    return this.http
      .post<Quote[]>(`${environment.apiUrl}/quotes/search`, value)
      .pipe(map((quotes: Quote[]) => quotes));
  };

  invoice = (form: { quotes: number[] }) => {
    return this.http
      .post<Quote[]>(`${environment.apiUrl}/quotes/invoice`, form)
      .pipe(map((quotes: Quote[]) => quotes));
  };

  invoiceByCML = (form: { typeInvoice: number; agency?: number }) => {
    return this.http
      .post<number[]>(`${environment.apiUrl}/quotes/invoice-by-cml`, form)
      .pipe(map((quotes: number[]) => quotes));
  };

  payShipping = (form: {
    amount: number;
    token: string;
    quoteIds: number[];
    agency: number;
    latitude: string;
    longitude: string;
  }) => {
    return this.http
      .post<number[]>(`${environment.apiUrl}/payments`, form)
      .pipe(map((quotes: number[]) => quotes));
  };

  payShippingTransfer = (form: {
    quoteIds: number[];
    issueDate: Date;
    origin: string;
    lastDigitsOrigin: number;
    destiny: string;
    lastDigitsDestiny: number;
    amount: number;
    reference: string;
    voucher: string;
    latitude: string;
    longitude: string;
    agency: number;
  }) => {
    return this.http
      .post<number[]>(`${environment.apiUrl}/payments/transfer`, form)
      .pipe(map((quotes: number[]) => quotes));
  };

  payShippingCheck = (form: {
    quoteIds: number[];
    origin: string;
    amount: number;
    reference: string;
    voucher: string;
    latitude: string;
    longitude: string;
    agency: number;
  }) => {
    return this.http
      .post<number[]>(`${environment.apiUrl}/payments/check`, form)
      .pipe(map((quotes: number[]) => quotes));
  };

  store = (form: QuoteForm) => {
    return this.http
      .post<Quote>(`${environment.apiUrl}/quotes`, form)
      .pipe(map((quote: Quote) => quote));
  };

  getPackageTracking = (nameField: string, nameValue: string) => {
    return this.http
      .post<Quote[]>(`${environment.apiUrl}/quotes/tracking`, {
        [nameField]: nameValue,
      })
      .pipe(map((quote: Quote[]) => quote));
  };

  getPackagesToBeInvoiced = () => {
    return this.http
      .post<Quote[]>(`${environment.apiUrl}/quotes/invoiced`, {})
      .pipe(map((quotes: Quote[]) => quotes));
  };

  getAgencyWithInvoice = () => {
    return this.http
      .post<User[]>(`${environment.apiUrl}/quotes/agencies`, {})
      .pipe(map((agencies: User[]) => agencies));
  };

  setStatusCharge = (form: {
    quoteId: number;
    progress: number[];
    latitude: string;
    longitude: string;
  }) => {
    return this.http
      .post<Charge[]>(`${environment.apiUrl}/quotes/packing/status`, form)
      .pipe(map((charge: Charge[]) => charge));
  };

  setInvoiceFee = (quoteId: number) => {
    return this.http
      .post<Quote>(`${environment.apiUrl}/quotes/fee`, { quoteId: quoteId })
      .pipe(map((quote: Quote) => quote));
  };

  printPackging = (form: { quoteId: number }) => {
    return this.http
      .post<string>(`${environment.apiUrl}/quotes/packing`, form)
      .pipe(map((url: string) => url));
  };

  printPackgingTag = (form: { quoteId: number; chargeId: number }) => {
    return this.http
      .post<string>(`${environment.apiUrl}/quotes/packingtag`, form)
      .pipe(map((url: string) => url));
  };

  printBoarding = (form: {
    quoteId: number;
    chargeId: number;
    namePackage: string;
  }) => {
    return this.http
      .post<string>(`${environment.apiUrl}/quotes/boarding`, form)
      .pipe(map((url: string) => url));
  };

  printInternalTraffic = (form: {
    quoteId: number;
    chargeId: number;
    namePackage: string;
  }) => {
    return this.http
      .post<string>(`${environment.apiUrl}/quotes/traffic`, form)
      .pipe(map((url: string) => url));
  };
}
