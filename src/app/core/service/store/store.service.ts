import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Package, Payment } from "@models";
import { DepositForm, StripeForm, TransferForm } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class StoreService {
  constructor(private http: HttpClient) {}

  getPackageWithStock = () => {
    return this.http
      .get<Package[]>(`${environment.apiUrl}/store`)
      .pipe(map((articles: Package[]) => articles));
  };

  payStripeStore = (form: StripeForm) => {
    return this.http
      .post<Payment>(`${environment.apiUrl}/store/pay/stripe`, form)
      .pipe(map((payment: Payment) => payment));
  };

  payTransferStore = (form: TransferForm) => {
    return this.http
      .post<Payment>(`${environment.apiUrl}/store/pay/transfer`, form)
      .pipe(map((payment: Payment) => payment));
  };

  payDepositStore = (form: DepositForm) => {
    return this.http
      .post<Payment>(`${environment.apiUrl}/store/pay/deposit`, form)
      .pipe(map((payment: Payment) => payment));
  };
}
