import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Dashboard } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class DashboardService {
  constructor(private http: HttpClient) {}

  getDashboard = () => {
    return this.http
      .get<Dashboard>(`${environment.apiUrl}/dashboard`)
      .pipe(map((dashboard: Dashboard) => dashboard));
  };
}
