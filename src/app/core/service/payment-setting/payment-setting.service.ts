import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { PaymentSetting, PaymentSettingForm } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class PaymentSettingService {
  constructor(private http: HttpClient) {}

  storeOrUpdate = (setting: PaymentSettingForm) => {
    return this.http
      .post<PaymentSetting>(`${environment.apiUrl}/payments/settings`, setting)
      .pipe(map((payment: PaymentSetting) => payment));
  };
}
