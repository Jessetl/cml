import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "environments/environment";
import { Chat, User, Message, Msg } from "@models";
const headers = new HttpHeaders({
  "Content-Type": "application/json",
  Accept: ["jpg", "png", "jpeg", "application/pdf"],
});

@Injectable({
  providedIn: "root",
})
export class ChatService {
  constructor(private http: HttpClient) {}

  getChats = () => {
    return this.http
      .get<Chat[]>(`${environment.apiUrl}/chat`)
      .pipe(map((chat: Chat[]) => chat));
  };

  getArchivedChats = () => {
    return this.http
      .get<Chat[]>(`${environment.apiUrl}/chat/archived`)
      .pipe(map((chat: Chat[]) => chat));
  };

  getChat = (id: number) => {
    return this.http
      .get<Chat>(`${environment.apiUrl}/chat/user/${id}`)
      .pipe(map((chat: Chat) => chat));
  };

  getUsersOnline = () => {
    return this.http
      .get<User[]>(`${environment.apiUrl}/chat/users`)
      .pipe(map((users: User[]) => users));
  };

  createChat = (form) => {
    return this.http
      .post<Chat>(`${environment.apiUrl}/chat/create`, form)
      .pipe(map((chat: Chat) => chat));
  };

  sendFile = (form) => {
    return this.http
      .post<Message>(`${environment.apiUrl}/chat/file`, form)
      .pipe(map((message: Message) => message));
  };

  downloadFile = (form) => {
    return this.http.post(`${environment.apiUrl}/chat/download`, form, {
      headers,
      responseType: "blob",
    });
  };

  archiveChat = (form) => {
    return this.http
      .post<Chat>(`${environment.apiUrl}/chat/archive`, form)
      .pipe(map((chat: Chat) => chat));
  };

  message = (id: number, page: number) => {
    return this.http
      .get<Msg>(`${environment.apiUrl}/chat/messages?id=${id}&page=${page}`)
      .pipe(map((messages: Msg) => messages));
  };
}
