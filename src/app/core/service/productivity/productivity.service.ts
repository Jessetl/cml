import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";

import { UserProductivity, Productivity } from "@models";
import { DepositForm, PaypalForm, TransferForm } from "@models";
// Environment
import { environment } from "environments/environment";

const headers = new HttpHeaders({
  "Content-Type": "application/json",
  Accept: "application/pdf",
});

@Injectable({
  providedIn: "root",
})
export class ProductivityService {
  constructor(private http: HttpClient) {}

  getProductivity = () => {
    return this.http
      .get<Productivity[]>(`${environment.apiUrl}/productivity`)
      .pipe(map((productivity: Productivity[]) => productivity));
  };

  signature = (form: {
    id: number;
    signaturename: string;
    signature: string;
  }) => {
    return this.http
      .post<Productivity>(`${environment.apiUrl}/productivity/signature`, form)
      .pipe(map((productivity: Productivity) => productivity));
  };

  search = (value: {}) => {
    return this.http
      .post<Productivity[]>(`${environment.apiUrl}/productivity/search`, value)
      .pipe(map((productivity: Productivity[]) => productivity));
  };

  getShippingPaymentsByUsers = () => {
    return this.http
      .get<UserProductivity[]>(`${environment.apiUrl}/productivity/payments`)
      .pipe(map((productivity: UserProductivity[]) => productivity));
  };

  payPaypalStore = (productivity: UserProductivity, paypal: PaypalForm) => {
    return this.http
      .post<string>(`${environment.apiUrl}/productivity/paypal`, {
        ...productivity,
        ...paypal,
      })
      .pipe(map((url: string) => url));
  };

  payDepositStore = (productivity: UserProductivity, deposit: DepositForm) => {
    return this.http
      .post<string>(`${environment.apiUrl}/productivity/deposit`, {
        ...productivity,
        ...deposit,
      })
      .pipe(map((url: string) => url));
  };

  payTransferStore = (
    productivity: UserProductivity,
    transfer: TransferForm
  ) => {
    return this.http
      .post<string>(`${environment.apiUrl}/productivity/transfer`, {
        ...productivity,
        ...transfer,
      })
      .pipe(map((url: string) => url));
  };
}
