import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { User, FormUser, Uploaded } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class SupervisorService {
  constructor(private http: HttpClient) {}

  getResellers() {
    return this.http
      .get<User[]>(`${environment.apiUrl}/supervisors`)
      .pipe(map((resellers: User[]) => resellers));
  }

  getResellersBySearch = (form: {}) => {
    return this.http
      .post<User[]>(`${environment.apiUrl}/supervisors/search`, {
        ...form,
      })
      .pipe(map((resellers: User[]) => resellers));
  };

  store = (user: FormUser) => {
    return this.http
      .post<User>(`${environment.apiUrl}/supervisors`, {
        ...user,
      })
      .pipe(map((user: User) => user));
  };

  update = (user: FormUser) => {
    return this.http
      .put<User>(`${environment.apiUrl}/supervisors/${user.id}`, user)
      .pipe(map((user: User) => user));
  };

  status = (form: { id: number }) => {
    return this.http
      .post<User>(`${environment.apiUrl}/supervisors/status`, form)
      .pipe(map((user: User) => user));
  };

  link = (form: { id: number }) => {
    return this.http
      .post<User>(`${environment.apiUrl}/supervisors/link`, form)
      .pipe(map((user: User) => user));
  };

  upload = (userId: number, form: Uploaded) => {
    return this.http
      .put<User>(`${environment.apiUrl}/supervisors/images/${userId}`, form)
      .pipe(map((user: User) => user));
  };
}
