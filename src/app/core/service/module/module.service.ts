import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { ModulesAndAssignments } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class ModuleService {
  constructor(private http: HttpClient) {}

  getModules() {
    return this.http
      .get<ModulesAndAssignments>(`${environment.apiUrl}/modules`)
      .pipe(map((module: ModulesAndAssignments) => module));
  }
}
