import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Invoicing, Payment } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class InvoicingService {
  constructor(private http: HttpClient) {}

  getInvoicing = () => {
    return this.http
      .get<Invoicing[]>(`${environment.apiUrl}/invoicing`)
      .pipe(map((charge: Invoicing[]) => charge));
  };

  printInvoicing = (invoiceId: number) => {
    return this.http
      .get<string>(`${environment.apiUrl}/invoicing/print/${invoiceId}`)
      .pipe(map((url: string) => url));
  };

  search = (value: {}) => {
    return this.http
      .post<Invoicing[]>(`${environment.apiUrl}/invoicing/search`, value)
      .pipe(map((quotes: Invoicing[]) => quotes));
  };

  remove = (id: number) => {
    return this.http
      .post<Invoicing>(`${environment.apiUrl}/invoicing/remove`, {
        id: id,
      })
      .pipe(map((invoicing: Invoicing) => invoicing));
  };

  payments = (id: number) => {
    return this.http
      .get<Payment[]>(`${environment.apiUrl}/invoicing/payments/${id}`)
      .pipe(map((payments: Payment[]) => payments));
  };

  status = (id: number) => {
    return this.http
      .post<Invoicing>(`${environment.apiUrl}/invoicing/status`, {
        id: id,
      })
      .pipe(map((invoicing: Invoicing) => invoicing));
  };
}
