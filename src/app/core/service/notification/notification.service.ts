import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Notification } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class NotificationService {
  constructor(private http: HttpClient) {}

  getNotifications = () => {
    return this.http
      .get<Notification[]>(`${environment.apiUrl}/notification`)
      .pipe(map((notification: Notification[]) => notification));
  };

  create = (form) => {
    return this.http
      .post<Notification>(`${environment.apiUrl}/notification/create`, {
        ...form,
      })
      .pipe(map((notification: Notification) => notification));
  };

  update = (form) => {
    return this.http
      .post<Notification>(`${environment.apiUrl}/notification/update`, {
        ...form,
      })
      .pipe(map((notification: Notification) => notification));
  };
}
