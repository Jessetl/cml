import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Document } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class FilesService {
  constructor(private http: HttpClient) {}

  getUserFiles = () => {
    return this.http
      .get<Document[]>(`${environment.apiUrl}/files`)
      .pipe(map((files: Document[]) => files));
  };
}
