import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, Observer } from "rxjs";
import { async } from "rxjs/internal/scheduler/async";

@Injectable({
  providedIn: "root",
})
export class GooglemapsService {
  private endpoint = "https://restcountries.eu/rest/v2";
  private geocoder: google.maps.Geocoder;

  constructor(private http: HttpClient) {
    this.geocoder = new google.maps.Geocoder();
  }

  getCountries = () => {
    return this.http.get(`${this.endpoint}/all`);
  };

  getCountryByCode = (alphacode: string) => {
    return this.http.get(`${this.endpoint}/alpha/${alphacode}`);
  };

  getCountryByName = (name: string) => {
    return this.http.get(`${this.endpoint}/name/${name}`);
  };

  getLocation = async ({ timeout }) => {
    var options = {
      enableHighAccuracy: true,
      timeout: timeout,
      maximumAge: 0,
    };
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
  };

  getZipCode(
    components: google.maps.GeocoderComponentRestrictions
  ): Observable<google.maps.GeocoderResult[]> {
    return Observable.create(
      (observer: Observer<google.maps.GeocoderResult[]>) => {
        // Invokes geocode method of Google Maps API geocoding.
        this.geocoder.geocode(
          { componentRestrictions: components },
          (
            results: google.maps.GeocoderResult[],
            status: google.maps.GeocoderStatus
          ) => {
            if (status === google.maps.GeocoderStatus.OK) {
              observer.next(results);
              observer.complete();
            } else {
              console.log(
                "Geocoding service: geocode was not successful for the following reason: " +
                  status
              );
              observer.error(status);
            }
          }
        );
      }
    );
  }

  getStateAndCity(res: any) {
    const stateKey = "locality",
      cityKey = "administrative_area_level_1",
      response = res[0];

    let state = "",
      city = "";

    response.address_components.forEach((component) => {
      component.types.forEach((type) => {
        if (type === stateKey) {
          city = component.long_name;
        } else if (type === cityKey) {
          state = component.long_name;
        }
      });
    });

    return new Promise<any>((resolve, _) => {
      resolve({ state: state, city: city });
    });
  }
}
