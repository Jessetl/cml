import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import {
  Role as Profile,
  FormRole as FormProfile,
  Roles as Profiles,
} from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class ProfileService {
  constructor(private http: HttpClient) {}

  getProfiles() {
    return this.http
      .get<Profile[]>(`${environment.apiUrl}/profiles`)
      .pipe(map((profiles: Profile[]) => profiles));
  }

  getProfilesByUser(id: string) {
    return this.http
      .post<Profile[]>(`${environment.apiUrl}/profiles/user`, { id: id })
      .pipe(map((profiles: Profile[]) => profiles));
  }

  getProfilesByPage(paginate: number) {
    return this.http
      .get<Profiles>(`${environment.apiUrl}/profiles?paginate=${paginate}`)
      .pipe(map((profiles: Profiles) => profiles));
  }

  store = (profile: FormProfile) => {
    return this.http
      .post<FormProfile>(`${environment.apiUrl}/profiles`, profile)
      .pipe(map((profile: FormProfile) => profile));
  };

  update = (profile: FormProfile) => {
    return this.http
      .put<FormProfile>(`${environment.apiUrl}/profiles/${profile.id}`, profile)
      .pipe(map((profile: FormProfile) => profile));
  };

  destroy = (profileId: number) => {
    return this.http
      .delete<Profile>(`${environment.apiUrl}/profiles/${profileId}`)
      .pipe(map((profile: Profile) => profile));
  };
}
