import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Package, StockForm, Uploaded } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class InventoryService {
  constructor(private http: HttpClient) {}

  getPackageForSales = () => {
    return this.http
      .get<Package[]>(`${environment.apiUrl}/inventory/sale`)
      .pipe(map((packages: Package[]) => packages));
  };

  update = (form: StockForm) => {
    return this.http
      .put<Package>(`${environment.apiUrl}/inventory/${form.id}`, form)
      .pipe(map((packages: Package) => packages));
  };

  upload = (packageId: number, form: Uploaded) => {
    return this.http
      .put<Package>(`${environment.apiUrl}/inventory/images/${packageId}`, form)
      .pipe(map((packages: Package) => packages));
  };
}
