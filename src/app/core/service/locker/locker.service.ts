import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Quote, User, FormUser, AddressForm, Subscription } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class LockerService {
  constructor(private http: HttpClient) {}

  getShipmentsByLocker = () => {
    return this.http
      .get<Quote[]>(`${environment.apiUrl}/locker`)
      .pipe(map((shipments: Quote[]) => shipments));
  };

  getQuotes = () => {
    return this.http
      .get<Quote[]>(`${environment.apiUrl}/locker/shipments`)
      .pipe(map((shipments: Quote[]) => shipments));
  };

  search = (form) => {
    return this.http
      .post<Quote[]>(`${environment.apiUrl}/locker/search`, form)
      .pipe(map((shipments: Quote[]) => shipments));
  };

  byTracking = (form) => {
    return this.http
      .post<Quote[]>(`${environment.apiUrl}/locker/tracking`, form)
      .pipe(map((shipments: Quote[]) => shipments));
  };

  shippingAddresss = (form: AddressForm, id: number) => {
    return this.http
      .post<Quote>(`${environment.apiUrl}/locker/shipping/${id}`, form)
      .pipe(map((quote: Quote) => quote));
  };

  store = (user: FormUser) => {
    return this.http
      .post<User>(`${environment.apiUrl}/locker`, {
        ...user,
      })
      .pipe(map((user: User) => user));
  };

  update = (user: FormUser) => {
    return this.http
      .put<User>(`${environment.apiUrl}/locker/${user.id}`, user)
      .pipe(map((user: User) => user));
  };

  assignment = (form: { user_id: number; reseller_id: number }) => {
    return this.http
      .post<User>(`${environment.apiUrl}/locker/reseller`, form)
      .pipe(map((user: User) => user));
  };

  payShipping = (form: {
    amount: number;
    token: string;
    quoteId: number;
    latitude: string;
    longitude: string;
  }) => {
    return this.http
      .post<Quote>(`${environment.apiUrl}/locker/pay`, form)
      .pipe(map((quote: Quote) => quote));
  };

  getPaymentInvoice = (invoiceId: number) => {
    return this.http
      .get<string>(`${environment.apiUrl}/locker/print/${invoiceId}`)
      .pipe(map((url: string) => url));
  };

  subscriptionBasic = (type_pay: number) => {
    return this.http
      .post<User>(`${environment.apiUrl}/locker/subscription-shipping`, {
        type_pay: type_pay,
      })
      .pipe(map((user: User) => user));
  };

  subscription = (form: any) => {
    return this.http
      .post<Subscription>(`${environment.apiUrl}/locker/membership`, {
        ...form,
      })
      .pipe(map((subscription: Subscription) => subscription));
  };

  cancelSubscription = (type: number) => {
    return this.http
      .post<Subscription>(`${environment.apiUrl}/locker/subscription-cancel`, {
        type: type,
      })
      .pipe(map((subscription: Subscription) => subscription));
  };

  reactivate = (form) => {
    return this.http
      .post<User>(`${environment.apiUrl}/locker/reactivate`, {
        ...form,
      })
      .pipe(map((user: User) => user));
  };
}
