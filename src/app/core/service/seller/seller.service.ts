import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { User, FormUser, PayPalVerification } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class SellerService {
  constructor(private http: HttpClient) {}

  getResellers() {
    return this.http
      .get<User[]>(`${environment.apiUrl}/sellers`)
      .pipe(map((resellers: User[]) => resellers));
  }

  getResellersBySearch = (form: {}) => {
    return this.http
      .post<User[]>(`${environment.apiUrl}/sellers/search`, {
        ...form,
      })
      .pipe(map((resellers: User[]) => resellers));
  };

  store = (user: FormUser) => {
    return this.http
      .post<User>(`${environment.apiUrl}/sellers`, {
        ...user,
      })
      .pipe(map((user: User) => user));
  };

  update = (user: FormUser) => {
    return this.http
      .put<User>(`${environment.apiUrl}/sellers/${user.id}`, user)
      .pipe(map((user: User) => user));
  };

  status = (form: { id: number }) => {
    return this.http
      .post<User>(`${environment.apiUrl}/sellers/status`, form)
      .pipe(map((user: User) => user));
  };

  link = (form: { id: number }) => {
    return this.http
      .post<User>(`${environment.apiUrl}/sellers/link`, form)
      .pipe(map((user: User) => user));
  };

  changeSupervisor = (form: { id: number }) => {
    return this.http
      .post<User>(`${environment.apiUrl}/sellers/change`, form)
      .pipe(map((user: User) => user));
  };

  verificationPaypal = (form: any) => {
    return this.http
      .post<PayPalVerification>(`${environment.apiUrl}/sellers/verification`, {
        ...form,
      })
      .pipe(map((verification: PayPalVerification) => verification));
  };
}
