import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { environment } from "environments/environment";

const headers = new HttpHeaders({
  "Content-Type": "application/json",
  Accept: "application/pdf",
});

@Injectable({
  providedIn: "root",
})
export class ReportsService {
  constructor(private http: HttpClient) {}

  getMovements(form: {
    from: Date;
    to: Date;
    category?: number;
    type: number;
    total: number;
  }) {
    return this.http.post(`${environment.apiUrl}/reports/movements`, form, {
      headers,
      responseType: "blob",
    });
  }

  getCommissions(form: {
    from: Date;
    to: Date;
    typeAgency: number;
    agency?: number;
    status: number;
  }) {
    return this.http.post(`${environment.apiUrl}/reports/commissions`, form, {
      headers,
      responseType: "blob",
    });
  }

  getAdditonals(form: {
    from: Date;
    to: Date;
    charge_additionals: number;
    filter: number;
    total: number;
  }) {
    return this.http.post(`${environment.apiUrl}/reports/additionals`, form, {
      headers,
      responseType: "blob",
    });
  }

  getLocations(form: {
    from: Date;
    to: Date;
    typeAgency: number;
    agency?: number;
    status: number;
  }) {
    return this.http.post(`${environment.apiUrl}/reports/locations`, form, {
      headers,
      responseType: "blob",
    });
  }

  getGlobals(form: { type: number; period: number }) {
    return this.http.post(`${environment.apiUrl}/reports/globals`, form, {
      headers,
      responseType: "blob",
    });
  }
  getPay(form: any) {
    return this.http.post(
      `${environment.apiUrl}/reports/pay`,
      { ...form },
      {
        headers,
        responseType: "blob",
      }
    );
  }
  getAccountStatus(form: any) {
    return this.http.post(
      `${environment.apiUrl}/reports/account-status`,
      { ...form },
      {
        headers,
        responseType: "blob",
      }
    );
  }
  getProductivity(form: any) {
    return this.http.post(
      `${environment.apiUrl}/reports/productivity`,
      { ...form },
      {
        headers,
        responseType: "blob",
      }
    );
  }
}
