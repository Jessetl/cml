import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";
import { environment } from "environments/environment";
import { Commodity } from "@models";

@Injectable({
  providedIn: "root",
})
export class ConmodityService {
  constructor(private http: HttpClient) {}

  getCommoditys = () => {
    return this.http
      .get<Commodity[]>(`${environment.apiUrl}/commodity`)
      .pipe(map((commodity: Commodity[]) => commodity));
  };

  create = (form) => {
    return this.http
      .post<Commodity>(`${environment.apiUrl}/commodity/create`, { ...form })
      .pipe(map((commodity: Commodity) => commodity));
  };

  update = (form) => {
    return this.http
      .post<Commodity>(`${environment.apiUrl}/commodity/update`, { ...form })
      .pipe(map((commodity: Commodity) => commodity));
  };
}
