import { TestBed } from '@angular/core/testing';

import { ConmodityService } from './conmodity.service';

describe('ConmodityService', () => {
  let service: ConmodityService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConmodityService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
