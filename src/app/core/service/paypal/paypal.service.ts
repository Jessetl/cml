import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { AccessTokenPayPal, CustomerPaypal } from "@models";
import { map } from "rxjs/operators";

import { environment } from "environments/environment";

const headers = new HttpHeaders({
  "Content-Type": "application/x-www-form-urlencoded",
  Authorization: `Basic ${btoa(
    environment.paypal_client_id + ":" + environment.paypal_secret
  )}`,
});

@Injectable({
  providedIn: "root",
})
export class PaypalService {
  constructor(private http: HttpClient) {}

  getAccessToken = (code: string) => {
    let urlSearchParams = new URLSearchParams();
    urlSearchParams.set("grant_type", "authorization_code");
    urlSearchParams.set("code", code);

    let body = urlSearchParams.toString();

    return this.http
      .post(
        environment.paypal_auth1,

        body,
        {
          headers: headers,
        }
      )
      .pipe(map((subscription: AccessTokenPayPal) => subscription));
  };

  getCustomerInfo = (token_access: string) => {
    var headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: `Bearer ${token_access}`,
    });

    return this.http
      .get(environment.paypal_auth2, {
        headers: headers,
      })
      .pipe(map((customer: CustomerPaypal) => customer));
  };
}
