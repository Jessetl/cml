import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Auth, AuthUser, Code, User, UserForm } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  constructor(private http: HttpClient) {}

  login = (auth: Auth) => {
    return this.http
      .post<AuthUser>(`${environment.apiUrl}/login`, auth)
      .pipe(map((user: AuthUser) => user));
  };

  logout = (id: number) => {
    return this.http
      .post<string>(`${environment.apiUrl}/logout`, { id: id })
      .pipe(map((response: string) => response));
  };

  recovery = (email: string) => {
    return this.http
      .post<Code>(`${environment.apiUrl}/recovery`, { email })
      .pipe(map((code: Code) => code));
  };

  register = (form: UserForm) => {
    return this.http
      .post<User>(`${environment.apiUrl}/register`, form)
      .pipe(map((user: User) => user));
  };

  refreshToken = () => {
    return this.http
      .post<AuthUser>(`${environment.apiUrl}/refresh`, {})
      .pipe(map((user: AuthUser) => user));
  };

  availableCode = (code: string) => {
    return this.http
      .post<Code>(`${environment.apiUrl}/avaliable`, { code })
      .pipe(map((code: Code) => code));
  };

  newpassword = (form: {}) => {
    return this.http
      .post<User>(`${environment.apiUrl}/update/password`, form)
      .pipe(map((user: User) => user));
  };

  verifiedAccount = (form: { urlCode: string }) => {
    return this.http
      .post<User>(`${environment.apiUrl}/verified/account`, form)
      .pipe(map((user: User) => user));
  };

  verifiedCode = (code: string) => {
    return this.http
      .post<Code>(`${environment.apiUrl}/verified/code`, { code })
      .pipe(map((code: Code) => code));
  };
}
