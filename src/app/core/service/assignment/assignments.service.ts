import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Role as Profiles, Module, SubModule, AssignmentForm } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class AssignmentsService {
  constructor(private http: HttpClient) {}

  getAssignments() {
    return this.http
      .get<Profiles[]>(`${environment.apiUrl}/assignments`)
      .pipe(map((profiles: Profiles[]) => profiles));
  }

  getSubmodules() {
    return this.http
      .get<SubModule[]>(`${environment.apiUrl}/assignments/submodules`)
      .pipe(map((submodules: SubModule[]) => submodules));
  }

  store(form: AssignmentForm) {
    return this.http
      .post<Profiles>(`${environment.apiUrl}/assignments`, form)
      .pipe(map((profiles: Profiles) => profiles));
  }
}
