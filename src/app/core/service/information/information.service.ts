import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { User, Person, BanckingForm, Bancking } from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class InformationService {
  constructor(private http: HttpClient) {}

  update = (person: Person) => {
    const { user_id: personId } = person;

    return this.http
      .put<User>(`${environment.apiUrl}/information/${personId}`, person)
      .pipe(map((person: User) => person));
  };

  updateBancking = (banking: BanckingForm) => {
    return this.http
      .post<Bancking>(`${environment.apiUrl}/bancking`, {
        ...banking,
      })
      .pipe(map((bancking: Bancking) => bancking));
  };
}
