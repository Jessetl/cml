import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import {
  Packages,
  Package,
  PackageForm,
  PackageType,
  ChargeForm,
  RemitterReceiverForm,
  User,
} from "@models";
// Environment
import { environment } from "environments/environment";

@Injectable({
  providedIn: "root",
})
export class PackageService {
  constructor(private http: HttpClient) {}

  getPackagesByPage = (paginate?: number) => {
    return this.http
      .get<Package[]>(`${environment.apiUrl}/packages`)
      .pipe(map((packages: Package[]) => packages));
  };

  getPackagesByType = (countryId: number) => {
    return this.http
      .get<[][]>(`${environment.apiUrl}/packages/${countryId}/type`)
      .pipe(map((packages: [][]) => packages));
  };

  getCharge = (form: ChargeForm, isLocker?: boolean) => {
    return this.http
      .post<ChargeForm>(`${environment.apiUrl}/packages/charge`, {
        ...form,
        locker: isLocker,
      })
      .pipe(map((charge: ChargeForm) => charge));
  };

  storeUser = (form: RemitterReceiverForm, isLocker?: boolean) => {
    return this.http
      .post<User>(`${environment.apiUrl}/packages/user`, {
        ...form,
        isLocker: isLocker,
      })
      .pipe(map((user: User) => user));
  };

  updateUser = (form: RemitterReceiverForm) => {
    return this.http
      .put<User>(`${environment.apiUrl}/packages/user/${form.id}`, form)
      .pipe(map((user: User) => user));
  };

  search = (value: {}, paginate: number) => {
    return this.http
      .post<Package[]>(`${environment.apiUrl}/packages/search`, value)
      .pipe(map((packages: Package[]) => packages));
  };

  store = (form: PackageForm) => {
    return this.http
      .post<Package>(`${environment.apiUrl}/packages`, form)
      .pipe(map((service: Package) => service));
  };

  update = (form: PackageForm) => {
    return this.http
      .put<Package>(`${environment.apiUrl}/packages/${form.id}`, form)
      .pipe(map((service: Package) => service));
  };

  destroy = (packageId: number) => {
    return this.http
      .delete<Package>(`${environment.apiUrl}/packages/${packageId}`)
      .pipe(map((service: Package) => service));
  };
}
