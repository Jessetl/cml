import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import {
  User as Agency,
  Users as Agencies,
  UserForm as AgencyForm,
  RemitterReceiverForm,
} from "@models";
// Environment
import { environment } from "environments/environment";
const paginated = "";
@Injectable({
  providedIn: "root",
})
export class AgencyService {
  constructor(private http: HttpClient) {}

  getAgencies() {
    return this.http
      .get<Agency[]>(`${environment.apiUrl}/agencies`)
      .pipe(map((agencies: Agency[]) => agencies));
  }

  getAgenciesToTransfer(id: number) {
    return this.http
      .get<Agency[]>(`${environment.apiUrl}/agencies/transfer/${id}`)
      .pipe(map((agencies: Agency[]) => agencies));
  }

  getAgenciesByPage(paginate: number) {
    return this.http
      .get<Agencies[]>(
        `${environment.apiUrl}/agencies?paginated=${paginated}&paginate=${paginate}`
      )
      .pipe(map((agencies: Agencies[]) => agencies));
  }

  store = (agency: AgencyForm) => {
    return this.http
      .post<Agency>(`${environment.apiUrl}/agencies`, agency)
      .pipe(map((agency: Agency) => agency));
  };

  search = (value: {}, paginate: number) => {
    return this.http
      .post<Agencies[]>(`${environment.apiUrl}/agencies/search`, value)
      .pipe(map((agencies: Agencies[]) => agencies));
  };

  update = (agency: AgencyForm) => {
    return this.http
      .put<Agency>(`${environment.apiUrl}/agencies/${agency.id}`, agency)
      .pipe(map((agency: Agency) => agency));
  };

  destroy = (agencyId: number) => {
    return this.http
      .delete<Agency>(`${environment.apiUrl}/agencies/${agencyId}`)
      .pipe(map((agency: Agency) => agency));
  };

  changeStatus = (form: { id: number }) => {
    return this.http
      .post<Agency>(`${environment.apiUrl}/agencies/status`, form)
      .pipe(map((agency: Agency) => agency));
  };

  getByFillable = (form: { filter: string; filterBy: number }) => {
    return this.http
      .post<RemitterReceiverForm>(
        `${environment.apiUrl}/agencies/by-fillable`,
        form
      )
      .pipe(map((agency: RemitterReceiverForm) => agency));
  };

  getEmployeeByAgency = (form: { id: string }) => {
    return this.http
      .post<Agency[]>(`${environment.apiUrl}/agencies/employee`, form)
      .pipe(map((user: Agency[]) => user));
  };
}
