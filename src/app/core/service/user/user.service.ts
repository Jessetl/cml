import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { User, Users, UserForm, RemitterReceiverForm } from "@models";
// Environment
import { environment } from "environments/environment";

const paginated = "";

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http
      .get<User[]>(`${environment.apiUrl}/users`)
      .pipe(map((users: User[]) => users));
  }

  getResellers() {
    return this.http
      .get<User[]>(`${environment.apiUrl}/users/resellers`)
      .pipe(map((users: User[]) => users));
  }

  getLockers() {
    return this.http
      .get<Users[]>(`${environment.apiUrl}/users/lockers`)
      .pipe(map((users: Users[]) => users));
  }

  getUsersByPage(paginate: number) {
    return this.http
      .get<Users[]>(
        `${environment.apiUrl}/users?paginated=${paginated}&paginate=${paginate}`
      )
      .pipe(map((users: Users[]) => users));
  }

  getEmployess() {
    return this.http
      .get<User[]>(`${environment.apiUrl}/users/employees`)
      .pipe(map((users: User[]) => users));
  }

  searchEmployees = (form: {}, type: number) => {
    return this.http
      .post<User[]>(`${environment.apiUrl}/users/search/employees`, {
        ...form,
        type: type,
      })
      .pipe(map((users: User[]) => users));
  };

  search = (form: {}, type: number) => {
    return this.http
      .post<Users[]>(`${environment.apiUrl}/users/search`, {
        ...form,
        type: type,
      })
      .pipe(map((users: Users[]) => users));
  };

  getLockersByFilters = (form: {}, type: number) => {
    return this.http
      .post<Users[]>(`${environment.apiUrl}/users/lockers/search`, {
        ...form,
        type: type,
      })
      .pipe(map((users: Users[]) => users));
  };

  store = (user: UserForm, agencyId?: number) => {
    return this.http
      .post<User>(`${environment.apiUrl}/users`, {
        ...user,
        agencyId: agencyId,
      })
      .pipe(map((user: User) => user));
  };

  update = (user: UserForm) => {
    return this.http
      .put<User>(`${environment.apiUrl}/users/${user.id}`, user)
      .pipe(map((user: User) => user));
  };

  assign = (user: UserForm) => {
    return this.http
      .post<User>(`${environment.apiUrl}/users/assign`, user)
      .pipe(map((user: User) => user));
  };

  destroy = (userId: number) => {
    return this.http
      .delete<User>(`${environment.apiUrl}/users/${userId}`)
      .pipe(map((user: User) => user));
  };

  getByFillable = (form: {
    filter: string;
    filterBy: number;
    locker?: boolean;
  }) => {
    return this.http
      .post<RemitterReceiverForm>(
        `${environment.apiUrl}/users/by-fillable`,
        form
      )
      .pipe(map((agency: RemitterReceiverForm) => agency));
  };

  changeStatus = (form: { id: number }) => {
    return this.http
      .post<User>(`${environment.apiUrl}/users/status`, form)
      .pipe(map((user: User) => user));
  };
}
