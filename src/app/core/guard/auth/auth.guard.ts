import { Injectable } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { Router, CanActivate } from "@angular/router";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

import * as fromUser from "@reducers";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private store: Store<fromUser.State>) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.pipe(
      select((state) => state.user.isAuthenticated),
      map((auth) => {
        if (!auth) {
          this.router.navigate(["/auth/login"]);
          return false;
        }

        return true;
      })
    );
  }
}
