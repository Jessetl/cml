import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import * as fromUser from '@reducers';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor (private router: Router, private store: Store<fromUser.State>) {}
  
  canActivate(): Observable<boolean> {
    
    return this.store.pipe(
      select((state) => state.user.isAuthenticated),
        map(auth => {
   
          if(auth) {
            this.router.navigate(['/dashboard']);
            return false;
          }
          
          return true;  
        })
    );  
  }
}
