import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class HelperService {
  private message = new BehaviorSubject<any>("En espera de un nuevo mensaje");
  private chat = new BehaviorSubject<any>("En espera de un nuevo mensaje");
  public customMessage = this.message.asObservable();
  public customChat = this.chat.asObservable();

  constructor() {}

  public changeMessage(msg: any): void {
    this.message.next(msg);
  }

  public changeChat(chat: any): void {
    this.chat.next(chat);
  }
}
