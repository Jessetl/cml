import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, throwError } from "rxjs";
import { first, mergeMap, catchError, switchMap } from "rxjs/operators";

import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpErrorResponse,
} from "@angular/common/http";

import { AuthenticationService } from "../service/authentication/authentication.service";
// import { TranslateService } from "@ngx-translate/core";
import { Store, select } from "@ngrx/store";
import { Token } from "@models";

import {
  INTERNAL_SERVER_ERROR,
  NOT_FOUND_ERROR,
  UNAUTHORIZED,
  UNPROCESSABLE_ENTITY,
  FORBIDDEN,
  METHOD_NOT_ALLOWED,
} from "@shared/utils";

import * as fromRoot from "@reducers";
import * as reduxUser from "@actions";
import * as reduxToken from "@actions";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  public tokenRefresh: boolean = true;

  constructor(
    // private translate: TranslateService,
    private router: Router,
    private authService: AuthenticationService,
    private store: Store<fromRoot.State>
  ) {}

  private redirectToLogin = () => {
    //----------------------------------------------------------/
    this.store.dispatch(new reduxUser.RemoveUserAction(null));
    this.store.dispatch(new reduxToken.RemoveTokenAction(null));

    return this.router.navigate(["auth/login"]);
  };

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return this.store.pipe(
      select(fromRoot.getToken),
      first(),
      mergeMap((token: Token) => {
        const setHeader = {
          headers: request.headers.set(
            "X-localization",
            localStorage.getItem("language")
          ),
        };

        if (!!token) {
          const { token: strToken } = token;
          if (
            request.url.indexOf("https://restcountries.eu") === -1 &&
            request.url.indexOf(
              "https://api.sandbox.paypal.com/v1/oauth2/token"
            ) === -1 &&
            request.url.indexOf(
              "https://api.sandbox.paypal.com/v1/identity/oauth2/userinfo?schema=paypalv1.1"
            ) === -1
          ) {
            request = request.clone({
              setHeaders: {
                Authorization: `Bearer ${strToken}`,
              },
              ...setHeader,
            });
          }
        }

        return next.handle(request).pipe(
          catchError((error: HttpErrorResponse) => {
            if (
              error.status === INTERNAL_SERVER_ERROR &&
              error.error.message === "The token has been blacklisted"
            ) {
              this.redirectToLogin();
            } else if (
              error.status === INTERNAL_SERVER_ERROR &&
              error.error.message ===
                "Token has expired and can no longer be refreshed"
            ) {
              this.redirectToLogin();
            } else if (
              error.status === UNAUTHORIZED &&
              error.error.message === "Token has expired"
            ) {
              this.authService.refreshToken().pipe(
                switchMap((res) => {
                  //-------------------------------/
                  this.store.dispatch(
                    new reduxToken.SetTokenAction({
                      token: res.access_token,
                      token_type: res.token_type,
                      expires_in: res.expires_in,
                    })
                  );

                  request = request.clone({
                    setHeaders: {
                      Authorization: `Bearer ${res["access_token"]}`,
                    },
                    ...setHeader,
                  });

                  return next.handle(request);
                })
              );
            } else if (
              error.status === UNPROCESSABLE_ENTITY &&
              error.error.message === "The given data was invalid."
            ) {
              // this.globalService.showNotify('default', this.translate.instant('messages.auth.token.invalid'))
            } else if (error.status === INTERNAL_SERVER_ERROR) {
              //this.globalService.showNotify('default', this.translate.instant('messages.auth.token.500'))
              return next.handle(request);
            } else if (error.status === NOT_FOUND_ERROR) {
              // this.globalService.showNotify('default', this.translate.instant('messages.auth.token.404'))
            } else if (error.status === UNPROCESSABLE_ENTITY) {
            } else if (error.status === METHOD_NOT_ALLOWED) {
            } else if (error.status === FORBIDDEN) {
              this.redirectToLogin();
            } else {
              this.redirectToLogin();
            }

            return throwError(error);
          })
        );
      })
    );
  }
}
