import { FormControl, Validators } from "@angular/forms";

export const MembershipValidators = {
  type_pay: new FormControl(null, [Validators.required]),
  payment_day: new FormControl(null, []),
  name: new FormControl(null, [Validators.required]),
  email: new FormControl(null, [Validators.required]),
};
