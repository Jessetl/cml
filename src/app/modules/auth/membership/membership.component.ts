import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { User, Subscription, Product } from "@models";
import { MembershipValidators } from "./FormMembershipValidators";
import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as user from "@actions";
import * as token from "@actions";
import * as shop from "@actions";
import { PaymentMethod } from "@stripe/stripe-js";
import { LockerService, AuthenticationService } from "@core/service";
import { CountryService, GooglemapsService } from "@core/service";
import { validationsForm, showMessage, handlerError } from "@shared/utils";
import { rawError, MEMBERSHIP_PREMIUM } from "@shared/utils";
import { MEMBESHIP_BASIC, MEMBERSHIP_CORPORATE } from "@shared/utils";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
  selector: "cml-membership",
  templateUrl: "./membership.component.html",
  styles: [],
})
export class MembershipComponent implements OnInit, OnDestroy {
  public membership: FormGroup;
  public user: User;
  public submitted: boolean = false;
  public errors: Array<any>;
  public amount: number;
  public price: string;
  public memberships: Product[];
  public basic_submitted: boolean = false;
  public products: Product[] = [];
  public product_suscription: Product;
  public load_products: boolean = true;
  public latitude: string;
  public longitude: string;

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private store: Store<fromRoot.State>,
    private lockerService: LockerService,
    private authService: AuthenticationService,
    private countryService: CountryService,
    private router: Router,
    private googleService: GooglemapsService
  ) {
    this.membership = this.formBuilder.group(MembershipValidators);
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit(): void {
    this.countryService.getProduct().subscribe(
      (product: Product[]) => {
        this.load_products = false;
        if (!!product) {
          this.products = product;
        }
      },
      (error: HttpErrorResponse) => {
        this.load_products = false;
        this.submitted = false;
        this.handleError(error);
      }
    );

    this.googleService
      .getLocation({ timeout: 15000 })
      .then(({ coords }) => {
        this.latitude = coords.latitude.toString();
        this.longitude = coords.longitude.toString();
      })
      .catch((error) => {
        console.log("error al obtener la ubicacion", error);
      });
  }

  ngOnDestroy(): void {
    this.membership.reset();
  }

  isFieldValid = (field: string): boolean => {
    return (
      this.membership.get(field).invalid && this.membership.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  responsePaymentMethod = (paymentMethod: PaymentMethod) => {
    this.subscriptionUser(paymentMethod.id);
  };

  membershipValue = (name: string): any => {
    return this.membership.get(name).value;
  };

  eventLoadSubmitted = (submitted: boolean) => {
    const infoValid = this.membership.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.membership);
    } else {
      this.submitted = submitted;
    }
  };

  getTypeShippingMembership = () => {
    return (
      this.membershipValue("type_pay") === MEMBERSHIP_PREMIUM ||
      this.membershipValue("type_pay") === MEMBERSHIP_CORPORATE
    );
  };

  getTypeShipping = () => {
    return this.membershipValue("type_pay") === MEMBESHIP_BASIC;
  };

  isVisible = () => {
    return this.membershipValue("type_pay") !== null;
  };

  setValueType = (type: number, id: number | null) => {
    type === null ? this.membership.reset() : null;
    if (id !== null) {
      this.changePrice(id);
    }
    this.membership.patchValue({ type_pay: type }, { onlySelf: true });
  };

  changePrice = (id_product: number) => {
    let product = this.products.find(({ id }) => id === id_product);
    this.amount = product.unit_amount;
    this.price = product.price;
  };

  onSubmit = () => {
    if (this.membership.get("type_pay").value === 0) {
      this.errors = null;

      if (this.submitted) {
        return;
      }

      this.submitted = true;

      this.lockerService
        .subscriptionBasic(this.membership.get("type_pay").value)
        .subscribe(
          (user: User) => {
            this.submitted = false;
            this.updateUser(user.type_pay);

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.subscription"),
              "success",
              true
            );

            this.router.navigate(["/dashboard"]);
          },
          (error: HttpErrorResponse) => {
            this.basic_submitted = false;
            this.handleError(error);
          }
        );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  updateUser = (type: number) => {
    let userBefore = this.user;
    this.store.dispatch(
      new user.SetUserAction({
        ...userBefore,
        type_pay: type,
      })
    );
  };

  getEmail = (): string => {
    return this.membershipValue("email");
  };

  subscriptionUser = (id: string) => {
    this.lockerService
      .subscription({
        paymentMethodId: id,
        email: this.membership.get("email").value,
        price: this.price,
        type: this.membership.get("type_pay").value,
        latitude: this.latitude,
        longitude: this.longitude,
      })
      .subscribe(
        (susbcription: Subscription) => {
          this.submitted = false;
          this.updateUser(MEMBERSHIP_PREMIUM);

          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.subscription"),
            "success",
            true
          );

          this.router.navigate(["/dashboard"]);
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
  };

  logout(isTimedOut: boolean = false): void {
    if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.authService.logout(this.user.id).subscribe(
      (response: string) => {
        this.submitted = false;

        this.store.dispatch(new user.RemoveUserAction(null));
        this.store.dispatch(new token.RemoveTokenAction(null));
        this.store.dispatch(new shop.RemoveShopAction([]));

        const message = isTimedOut
          ? this.translate.instant("notifications.sessionExpired")
          : this.translate.instant(`notifications.${response[0]}`);

        this.router.navigate(["/auth/login"]);

        return showMessage(
          this.translate.instant("messages.congratulations"),
          message,
          "success",
          true
        );
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        const errorResponse = handlerError(error);

        showMessage(
          "¡Error!",
          this.translate.instant(`errors${errorResponse}`),
          "error",
          true
        );
      }
    );
  }

  getMembershipt = (product: Product): string => {
    let name =
      product.type === MEMBERSHIP_PREMIUM
        ? "memberships.premium"
        : "memberships.corporate";
    return this.translate.instant(name, { price: product.unit_amount });
  };
}
