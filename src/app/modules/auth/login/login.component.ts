import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";

import { AuthenticationService } from "@core/service";
import { TranslateService } from "@ngx-translate/core";

import {
  handlerError,
  rawError,
  showMessage,
  validationsForm,
} from "@shared/utils";

import * as fromRoot from "@reducers";
import * as user from "@actions";
import * as token from "@actions";

@Component({
  selector: "cml-login",
  templateUrl: "./login.component.html",
  styles: [],
})
export class LoginComponent implements OnInit {
  public auth: FormGroup;
  public submitted: boolean;
  public errors: Array<any>;

  constructor(
    private router: Router,
    private translate: TranslateService,
    private authService: AuthenticationService,
    private formBuilder: FormBuilder,
    private store: Store<fromRoot.State>
  ) {}

  ngOnInit() {
    const strLanguage = localStorage.getItem("language");

    this.auth = this.formBuilder.group({
      email: new FormControl(null, [Validators.required, Validators.email]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6),
      ]),
      language: new FormControl(strLanguage, [Validators.required]),
    });
  }

  setLocalization() {
    const strLanguage = this.auth.get("language").value;
    localStorage.setItem("language", strLanguage);

    this.translate.setDefaultLang(strLanguage);
    this.translate.use(strLanguage);
  }

  isFieldValid = (field: string): boolean => {
    return this.auth.get(field).invalid && this.auth.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  onSubmit = () => {
    const authValid = this.auth.valid;

    this.errors = null;

    if (!authValid) {
      return validationsForm(this.auth);
    }

    this.submitted = true;

    this.authService
      .login({
        email: this.auth.get("email").value,
        password: this.auth.get("password").value,
      })
      .subscribe(
        (data) => {
          const rawUser = data.user.person;
          const rawRole = data.user.role[0] || {};
          const agency = data.user.agency[0] || null;
          const platforms = data.platforms;

          this.store.dispatch(
            new user.SetUserAction({
              ...data.user,
              person: rawUser,
              role: rawRole,
              agency: agency,
              image_admin: data.image_admin,
              admin_name: data.admin_name,
              app_ios: platforms.app_ios,
              app_android: platforms.app_android,
            })
          );

          this.store.dispatch(
            new token.SetTokenAction({
              token: data.access_token,
              token_type: data.token_type,
              expires_in: data.expires_in,
            })
          );

          this.router.navigate(["/dashboard"]);
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof error.error === "string") {
      return showMessage(null, error.error, "warning", true);
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    let name = this.translate.instant(value).toLowerCase();
    return this.translate.instant("validations.required", { name: name });
  };
}
