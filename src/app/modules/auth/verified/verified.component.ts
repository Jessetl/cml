import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";

import { User } from "@models";
import { AuthenticationService } from "@core/service";

import { rawError, handlerError } from "@shared/utils";
import { showMessage, STATUS_ACTIVE } from "@shared/utils";

@Component({
  selector: "cml-verified",
  templateUrl: "./verified.component.html",
  styles: [],
})
export class VerifiedComponent implements OnInit {
  public isLoading: boolean = true;
  public isVerified: boolean = false;
  public isError: boolean = false;
  public urlCode: string;
  public errors: Array<any>;

  constructor(
    private router: Router,
    private verifiedService: AuthenticationService,
    private route: ActivatedRoute
  ) {
    this.urlCode = this.route.snapshot.paramMap.get("code");
  }

  ngOnInit() {
    this.verifiedService.verifiedAccount({ urlCode: this.urlCode }).subscribe(
      (user: User) => {
        this.isLoading = false;
        this.isVerified = user.activated === STATUS_ACTIVE;

        setTimeout(() => {
          this.router.navigate(["auth/login"]);
        }, 5000);
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        this.isError = true;
        this.handleError(error);
      }
    );
  }

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof error.error === "string") {
      return showMessage("¡Error!", error.error, "error", true);
    }

    this.errors = rawError(errorResponse);
  };
}
