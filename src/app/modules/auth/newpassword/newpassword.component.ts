import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";

import { AuthenticationService } from "@core/service";
import { TranslateService } from "@ngx-translate/core";
import { Code } from "@models";

import {
  handlerError,
  rawError,
  showMessage,
  validationsForm,
} from "@shared/utils";
const newPasswordValidators = {
  password: new FormControl(null, [
    Validators.required,
    Validators.minLength(6),
    Validators.pattern(
      "(?=.{6,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$"
    ),
  ]),
  repassword: new FormControl(null, [
    Validators.required,
    Validators.minLength(6),
    Validators.pattern(
      "(?=.{6,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$"
    ),
  ]),
};
@Component({
  selector: "cml-newpassword",
  templateUrl: "./newpassword.component.html",
  styles: [],
})
export class NewpasswordComponent implements OnInit {
  @ViewChild("password") passwordField: ElementRef;
  @ViewChild("repeatPassword")
  private readonly encrypt: string;

  public repeatPasswordField: ElementRef;
  public newPassword: FormGroup;
  public submitted: boolean;
  public errors: Array<any>;
  public statusCode: number;
  public focusPass: boolean = false;
  public focusRePass: boolean = false;
  public minChar: boolean = false;
  public requireNumber: boolean = false;
  public requireCharEs: boolean = false;
  public requiteText: boolean = false;

  constructor(
    private router: Router,
    private translate: TranslateService,
    private newPasswordService: AuthenticationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.encrypt = this.route.snapshot.paramMap.get("code");
  }

  ngOnInit() {
    this.newPassword = this.formBuilder.group(newPasswordValidators);
    this.clean();
    this.newPasswordService.availableCode(this.encrypt).subscribe(
      (code: Code) => {
        const { status } = code;
        this.statusCode = status;
      },
      (error: HttpErrorResponse) => {
        this.handleError(error);
      }
    );
  }
  isFieldValid = (field: string): boolean => {
    return (
      this.newPassword.get(field).invalid && this.newPassword.get(field).touched
    );
  };
  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };
  onSubmit = async () => {
    const newPasswordValid = this.newPassword.valid;
    this.errors = null;

    const validateEquals = await this.getValidatePass();

    if (validateEquals) {
      return;
    }
    if (!newPasswordValid) {
      return validationsForm(this.newPassword);
    }

    this.submitted = true;

    this.newPasswordService
      .newpassword({
        code: this.encrypt,
        password: this.newPassword.get("password").value,
        repassword: this.newPassword.get("repassword").value,
      })
      .subscribe(
        (data) => {
          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.updatePassword"),
            "success",
            true
          );
          this.router.navigate(["auth/login"]);
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
  };

  fPass = () => {
    this.focusPass = true;
    const pass = this.newPassword.get("password").value;

    if (pass !== null) {
      this.validate(pass);
    }
  };

  fRePass = () => {
    this.focusRePass = true;
    const pass = this.newPassword.get("repassword").value;

    if (pass !== null) {
      this.validate(pass);
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };
  validate = (variable: any) => {
    // const variable = event.target.value;
    const regexMin = /(?=.{6,})/;
    const regexNumber = /(?=.*[0-9])/;
    const regexText = /(?=.*[A-Z])/;
    const regexChar = /(?=.*[@#$%^&*-/%+=]).*$/;
    this.requiteText = false;
    if (regexText.test(variable)) {
      this.requiteText = true;
    }
    this.requireNumber = false;
    if (regexNumber.test(variable)) {
      this.requireNumber = true;
    }
    this.requireCharEs = false;
    if (regexChar.test(variable)) {
      this.requireCharEs = true;
    }
    this.minChar = false;
    if (regexMin.test(variable)) {
      this.minChar = true;
    }
  };

  getValidatePass = () => {
    let isInvalid: boolean = false;
    const pass = this.newPassword.get("password").value;
    const rePass = this.newPassword.get("repassword").value;

    if (pass !== rePass && pass !== null && rePass != null) {
      showMessage(
        null,
        this.translate.instant("validations.passwordEquals"),
        "warning",
        true
      );

      isInvalid = true;
    }

    if (pass === null || rePass === null) {
      showMessage(
        null,
        this.translate.instant("validations.requiredPass"),
        "warning",
        true
      );
      isInvalid = true;
    }
    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationRegex = (nameInput: string, ref: any) => {
    this.focusPass = false;
    this.focusRePass = false;
    let isInvalid: boolean = false;
    const pass = this.newPassword.get(nameInput).value;
    const regex = /(?=.{6,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$/;

    if (pass === null || !regex.test(pass)) {
      ref.nativeElement.focus();
      showMessage(
        null,
        this.translate.instant("validations.password"),
        "warning",
        true
      );

      isInvalid = true;
    }
    if (!isInvalid) {
      this.clean();
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  clean = () => {
    this.requiteText = false;
    this.requireNumber = false;
    this.requireCharEs = false;
    this.minChar = false;
  };

  validations = (value: string): string => {
    let name = this.translate.instant(value).toLowerCase();
    return this.translate.instant("validations.required", { name: name });
  };
}
