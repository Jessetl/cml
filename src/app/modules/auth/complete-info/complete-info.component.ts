import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";

import { User, Country } from "@models";
import { Store } from "@ngrx/store";

import { FormInformationValidator } from "./FormInformationValidator";
import { handlerError, rawError, TierId, TIER_LOCKER } from "@shared/utils";
import { showMessage, validationsForm } from "@shared/utils";
import { confirm, showMessageConfirm } from "@shared/utils";
import { countriesLang } from "@shared/utils";

import { InformationService, CountryService } from "@core/service";
import { GooglemapsService, AuthenticationService } from "@core/service";

import * as fromRoot from "@reducers";
import * as user from "@actions";
import * as token from "@actions";
import * as shop from "@actions";

@Component({
  selector: "cml-complete-info",
  templateUrl: "./complete-info.component.html",
  styles: [],
})
export class CompleteInfoComponent implements OnInit {
  public isLoading: boolean = true;
  public user: User;
  public countries: Country[];
  public information: FormGroup;
  public submitted: boolean;
  public searching: boolean = false;

  public errors: Array<any>;
  public errorZipCode: boolean;
  public shouldEnableStateAndCity: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private infoService: InformationService,
    private countryService: CountryService,
    private googleService: GooglemapsService,
    private store: Store<fromRoot.State>,
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.information = this.formBuilder.group(FormInformationValidator);
  }

  ngOnInit(): void {
    const { person } = this.user;

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.information.patchValue({
      ...this.user,
      ...person,
      image: null,
      password: null,
    });

    this.isLoading = false;
  }

  isFieldValid = (field: string): boolean => {
    return (
      this.information.get(field).invalid && this.information.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  showTierByReseller = (): boolean => {
    if (!!this.user) {
      return (
        this.user.tier === TierId.TIER_SUPERVISOR ||
        this.user.tier === TierId.TIER_SELLER
      );
    }
  };

  onSubmit = () => {
    const infoValid = this.information.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.information);
    }

    if (!!!this.submitted) {
      this.submitted = true;

      this.infoService
        .update({ ...this.information.getRawValue(), id: this.user.id })
        .subscribe(
          (data) => {
            this.store.dispatch(new user.UpdatePersonAction(data.person));
            this.store.dispatch(
              new user.SetUserAction({
                ...this.user,
                completed: 1,
                person: data.person,
              })
            );
            this.submitted = false;

            this.user.tier === TIER_LOCKER && this.user.type_pay === null
              ? this.router.navigate(["/auth/subscription"])
              : this.router.navigate(["/dashboard"]);
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;

            const errorResponse = handlerError(error);

            if (typeof errorResponse === "string") {
              return showMessage(
                "¡Error!",
                this.translate.instant(`errors${errorResponse}`),
                "error",
                true
              );
            }

            this.errors = rawError(errorResponse);
          }
        );
    }
  };

  onDefaultValuesStateAndCountry = () => {
    this.information.get("state").reset(null, { onlySelf: true });
    this.information.get("city").reset(null, { onlySelf: true });
  };

  onBlurZipCode = (nameField: string) => {
    const zipCode = this.information.get(nameField).value;
    const countryId = this.information.get("country_id").value;
    const countryAlphacode = this.countries.find(({ id }) => id === countryId);

    if (!zipCode) {
      return;
    }

    this.onDefaultValuesStateAndCountry();
    this.searching = true;
    this.errorZipCode = false;

    this.googleService
      .getZipCode({
        postalCode: zipCode.toString(),
        country: !!countryAlphacode ? countryAlphacode.alpha2Code : "US",
      })
      .subscribe(
        async (res: any) => {
          const response = await this.googleService.getStateAndCity(res);

          this.information.patchValue(
            {
              state: response.state,
              city: response.city,
            },
            { onlySelf: true, emitEvent: false }
          );

          this.searching = false;
        },
        async () => {
          if (!this.shouldEnableStateAndCity) {
            this.searching = false;
            this.errorZipCode = true;

            const wantWrite = await confirm({
              text: this.translate.instant("ngbAlert.watWriteZipCode"),
              confirmButtonText: this.translate.instant(
                "buttonText.confirmButtonText"
              ),
              cancelButtonText: this.translate.instant(
                "buttonText.cancelButtonText"
              ),
            });

            if (wantWrite) {
              this.information.get("state").enable({ onlySelf: true });
              this.information.get("city").enable({ onlySelf: true });
            }
          }
        }
      );
  };

  shouldStateAndCity = () => {
    this.shouldEnableStateAndCity = !this.shouldEnableStateAndCity;

    if (this.shouldEnableStateAndCity) {
      this.information.get("state").enable({ onlySelf: true });
      this.information.get("city").enable({ onlySelf: true });
    } else {
      this.information.get("state").disable({ onlySelf: true });
      this.information.get("city").disable({ onlySelf: true });
    }
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  logout(isTimedOut: boolean = false): void {
    if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.authService.logout(this.user.id).subscribe(
      (response: string) => {
        this.submitted = false;

        this.store.dispatch(new user.RemoveUserAction(null));
        this.store.dispatch(new token.RemoveTokenAction(null));
        this.store.dispatch(new shop.RemoveShopAction([]));

        const message = isTimedOut
          ? this.translate.instant("notifications.sessionExpired")
          : this.translate.instant(`notifications.${response[0]}`);

        this.router.navigate(["/auth/login"]);

        return showMessage(
          this.translate.instant("messages.congratulations"),
          message,
          "success",
          true
        );
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        const errorResponse = handlerError(error);

        showMessage(
          "¡Error!",
          this.translate.instant(`errors${errorResponse}`),
          "error",
          true
        );
      }
    );
  }
}
