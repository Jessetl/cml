import { FormControl, Validators } from "@angular/forms";

export const FormInformationValidator = {
  id: new FormControl({ value: null, disabled: false }, []),
  identification_id: new FormControl(null, [
    Validators.maxLength(10),
    Validators.required,
  ]),
  name: new FormControl({ value: null, disabled: false }, [
    Validators.required,
    Validators.maxLength(100),
  ]),
  phone: new FormControl({ value: null, disabled: false }, [
    Validators.maxLength(30),
  ]),
  cellphone: new FormControl({ value: null, disabled: false }, [
    Validators.maxLength(30),
    Validators.required,
  ]),
  zip_code: new FormControl({ value: null, disabled: false }, [
    Validators.required,
    Validators.maxLength(5),
  ]),
  country_id: new FormControl({ value: null, disabled: false }, [
    Validators.required,
  ]),
  state: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.maxLength(30),
  ]),
  city: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.maxLength(30),
  ]),
  address: new FormControl({ value: null, disabled: false }, [
    Validators.required,
  ]),
};
