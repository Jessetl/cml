import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { HttpErrorResponse } from "@angular/common/http";
import { AuthenticationService, LockerService } from "@core/service";
import { handlerError, showMessage, rawError } from "@shared/utils";
import { Code, User } from "@models";

@Component({
  selector: "cml-reactivate",
  templateUrl: "./reactivate.component.html",
  styles: [],
})
export class ReactivateComponent implements OnInit {
  private readonly code: string;
  public statusCode: Code;
  public errors: Array<any>;
  public isLoading: boolean = true;
  public submitted: boolean;

  constructor(
    private router: Router,
    private translate: TranslateService,
    private reactivateService: AuthenticationService,
    private route: ActivatedRoute,
    private lockerService: LockerService
  ) {
    this.code = this.route.snapshot.paramMap.get("code");
  }

  ngOnInit(): void {
    this.reactivateService.verifiedCode(this.code).subscribe(
      (code: Code) => {
        this.isLoading = false;
        const { status } = code;
        this.statusCode = code;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        this.handleError(error);
      }
    );
  }

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  reactivate = () => {
    if (!!this.submitted) {
      return;
    }
    this.submitted = true;

    this.lockerService
      .reactivate({
        user_id: this.statusCode.user_id,
        code_id: this.statusCode.id,
      })
      .subscribe(
        (user: User) => {
          this.submitted = false;
          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.default"),
            "success",
            true
          );
          this.router.navigate(["auth/login"]);
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
  };
}
