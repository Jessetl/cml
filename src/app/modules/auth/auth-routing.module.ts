import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Components
import { LoginComponent } from "./login/login.component";
import { RecoveryComponent } from "./recovery/recovery.component";
import { NewpasswordComponent } from "./newpassword/newpassword.component";
import { VerifiedComponent } from "./verified/verified.component";
import { RegisterComponent } from "./register/register.component";
import { MembershipComponent } from "./membership/membership.component";
import { CompleteInfoComponent } from "./complete-info/complete-info.component";
import { ReactivateComponent } from "./reactivate/reactivate.component";

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent,
  },
  {
    path: "register",
    component: RegisterComponent,
  },
  {
    path: "register/:code",
    component: RegisterComponent,
  },
  {
    path: "recovery",
    component: RecoveryComponent,
  },
  {
    path: "password/:code",
    component: NewpasswordComponent,
  },
  {
    path: "verified/:code",
    component: VerifiedComponent,
  },
  {
    path: "subscription",
    component: MembershipComponent,
  },
  {
    path: "complete",
    component: CompleteInfoComponent,
  },
  {
    path: "reactivate/:code",
    component: ReactivateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}

export const routingComponents = [
  LoginComponent,
  RecoveryComponent,
  NewpasswordComponent,
  VerifiedComponent,
  RegisterComponent,
  MembershipComponent,
  CompleteInfoComponent,
  ReactivateComponent,
];
