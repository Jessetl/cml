import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";

import { AuthenticationService } from "@core/service";
import { TranslateService } from "@ngx-translate/core";

import {
  handlerError,
  rawError,
  showMessage,
  validationsForm,
} from "@shared/utils";

const recoveryValidators = {
  email: new FormControl(null, Validators.required),
};

@Component({
  selector: "cml-recovery",
  templateUrl: "./recovery.component.html",
  styles: [],
})
export class RecoveryComponent implements OnInit {
  public recovery: FormGroup;
  public submitted: boolean;
  public errors: Array<any>;

  constructor(
    private router: Router,
    private translate: TranslateService,
    private recoveryService: AuthenticationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.recovery = this.formBuilder.group(recoveryValidators);
  }

  isFieldValid = (field: string): boolean => {
    return this.recovery.get(field).invalid && this.recovery.get(field).touched;
  };

  onSubmit = () => {
    const recoveryValid = this.recovery.valid;
    this.errors = null;

    if (!recoveryValid) {
      return validationsForm(this.recovery);
    }

    this.submitted = true;
    this.recoveryService.recovery(this.recovery.get("email").value).subscribe(
      (data) => {
        showMessage(
          "¡Enhorabuena!",
          this.translate.instant("notifications.sendEmail"),
          "success",
          true
        );
        this.recovery.reset();
        this.router.navigate(["auth/login"]);
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    let name = this.translate.instant(value).toLowerCase();
    return this.translate.instant("validations.required", { name: name });
  };
}
