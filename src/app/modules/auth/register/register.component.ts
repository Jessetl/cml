import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { FormGroup, FormBuilder } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { ActivatedRoute, Router } from "@angular/router";

import { User, Country } from "@models";
import { FormUserValidator } from "./FormBuilder";

import { AuthenticationService, CountryService } from "@core/service";
import { GooglemapsService } from "@core/service";

import { rawError, handlerError } from "@shared/utils";
import { confirm, showMessage } from "@shared/utils";
import { validationsForm, countriesLang } from "@shared/utils";

var timeout = null;
@Component({
  selector: "cml-register",
  templateUrl: "./register.component.html",
  styles: [],
})
export class RegisterComponent implements OnInit {
  public submitted: boolean = false;
  public user: FormGroup;
  public countries: Country[];
  public referralLink: string;

  public searching: boolean = false;

  public errors: Array<any>;
  public errorZipCode: boolean;
  public shouldEnableStateAndCity: boolean = false;

  // Password
  public validatePass: boolean = false;
  public minChar: boolean = false;
  public requireNumber: boolean = false;
  public requireCharEs: boolean = false;
  public requiteText: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private countryService: CountryService,
    private googleService: GooglemapsService,
    private translate: TranslateService,
    private authService: AuthenticationService
  ) {
    this.referralLink = this.route.snapshot.paramMap.get("code");

    this.user = this.formBuilder.group(FormUserValidator);
  }

  ngOnInit() {
    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  isFieldValid = (field: string): boolean => {
    return this.user.get(field).invalid && this.user.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getValidationRegex = (): boolean => {
    const password = this.user.get("password").value;
    const regex = /(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$/;

    return (this.validatePass =
      password === null || !regex.test(password) ? true : false);
  };

  validate = (variable: any) => {
    const regexMin = /(?=.{6,})/;
    const regexNumber = /(?=.*[0-9])/;
    const regexText = /(?=.*[A-Z])/;
    const regexChar = /(?=.*[@#$%^&*-/%+=]).*$/;

    this.requiteText = false;

    if (regexText.test(variable)) {
      this.requiteText = true;
    }

    this.requireNumber = false;
    if (regexNumber.test(variable)) {
      this.requireNumber = true;
    }

    this.requireCharEs = false;
    if (regexChar.test(variable)) {
      this.requireCharEs = true;
    }

    this.minChar = false;
    if (regexMin.test(variable)) {
      this.minChar = true;
    }
  };

  fPass = () => {
    this.validatePass = true;
  };

  onDefaultValuesStateAndCountry = () => {
    this.user.get("state").reset(null, { onlySelf: true });
    this.user.get("city").reset(null, { onlySelf: true });
  };

  shouldStateAndCity = () => {
    this.shouldEnableStateAndCity = !this.shouldEnableStateAndCity;

    if (this.shouldEnableStateAndCity) {
      this.user.get("state").enable({ onlySelf: true });
      this.user.get("city").enable({ onlySelf: true });
    } else {
      this.user.get("state").disable({ onlySelf: true });
      this.user.get("city").disable({ onlySelf: true });
    }
  };

  onKeyZipCode = (nameField: string) => {
    const zipCode = this.user.get(nameField).value;
    const countryId = this.user.get("country_id").value;
    const countryAlphacode = this.countries.find(({ id }) => id === countryId);

    if (!zipCode) {
      return;
    }

    if (timeout) {
      clearTimeout(timeout);
    }

    this.onDefaultValuesStateAndCountry();
    this.searching = true;
    this.errorZipCode = false;

    timeout = setTimeout(() => {
      this.googleService
        .getZipCode({
          postalCode: zipCode.toString(),
          country: !!countryAlphacode ? countryAlphacode.alpha2Code : "US",
        })
        .subscribe(
          async (res: any) => {
            const response = await this.googleService.getStateAndCity(res);

            this.user.patchValue(
              {
                state: response.state,
                city: response.city,
              },
              { onlySelf: true, emitEvent: false }
            );

            this.searching = false;
          },
          async () => {
            if (!this.shouldEnableStateAndCity) {
              this.searching = false;
              this.errorZipCode = true;

              const wantWrite = await confirm({
                text: this.translate.instant("ngbAlert.watWriteZipCode"),
                confirmButtonText: this.translate.instant(
                  "buttonText.confirmButtonText"
                ),
                cancelButtonText: this.translate.instant(
                  "buttonText.cancelButtonText"
                ),
              });

              if (wantWrite) {
                this.user.get("state").enable({ onlySelf: true });
                this.user.get("city").enable({ onlySelf: true });
              }
            }
          }
        );
    }, 1000);
  };

  onSubmit = () => {
    const infoValid = this.user.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.user);
    }

    if (!!!this.submitted) {
      this.submitted = true;

      this.authService
        .register({
          ...this.user.getRawValue(),
          referralLink: this.referralLink,
        })
        .subscribe(
          (user: User) => {
            this.submitted = false;
            this.user.reset();

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.register"),
              "success",
              true
            );

            this.router.navigate(["/auth/login"]);
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof error.error === "string") {
      return showMessage("¡Error!", error.error, "error", true);
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
