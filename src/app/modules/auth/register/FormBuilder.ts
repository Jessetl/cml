import { FormControl, Validators } from "@angular/forms";

export const FormUserValidator = {
  id: new FormControl(null, []),
  identification_id: new FormControl(null, [Validators.maxLength(10)]),
  name: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
  email: new FormControl(null, [Validators.required, Validators.maxLength(60)]),
  address: new FormControl(null, []),
  phone: new FormControl(null, [Validators.maxLength(20)]),
  cellphone: new FormControl(null, [Validators.maxLength(20)]),
  zip_code: new FormControl(null, [Validators.maxLength(7)]),
  country_id: new FormControl(null, []),
  state: new FormControl({ value: null, disabled: true }, [
    Validators.maxLength(30),
  ]),
  city: new FormControl({ value: null, disabled: true }, [
    Validators.maxLength(30),
  ]),
  password: new FormControl(null, [
    Validators.required,
    Validators.minLength(6),
    Validators.maxLength(30),
    Validators.pattern(
      "(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$"
    ),
  ]),
};
