import { Component, OnInit, TemplateRef } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { AngularEditorConfig } from "@kolkov/angular-editor";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { showMessage, handlerError, TierId } from "@shared/utils";
import { validationsForm, rawError } from "@shared/utils";
import { Notification, User } from "@models";
import { NotificationService } from "@core/service";
import { NotificationFormValidator } from "./FormNotificationValidators";
import { REAL_TIME, Socket, langNotification } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-notification",
  templateUrl: "./notification.component.html",
  styles: [],
})
export class NotificationComponent implements OnInit {
  private modalRef: NgbModalRef;

  public user: User;
  public isLoading: boolean = true;
  public submitted: boolean = false;
  public users: User[];
  public notifications: Notification[];
  public notification: FormGroup;
  public isEditing: boolean = false;
  public errors: Array<any>;
  public lang: string;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "auto",
    minHeight: "0",
    maxHeight: "auto",
    width: "100%",
    minWidth: "0",
    translate: "yes",
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Enter text here...",
    defaultParagraphSeparator: "",
    defaultFontName: "",
    defaultFontSize: "",
    fonts: [
      { class: "arial", name: "Arial" },
      { class: "times-new-roman", name: "Times New Roman" },
      { class: "calibri", name: "Calibri" },
      { class: "comic-sans-ms", name: "Comic Sans MS" },
    ],
    customClasses: [],
    uploadUrl: "v1/image",
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: "top",
    toolbarHiddenButtons: [
      [
        "justifyLeft",
        "justifyCenter",
        "justifyRight",
        "justifyFull",
        "strikeThrough",
        "subscript",
        "superscript",
        "outdent",
        "insertUnorderedList",
        "insertOrderedList",
        "fontName",
      ],
      [
        "backgroundColor",
        "customClasses",
        "link",
        "unlink",
        "insertImage",
        "insertVideo",
      ],
    ],
  };

  constructor(
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private notificationService: NotificationService,
    private store: Store<fromRoot.State>,
    private formBuilder: FormBuilder
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.notification = this.formBuilder.group(NotificationFormValidator);
  }

  ngOnInit(): void {
    this.lang = this.translate.getDefaultLang();
    this.getNotification();
  }

  getNotification = () => {
    this.isLoading = true;

    this.notificationService.getNotifications().subscribe(
      (notifications: Notification[]) => {
        this.isLoading = false;
        this.notifications = notifications.map((n) => {
          return {
            ...n,
            translation: langNotification(n, this.lang),
          };
        });
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        this.handleError(error);
      }
    );
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
    this.notification.reset();
  };

  isFieldValid = (field: string): boolean => {
    return (
      this.notification.get(field).invalid &&
      this.notification.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter(
                (assignment) => assignment.submodule.path === "notifications"
              )
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  shouldToggleCreate = (ngbNotificationCreate: TemplateRef<any>) => {
    this.errors = null;
    this.isEditing = false;
    this.notification.reset();
    this.modalRef = this.ngbModal.open(ngbNotificationCreate);
  };

  onSubmit = () => {
    const infoValid = this.notification.valid;
    const isEditing = this.isEditing;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.notification);
    } else if (this.submitted) {
      return;
    }

    this.submitted = true;

    switch (isEditing) {
      case true:
        return this.notificationService
          .update(this.notification.getRawValue())
          .subscribe(
            (notification: Notification) => {
              this.submitted = false;

              this.shoulderUpdateNotification(notification);

              this.notification.reset();

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.updateNotifications"),
                "success",
                true
              );

              this.modalRef.close();
              Socket.emit(REAL_TIME.NOTIFICATION, notification);
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
      case false:
        return this.notificationService
          .create(this.notification.getRawValue())
          .subscribe(
            (notification: Notification) => {
              this.submitted = false;

              this.shouldMakeNotification(notification);

              this.notification.reset();

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.createNotifications"),
                "success",
                true
              );

              this.modalRef.close();
              Socket.emit(REAL_TIME.NOTIFICATION, notification);
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
    }
  };

  shouldMakeNotification = (notification: Notification) => {
    const resNotification = {
      ...notification,
      translation: langNotification(notification, this.lang),
    };

    this.notifications.unshift(resNotification);
  };

  shoulderUpdateNotification = (notification: Notification) => {
    let keyNotification = this.notifications.findIndex(
      ({ id }) => id === notification.id
    );
    let resNotification = [...this.notifications];

    resNotification[keyNotification] = {
      ...notification,
      translation: langNotification(notification, this.lang),
    };

    this.notifications = resNotification;
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  shouldToggleUpdate = (
    ngbNotificationCreate: TemplateRef<any>,
    notificationId: number
  ) => {
    this.isEditing = true;

    const notification: Notification = this.notifications.find(
      ({ id }) => id === notificationId
    );

    this.notification.patchValue({
      ...notification,
    });
    this.modalRef = this.ngbModal.open(ngbNotificationCreate);
  };
}
