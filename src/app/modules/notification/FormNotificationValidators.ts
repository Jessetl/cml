import { FormControl, Validators } from "@angular/forms";

export const NotificationFormValidator = {
  id: new FormControl(null),
  content: new FormControl(null, [Validators.required]),
  content_en: new FormControl(null, [Validators.required]),
};
