import { Component, OnInit } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { HttpErrorResponse } from "@angular/common/http";
// import { FileSaverService } from "ngx-filesaver";

import { User as Agency, Country, User } from "@models";
import { AgencyService, CountryService, ReportsService } from "@core/service";
import { ReportsFormValidators } from "./FormReportsValidators";

import { TranslateService } from "@ngx-translate/core";
import { FormGroup, FormBuilder } from "@angular/forms";

import { GLOBAL, NO_TOTAL, UNPROCESSABLE_ENTITY } from "@shared/utils";
import {
  FOR_AGENCY,
  FOR_COUNTRY,
  FOR_DAY,
  TierId,
  countriesLang,
} from "@shared/utils";
import { showMessage, validationsForm } from "@shared/utils";
import { rawError, handlerError } from "@shared/utils";

import { Store } from "@ngrx/store";

import * as fromRoot from "@reducers";
import * as moment from "moment";

@Component({
  selector: "cml-reports",
  templateUrl: "./reports.component.html",
})
export class ReportsComponent implements OnInit {
  private format: string = "MM-DD-YYYY-hh:mm";

  public reports: FormGroup;
  public submitted: boolean = false;
  public user: User;
  public errors: Array<any>;
  public agencies: Agency[];
  public countries: Country[];
  public categories: any[];
  public label: string;
  public totals: any[];
  public isVisible: boolean = false;
  public urlFile: SafeResourceUrl;
  public language: string;

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private agencyService: AgencyService,
    private countryService: CountryService,
    private reportService: ReportsService,
    public domSanitizer: DomSanitizer,
    // private fileSaverService: FileSaverService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.reports = this.formBuilder.group(ReportsFormValidators);
    this.language = localStorage.getItem("language");
  }

  ngOnInit() {
    const { agency } = this.user;

    if (
      (!!this.user && !!agency && agency.tier === TierId.TIER_ADMIN) ||
      (!!this.user && this.user.tier === TierId.TIER_ADMIN)
    ) {
      this.agencyService.getAgencies().subscribe(
        (agencies: Agency[]) => {
          const rawData = agencies
            .map((agency: Agency) => {
              return {
                ...agency,
                name: agency.person.name,
              };
            })
            .filter((user) => !!user.person);

          this.agencies = rawData;
        },
        (error: HttpErrorResponse) => {
          showMessage(
            "¡Error!",
            this.translate.instant(`errors${handlerError(error)}`),
            "error",
            true
          );
        }
      );
    }

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.reports.reset();
  }

  isFieldValid = (field: string): boolean => {
    return this.reports.get(field).invalid && this.reports.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getAgenciesByTier = () => {
    const { agency } = this.user;

    if (
      (!!this.user && !!agency && agency.tier !== TierId.TIER_ADMIN) ||
      (!!this.user && this.user.tier === TierId.TIER_AGENCY)
    ) {
      const id = !!agency ? agency.id : this.user.id;
      const name = !!agency ? agency.person.name : this.user.person.name;

      return [
        {
          id: id,
          name: name,
        },
      ];
    }

    return this.agencies;
  };

  selecCategory = () => {
    const type = this.reports.get("type").value;

    switch (type) {
      case FOR_COUNTRY:
        this.totals = [
          { id: NO_TOTAL, name: this.translateName("select.no-total") },
          { id: FOR_AGENCY, name: this.translateName("select.for-agency") },
          { id: FOR_DAY, name: this.translateName("select.for-day") },
        ];

        this.label = this.translate.instant("submodules.country");
        this.reports.get("category").enable();
        this.isVisible = true;

        return (this.categories = this.countries);

      case FOR_AGENCY:
        this.totals = [
          { id: NO_TOTAL, name: this.translateName("select.no-total") },
          { id: FOR_COUNTRY, name: this.translateName("select.for-country") },
          { id: FOR_DAY, name: this.translateName("select.for-day") },
        ];

        this.label = this.translate.instant("tableRender.agency");
        this.reports.get("category").enable();
        this.categories = this.getAgenciesByTier();
        this.isVisible = true;

        return;

      case GLOBAL:
        this.totals = [
          { id: NO_TOTAL, name: this.translateName("select.no-total") },
          { id: FOR_COUNTRY, name: this.translateName("select.for-country") },
          { id: FOR_AGENCY, name: this.translateName("select.for-agency") },
          { id: FOR_DAY, name: this.translateName("select.for-day") },
        ];

        this.label = "";
        this.reports.get("category").disable();
        this.isVisible = false;

        return;

      default:
        this.label = "";
        this.reports.get("category").disable();
        this.reports.reset({ total: null }, { onlySelf: true });
        this.totals = [];
        this.isVisible = false;

        return;
    }
  };

  translateName = (name: string) => {
    return this.translate.instant(name);
  };

  getFileName = (): string => {
    return moment().format(this.format).toString();
  };

  onSubmit = () => {
    const infoValid = this.reports.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.reports);
    } else if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.reportService.getMovements({ ...this.reports.value }).subscribe(
      (file: Blob) => {
        this.submitted = false;
        // this.fileSaverService.save(file, this.getFileName());
        this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(
          URL.createObjectURL(file)
        );
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (error.status === UNPROCESSABLE_ENTITY) {
      return showMessage(
        null,
        this.translate.instant("validators.emptyDataDisplay"),
        "warning",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string, translator?: string): string => {
    return this.translate.instant("validations.required", {
      name: translator
        ? translator.toLowerCase()
        : this.translate.instant(value).toLowerCase(),
    });
  };
}
