import { Component, OnInit } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
// import { FileSaverService } from "ngx-filesaver";

import { TranslateService } from "@ngx-translate/core";

import { AgencyService, ReportsService } from "@core/service";
import { User as Agency } from "@models";

import { CommisionsReportValidators } from "./FormCommisionsValidators";

import { validationsForm, UNPROCESSABLE_ENTITY } from "@shared/utils";
import { rawError, handlerError, showMessage, TierId } from "@shared/utils";

import { Store } from "@ngrx/store";
import { User } from "@models";

import * as fromRoot from "@reducers";
import * as moment from "moment";

@Component({
  selector: "cml-commisions",
  templateUrl: "./commisions.component.html",
})
export class ReportCommissionsComponent implements OnInit {
  private format: string = "MM-DD-YYYY-hh:mm";

  public commisions: FormGroup;
  public agencies: Array<any>;
  public submitted: boolean;
  public errors: Array<any>;
  public user: User;
  public selectAgency: Agency[];
  public urlFile: SafeResourceUrl;

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private agencyService: AgencyService,
    private reportService: ReportsService,
    public domSanitizer: DomSanitizer,
    // private fileSaverService: FileSaverService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.commisions = this.formBuilder.group(CommisionsReportValidators);
  }

  ngOnInit() {
    const { agency } = this.user;

    if (
      (!!this.user && !!agency && agency.tier !== TierId.TIER_ADMIN) ||
      (!!this.user && this.user.tier === TierId.TIER_AGENCY)
    ) {
      this.commisions.patchValue({ typeAgency: 1 }, { onlySelf: true });

      return (this.agencies = [
        { id: this.getKeyByTier("id"), name: this.getKeyByTier("name") },
      ]);
    }

    this.agencyService.getAgencies().subscribe(
      (agencies: Agency[]) => {
        const rawData = agencies
          .map((agency: Agency) => {
            return {
              ...agency,
              name: agency.person.name,
            };
          })
          .filter((user) => !!user.person);

        this.agencies = rawData;
        this.commisions.patchValue({ typeAgency: 0 }, { onlySelf: true });
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.commisions.reset();
  }

  isFieldValid = (field: string): boolean => {
    return (
      this.commisions.get(field).invalid && this.commisions.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getKeyByTier = (key: string) => {
    return this.user.tier === TierId.TIER_AGENCY
      ? this.user.person[key]
      : this.user.agency.person[key];
  };

  getFileName = (): string => {
    return moment().format(this.format).toString();
  };

  onToggleByType = (): boolean => {
    if (!!this.user) {
      const { agency } = this.user;

      return (
        (!!this.user && !!agency && agency.tier === TierId.TIER_ADMIN) ||
        (!!this.user && this.user.tier === TierId.TIER_ADMIN)
      );
    }
  };

  onSubmit = () => {
    const infoValid = this.commisions.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.commisions);
    } else if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.reportService.getCommissions({ ...this.commisions.value }).subscribe(
      (file: Blob) => {
        this.submitted = false;
        // this.fileSaverService.save(file, this.getFileName());
        this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(
          URL.createObjectURL(file)
        );
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (error.status === UNPROCESSABLE_ENTITY) {
      return showMessage(
        null,
        this.translate.instant("validators.emptyDataDisplay"),
        "warning",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };

  setValueType = (type: number) => {
    this.commisions.patchValue({ typeAgency: type }, { onlySelf: true });
  };

  isAgencyChecked = (): boolean => {
    if (this.commisions.get("typeAgency").value) {
      this.commisions.get("agency").enable();
      return true;
    } else {
      this.commisions.get("agency").disable({ onlySelf: true });
      return false;
    }
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
