import { FormControl, Validators } from "@angular/forms";

export const CommisionsReportValidators = {
  from: new FormControl(null, [Validators.required]),
  to: new FormControl(null, [Validators.required]),
  typeAgency: new FormControl(0, [Validators.required]),
  agency: new FormControl(null, [Validators.required]),
  status: new FormControl(null, [Validators.required]),
};
