import { Component, OnInit } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { TranslateService } from "@ngx-translate/core";
import { AgencyService, ReportsService } from "@core/service";
import { User as Agency } from "@models";
import { AccountStatusReportValidators } from "./FormAccountStatusValidators";
import { validationsForm, UNPROCESSABLE_ENTITY } from "@shared/utils";
import { rawError, handlerError, showMessage, TierId } from "@shared/utils";
import { Store } from "@ngrx/store";
import { User } from "@models";

import * as fromRoot from "@reducers";
import * as moment from "moment";

@Component({
  selector: "cml-account-status",
  templateUrl: "./account-status.component.html",
  styles: [],
})
export class AccountStatusComponent implements OnInit {
  private format: string = "MM-DD-YYYY-hh:mm";
  public accountstatus: FormGroup;
  public agencies: Array<any>;
  public submitted: boolean;
  public errors: Array<any>;
  public user: User;
  public selectAgency: Agency[];
  public urlFile: SafeResourceUrl;

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private agencyService: AgencyService,
    private reportService: ReportsService,
    public domSanitizer: DomSanitizer,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.accountstatus = this.formBuilder.group(AccountStatusReportValidators);
  }

  ngOnInit(): void {
    const { agency, tier } = this.user;

    if (
      (!!agency && agency.tier !== TierId.TIER_ADMIN) ||
      (!!this.user && tier === TierId.TIER_AGENCY)
    ) {
      this.accountstatus.patchValue({ typeAgency: 1 }, { onlySelf: true });
      this.agencies = [
        { id: this.getKeyByTier("id"), name: this.getKeyByTier("name") },
      ];
      return;
    }

    this.agencyService.getAgencies().subscribe(
      (agencies: Agency[]) => {
        const rawData = agencies
          .map((agency: Agency) => {
            return {
              ...agency,
              name: agency.person.name,
            };
          })
          .filter((user) => !!user.person);

        this.agencies = rawData;
        this.accountstatus.patchValue({ typeAgency: 0 }, { onlySelf: true });
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.accountstatus.reset();
  }

  isFieldValid = (field: string): boolean => {
    return (
      this.accountstatus.get(field).invalid &&
      this.accountstatus.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getKeyByTier = (key: string) => {
    return this.user.tier === TierId.TIER_AGENCY
      ? this.user.person[key]
      : this.user.agency.person[key];
  };

  getFileName = (): string => {
    return moment().format(this.format).toString();
  };

  onToggleByType = (): boolean => {
    if (!!this.user) {
      const { agency } = this.user;

      return (
        (!!this.user && !!agency && agency.tier === TierId.TIER_ADMIN) ||
        (!!this.user && this.user.tier === TierId.TIER_ADMIN)
      );
    }
  };

  onSubmit = () => {
    const infoValid = this.accountstatus.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.accountstatus);
    } else if (this.submitted) {
      return;
    }
    this.submitted = true;

    this.reportService.getPay({ ...this.accountstatus.value }).subscribe(
      (file: Blob) => {
        this.submitted = false;
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (error.status === UNPROCESSABLE_ENTITY) {
      return showMessage(
        null,
        this.translate.instant("validators.emptyDataDisplay"),
        "warning",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };

  setValueType = (type: number) => {
    this.accountstatus.patchValue(
      { typeAgency: type, agency: null, status: null },
      { onlySelf: true }
    );
  };

  isAgencyChecked = (): boolean => {
    let value = this.accountstatus.get("typeAgency").value;
    if (!!value) {
      this.accountstatus.get("agency").enable();
      return true;
    }
    this.accountstatus.get("agency").disable({ onlySelf: true });
    return false;
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
