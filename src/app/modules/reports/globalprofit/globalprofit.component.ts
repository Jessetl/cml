import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
// import { FileSaverService } from "ngx-filesaver";

import { TranslateService } from "@ngx-translate/core";

import { ReportsService } from "@core/service";

import { GlobalReportValidators, months } from "./FormGlobalValidators";

import { rawError, handlerError, UNPROCESSABLE_ENTITY } from "@shared/utils";
import { showMessage, validationsForm, TierId } from "@shared/utils";

import { Store } from "@ngrx/store";
import { User } from "@models";

import * as fromRoot from "@reducers";
import * as moment from "moment";

@Component({
  selector: "cml-globalprofit",
  templateUrl: "./globalprofit.component.html",
})
export class GlobalprofitComponent implements OnInit {
  private format: string = "MM-DD-YYYY-hh:mm";

  public global: FormGroup;
  public errors: Array<any>;
  public months: any[] = months;
  public user: User;
  public submitted: boolean;
  public urlFile: SafeResourceUrl;

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private reportService: ReportsService,
    public domSanitizer: DomSanitizer,
    // private fileSaverService: FileSaverService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.global = this.formBuilder.group(GlobalReportValidators);
  }

  ngOnInit() {
    this.global.reset();
    this.global.patchValue({ type: 1 }, { onlySelf: true });
  }

  isFieldValid = (field: string): boolean => {
    return this.global.get(field).invalid && this.global.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getFileName = (): string => {
    return moment().format(this.format).toString();
  };

  onToggleByType = (): boolean => {
    if (!!this.user) {
      const { agency } = this.user;

      return (
        (!!this.user && !!agency && agency.tier === TierId.TIER_ADMIN) ||
        (!!this.user && this.user.tier === TierId.TIER_ADMIN)
      );
    }
  };

  onSubmit = () => {
    const infoValid = this.global.valid;
    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.global);
    } else if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.reportService.getGlobals({ ...this.global.value }).subscribe(
      (file: Blob) => {
        this.submitted = false;
        // this.fileSaverService.save(file, this.getFileName());
        this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(
          URL.createObjectURL(file)
        );
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (error.status === UNPROCESSABLE_ENTITY) {
      return showMessage(
        null,
        this.translate.instant("validators.emptyDataDisplay"),
        "warning",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };
}
