import { FormControl, Validators } from "@angular/forms";
export const GlobalReportValidators = {
  type: new FormControl(1),
  period: new FormControl(null, [Validators.required]),
};

export const months = [
  {
    id: 1,
    name: "month.January",
  },
  {
    id: 2,
    name: "month.February",
  },
  {
    id: 3,
    name: "month.March",
  },
  {
    id: 4,
    name: "month.April",
  },
  {
    id: 5,
    name: "month.May",
  },
  {
    id: 6,
    name: "month.June",
  },
  {
    id: 7,
    name: "month.July",
  },
  {
    id: 8,
    name: "month.August",
  },
  {
    id: 9,
    name: "month.September",
  },
  {
    id: 10,
    name: "month.October",
  },
  {
    id: 11,
    name: "month.November",
  },
  {
    id: 12,
    name: "month.December",
  },
  {
    id: 0,
    name: "month.Annual",
  },
];
