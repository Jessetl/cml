import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
// import { FileSaverService } from "ngx-filesaver";

import { TranslateService } from "@ngx-translate/core";
import { FormBuilder, FormGroup } from "@angular/forms";

import { AgencyService, ReportsService } from "@core/service";
import { User as Agency } from "@models";

import { StatusLocation } from "./FormLocationValidators";
import { LocationReportValidators } from "./FormLocationValidators";

import { Store } from "@ngrx/store";
import { User } from "@models";

import { rawError, handlerError, UNPROCESSABLE_ENTITY } from "@shared/utils";
import { showMessage, validationsForm, TierId } from "@shared/utils";
import { STATUS_CENTER, STATUS_SHIPMENT_CENTER } from "@shared/utils";
import { STATUS_ALL, TYPE_ALL, TYPE_AGENCY, TYPE_CML } from "@shared/utils";

import * as fromRoot from "@reducers";
import * as moment from "moment";

@Component({
  selector: "cml-location",
  templateUrl: "./location.component.html",
})
export class LocationComponent implements OnInit {
  private format: string = "MM-DD-YYYY-hh:mm";

  public locations: FormGroup;
  public errors: Array<any>;
  public agencies: Array<any>;
  public locationStatus: any[];
  public submitted: boolean;
  public user: User;
  public nameAgency: string;
  public urlFile: SafeResourceUrl;

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private agencyService: AgencyService,
    private reportService: ReportsService,
    public domSanitizer: DomSanitizer,
    // private fileSaverService: FileSaverService,
    private store: Store<fromRoot.State>
  ) {
    this.locations = this.formBuilder.group(LocationReportValidators);
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    const { agency } = this.user;

    this.locations.reset();

    if (
      (!!this.user && !!agency && agency.tier !== TierId.TIER_ADMIN) ||
      (!!this.user && this.user.tier === TierId.TIER_AGENCY)
    ) {
      this.locations.patchValue({ typeAgency: 1 }, { onlySelf: true });

      return (this.agencies = [
        { id: this.getKeyByTier("id"), name: this.getKeyByTier("name") },
      ]);
    }

    this.locations.patchValue({ typeAgency: 0 }, { onlySelf: true });

    this.agencyService.getAgencies().subscribe(
      (agencies: Agency[]) => {
        const rawData = agencies
          .map((agency: Agency) => {
            return {
              ...agency,
              name: agency.person.name,
            };
          })
          .filter((user) => !!user.person);

        this.agencies = rawData;
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  isFieldValid = (field: string): boolean => {
    return (
      this.locations.get(field).invalid && this.locations.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getKeyByTier = (key: string) => {
    return this.user.tier === TierId.TIER_AGENCY
      ? this.user.person[key]
      : this.user.agency.person[key];
  };

  getFileName = (): string => {
    return moment().format(this.format).toString();
  };

  onToggleByType = (): boolean => {
    if (!!this.user) {
      const { agency } = this.user;

      return (
        (!!this.user && !!agency && agency.tier === TierId.TIER_ADMIN) ||
        (!!this.user && this.user.tier === TierId.TIER_ADMIN)
      );
    }
  };

  getCheckedRadio = (type: number): boolean => {
    return this.locations.get("typeAgency").value === type;
  };

  setValueType = (type: number) => {
    this.locations.patchValue({ typeAgency: type }, { onlySelf: true });
  };

  isAgencyChecked = (): boolean => {
    let type = this.locations.get("typeAgency").value;
    switch (type) {
      case TYPE_CML: {
        this.locations.get("agency").disable({ onlySelf: true });
        this.locationStatus = [
          {
            id: STATUS_SHIPMENT_CENTER,
            name: "timeLine.inShippingCenter",
          },
          ...StatusLocation.filter(({ id }) => id >= STATUS_CENTER),
        ];
        return false;
      }
      case TYPE_AGENCY: {
        this.locations.get("agency").enable();
        this.locationStatus = [...StatusLocation];
        return true;
      }
      case TYPE_ALL: {
        this.locations.get("agency").disable({ onlySelf: true });
        this.locationStatus = [
          ...StatusLocation.filter(({ id }) => id !== STATUS_ALL),
        ];

        return false;
      }
    }
  };

  onSubmit = () => {
    const infoValid = this.locations.valid;
    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.locations);
    } else if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.reportService.getLocations({ ...this.locations.value }).subscribe(
      (file: Blob) => {
        this.submitted = false;
        // this.fileSaverService.save(file, this.getFileName());
        this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(
          URL.createObjectURL(file)
        );
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (error.status === UNPROCESSABLE_ENTITY) {
      return showMessage(
        null,
        this.translate.instant("validators.emptyDataDisplay"),
        "warning",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
