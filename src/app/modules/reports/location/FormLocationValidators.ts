import { FormControl, Validators } from "@angular/forms";

import {
  STATUS_AGENCY,
  STATUS_SHIPMENT,
  STATUS_DESTINY,
  STATUS_CENTER,
  STATUS_TRANSIT,
  STATUS_DELIVERED,
  STATUS_SHIPMENT_CENTER,
  STATUS_ALL,
} from "@shared/utils";

export const LocationReportValidators = {
  from: new FormControl(null, [Validators.required]),
  to: new FormControl(null, [Validators.required]),
  typeAgency: new FormControl(0, [Validators.required]),
  agency: new FormControl(null, [Validators.required]),
  status: new FormControl(null, [Validators.required]),
};

export const StatusLocation = [
  {
    id: STATUS_AGENCY,
    name: "timeLine.inAgency",
  },
  {
    id: STATUS_SHIPMENT,
    name: "timeLine.inShipment",
  },
  {
    id: STATUS_SHIPMENT_CENTER,
    name: "timeLine.inShippingCenter",
  },
  {
    id: STATUS_DESTINY,
    name: "timeLine.inTransit",
  },
  {
    id: STATUS_CENTER,
    name: "timeLine.inDistribution",
  },
  {
    id: STATUS_TRANSIT,
    name: "timeLine.toReceiver",
  },
  {
    id: STATUS_DELIVERED,
    name: "timeLine.delivered",
  },
  {
    id: STATUS_ALL,
    name: "select.allAgencies",
  },
];
