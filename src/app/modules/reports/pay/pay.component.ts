import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { TranslateService } from "@ngx-translate/core";
import { AgencyService, ReportsService } from "@core/service";
import { User as Agency } from "@models";
import { PayReportValidators } from "./FormPayValidators";
import { validationsForm, UNPROCESSABLE_ENTITY } from "@shared/utils";
import { rawError, handlerError, showMessage, TierId } from "@shared/utils";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { Store } from "@ngrx/store";
import { User } from "@models";
import * as fromRoot from "@reducers";
import * as moment from "moment";

@Component({
  selector: "cml-pay",
  templateUrl: "./pay.component.html",
  styles: [],
})
export class PayComponent implements OnInit {
  private format: string = "MM-DD-YYYY-hh:mm";
  public urlFile: SafeResourceUrl;
  public pay: FormGroup;
  public agencies: Array<any>;
  public submitted: boolean;
  public errors: Array<any>;
  public user: User;
  public selectAgency: Agency[];

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private agencyService: AgencyService,
    private reportService: ReportsService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.pay = this.formBuilder.group(PayReportValidators);
  }

  ngOnInit(): void {
    const { agency, tier } = this.user;

    if (
      (!!agency && agency.tier !== TierId.TIER_ADMIN) ||
      (!!this.user && tier === TierId.TIER_AGENCY)
    ) {
      this.pay.patchValue({ typeAgency: 1 }, { onlySelf: true });
      this.agencies = [
        { id: this.getKeyByTier("id"), name: this.getKeyByTier("name") },
      ];
      return;
    }

    this.agencyService.getAgencies().subscribe(
      (agencies: Agency[]) => {
        const rawData = agencies
          .map((agency: Agency) => {
            return {
              ...agency,
              name: agency.person.name,
            };
          })
          .filter((user) => !!user.person);

        this.agencies = rawData;
        this.pay.patchValue({ typeAgency: 0 }, { onlySelf: true });
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.pay.reset();
  }

  isFieldValid = (field: string): boolean => {
    return this.pay.get(field).invalid && this.pay.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (error.status === UNPROCESSABLE_ENTITY) {
      return showMessage(
        null,
        this.translate.instant("validators.emptyDataDisplay"),
        "warning",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };

  setValueType = (type: number) => {
    this.pay.patchValue(
      { typeAgency: type, status: null, agency: null },
      { onlySelf: true }
    );
  };

  isAgencyChecked = (): boolean => {
    let typeAgency = this.pay.get("typeAgency").value;
    if (!!typeAgency) {
      this.pay.get("agency").enable();
      return true;
    }
    this.pay.get("agency").disable({ onlySelf: true });
    return false;
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  onSubmit = () => {
    const infoValid = this.pay.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.pay);
    } else if (this.submitted) {
      return;
    }
    this.submitted = true;

    this.reportService.getPay({ ...this.pay.value }).subscribe(
      (file: Blob) => {
        this.submitted = false;
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  onToggleByType = (): boolean => {
    if (!!this.user) {
      const { agency } = this.user;

      return (
        (!!this.user && !!agency && agency.tier === TierId.TIER_ADMIN) ||
        (!!this.user && this.user.tier === TierId.TIER_ADMIN)
      );
    }
  };
  getKeyByTier = (key: string): any => {
    return this.user.tier === TierId.TIER_AGENCY
      ? this.user.person[key]
      : this.user.agency.person[key];
  };
}
