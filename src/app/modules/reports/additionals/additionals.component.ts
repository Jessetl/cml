import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
// import { FileSaverService } from "ngx-filesaver";

import { TranslateService } from "@ngx-translate/core";

import { ReportsService } from "@core/service";
import { AdditionalsValidators } from "./FormAdditionalsValidators";

import { showMessage, validationsForm } from "@shared/utils";
import { UNPROCESSABLE_ENTITY } from "@shared/utils";
import { handlerError, rawError } from "@shared/utils";

import * as moment from "moment";

@Component({
  selector: "cml-additionals",
  templateUrl: "./additionals.component.html",
})
export class AdditionalsComponent implements OnInit {
  private format: string = "MM-DD-YYYY-hh:mm";

  public additionals: FormGroup;
  public errors: Array<any>;
  public filterBy: number = 3;
  public submitted: boolean = false;
  public urlFile: SafeResourceUrl;

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private reportService: ReportsService,
    public domSanitizer: DomSanitizer
  ) // private fileSaverService: FileSaverService
  {
    this.additionals = this.formBuilder.group(AdditionalsValidators);
  }

  ngOnInit() {}

  isFieldValid = (field: string): boolean => {
    return (
      this.additionals.get(field).invalid && this.additionals.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getFileName = (): string => {
    return moment().format(this.format).toString();
  };

  onSubmit = () => {
    const infoValid = this.additionals.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.additionals);
    } else if (this.submitted) {
      return;
    }

    this.additionals.patchValue({ filter: this.filterBy }, { onlySelf: true });
    this.submitted = true;

    this.reportService.getAdditonals({ ...this.additionals.value }).subscribe(
      (file: Blob) => {
        this.submitted = false;
        // this.fileSaverService.save(file, this.getFileName());
        this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(
          URL.createObjectURL(file)
        );
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (error.status === UNPROCESSABLE_ENTITY) {
      return showMessage(
        null,
        this.translate.instant("validators.emptyDataDisplay"),
        "warning",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
