import { FormControl, Validators } from "@angular/forms";

export const AdditionalsValidators = {
  from: new FormControl(null, [Validators.required]),
  to: new FormControl(null, [Validators.required]),
  charge_additionals: new FormControl(0, [Validators.required]),
  day: new FormControl(null),
  shipping: new FormControl(null),
  total: new FormControl(null, [Validators.required]),
  filter: new FormControl(3, []),
};
