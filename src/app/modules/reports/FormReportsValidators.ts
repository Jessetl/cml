import { FormControl, Validators } from "@angular/forms";

export const ReportsFormValidators = {
  from: new FormControl(null, [Validators.required]),
  to: new FormControl(null, [Validators.required]),
  category: new FormControl({ value: null, disabled: true }, [
    Validators.required,
  ]),
  type: new FormControl(null, [Validators.required]),
  total: new FormControl(null, [Validators.required]),
};
