import { Component, OnInit } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";
import { FormGroup, FormBuilder } from "@angular/forms";

import { ProductivityFormValidators } from "./FormProductivityValidators";

import { User as Agency, Country, User } from "@models";
import { ReportsService } from "@core/service";
import { UNPROCESSABLE_ENTITY } from "@shared/utils";
import { showMessage, validationsForm } from "@shared/utils";
import { rawError, handlerError } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as moment from "moment";

@Component({
  selector: "cml-productivity",
  templateUrl: "./productivity.component.html",
  styles: [],
})
export class ProductivityComponent implements OnInit {
  private format: string = "MM-DD-YYYY-hh:mm";
  public productivity: FormGroup;
  public submitted: boolean = false;
  public user: User;
  public errors: Array<any>;
  public agencies: Agency[];
  public countries: Country[];
  public categories: any[];
  public label: string;
  public totals: any[];
  public isVisible: boolean = false;
  public urlFile: SafeResourceUrl;
  public language: string;

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private reportService: ReportsService,
    public domSanitizer: DomSanitizer,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.productivity = this.formBuilder.group(ProductivityFormValidators);
    this.language = localStorage.getItem("language");
  }

  ngOnInit(): void {}

  isFieldValid = (field: string): boolean => {
    return (
      this.productivity.get(field).invalid &&
      this.productivity.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  translateName = (name: string) => {
    return this.translate.instant(name);
  };

  getFileName = (): string => {
    return moment().format(this.format).toString();
  };

  onSubmit = () => {
    const infoValid = this.productivity.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.productivity);
    } else if (!!!this.submitted) {
      this.submitted = true;

      this.reportService
        .getProductivity({ ...this.productivity.value })
        .subscribe(
          (file: Blob) => {
            this.submitted = false;
            this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(
              URL.createObjectURL(file)
            );
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (error.status === UNPROCESSABLE_ENTITY) {
      return showMessage(
        null,
        this.translate.instant("validators.emptyDataDisplay"),
        "warning",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string, translator?: string): string => {
    return this.translate.instant("validations.required", {
      name: translator
        ? translator.toLowerCase()
        : this.translate.instant(value).toLowerCase(),
    });
  };
}
