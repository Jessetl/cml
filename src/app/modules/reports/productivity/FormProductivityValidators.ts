import { FormControl, Validators } from "@angular/forms";

export const ProductivityFormValidators = {
  from: new FormControl(null, [Validators.required]),
  to: new FormControl(null, [Validators.required]),
  type: new FormControl(null, [Validators.required]),
  level: new FormControl(null, [Validators.required]),
  total: new FormControl(null, [Validators.required]),
};
