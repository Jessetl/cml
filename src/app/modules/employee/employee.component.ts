import { Component, OnInit, TemplateRef, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { User as Employee, Country, Role as Profile } from "@models";
import { User as Agency, User } from "@models";
import { AgencyService, UserService, ProfileService } from "@core/service";
import { CountryService, GooglemapsService } from "@core/service";
import { FormEmployeeValidator } from "./FormEmployeeValidators";
import {
  rawError,
  handlerError,
  validationsForm,
  STATUS_ACTIVE,
} from "@shared/utils";
import { EDIT, CREATE, STATUS_INACTIVE, TierId } from "@shared/utils";
import { confirm, showMessage, formatingPhone } from "@shared/utils";
import { profilesLang, countriesLang, DELETE } from "@shared/utils";
import { Socket, REAL_TIME } from "@shared/utils";
import { on, unsubscribeOf } from "jetemit";
import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-employee",
  templateUrl: "./employee.component.html",
  styles: [],
})
export class EmployeeComponent implements OnInit, OnDestroy {
  private modalRef: NgbModalRef;
  private readonly agencyId: string;
  private readonly statusAgency: string;
  public readonly nameAgency: string;

  public employees: Employee[];
  public backupEmployees: Employee[];
  public employee: FormGroup;
  public displayEmployee: Employee;
  public isLoading: boolean = true;
  public isEditing: boolean = false;
  public submitted: boolean;
  public searching: boolean = false;
  public countries: Country[];
  public errors: Array<any>;
  public profiles: Profile[];
  public backupProfile: Profile[];
  public errorZipCode: boolean;
  public shouldEnableStateAndCity: boolean = false;
  public addAnother: boolean = false;
  public paginate: number = 1;
  public agencies: Agency[];
  public user: User;
  public filtered: boolean = false;
  public status: number = STATUS_ACTIVE;

  // Password
  public validatePass: boolean = false;
  public minChar: boolean = false;
  public requireNumber: boolean = false;
  public requireCharEs: boolean = false;
  public requiteText: boolean = false;

  //Search
  public search = {
    name: null,
    email: null,
    phone: null,
    cellphone: null,
    id: null,
  };

  constructor(
    private ngbModal: NgbModal,
    private agencyService: AgencyService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private profileService: ProfileService,
    private countryService: CountryService,
    private googleService: GooglemapsService,
    private store: Store<fromRoot.State>
  ) {
    this.agencyId = this.route.snapshot.paramMap.get("id");
    this.statusAgency = this.route.snapshot.paramMap.get("status");
    this.nameAgency = this.route.snapshot.paramMap.get("name_agency");

    this.employee = this.formBuilder.group(FormEmployeeValidator);

    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    this.isLoading = true;

    this.getEmployees();
    this.profileService.getProfilesByUser(this.agencyId).subscribe(
      (profiles: Profile[]) => {
        this.profiles = profilesLang(profiles, this.translate.getDefaultLang());
        this.backupProfile = profilesLang(
          profiles,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.agencyService.getAgenciesToTransfer(parseInt(this.agencyId)).subscribe(
      (agencies: Agency[]) => {
        this.isLoading = false;
        this.agencies = agencies;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
    on(REAL_TIME.NEW_EMPLOYEE, (data) => {
      if (!this.filtered && data.agency == this.agencyId) {
        this.getEmployees();
      }
    });
  }

  ngOnDestroy(): void {
    unsubscribeOf(REAL_TIME.NEW_EMPLOYEE);
  }

  getEmployees = () => {
    this.agencyService.getEmployeeByAgency({ id: this.agencyId }).subscribe(
      (agencies: Employee[]) => {
        this.isLoading = false;
        const rawData = agencies
          .map((agency: Employee) => {
            return {
              ...agency,
              person: { ...agency.person },
              role: { ...agency.role[0] },
              agency: agency.agency[0] || null,
              agencyId: this.agencyId,
            };
          })
          .filter((user) => !!user.person);

        this.backupEmployees = rawData;
        this.employees = rawData.filter(({ status }) => status === this.status);
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  changeStatus = () => {
    this.status =
      this.status === STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
    let employees = [...this.backupEmployees];
    this.employees = employees.filter(({ status }) => status === this.status);
  };

  updateBackupEmployees = (employee: Employee, type: number) => {
    switch (type) {
      case CREATE:
        this.backupEmployees = [...this.backupEmployees, employee];
        return;
      case EDIT:
        let keyEmployee = this.backupEmployees.findIndex(
          ({ id }) => id === employee.id
        );
        let resEmployee = [...this.backupEmployees];

        resEmployee[keyEmployee] = {
          ...employee,
          person: { ...employee.person },
          role: { ...employee.role[0] },
          id: employee.id,
          agencyId: this.agencyId,
        };

        this.backupEmployees = resEmployee;
        return;
      case DELETE:
        this.backupEmployees = this.backupEmployees.filter(
          ({ id }) => id !== employee.id
        );
        return;
    }
  };

  onBlurZipCode = (nameField: string) => {
    const zipCode = this.employee.get(nameField).value;
    const countryId = this.employee.get("country_id").value;
    const countryAlphacode = this.countries.find(({ id }) => id === countryId);

    if (!zipCode) {
      return;
    }

    this.onDefaultValuesStateAndCountry();
    this.searching = true;
    this.errorZipCode = false;

    this.googleService
      .getZipCode({
        postalCode: zipCode.toString(),
        country: !!countryAlphacode ? countryAlphacode.alpha2Code : "US",
      })
      .subscribe(
        async (res: any) => {
          const response = await this.googleService.getStateAndCity(res);

          this.employee.patchValue(
            {
              state: response.state,
              city: response.city,
            },
            { onlySelf: true, emitEvent: false }
          );

          this.searching = false;
        },
        async () => {
          if (!this.shouldEnableStateAndCity) {
            this.searching = false;
            this.errorZipCode = true;

            const wantWrite = await confirm({
              text: this.translate.instant("ngbAlert.watWriteZipCode"),
              confirmButtonText: this.translate.instant(
                "buttonText.confirmButtonText"
              ),
              cancelButtonText: this.translate.instant(
                "buttonText.cancelButtonText"
              ),
            });

            if (wantWrite) {
              this.employee.get("state").enable({ onlySelf: true });
              this.employee.get("city").enable({ onlySelf: true });
            }
          }
        }
      );
  };

  onDefaultValuesStateAndCountry = () => {
    this.employee.get("state").reset(null, { onlySelf: true });
    this.employee.get("city").reset(null, { onlySelf: true });
  };

  shouldStateAndCity = () => {
    this.shouldEnableStateAndCity = !this.shouldEnableStateAndCity;

    if (this.shouldEnableStateAndCity) {
      this.employee.get("state").enable({ onlySelf: true });
      this.employee.get("city").enable({ onlySelf: true });
    } else {
      this.employee.get("state").disable({ onlySelf: true });
      this.employee.get("city").disable({ onlySelf: true });
    }
  };

  onSubmit = () => {
    const infoValid = this.employee.valid;
    const isEditing = this.isEditing;
    const { agency } = this.user;
    const agencyId = !!agency ? agency.id : this.user.id;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.employee);
    } else if (this.submitted) {
      return;
    }

    this.submitted = true;

    switch (isEditing) {
      case true:
        return this.addAnother
          ? this.userService.assign(this.employee.getRawValue()).subscribe(
              (user: Employee) => {
                this.submitted = false;

                this.destroyEmployeee(user);

                this.employee.reset();

                showMessage(
                  "¡Enhorabuena!",
                  this.translate.instant("notifications.updateUser"),
                  "success",
                  true
                );

                this.modalRef.close();
              },
              (error: HttpErrorResponse) => {
                this.submitted = false;
                this.handleError(error);
              }
            )
          : this.userService.update(this.employee.getRawValue()).subscribe(
              (user: Employee) => {
                this.submitted = false;

                this.shoulderUpdateUser(user);

                this.employee.reset();

                showMessage(
                  "¡Enhorabuena!",
                  this.translate.instant("notifications.updateUser"),
                  "success",
                  true
                );

                this.modalRef.close();
              },
              (error: HttpErrorResponse) => {
                this.submitted = false;
                this.handleError(error);
              }
            );
      case false:
        return this.userService
          .store(this.employee.getRawValue(), parseInt(this.agencyId))
          .subscribe(
            (user: Employee) => {
              this.submitted = false;

              this.shouldMakeUser(user);

              this.employee.reset();

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.createUser"),
                "success",
                true
              );

              this.modalRef.close();
              Socket.emit(REAL_TIME.EMPLOYEE, {
                id: agencyId,
                agency: this.agencyId,
              });
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
    }
  };

  shoulderUpdateUser = (user: Employee) => {
    let keyEmployee = this.employees.findIndex(({ id }) => id === user.id);
    let resEmployee = [...this.employees];

    resEmployee[keyEmployee] = {
      ...user,
      person: { ...user.person },
      role: { ...user.role[0] },
      id: user.id,
      agencyId: this.agencyId,
    };

    this.employees = resEmployee;
    this.updateBackupEmployees(user, EDIT);
  };

  shoulderUpdateUserStatus = (user: Employee) => {
    let resEmployee = [...this.employees];
    this.employees = resEmployee.filter(({ id }) => id !== user.id);
    this.updateBackupEmployees(user, EDIT);
  };

  shouldMakeUser = (employee: Employee) => {
    const resUser = {
      ...employee,
      person: { ...employee.person },
      role: { ...employee.role[0] },
      id: employee.id,
      agencyId: this.agencyId,
    };

    this.employees = [...this.employees, resUser];
    this.updateBackupEmployees(resUser, CREATE);
  };

  destroyEmployeee = (employee: Employee) => {
    this.employees = this.employees.filter(({ id }) => id !== employee.id);
    this.updateBackupEmployees(employee, DELETE);
  };

  formating = (phone: any) => {
    return phone ? formatingPhone(phone) : "";
  };

  goToAgencies = () => {
    return this.router.navigate(["/dashboard/agencies"]);
  };

  isFieldValid = (field: string): boolean => {
    return this.employee.get(field).invalid && this.employee.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };
  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  getValidationRegex = () => {
    const pass = this.employee.get("password").value;
    const regex = /(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$/;
    const isEditing = this.isEditing;
    if ((pass === null || !regex.test(pass)) && !isEditing) {
      return (this.validatePass = true);
    }
    return (this.validatePass = false);
  };

  validate = (variable: any) => {
    const regexMin = /(?=.{6,})/;
    const regexNumber = /(?=.*[0-9])/;
    const regexText = /(?=.*[A-Z])/;
    const regexChar = /(?=.*[@#$%^&*-/%+=]).*$/;
    this.requiteText = false;
    if (regexText.test(variable)) {
      this.requiteText = true;
    }
    this.requireNumber = false;
    if (regexNumber.test(variable)) {
      this.requireNumber = true;
    }
    this.requireCharEs = false;
    if (regexChar.test(variable)) {
      this.requireCharEs = true;
    }
    this.minChar = false;
    if (regexMin.test(variable)) {
      this.minChar = true;
    }
  };

  fPass = () => {
    this.validatePass = true;
  };

  clean = () => {
    this.requiteText = false;
    this.requireNumber = false;
    this.requireCharEs = false;
    this.minChar = false;

    this.employee.get("state").disable({ onlySelf: true });
    this.employee.get("city").disable({ onlySelf: true });
  };

  shouldToggleStatus = async (userId: number) => {
    let agencyInac = this.agencyInactive();
    if (agencyInac) return;

    const wantStatus = await confirm({
      text: this.translate.instant("ngbAlert.wantToChangeStatus"),
      confirmButtonText: this.translate.instant("buttonText.confirmButtonText"),
      cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
    });

    if (wantStatus) {
      this.userService.changeStatus({ id: userId }).subscribe(
        (user: Employee) => {
          this.submitted = false;

          this.shoulderUpdateUserStatus(user);

          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.updateUser"),
            "success",
            true
          );
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;

          if (typeof error.error === "string") {
            return showMessage(null, error.error, "error", true);
          }

          this.handleError(error);
        }
      );
    }
  };

  agencyInactive = (): boolean => {
    if (this.statusAgency === "0") {
      showMessage(
        "¡Error!",
        this.translate.instant("validations.agencyInactive"),
        "error",
        true
      );
      return true;
    }
    return false;
  };

  shouldToggleUpdate = (ngbUser, userId: number) => {
    this.isEditing = true;
    this.validatePass = false;

    this.clean();

    const employee: Employee = this.employees.find(({ id }) => id === userId);

    this.employee.patchValue({
      ...employee,
      ...employee.person,
      role: employee.role.id,
      id: employee.id,
      password: null,
      agencyId: this.agencyId,
    });

    this.employee.get("password").setValidators([]);

    this.modalRef = this.ngbModal.open(ngbUser);
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
    this.validatePass = false;
    this.displayEmployee = null;
    this.addAnother = false;
    this.profiles = this.backupProfile;
    this.employee.get("agency").setValidators([]);
    this.employee.get("reason").setValidators([]);
    this.employee.patchValue(
      { reason: null, agency: null },
      { onlySelf: true }
    );
    this.employee.reset();
    this.clean();
  };

  shouldToggleCreate = (ngbUser: TemplateRef<any>) => {
    this.isEditing = false;
    this.errors = null;
    this.validatePass = false;
    this.clean();

    this.employee.reset();

    this.employee
      .get("password")
      .setValidators([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.pattern(
          "(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$"
        ),
      ]);

    const countryId = this.countries.find(
      ({ alpha2Code }) => alpha2Code === "US"
    )?.id;

    this.employee.patchValue(
      { agencyId: this.agencyId, country_id: countryId || null },
      { onlySelf: true }
    );
    this.modalRef = this.ngbModal.open(ngbUser);
  };

  clear = () => {
    this.filtered = false;
    this.emptySearch();
    this.onKey("");
  };

  searchButton = () => {
    let name;
    for (var key in this.search) {
      if (this.search[key] != null) name = key;
    }
    this.onKey(name);
  };

  onKey(key: string) {
    let name = key;
    let variable = this.search[key];
    if (this.isLoading) return;
    this.isLoading = true;
    this.filtered = true;

    this.userService
      .search(
        { [name]: variable, agency: this.agencyId, status: this.status },
        this.paginate
      )
      .subscribe(
        (users: Employee[]) => {
          this.isLoading = false;
          const rawData = users
            .map((employee: Employee) => {
              return {
                ...employee,
                person: { ...employee.person },
                role: { ...employee.role[0] },
              };
            })
            .filter((employee) => !!employee.person);

          this.employees = rawData;
          if (!name && !variable) this.backupEmployees = rawData;
        },
        (error: HttpErrorResponse) => {
          this.isLoading = false;
          this.emptySearch();
          showMessage(
            "¡Error!",
            this.translate.instant(`errors${handlerError(error)}`),
            "error",
            true
          );
        }
      );
  }

  emptySearch = () => {
    this.search = {
      name: null,
      email: null,
      phone: null,
      cellphone: null,
      id: null,
    };
  };

  clearInput = (name: string) => {
    for (var key in this.search) {
      if (key != name && this.search[key] != null) {
        this.search[key] = null;
      }
    }
    let backup = [...this.backupEmployees];
    this.employees = backup.filter(({ status }) => status === this.status);
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  isToggleEmployee = (ngbDisplayEmployee: TemplateRef<any>, key: number) => {
    this.displayEmployee = this.employees[key];

    if (!!this.displayEmployee) {
      this.modalRef = this.ngbModal.open(ngbDisplayEmployee);
    }
  };

  isInactiveEmployee = () => {
    return this.employee.get("status").value === STATUS_INACTIVE;
  };

  addAnotherAgency = () => {
    this.addAnother = !this.addAnother;

    switch (this.addAnother) {
      case true:
        this.employee.patchValue(
          { email: null, cellphone: null, agency: null, role: null },
          { onlySelf: true }
        );
        this.employee.get("agency").setValidators([Validators.required]);
        this.employee.get("reason").setValidators([Validators.required]);
        this.profiles = [];
        return;
      case false:
        const employee: Employee = this.employees.find(
          ({ id }) => id === this.employee.get("id").value
        );
        this.employee.patchValue(
          {
            email: employee.email,
            cellphone: employee.person.cellphone,
            role: employee.role.id,
          },
          { onlySelf: true }
        );
        this.profiles = this.backupProfile;
        this.employee.get("agency").setValidators([]);
        this.employee.get("reason").setValidators([]);
        return;
    }
  };

  changeProfilesAgency = () => {
    let idAgency = parseInt(this.employee.get("agency").value);

    if (idAgency !== null) {
      let profiles = this.agencies.find(({ id }) => id === idAgency).profiles;
      this.profiles = profilesLang(profiles, this.translate.getDefaultLang());
    }
  };

  toggleButtonByTierIdStatus = (type: number, name_path: string): Boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === name_path)
              .find((assignment) => assignment.type === type)
          : null;
      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };
}
