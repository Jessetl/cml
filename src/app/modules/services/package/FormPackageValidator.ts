import { FormControl, Validators } from "@angular/forms";

export const PackageFormValidator = {
  id: new FormControl(null, []),
  country_id: new FormControl(null, [Validators.required]),
  type: new FormControl(null, [Validators.required]),
  formula: new FormControl(null, [Validators.required]),
  cost_pound: new FormControl(null, []),
  status: new FormControl(1, [Validators.required]),
  name: new FormControl(null, [Validators.required, Validators.maxLength(120)]),
  name_en: new FormControl(null, [
    Validators.required,
    Validators.maxLength(120),
  ]),
  description: new FormControl(null, [Validators.required]),
  description_en: new FormControl(null, [Validators.required]),
  long: new FormControl(null, [Validators.required]),
  high: new FormControl(null, [Validators.required]),
  width: new FormControl(null, [Validators.required]),
  base_price: new FormControl(null, []),
  agency_price: new FormControl(null, [Validators.required]),
  corporate_price: new FormControl(null, [Validators.required]),
  public_price: new FormControl(null, [Validators.required]),
  minimum_weight: new FormControl(null, [Validators.required]),
  maximum_weight: new FormControl(null, [Validators.required]),
  minimum_insurance: new FormControl(null, [Validators.required]),
  maximum_insurance: new FormControl(null, [Validators.required]),
  commercial_surplus: new FormControl(null, []),
  promotion: new FormControl(0, []),
  sale: new FormControl(0, []),
};
