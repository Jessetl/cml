import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { PackageFormValidator } from "../FormPackageValidator";

import { Package, Country } from "@models";
import { PackageService } from "@core/service";

import { TranslateService } from "@ngx-translate/core";

import { validationsForm } from "@shared/utils";
import { getNameMinByFormula, getNameMaxByFormula } from "@shared/utils";
import { showMessage, handlerError, rawError, getCost } from "@shared/utils";

@Component({
  selector: "cml-package",
  templateUrl: "./form.component.html",
  styles: [],
})
export class FormComponent implements OnInit {
  @Input() isEditing: boolean;
  @Input() pack: Package;
  @Input() countries: Country[];
  @Output() shoulderUpdatePackage = new EventEmitter<Package>();
  @Output() shouldMakePackage = new EventEmitter<Package>();
  @Output() onClose = new EventEmitter<null>();

  public submitted: boolean;
  public package: FormGroup;
  public errors: Array<any>;

  public regutar: number = 1;
  public electronic: number = 2;
  public mixed: number = 3;
  public commercial: number = 4;

  public type_contents: number[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private packageService: PackageService
  ) {
    this.package = this.formBuilder.group(PackageFormValidator);
  }

  ngOnInit() {
    const isEditing = this.isEditing;

    this.package.reset();
    this.package.patchValue({ status: 1 }, { onlySelf: true });

    if (isEditing) {
      this.package.patchValue({
        ...this.pack,
      });

      this.type_contents = this.pack.type_content.map(({ content }) => content);
    }
  }

  isFieldValid = (field: string): boolean => {
    return this.package.get(field).invalid && this.package.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getNameMinByType = (): string => {
    return getNameMinByFormula(
      this.package.get("formula").value,
      this.translate
    );
  };

  getNameMaxByType = (): string => {
    return getNameMaxByFormula(
      this.package.get("formula").value,
      this.translate
    );
  };

  getNameCost = (): string => {
    return getCost(this.package.get("formula").value, this.translate);
  };

  getPromoValue = (): boolean => {
    return this.package.get("promotion").value;
  };

  getSaleValue = (): boolean => {
    return this.package.get("sale").value;
  };

  getContentType = (value: number) => {
    return !!this.type_contents.find((id) => id == value);
  };

  getCommercialSurplus = () => {
    const max = this.package.get("maximum_insurance").value;
    const surplus = this.package.get("commercial_surplus").value;
    let isInvalid: boolean = false;

    if (surplus !== null && surplus <= max) {
      isInvalid = true;

      showMessage(
        "",
        this.translate.instant("validations.commercial_surplus"),
        "warning",
        true
      );
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  setCheckboxValue = (name: string): void => {
    this.package.patchValue(
      { [name]: !this.package.get(name).value },
      { onlySelf: true }
    );
  };

  setContentType = (value: number) => {
    this.type_contents = this.type_contents.find((id) => id == value)
      ? this.type_contents.filter((id) => id != value)
      : [...this.type_contents, value];
  };

  requiredCost = () => {
    return this.package.get("formula").value === 5 ? true : false;
  };

  onSubmit = async () => {
    const isSubmitted = this.submitted;
    const infoValid = this.package.valid;
    const isEditing = this.isEditing;

    this.errors = null;
    const value = this.package.get("formula").value;
    value === 5
      ? this.package.controls["cost_pound"].setValidators([Validators.required])
      : this.package.controls["cost_pound"].setValidators([]);

    if (!infoValid) {
      return validationsForm(this.package);
    }

    if (!!!isSubmitted) {
      const surplus = await this.getCommercialSurplus();

      if (surplus) {
        return;
      }

      this.submitted = true;

      switch (isEditing) {
        case true:
          return this.packageService
            .update({
              ...this.package.value,
              type_contents: this.type_contents,
            })
            .subscribe(
              (pack: Package) => {
                this.submitted = false;

                this.shoulderUpdatePackage.emit(pack);

                this.package.reset();

                showMessage(
                  "¡Enhorabuena!",
                  this.translate.instant("notifications.updatePackage"),
                  "success",
                  true
                );

                this.onClose.emit(null);
              },
              (error: HttpErrorResponse) => {
                this.submitted = false;
                this.handleError(error);
              }
            );

        case false:
          this.submitted = true;

          return this.packageService
            .store({ ...this.package.value, type_contents: this.type_contents })
            .subscribe(
              (pack: Package) => {
                this.submitted = false;

                this.shouldMakePackage.emit(pack);

                this.package.reset();

                showMessage(
                  "¡Enhorabuena!",
                  this.translate.instant("notifications.createPackage"),
                  "success",
                  true
                );

                this.onClose.emit(null);
              },
              (error: HttpErrorResponse) => {
                this.submitted = false;
                this.handleError(error);
              }
            );
      }
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string, traduc?: string): string => {
    return this.translate.instant("validations.required", {
      name: traduc
        ? traduc.toLowerCase()
        : this.translate.instant(value).toLowerCase(),
    });
  };
}
