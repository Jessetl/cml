import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

// Components
import { FormComponent } from "./Form/form.component";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { PackageService, CountryService } from "@core/service";

import { Package, Country, User } from "@models";

import { countriesLang, getNameCountryLang } from "@shared/utils";

import { TierId, rawError, handlerError } from "@shared/utils";
import { showMessage, showConfirmButton } from "@shared/utils";
import { SETTINGS_AGENCY } from "@shared/utils";
import { CREATE, EDIT, DELETE } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-packages",
  templateUrl: "./package.component.html",
  styles: [],
})
export class PackageComponent implements OnInit {
  @ViewChild(FormComponent)
  formPackage: FormComponent;

  private modalRef: NgbModalRef;
  public isLoading: boolean = true;
  public isEditing: boolean = false;
  public user: User;
  public backupPackages: Package[];
  public packages: Package[];
  public countries: Country[];
  public package: Package;
  public errors: Array<any>;
  public paginate: number = 1;
  public submitted: boolean = false;
  public searching: boolean = false;
  public viewService: Package;

  // Search
  public search = {
    name: null,
    description: null,
    country: null,
  };

  public language: string;

  constructor(
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private packageService: PackageService,
    private countryService: CountryService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    this.submitted = true;
    this.packageService.getPackagesByPage(this.paginate).subscribe(
      (packages: Package[]) => {
        this.submitted = false;
        this.packages = packages;
        this.backupPackages = packages;
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  getPackageName = (service: Package): string => {
    return this.translate.getDefaultLang() === "es"
      ? service.name
      : service.name_en;
  };

  getCountryName = (country: Country): string => {
    return this.translate.getDefaultLang() === "es"
      ? country.name
      : country.name_en;
  };

  getPackageDescription = (service: Package): string => {
    return this.translate.getDefaultLang() === "es"
      ? service.description
      : service.description_en;
  };

  updateBackupPackages = (pack: Package, type: number) => {
    switch (type) {
      case CREATE:
        return (this.backupPackages = [...this.backupPackages, pack]);
      case EDIT:
        let resPackage = [...this.backupPackages];
        let keyAgency = this.backupPackages.findIndex(
          ({ id }) => id === pack.id
        );
        resPackage[keyAgency] = {
          ...pack,
        };
        return (this.backupPackages = resPackage);
      case DELETE:
        this.backupPackages = this.backupPackages.filter(
          ({ id }) => id !== pack.id
        );
      default:
        this.backupPackages;
    }
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === "services")
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  toggleButtonByOnlyAdmin = (): boolean => {
    if (!!this.user) {
      const { agency } = this.user;

      return (
        this.user.tier === TierId.TIER_ADMIN ||
        (!!agency && agency.tier === TierId.TIER_ADMIN)
      );
    }
  };

  toggleButtonByTierAgency = (setting: number): boolean => {
    if (!!this.user) {
      const { agency, person } = this.user;

      const tierId = !!agency ? agency.tier : this.user.tier;

      const agencySettings = !!agency
        ? agency.person.agency_settings
        : person.agency_settings;

      return tierId === TierId.TIER_ADMIN || agencySettings === setting;
    }
  };

  shouldToggleCreate = (ngbPackage: TemplateRef<any>) => {
    this.isEditing = false;

    this.modalRef = this.ngbModal.open(ngbPackage, {
      size: "lg",
    });
  };

  shouldToggleUpdate = (ngbPackage: TemplateRef<any>, packageId: number) => {
    this.isEditing = true;
    const service: Package = this.packages.find(({ id }) => id === packageId);

    this.package = {
      ...service,
      cost_pound: parseFloat(service.cost_pound.toFixed(2)),
      base_price: parseFloat(service.base_price.toFixed(2)),
      agency_price: parseFloat(service.agency_price.toFixed(2)),
      corporate_price: parseFloat(service.corporate_price.toFixed(2)),
      public_price: parseFloat(service.public_price.toFixed(2)),
    };

    this.modalRef = this.ngbModal.open(ngbPackage, {
      size: "lg",
    });
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
  };

  shouldToggleDelete = (packageId: number) => {
    showConfirmButton(
      this.translate.instant("ngbAlert.deletePackage"),
      this.translate.instant("ngbAlert.btnYes"),
      this.translate.instant("ngbAlert.btnNo"),
      () => {
        this.packageService.destroy(packageId).subscribe(
          (service: Package) => {
            this.shoulderDestroyPackage(packageId);
            this.updateBackupPackages(service, DELETE);

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.destroyPackage"),
              "success",
              true
            );

            this.modalRef.close();
          },
          (error: HttpErrorResponse) => {
            this.handleError(error);
          }
        );
      }
    );
  };

  shouldMakePackage = (pack: Package) => {
    this.packages = [...this.packages, pack];
    this.updateBackupPackages(pack, CREATE);
  };

  shoulderUpdatePackage = (pack: Package) => {
    let keyAgency = this.packages.findIndex(({ id }) => id === pack.id);
    let resPackage = [...this.packages];

    resPackage[keyAgency] = {
      ...pack,
    };

    this.packages = resPackage;
    this.updateBackupPackages(pack, EDIT);
  };

  shoulderDestroyPackage = (packageId: number) => {
    this.packages = this.packages.filter(({ id }) => id !== packageId);
  };

  getPriceByAgency = (agency_settings: number, packet: Package): number => {
    return agency_settings === SETTINGS_AGENCY
      ? packet.agency_price
      : packet.corporate_price;
  };

  getPriceByTierId = (packet: Package): number => {
    const { person } = this.user;
    const price =
      this.user.tier === TierId.TIER_AGENCY
        ? this.getPriceByAgency(person.agency_settings, packet)
        : packet.base_price;

    return price;
  };

  emptySearch = () => {
    this.search = {
      name: null,
      description: null,
      country: null,
    };
  };

  clearInput = (name: string) => {
    for (var key in this.search) {
      if (key != name && this.search[key] != null) {
        this.search[key] = null;
        this.packages = this.backupPackages;
      }
    }
  };

  clear = () => {
    this.emptySearch();
    this.onKey("");
  };

  searchButton = () => {
    let name;
    for (var key in this.search) {
      if (this.search[key] != null) name = key;
    }
    this.onKey(name);
  };

  onKey(key: string) {
    let name = key;
    let variable = this.search[key];
    if (this.submitted) return;
    this.submitted = true;
    this.packageService.search({ [name]: variable }, this.paginate).subscribe(
      (packages: Package[]) => {
        this.submitted = false;
        this.packages = packages;
        if (!name && !variable) this.backupPackages = packages;
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.emptySearch();
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  getNameCountry = (country: Country): string => {
    return getNameCountryLang(country, this.translate.getDefaultLang()).name;
  };

  openViewService = (ngbView: TemplateRef<any>, key: number) => {
    let service = this.packages[key];
    let base = this.toggleButtonByOnlyAdmin();
    let corp = this.toggleButtonByTierAgency(2);
    let agency_price = this.toggleButtonByTierAgency(1);

    this.viewService = {
      ...service,
      base_price: base ? this.getPriceByTierId(service) : 0,
      corporate_price: corp ? service.corporate_price : 0,
      agency_price: agency_price ? service.agency_price : 0,
    };

    if (!!this.viewService) {
      this.modalRef = this.ngbModal.open(ngbView);
    }
  };
}
