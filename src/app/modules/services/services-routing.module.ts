import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Components
import { FormComponent } from "./package/Form/form.component";

import { PackageComponent } from "./package/package.component";

const routes: Routes = [
  {
    path: "",
    component: PackageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServicesRoutingModule {}

export const routingComponents = [PackageComponent, FormComponent];
