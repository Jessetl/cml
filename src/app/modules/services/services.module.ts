import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@shared";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import {
  ServicesRoutingModule,
  routingComponents,
} from "./services-routing.module";

@NgModule({
  declarations: [routingComponents],
  imports: [
    CommonModule,
    ServicesRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class ServicesModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
