import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { ClipboardService } from "ngx-clipboard";

import { FormInformationValidator } from "./FormInformationValidator";
import { User, Country, Subscription } from "@models";
import { InformationService, CountryService } from "@core/service";
import { GooglemapsService, LockerService } from "@core/service";
import { environment } from "environments/environment";
import { handlerError, rawError, SIZE_FILE, TierId } from "@shared/utils";
import { showMessage, validationsForm, TIER_ADMIN } from "@shared/utils";
import { confirm, showMessageConfirm } from "@shared/utils";
import { countriesLang, showConfirmButton } from "@shared/utils";
import { CANCEL_SUBSCRIPTION } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as user from "@actions";
import * as token from "@actions";
import * as shop from "@actions";

@Component({
  selector: "cml-information",
  templateUrl: "./information.component.html",
  styles: [],
})
export class InformationComponent implements OnInit {
  public imageSrc: string;
  public isLoading: boolean = true;
  public user: User;
  public countries: Country[];
  public information: FormGroup;
  public submitted: boolean;
  public searching: boolean = false;

  public errors: Array<any>;
  public errorZipCode: boolean;
  public shouldEnableStateAndCity: boolean = false;
  public enableStateAndCityConsolidation: boolean = false;
  public isCancel: boolean = false;
  public errorCancel: boolean = false;

  //password
  public validatePass: boolean = false;
  public minChar: boolean = false;
  public requireNumber: boolean = false;
  public requireCharEs: boolean = false;
  public requireText: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private infoService: InformationService,
    private countryService: CountryService,
    private googleService: GooglemapsService,
    private clipboardService: ClipboardService,
    private lockerService: LockerService,
    private store: Store<fromRoot.State>,
    private router: Router
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.information = this.formBuilder.group(FormInformationValidator);
  }

  ngOnInit() {
    const { person } = this.user;

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.clipboardService.copyResponse$.subscribe((re) => {
      if (re.isSuccess) {
        showMessage(
          "",
          this.translate.instant("notifications.copyClipboard"),
          "success",
          true
        );
      }
    });

    let consolidation = this.user.consolidation;

    this.information.patchValue({
      ...this.user,
      ...person,
      consolidation_zip_code: !!consolidation
        ? this.user.consolidation.zip_code
        : null,
      consolidation_state: !!consolidation
        ? this.user.consolidation.state
        : null,
      consolidation_city: !!consolidation ? this.user.consolidation.city : null,
      consolidation_address: !!consolidation
        ? this.user.consolidation.address
        : null,
      image: null,
      password: null,
      referralLink: this.getReferralLink(this.user),
    });

    this.imageSrc = person.image ? person.image_url : null;
    this.isLoading = false;

    if (this.user.tier === TIER_ADMIN) {
      this.platformsRequire();
      return this.consolidationRequire();
    }

    this.consolidationNot();
    this.platformsNot();
  }

  isFieldValid = (field: string): boolean => {
    return (
      this.information.get(field).invalid && this.information.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  platformsRequire = () => {
    this.information
      .get("app_ios")
      .setValidators([Validators.required, Validators.maxLength(190)]);

    this.information
      .get("app_android")
      .setValidators([Validators.required, Validators.maxLength(190)]);
  };

  consolidationRequire = () => {
    this.information
      .get("consolidation_address")
      .setValidators([Validators.required]);
    this.information
      .get("consolidation_zip_code")
      .setValidators([Validators.required, Validators.maxLength(5)]);
    this.information
      .get("consolidation_state")
      .setValidators([Validators.required, Validators.maxLength(30)]);
    this.information
      .get("consolidation_city")
      .setValidators([Validators.required, Validators.maxLength(30)]);
  };

  consolidationNot = () => {
    this.information.get("consolidation_address").setValidators([]);
    this.information.get("consolidation_zip_code").setValidators([]);
    this.information.get("consolidation_state").setValidators([]);
    this.information.get("consolidation_city").setValidators([]);
  };

  platformsNot = () => {
    this.information.get("app_ios").setValidators([]);
    this.information.get("app_android").setValidators([]);
  };

  onToggleByRole = (type: number): boolean => {
    if (!!this.user) {
      const { role } = this.user;

      const module = !!role.assignments
        ? role.assignments
            .filter((assignment) => assignment.submodule.path === "lockers")
            .find((assignment) => assignment.type === type)
        : null;

      return !!module;
    }
  };

  getReferralLink = (user: User) => {
    if (!!user) {
      return `${
        environment.baseUrl
      }/auth/register/${this.user.alphanumeric.toLocaleLowerCase()}`;
    }
  };

  callServiceToCopy() {
    if (!!this.user) {
      this.clipboardService.copy(this.getReferralLink(this.user));
    }
  }

  onSubmit = () => {
    const infoValid = this.information.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.information);
    }

    if (!!!this.submitted) {
      this.submitted = true;
      this.isCancel = false;

      this.infoService
        .update({
          ...this.information.getRawValue(),
          user_id: this.user.id,
        })
        .subscribe(
          (data) => {
            this.updateUser(data);
            this.submitted = false;

            showMessageConfirm(
              this.translate.instant("messages.congratulations"),
              this.translate.instant("notifications.updateInfo"),
              "success",
              this.translate.instant("tableRender.ok"),
              () => {
                this.router.navigate(["/dashboard"]);
              }
            );
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;

            const errorResponse = handlerError(error);

            if (typeof errorResponse === "string") {
              return showMessage(
                "¡Error!",
                this.translate.instant(`errors${errorResponse}`),
                "error",
                true
              );
            }

            this.errors = rawError(errorResponse);
          }
        );
    }
  };

  onDefaultValuesStateAndCountry = () => {
    this.information.get("state").reset(null, { onlySelf: true });
    this.information.get("city").reset(null, { onlySelf: true });
  };

  onDefaultCosolidation = () => {
    this.information.get("consolidation_state").reset(null, { onlySelf: true });
    this.information.get("consolidation_city").reset(null, { onlySelf: true });
  };

  onBlurZipCode = (nameField: string, consolidation: boolean) => {
    const zipCode = this.information.get(nameField).value;
    const countryId = this.information.get("country_id").value;
    const countryAlphacode = this.countries.find(({ id }) => id === countryId);

    if (!zipCode) {
      return;
    }

    !!!consolidation
      ? this.onDefaultValuesStateAndCountry()
      : this.onDefaultCosolidation();
    this.searching = true;
    this.errorZipCode = false;

    this.googleService
      .getZipCode({
        postalCode: zipCode.toString(),
        country: !!countryAlphacode ? countryAlphacode.alpha2Code : "US",
      })
      .subscribe(
        async (res: any) => {
          const response = await this.googleService.getStateAndCity(res);

          !!!consolidation
            ? this.information.patchValue(
                {
                  state: response.state,
                  city: response.city,
                },
                { onlySelf: true, emitEvent: false }
              )
            : this.information.patchValue(
                {
                  consolidation_state: response.state,
                  consolidation_city: response.city,
                },
                { onlySelf: true, emitEvent: false }
              );

          this.searching = false;
        },
        async () => {
          if (!this.shouldEnableStateAndCity) {
            this.searching = false;
            this.errorZipCode = true;

            const wantWrite = await confirm({
              text: this.translate.instant("ngbAlert.watWriteZipCode"),
              confirmButtonText: this.translate.instant(
                "buttonText.confirmButtonText"
              ),
              cancelButtonText: this.translate.instant(
                "buttonText.cancelButtonText"
              ),
            });

            if (wantWrite) {
              if (!!!consolidation) {
                this.information.get("state").enable({ onlySelf: true });
                this.information.get("city").enable({ onlySelf: true });
              } else {
                this.information
                  .get("consolidation_state")
                  .enable({ onlySelf: true });
                this.information
                  .get("consolidation_city")
                  .enable({ onlySelf: true });
              }
            }
          }
        }
      );
  };

  shouldStateAndCity = () => {
    this.shouldEnableStateAndCity = !this.shouldEnableStateAndCity;

    if (this.shouldEnableStateAndCity) {
      this.information.get("state").enable({ onlySelf: true });
      this.information.get("city").enable({ onlySelf: true });
    } else {
      this.information.get("state").disable({ onlySelf: true });
      this.information.get("city").disable({ onlySelf: true });
    }
  };

  shouldConsolidation = () => {
    this.enableStateAndCityConsolidation = !this
      .enableStateAndCityConsolidation;

    if (this.enableStateAndCityConsolidation) {
      this.information.get("consolidation_state").enable({ onlySelf: true });
      this.information.get("consolidation_city").enable({ onlySelf: true });
    } else {
      this.information.get("consolidation_state").disable({ onlySelf: true });
      this.information.get("consolidation_city").disable({ onlySelf: true });
    }
  };

  onFileChanged = (event: any) => {
    const { person } = this.user;
    const readerFile = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];

      if (this.maxFileSize(file.size)) {
        this.information.patchValue(
          { image: person.image },
          { onlySelf: true }
        );

        return showMessage(
          "",
          this.translate.instant("validations.fileInvalid"),
          "warning",
          true
        );
      }

      if (this.formatTypeImage(file.type)) {
        this.information.patchValue(
          { image: person.image },
          { onlySelf: true }
        );

        return showMessage(
          "",
          this.translate.instant("validations.format"),
          "warning",
          true
        );
      }

      readerFile.readAsDataURL(file);

      readerFile.onload = () => {
        const strResult = readerFile.result as string;
        this.imageSrc = strResult;
        this.information.patchValue({ image: strResult }, { onlySelf: true });
      };
    }
  };

  formatTypeImage = (img: string): boolean => {
    const isTrue: boolean = true;

    switch (img) {
      case "image/jpeg":
        return !isTrue;
      case "image/png":
        return !isTrue;
      case "image/bmp":
        return !isTrue;
      default:
        return isTrue;
    }
  };

  maxFileSize = (fileSize: number): boolean => {
    const isTrue: boolean = true;
    const sizeMegabytes = fileSize / 1024 / 1024;

    if (sizeMegabytes > SIZE_FILE) {
      return isTrue;
    }

    return !isTrue;
  };

  getValidationRegex = () => {
    const pass = this.information.get("password").value;
    const regex = /(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$/;

    return (this.validatePass =
      pass !== null && pass !== "" && !regex.test(pass) ? true : false);
  };

  validate = (variable: any) => {
    const regexMin = /(?=.{6,})/;
    const regexNumber = /(?=.*[0-9])/;
    const regexText = /(?=.*[A-Z])/;
    const regexChar = /(?=.*[@#$%^&*-/%+=]).*$/;

    this.requireText = regexText.test(variable) ? true : false;
    this.requireNumber = regexNumber.test(variable) ? true : false;
    this.requireCharEs = regexChar.test(variable) ? true : false;
    this.minChar = regexMin.test(variable) ? true : false;
  };

  fPass = () => {
    this.validatePass = true;
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  updateUser = (newUser: User) => {
    let userBefore = this.user;
    this.store.dispatch(
      new user.SetUserAction({
        ...userBefore,
        person: newUser.person,
        consolidation: newUser.consolidation,
      })
    );
  };

  alphanumeric = () => {
    return this.information.get("alphanumeric").value;
  };

  showCancel = async () => {
    this.isCancel = !this.isCancel;
    this.errorCancel = false;
  };

  changeOrCancel = (type: number) => {
    let message =
      type === CANCEL_SUBSCRIPTION
        ? this.translate.instant("ngbAlert.cancelMembership")
        : this.translate.instant("ngbAlert.changeMembership");
    showConfirmButton(
      message,
      this.translate.instant("ngbAlert.btnYes"),
      this.translate.instant("ngbAlert.btnNo"),
      (susbcription: Subscription) => {
        if (!!this.submitted) {
          return;
        }
        this.submitted = true;
        this.errorCancel = false;
        this.lockerService.cancelSubscription(type).subscribe(
          (susbcription: Subscription) => {
            this.submitted = false;
            this.isCancel = false;
            showMessage(
              "¡Enhorabuena!",
              type === CANCEL_SUBSCRIPTION
                ? this.translate.instant("notifications.cancelSubscription", {
                    name: this.user.admin_name,
                  })
                : this.translate.instant("notifications.subscription"),
              "success",
              true
            );
            type === CANCEL_SUBSCRIPTION
              ? this.logout()
              : this.updateTypePay(type);
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
      }
    );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }
    if (error.status === 422) {
      this.errorCancel = true;
    }
  };

  logout(): void {
    this.store.dispatch(new user.RemoveUserAction(null));
    this.store.dispatch(new token.RemoveTokenAction(null));
    this.store.dispatch(new shop.RemoveShopAction([]));
    this.router.navigate(["/auth/login"]);
  }

  updateTypePay = (type: number) => {
    let userBefore = this.user;
    this.store.dispatch(
      new user.SetUserAction({
        ...userBefore,
        type_pay: type,
      })
    );
  };
}
