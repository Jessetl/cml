import { FormControl, Validators } from "@angular/forms";

export const FormInformationValidator = {
  id: new FormControl({ value: null, disabled: false }, []),
  name: new FormControl({ value: null, disabled: false }, [
    Validators.required,
    Validators.maxLength(100),
  ]),
  alphanumeric: new FormControl({ value: null, disabled: true }, [
    Validators.required,
  ]),
  identification_id: new FormControl(null, [Validators.required]),
  phone: new FormControl({ value: null, disabled: false }, [
    Validators.maxLength(30),
  ]),
  cellphone: new FormControl({ value: null, disabled: false }, [
    Validators.maxLength(30),
  ]),
  zip_code: new FormControl({ value: null, disabled: false }, [
    Validators.required,
    Validators.maxLength(5),
  ]),
  country_id: new FormControl({ value: null, disabled: false }, [
    Validators.required,
  ]),
  state: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.maxLength(30),
  ]),
  city: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.maxLength(30),
  ]),
  address: new FormControl({ value: null, disabled: false }, [
    Validators.required,
  ]),
  consolidation_address: new FormControl(null, []),
  consolidation_zip_code: new FormControl(null, []),
  consolidation_state: new FormControl({ value: null, disabled: true }, []),
  consolidation_city: new FormControl({ value: null, disabled: true }, []),
  image: new FormControl({ value: null, disabled: false }, []),
  password: new FormControl({ value: null, diabled: false }, [
    Validators.minLength(6),
    Validators.maxLength(30),
    Validators.pattern(
      "(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$"
    ),
  ]),
  referralLink: new FormControl(null, []),
  app_ios: new FormControl(null, []),
  app_android: new FormControl(null, []),
};
