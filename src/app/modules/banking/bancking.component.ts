import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";

import { FormBanckingValidator } from "./FormBanckingValidator";
import { Bancking, User } from "@models";
import { InformationService } from "@core/service";
import { handlerError, rawError } from "@shared/utils";
import { showMessageConfirm } from "@shared/utils";
import { showMessage, validationsForm } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as user from "@actions";

@Component({
  selector: "cml-bancking",
  templateUrl: "./bancking.component.html",
  styles: [],
})
export class BanckingComponent implements OnInit {
  public user: User;
  public bancking: FormGroup;
  public submitted: boolean = false;

  public errors: Array<any>;

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private infoService: InformationService,
    private store: Store<fromRoot.State>,
    private router: Router
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.bancking = this.formBuilder.group(FormBanckingValidator);
  }

  ngOnInit(): void {
    const { bancking } = this.user;

    if (!!bancking) {
      this.bancking.patchValue({
        ...bancking,
      });
    }
  }

  isFieldValid = (field: string): boolean => {
    return this.bancking.get(field).invalid && this.bancking.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  alphanumeric = () => {
    return this.user.alphanumeric;
  };

  updateUser = (bancking: Bancking) => {
    this.store.dispatch(
      new user.SetUserAction({
        ...this.user,
        bancking: bancking,
      })
    );
  };

  onSubmit = () => {
    const infoValid = this.bancking.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.bancking);
    }

    if (!!!this.submitted) {
      this.submitted = true;

      this.infoService
        .updateBancking({
          ...this.bancking.getRawValue(),
          user_id: this.user.id,
        })
        .subscribe(
          (data) => {
            this.submitted = false;

            this.updateUser(data);

            showMessageConfirm(
              this.translate.instant("messages.congratulations"),
              this.translate.instant("notifications.updateInfo"),
              "success",
              this.translate.instant("tableRender.ok"),
              () => {
                this.router.navigate(["/dashboard"]);
              }
            );
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;

            const errorResponse = handlerError(error);

            if (typeof errorResponse === "string") {
              return showMessage(
                "¡Error!",
                this.translate.instant(`errors${errorResponse}`),
                "error",
                true
              );
            }

            this.errors = rawError(errorResponse);
          }
        );
    }
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
