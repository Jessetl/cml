import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { User, Quote, Country } from "@models";
import { LockerService } from "@core/service";

import { showMessage, handlerError } from "@shared/utils";
import { getOriginNameLang, getCountryNameLang } from "@shared/utils";
import { TimelineComponent } from "../tracking/timeline/timeline.component";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-locker",
  templateUrl: "./locker.component.html",
  styles: [],
})
export class LockerComponent implements OnInit {
  @ViewChild(TimelineComponent)
  Timeline: TimelineComponent;

  private modalRef: NgbModalRef;
  public loading: boolean = false;
  public step: number = 1;
  public tracking: Quote[];
  public urlFile: SafeResourceUrl;
  public tracing: Quote;
  public user: User;
  public locationVisibility: boolean;
  public tier: number;
  public printing: any[];

  //-----------------------
  public packageTracking = {
    alphanumeric: null,
    phone: null,
    identificationId: null,
  };
  public alphanumeric: string;

  constructor(
    private lockerService: LockerService,
    private ngbModal: NgbModal,
    private translate: TranslateService,
    public domSanitizer: DomSanitizer,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    this.lockerService.getShipmentsByLocker().subscribe(
      (quotes: Quote[]) => {
        this.tracking = quotes;
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  isToggleShowTracking = (
    ngbTracking: TemplateRef<TimelineComponent>,
    keyId: number
  ) => {
    const tracing = this.tracking[keyId] || null;
    this.tier = tracing.user.tier;
    this.tracing = tracing;
    if (!!tracing) {
      this.modalRef = this.ngbModal.open(ngbTracking, {
        size: "lg",
      });
    }
  };

  getOriginName = (shipping: Quote): string => {
    return getOriginNameLang(shipping, this.translate.getDefaultLang());
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  onCloseNgbModal = (): void => {
    this.modalRef.close();
  };

  onKey = () => {
    if (!this.alphanumeric) return;
    this.loading = true;
    this.lockerService
      .byTracking({ alphanumeric: this.alphanumeric })
      .subscribe(
        (quotes: Quote[]) => {
          this.loading = false;
          this.tracking = quotes;
        },
        (error: HttpErrorResponse) => {
          this.loading = false;
          showMessage(
            "¡Error!",
            this.translate.instant(`errors${handlerError(error)}`),
            "error",
            true
          );
        }
      );
  };

  clearAll = () => {
    this.alphanumeric = null;
    this.tracking = [];
  };

  isToggleQuote = (ngbQuote: TemplateRef<any>, keyId: number) => {
    const quote = this.tracking[keyId] || null;

    if (!!quote) {
      this.tracing = { ...quote };

      this.printing = [
        ...this.tracing.charge.map(({ id }) => ({
          [id]: null,
        })),
      ];

      this.modalRef = this.ngbModal.open(ngbQuote, {
        size: "lg",
      });
    }
  };

  onBeforeStep = (ngbQuote: TemplateRef<any>) => {
    if (!!this.printing.length) {
      this.step = 1;

      this.modalRef = this.ngbModal.open(ngbQuote, {
        size: "lg",
      });
    }
  };

  openUrlLink = (url: string) => {
    this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    this.step = 2;

    this.onCloseNgbModal();
  };
}
