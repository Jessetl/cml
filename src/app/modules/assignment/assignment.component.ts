import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { FormGroup, FormControl } from "@angular/forms";
import { FormBuilder, Validators } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { Store } from "@ngrx/store";

import { Role as Profiles, Assignment, SubModule } from "@models";
import { Access, AssignmentForm, User } from "@models";
import { AssignmentsService } from "@core/service";

import { handlerError, rawError } from "@shared/utils";
import { showMessage, validationsForm } from "@shared/utils";
import { TierId, profilesLang } from "@shared/utils";
import { TYPE_CATCH, TYPE_REPORT } from "@shared/utils";
import { VISIBLE_GLOBAL, VISIBLE_LOCKER } from "@shared/utils";

import * as fromRoot from "@reducers";

const assignmentValidator = {
  role_id: new FormControl("", [Validators.required]),
};

@Component({
  selector: "cml-assignment",
  templateUrl: "./assignment.component.html",
  styles: [],
})
export class AssignmentComponent implements OnInit {
  public isLoading: boolean = true;
  public isEditing: boolean = false;
  public profiles: Profiles[];
  public profile: Profiles;
  public assignment: FormGroup;
  public assignments: Assignment[];
  public access: Access[] = [];
  public submitted: boolean;
  public errors: Array<any>;
  public paginate: number = 1;
  public user: User;
  public active: number = 1;
  public actions: Array<any>;
  public actionsAgency: Array<any>;
  public actionsSeller: Array<any>;
  public actionsLockers: Array<any>;
  public actionsInvoicing: any;
  public actionsStatus: Array<any>;
  public actionsInformation: Array<any>;
  public home: Array<any>;

  public catchesLocker: SubModule[];
  public catches: SubModule[];
  public reports: SubModule[];

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private assignmentsService: AssignmentsService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.assignment = this.formBuilder.group(assignmentValidator);
  }

  ngOnInit() {
    this.assignment.reset({ role_id: "" }, { onlySelf: true });

    this.assignmentsService.getSubmodules().subscribe(
      (submodules: SubModule[]) => {
        const { role } = this.user;

        this.catchesLocker = submodules
          .filter(
            ({ type, module }) =>
              type === TYPE_CATCH && module.visible === VISIBLE_LOCKER
          )
          .map((submodule) => {
            return {
              ...submodule,
              name: this.translate.instant(`submodules.${submodule.name}`),
              nameModule: this.translate.instant(
                `modules.${submodule.module.path}`
              ),
            };
          });

        this.catches = submodules
          .filter(
            ({ type, module }) =>
              type === TYPE_CATCH && module.visible === VISIBLE_GLOBAL
          )
          .map((submodule) => {
            return {
              ...submodule,
              name: this.translate.instant(`submodules.${submodule.name}`),
              nameModule: this.translate.instant(
                `modules.${submodule.module.path}`
              ),
            };
          });

        this.catches =
          this.user.tier !== TierId.TIER_ADMIN
            ? [
                ...this.catches.filter(({ id }) =>
                  role.assignments.some(
                    ({ submodule_id }) => submodule_id === id
                  )
                ),
              ]
            : this.catches;

        this.actionsAgency = submodules
          .filter(({ path }) => path === "agencies")
          .map((submodule) => {
            return {
              ...submodule,
              name: this.translate.instant(`submodules.${submodule.name}`),
              nameModule: this.translate.instant(
                `modules.${submodule.module.path}`
              ),
            };
          });

        this.reports = submodules
          .filter(({ type }) => type === TYPE_REPORT)
          .map((submodule) => {
            return {
              ...submodule,
              name: this.translate.instant(`submodules.${submodule.name}`),
            };
          });

        this.reports =
          this.user.tier !== TierId.TIER_ADMIN
            ? [
                ...this.reports.filter(({ id }) =>
                  role.assignments.some(
                    ({ submodule_id }) => submodule_id === id
                  )
                ),
              ]
            : this.reports;

        this.actions = submodules
          .filter(({ path }) => path === "quote/consult")
          .map((submodule) => {
            return {
              ...submodule,
              name: this.translate.instant(`submodules.${submodule.name}`),
              nameModule: this.translate.instant(
                `modules.${submodule.module.path}`
              ),
            };
          });

        this.actionsInvoicing = submodules
          .filter(({ path }) => path === "invoicing")
          .map((submodule) => {
            return {
              ...submodule,
              name: this.translate.instant(`submodules.pay`),
              nameModule: this.translate.instant(
                `modules.${submodule.module.path}`
              ),
            };
          });

        this.home = submodules
          .filter(({ path, module }) => {
            return (
              path === "users" ||
              (path === "store/pay" && module.visible === 0) ||
              path === "services" ||
              path === "quote/consult"
            );
          })
          .map((submodule) => {
            return {
              ...submodule,
              nameModule: this.nameModuleHome(submodule),
            };
          });

        this.actionsStatus = submodules
          .filter(({ path }) => {
            return (
              path === "users" ||
              path === "agencies" ||
              path === "lockers" ||
              path === "resellers/sellers" ||
              path === "resellers/supervisors" ||
              path === "employees" ||
              path === "invoicing" ||
              path === "productivity/payments"
            );
          })
          .map((submodule) => {
            return {
              ...submodule,
              nameModule: this.nameModuleHome(submodule),
            };
          });

        this.actionsSeller = submodules
          .filter(({ path }) => path === "resellers/sellers")
          .map((submodule) => {
            return {
              ...submodule,
              name: this.translate.instant(`submodules.${submodule.name}`),
              nameModule: this.translate.instant(
                `modules.${submodule.module.path}`
              ),
            };
          });

        this.actionsLockers = submodules
          .filter(({ path }) => path === "lockers")
          .map((submodule) => {
            return {
              ...submodule,
              name: this.translate.instant(`submodules.${submodule.name}`),
              nameModule: this.translate.instant(
                `modules.${submodule.module.path}`
              ),
            };
          });

        this.actionsInformation = submodules
          .filter(({ path }) => path === "lockers")
          .map((submodule) => {
            return {
              ...submodule,
              name: this.translate.instant(`submodules.${submodule.name}`),
              nameModule: this.translate.instant(`modules.setting`),
            };
          });

        console.log(this.actionsInformation);
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.assignmentsService.getAssignments().subscribe(
      (profiles: Profiles[]) => {
        this.profiles = profilesLang(profiles, this.translate.getDefaultLang());
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  isFieldValid = (field: string): boolean => {
    return (
      this.assignment.get(field).invalid && this.assignment.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getCatchByRole = () => {
    const roleId: number = this.assignment.get("role_id").value;
    const profile = this.profiles.find(({ id }) => id === roleId);

    if (!!profile) {
      switch (profile.type) {
        case 0:
          return this.catchesLocker;
        case 1:
          return this.catches;
      }
    }
  };

  showAssignments = () => {
    const roleId: number = this.assignment.get("role_id").value;
    const profile = this.profiles.find(({ id }) => id === roleId);

    if (!!profile) {
      this.assignments = profile.assignments;
      this.profile = profile;

      this.access = !!this.assignments
        ? this.assignments.map(({ submodule_id, type }) => ({
            id: submodule_id,
            type: type,
          }))
        : [];
    }
  };

  setModule = (moduleId: number, typeId: number) => {
    this.access = this.access.find(
      ({ id, type }) => id === moduleId && type === typeId
    )
      ? this.filterAccess(moduleId, typeId)
      : [...this.access, { id: moduleId, type: typeId }];
  };

  filterAccess = (moduleId: number, typeId: number): Access[] => {
    return this.access.filter(
      ({ id, type }) =>
        (!(id !== moduleId) && !!(type !== typeId)) ||
        (!!(id !== moduleId) && !(type !== typeId)) ||
        (!!(id !== moduleId) && !!(type !== typeId))
    );
  };

  isChecked = (moduleId: number, typeId: number) => {
    return !!this.access.find(
      ({ id, type }) => id === moduleId && type === typeId
    );
  };

  hasAccess = (moduleId: number, typeId: number) => {
    if (!!this.user) {
      const { role } = this.user;

      if (this.user.tier === TierId.TIER_ADMIN) return true;

      return !!role.assignments.find(
        ({ submodule_id, type }) => submodule_id === moduleId && type === typeId
      );
    }
  };

  onSubmit = () => {
    const infoValid = this.assignment.valid;

    if (!infoValid) {
      return validationsForm(this.assignment);
    } else if (this.submitted) {
      return;
    }

    this.submitted = true;

    const form: AssignmentForm = {
      profile_id: this.assignment.get("role_id").value,
      access: this.access,
    };

    this.assignmentsService.store(form).subscribe(
      (profile: Profiles) => {
        this.submitted = false;

        let keyProfile = this.profiles.findIndex(({ id }) => id === profile.id);
        let resProfiles = [...this.profiles];

        resProfiles[keyProfile] = {
          ...profile,
        };

        this.profiles = resProfiles;
        this.assignments = profile.assignments;

        this.errors = null;

        showMessage(
          "¡Enhorabuena!",
          this.translate.instant("notifications.createAgency"),
          "success",
          true
        );
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  filterActions = (submodules: any) => {
    let shipment = submodules.find(({ path }) => {
      path === "invoicing";
    });

    let invoicing = submodules.find(({ path }) => path == "shipments");

    return [
      {
        ...shipment,
        name: this.translate.instant(`submodules.${shipment.path}`),
        action: "Ubicacion",
        nameModule: this.translate.instant(`modules.${shipment.module.path}`),
      },
      {
        ...shipment,
        name: this.translate.instant(`submodules.${shipment.path}`),
        action: "Documento",
        nameModule: this.translate.instant(`modules.${shipment.module.path}`),
      },
      {
        ...invoicing,
        name: this.translate.instant(`submodules.${invoicing.path}`),
        action: "Revocar Pago",
        nameModule: this.translate.instant(`modules.${invoicing.module.path}`),
      },
    ];
  };

  nameModuleHome = (submodule: SubModule) => {
    switch (submodule.path) {
      case "users":
        return this.translate.instant(`submodules.${submodule.name}`);
      case "services":
        return this.translate.instant("cardHeader.promotion");
      case "store/pay":
        return this.translate.instant("cardText.storeCML");
      case "quote/consult":
        return this.translate.instant("cardHeader.generated_shipments");
      case "agencies":
        return this.translate.instant(`submodules.${submodule.name}`);
      case "lockers":
        return this.translate.instant(`submodules.${submodule.name}`);
      case "resellers/sellers":
        return this.translate.instant(`submodules.${submodule.name}`);
      case "resellers/supervisors":
        return this.translate.instant(`submodules.${submodule.name}`);
      case "employees":
        return this.translate.instant(`submodules.${submodule.name}`);
      case "invoicing":
        return this.translate.instant("formBuilder.approvePayments");
      case "productivity/payments":
        return this.translate.instant("formBuilder.payProductivity");
    }
  };
}
