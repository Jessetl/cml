import { Component, Input, Output, EventEmitter } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { Quote, Charge } from "@models";
import { QuoteService } from "@core/service";

import { TranslateService } from "@ngx-translate/core";

import {
  rawError,
  handlerError,
  showMessage,
  STATUS_AGENCY,
  STATUS_SHIPMENT,
  STATUS_DESTINY,
  STATUS_TRANSIT,
  STATUS_DELIVERED,
  STATUS_SHIPMENT_CENTER,
  STATUS_CENTER,
} from "@shared/utils";

@Component({
  selector: "cml-progress",
  templateUrl: "./progress.component.html",
  styles: [],
})
export class ProgressComponent {
  @Input() quote: Quote;
  @Input() progress: number[];
  @Input() latitude: string;
  @Input() longitude: string;
  @Output() updateStatusChargeToQuote = new EventEmitter<Quote>();

  public submitted: boolean = false;
  public status: number = null;
  public errors: any;

  constructor(
    private quoteService: QuoteService,
    private translate: TranslateService
  ) {}

  getStatusName = (status: number): string => {
    switch (status) {
      case STATUS_AGENCY:
        return this.translate.instant("timeLine.inAgency");
      case STATUS_SHIPMENT:
        return this.translate.instant("timeLine.inShipment");
      case STATUS_SHIPMENT_CENTER:
        return this.translate.instant("timeLine.inShippingCenter");
      case STATUS_DESTINY:
        return this.translate.instant("timeLine.inTransit");
      case STATUS_CENTER:
        return this.translate.instant("timeLine.inDistribution");
      case STATUS_TRANSIT:
        return this.translate.instant("timeLine.toReceiver");
      case STATUS_DELIVERED:
        return this.translate.instant("timeLine.delivered");
    }
  };

  onSubmit = () => {
    const quoteId = this.quote.id;
    const progress = this.progress.map((number) => number);
    if (!this.longitude && !this.latitude) {
      return showMessage(
        null,
        this.translate.instant("validations.noCoords"),
        "warning",
        true
      );
    }

    if (this.submitted) {
      return;
    }

    this.submitted = true;

    return this.quoteService
      .setStatusCharge({
        quoteId: quoteId,
        progress: progress,
        latitude: this.latitude,
        longitude: this.longitude,
      })
      .subscribe(
        (charge: Charge[]) => {
          const quote = {
            ...this.quote,
            charge: [...charge],
          };

          this.updateStatusChargeToQuote.emit(quote);
          this.submitted = false;
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };
}
