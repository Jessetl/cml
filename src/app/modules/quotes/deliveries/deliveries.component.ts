import { Component, Input, Output, EventEmitter } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { Quote, Charge, User, Package } from "@models";
import { QuoteService } from "@core/service";

import { TranslateService } from "@ngx-translate/core";

import { rawError, handlerError, showMessage } from "@shared/utils";
import { PRINT_DOCUMENT, TierId } from "@shared/utils";
import { getServiceDescriptionLang } from "@shared/utils";
import { PRINT_GUIDE, PRINT_PACKAGE, PRINT_BOARDING } from "@shared/utils";

@Component({
  selector: "cml-deliveries",
  templateUrl: "./deliveries.component.html",
  styles: [],
})
export class DeliveriesComponent {
  @Input() quote: Quote;
  @Input() printing: any[];
  @Input() user: User;
  @Output() openUrlLink = new EventEmitter<string>();

  public submitted: boolean = false;
  public errors: any;

  constructor(
    private quoteService: QuoteService,
    private translate: TranslateService
  ) {}

  getTotalCharges = (charge: Charge): number => {
    const { impost, insurance, nationalization, crossing } = charge;

    return impost + insurance + nationalization + crossing || 0;
  };

  printOutFile = (key: number, charge: Charge) => {
    const chargeId = charge.id;
    const quoteId = this.quote.id;
    const optionId =
      this.printing[key][chargeId] === "null"
        ? null
        : this.printing[key][chargeId];

    if (!optionId || !quoteId || this.submitted) {
      return;
    }

    const quantityCharge = this.quote.charge.length;
    const trueKey = key + 1;
    const namePackage = `(${trueKey}/${quantityCharge})`;

    this.submitted = true;

    switch (parseInt(optionId)) {
      case PRINT_PACKAGE:
        return this.quoteService
          .printPackgingTag({ quoteId, chargeId })
          .subscribe(
            (url: string) => {
              this.openUrlLink.emit(url);
            },
            (error: HttpErrorResponse) => {
              this.handleError(error);
            },
            () => {
              this.submitted = false;
            }
          );
      case PRINT_GUIDE:
        return this.quoteService
          .printInternalTraffic({ quoteId, chargeId, namePackage })
          .subscribe(
            (url: string) => {
              this.openUrlLink.emit(url);
            },
            (error: HttpErrorResponse) => {
              this.handleError(error);
            },
            () => {
              this.submitted = false;
            }
          );

      case PRINT_BOARDING:
        return this.quoteService
          .printBoarding({ quoteId, chargeId, namePackage })
          .subscribe(
            (url: string) => {
              this.openUrlLink.emit(url);
            },
            (error: HttpErrorResponse) => {
              this.handleError(error);
            },
            () => {
              this.submitted = false;
            }
          );
      case PRINT_DOCUMENT:
        this.printing[key][chargeId] = "null";
        this.submitted = false;
        return this.openUrlLink.emit(charge.url_file);
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === "shipments")
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };
  getDescriptionsService = (service: Package) => {
    return getServiceDescriptionLang(service, this.translate.getDefaultLang());
  };
}
