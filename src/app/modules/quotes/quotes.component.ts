import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { HttpErrorResponse } from "@angular/common/http";

import { Quote, User, Charge, Country } from "@models";
import { QuoteService, GooglemapsService } from "@core/service";

import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

import { DeliveriesComponent } from "./deliveries/deliveries.component";
import { ProgressComponent } from "./progress/progress.component";

import { confirm, rawError, handlerError } from "@shared/utils";
import { getOriginNameLang, getCountryNameLang } from "@shared/utils";
import { TierId, showMessage, STATUS_CHARGE_QUOTE } from "@shared/utils";
import { INVOICE_FEE, INVOICE_QUOTE, REAL_TIME } from "@shared/utils";

import { on, unsubscribeOf } from "jetemit";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-quotes",
  templateUrl: "./quotes.component.html",
  styles: [],
})
export class QuotesComponent implements OnInit {
  @ViewChild(DeliveriesComponent)
  Deliveries: DeliveriesComponent;

  @ViewChild(ProgressComponent)
  Progress: ProgressComponent;

  private modalRef: NgbModalRef;
  public submitted: boolean = false;
  public filtered: boolean = false;
  public urlFile: SafeResourceUrl;
  public step: number = 1;
  public user: User;
  public errors: any;
  public quotes: Quote[];
  public backupQuotes: Quote[];
  public quote: Quote;
  public printing: any[];
  public progress: any[];
  public latitude: string;
  public longitude: string;
  public charges: Charge[];
  public isLoading: boolean;

  //Search
  public search = {
    alphanumeric: null,
    cellphone: null,
    identificationId: null,
  };

  constructor(
    private quoteService: QuoteService,
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private store: Store<fromRoot.State>,
    public domSanitizer: DomSanitizer,
    private googleService: GooglemapsService
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    this.googleService
      .getLocation({ timeout: 15000 })
      .then(({ coords }) => {
        this.latitude = coords.latitude.toString();
        this.longitude = coords.longitude.toString();
      })
      .catch((error) => {
        console.log("error al obtener la ubicacion", error);
      });

    on(REAL_TIME.SHIPPING, (_: number) => {
      if (!this.filtered) {
        this.getShipments();
      }
    });

    this.getShipments();
  }

  ngOnDestroy(): void {
    unsubscribeOf(REAL_TIME.SHIPPING);
  }

  getShipments = () => {
    this.isLoading = true;

    this.quoteService.getCharge().subscribe(
      (quotes: Quote[]) => {
        this.isLoading = false;
        this.quotes = this.mapShipments(quotes);
        this.backupQuotes = this.mapShipments(quotes);
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  getOriginName = (shipping: Quote): string => {
    return getOriginNameLang(shipping, this.translate.getDefaultLang());
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  mapShipments = (array: Quote[]): Quote[] => {
    return array.map((quote) => {
      const { user } = quote;

      const agency = user.agency[0];
      const agencyName = !!agency ? agency.person.name : user.person.name;

      let document = quote.charge.find(({ url_file }) => !!url_file);

      return {
        ...quote,
        url: document ? document.url_file : null,
        agencyName: agencyName,
      };
    });
  };

  updateBackupQuote = (quoteKey: number, type: number, quote?: Quote) => {
    switch (type) {
      case INVOICE_FEE:
        this.backupQuotes[quoteKey] = {
          ...this.backupQuotes[quoteKey],
          invoiced: quote.invoiced,
        };
        return;
      case STATUS_CHARGE_QUOTE:
        this.backupQuotes[quoteKey] = { ...quote };
        return;
      case INVOICE_QUOTE:
        this.backupQuotes[quoteKey] = {
          ...this.backupQuotes[quoteKey],
          invoiced: 1,
        };
        return;
    }
  };

  isToggleQuote = (ngbQuote: TemplateRef<any>, keyId: number) => {
    const quote = this.quotes[keyId] || null;

    if (!!quote) {
      this.quote = { ...quote };

      this.printing = [
        ...this.quote.charge.map(({ id }) => ({
          [id]: null,
        })),
      ];

      this.modalRef = this.ngbModal.open(ngbQuote, {
        size: "lg",
      });
    }
  };

  onBeforeStep = (ngbQuote: TemplateRef<any>) => {
    if (!!this.printing.length) {
      this.step = 1;

      this.modalRef = this.ngbModal.open(ngbQuote, {
        size: "lg",
      });
    }
  };

  setInvoiceFee = async (quoteId: number) => {
    const quoteKey = this.quotes.findIndex(({ id }) => id === quoteId);
    const alphanumeric = this.quotes.find(({ id }) => id === quoteId)
      .alphanumeric;

    const wantInvoice = await confirm({
      text: this.translate.instant("ngbAlert.wantInvoice", {
        alphanumeric: alphanumeric,
      }),
      confirmButtonText: this.translate.instant("buttonText.confirmButtonText"),
      cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
    });

    if (this.submitted || !wantInvoice) {
      return;
    }

    this.submitted = true;

    this.quoteService.setInvoiceFee(quoteId).subscribe(
      (quote: Quote) => {
        showMessage(
          null,
          this.translate.instant("notifications.updateInvoiceFee", {
            alphanumeric: quote.alphanumeric,
          }),
          "success",
          true
        );

        this.submitted = false;
        this.updateBackupQuote(quoteKey, INVOICE_FEE, quote);
        return (this.quotes[quoteKey] = {
          ...this.quotes[quoteKey],
          invoiced: quote.invoiced,
        });
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  isToggleProgress = (ngbProgress: TemplateRef<any>, quoteId: number) => {
    const quote = this.quotes.find(({ id }) => id === quoteId);
    this.progress = [];

    if (!!quote) {
      this.quote = { ...quote };

      this.progress = [...this.quote.charge.map(({ status }) => status)];

      this.modalRef = this.ngbModal.open(ngbProgress, {
        size: "md",
      });
    }
  };

  openPayCML = (ngbPayCML: TemplateRef<any>, quote: Quote) => {
    this.quote = quote;

    this.modalRef = this.ngbModal.open(ngbPayCML, {
      size: "lg",
    });
  };

  updateStatusChargeToQuote = (quote: Quote) => {
    const quoteKey = this.quotes.findIndex(({ id }) => id === quote.id);

    if (quoteKey >= 0) {
      this.quotes[quoteKey] = { ...quote };
      this.updateBackupQuote(quoteKey, STATUS_CHARGE_QUOTE, quote);
    }

    showMessage(
      null,
      this.translate.instant("notifications.updateStatusCharge"),
      "success",
      true
    );

    this.onCloseNgbModal();
  };

  openUrlLink = (url: string) => {
    this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    this.step = 2;

    this.onCloseNgbModal();
  };

  onCloseNgbModal = (): void => {
    this.modalRef.close();
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  searchButton = () => {
    let name;

    for (var key in this.search) {
      if (this.search[key] != null) name = key;
    }
    this.onKey(name);
  };

  onKey = (key: string) => {
    let name = key;
    let variable = this.search[key];

    if (!variable) {
      return;
    }

    if (this.isLoading) {
      return;
    }

    this.isLoading = true;
    this.filtered = true;

    this.quoteService.searchQuote({ [name]: variable }).subscribe(
      (quotes: Quote[]) => {
        this.isLoading = false;
        this.quotes = this.mapShipments(quotes);
        if (!name && !variable) this.backupQuotes = this.mapShipments(quotes);
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  clearInput = (name: string) => {
    for (var key in this.search) {
      if (key != name && this.search[key] != null) {
        this.search[key] = null;
      }
    }
    this.quotes = this.backupQuotes;
  };

  clearAll = () => {
    this.filtered = false;

    this.search = {
      alphanumeric: null,
      cellphone: null,
      identificationId: null,
    };

    this.getShipments();
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === "shipments")
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  setIvoiceQuote = (quoteId: number[]) => {
    const quoteKey = this.quotes.findIndex(({ id }) => id === quoteId[0]);

    showMessage(
      null,
      this.translate.instant("notifications.updateQuotes"),
      "success",
      true
    );

    this.quotes[quoteKey] = {
      ...this.quotes[quoteKey],
      invoiced: 1,
    };
    this.updateBackupQuote(quoteKey, INVOICE_QUOTE);
    this.onCloseNgbModal();
  };

  totalByUser = (quote: Quote) => {
    const { user } = quote;
    const tier = !!user.agency[0] ? user.agency[0].tier : user.tier;

    return tier === TierId.TIER_ADMIN ? quote.total : quote.total_user;
  };
}
