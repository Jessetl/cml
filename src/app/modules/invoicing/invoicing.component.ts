import { Component, OnInit } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";

import { TranslateService } from "@ngx-translate/core";

import { Invoicing, User, Payment } from "@models";
import { InvoicingService } from "@core/service";
import { handlerError, TierId, confirm } from "@shared/utils";
import { showMessage, showConfirmButton } from "@shared/utils";
import { STATUS_ACTIVE, TIER_LOCKER, MEMBERSHIP } from "@shared/utils";
import { SHIPPING, PURCHASE, REAL_TIME, EDIT } from "@shared/utils";

import { on, unsubscribeOf } from "jetemit";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-invoicing",
  templateUrl: "./invoicing.component.html",
  styles: [],
})
export class InvoicingComponent implements OnInit {
  public submitted: boolean = false;
  public filtered: boolean = false;
  public isLoading: boolean;
  public user: User;
  public errors: any;
  public invoicing: Invoicing[];
  public backupInvoicing: Invoicing[];
  public urlFile: SafeResourceUrl;
  public step: number = 1;
  public typeSubmit: number;
  public typeInvoicing: any = null;

  //Search
  public search = {
    alphanumeric: null,
    code: null,
  };

  constructor(
    public domSanitizer: DomSanitizer,
    private invoicingService: InvoicingService,
    private translate: TranslateService,
    private store: Store<fromRoot.State>,
    private router: Router
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    this.submitted = true;

    on(REAL_TIME.INVOICE, (_: number) => {
      if (!this.filtered) {
        this.getInvoicing();
      }
    });

    this.getInvoicing();
  }

  ngOnDestroy(): void {
    unsubscribeOf(REAL_TIME.INVOICE);
  }

  shoulderUpdateInvoice = (invoice: Invoicing) => {
    let keyInvoice = this.invoicing.findIndex(({ id }) => id === invoice.id);
    let resInvoice = [...this.invoicing];
    let newInvoicing = this.invoicedChange(invoice);

    resInvoice[keyInvoice] = {
      ...newInvoicing,
    };

    this.invoicing = resInvoice;
    this.updateBackupInvoicing(invoice, EDIT);
  };

  updateBackupInvoicing = (invoice: Invoicing, type: number) => {
    switch (type) {
      case EDIT:
        let keyInvoicing = this.backupInvoicing.findIndex(
          ({ id }) => id === invoice.id
        );
        let resInvoice = [...this.backupInvoicing];
        let newInvoicing = this.invoicedChange(invoice);

        resInvoice[keyInvoicing] = {
          ...newInvoicing,
        };
        return (this.backupInvoicing = resInvoice);
      default:
        this.backupInvoicing;
    }
  };

  getInvoicing = () => {
    this.invoicingService.getInvoicing().subscribe(
      (invoicing: Invoicing[]) => {
        this.submitted = false;

        const mapInvoicing = this.mapInvoicing(invoicing);

        this.invoicing = [...mapInvoicing];
        this.backupInvoicing = [...mapInvoicing];
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  mapInvoicing = (invoicing: Invoicing[]): Invoicing[] => {
    return invoicing.map((invoicing) => {
      const shippingLength = invoicing.quotes.length;
      const packageLength = invoicing.packages.length;

      return {
        ...invoicing,
        quantity: invoicing.type === SHIPPING ? shippingLength : packageLength,
      };
    });
  };

  invoicedChange = (invoicing: Invoicing): Invoicing => {
    const shippingLength = invoicing.quotes.length;
    const packageLength = invoicing.packages.length;

    return {
      ...invoicing,
      quantity: invoicing.type === SHIPPING ? shippingLength : packageLength,
    };
  };

  printInvoicing = (invoiceId: number) => {
    if (!!!this.submitted) {
      this.submitted = true;

      this.invoicingService.printInvoicing(invoiceId).subscribe(
        (url: string) => {
          this.submitted = false;
          this.step = 2;
          this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          showMessage(
            "¡Error!",
            this.translate.instant(`errors${handlerError(error)}`),
            "error",
            true
          );
        }
      );
    }
  };

  getNameAgency = (invoice: Invoicing): string => {
    if (!!this.user) {
      const { user } = invoice;

      return user.tier === TierId.TIER_AGENCY
        ? user.person.name
        : this.user.agencyName;
    }
  };

  toggleActionsByTierId = (): boolean => {
    if (!!this.user) {
      return this.user.tier === TierId.TIER_ADMIN;
    }
  };

  onBeforeStep = (step: number) => {
    this.step = step;
  };

  searchButton = () => {
    let name;

    for (var key in this.search) {
      if (this.search[key] != null) name = key;
    }

    this.onKey(name);
  };

  onKey = (key: string) => {
    let name = key;
    let variable = this.search[key];

    if (!variable) {
      return;
    }

    if (!!!this.submitted) {
      this.submitted = true;
      this.filtered = true;

      this.invoicingService.search({ [name]: variable }).subscribe(
        (invoicing: Invoicing[]) => {
          this.submitted = false;
          this.invoicing = [...invoicing];
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          showMessage(
            "¡Error!",
            this.translate.instant(`errors${handlerError(error)}`),
            "error",
            true
          );
        }
      );
    }
  };

  clearInput = (name: string) => {
    for (var key in this.search) {
      if (key != name && this.search[key] != null) {
        this.search[key] = null;
      }
    }

    if (this.typeInvoicing !== null) {
      this.typeInvoicing = null;
    }

    this.invoicing = this.backupInvoicing;
  };

  clearAll = () => {
    this.search = {
      alphanumeric: null,
      code: null,
    };

    this.typeInvoicing = null;

    if (this.submitted) {
      return;
    }

    this.submitted = true;
    this.filtered = false;

    this.getInvoicing();
  };

  pay = (pay: Payment[]): string => {
    if (pay.length === 0) return "";
    return pay.length > 1
      ? this.translate.instant("formBuilder.various")
      : pay[0].reference;
  };

  revokeInvoice = (id: number) => {
    showConfirmButton(
      this.translate.instant("ngbAlert.revokeInvoicing"),
      this.translate.instant("ngbAlert.btnYes"),
      this.translate.instant("ngbAlert.btnNo"),
      () => {
        if (!!!this.submitted) {
          this.submitted = true;

          this.invoicingService.remove(id).subscribe(
            (invoice: Invoicing) => {
              this.submitted = false;
              let invoicingFilter = this.invoicing.filter(({ id }) => {
                return id != invoice.id;
              });
              this.invoicing = invoicingFilter;
              this.backupInvoicing = invoicingFilter;
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              showMessage(
                "¡Error!",
                this.translate.instant(`errors${handlerError(error)}`),
                "error",
                true
              );
            }
          );
        }
      }
    );
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === "invoicing")
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  nameAgency = (user: User) => {
    const agency = user.agency[0];
    return !!!agency || user.tier === TIER_LOCKER
      ? user.person.name
      : agency.person.name;
  };

  goToPayments = (key: number) => {
    let invoicing = this.invoicing[key];
    return this.router.navigate([`/dashboard/payments/${invoicing.id}`]);
  };

  changeTypeInvoicing = () => {
    this.search = {
      alphanumeric: null,
      code: null,
    };
    let invoicings = [...this.backupInvoicing];
    switch (this.typeInvoicing) {
      case SHIPPING:
        return (this.invoicing = invoicings.filter(
          ({ type }) => type === SHIPPING
        ));
      case PURCHASE:
        return (this.invoicing = invoicings.filter(
          ({ type }) => type === PURCHASE
        ));
      case MEMBERSHIP:
        return (this.invoicing = invoicings.filter(
          ({ type }) => type === MEMBERSHIP
        ));

      default:
        return (this.invoicing = this.backupInvoicing);
    }
  };

  statusInvoice = async (invoice: Invoicing) => {
    if (invoice.status === STATUS_ACTIVE) return;
    const wantStatus = await confirm({
      text: this.translate.instant("ngbAlert.confirmPayInvoice", {
        alphanumeric: invoice.alphanumeric,
      }),
      confirmButtonText: this.translate.instant("buttonText.confirmButtonText"),
      cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
    });

    if (wantStatus) {
      this.invoicingService.status(invoice.id).subscribe(
        (invoice: Invoicing) => {
          this.shoulderUpdateInvoice(invoice);
          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.payConfirm"),
            "success",
            true
          );
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          showMessage(
            "¡Error!",
            this.translate.instant(`errors${handlerError(error)}`),
            "error",
            true
          );
        }
      );
    }
  };

  getPriceByTypeUser = (invoice: Invoicing): number => {
    return invoice.locker ? invoice.cost : invoice.cost_user;
  };

  getTotalByTypeUser = (invoice: Invoicing): number => {
    return invoice.locker ? invoice.total : invoice.total_user;
  };
}
