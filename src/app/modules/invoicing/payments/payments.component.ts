import { Component, OnInit, TemplateRef } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { HttpErrorResponse } from "@angular/common/http";
import { Router, ActivatedRoute } from "@angular/router";

import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

import { Payment } from "@models";
import { InvoicingService } from "@core/service";
import { handlerError, showMessage, typePayments } from "@shared/utils";
import * as moment from "moment";

@Component({
  selector: "cml-payments",
  templateUrl: "./payments.component.html",
  styles: [],
})
export class PaymentsComponent implements OnInit {
  private readonly invoicingId: string;

  public isLoading: boolean = true;
  private modalRef: NgbModalRef;

  public errors: any;
  public urlFile: SafeResourceUrl;
  public payments: Payment[];
  public imageSrc: string;
  public step: number = 1;

  constructor(
    private invoicingService: InvoicingService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
    public domSanitizer: DomSanitizer,
    private ngbModal: NgbModal
  ) {
    this.invoicingId = this.route.snapshot.paramMap.get("id");
  }

  ngOnInit(): void {
    this.invoicingService.payments(parseInt(this.invoicingId)).subscribe(
      (payments: Payment[]) => {
        this.isLoading = false;
        this.payments = payments;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  goToInvoicing = () => {
    return this.router.navigate(["/dashboard/invoicing"]);
  };

  getDate = (date: string) => {
    return moment(date).format("MM/DD/YYYY").toString();
  };

  typePayments = (type: number) => {
    return typePayments(type, this.translate);
  };

  getFileExtension = (filename) => {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  };

  openModal = async (ngbModal: TemplateRef<any>, image: string) => {
    this.imageSrc = image;
    this.modalRef = this.ngbModal.open(ngbModal, {
      size: "md",
    });
  };

  onCloseNgbModal = (): void => {
    this.imageSrc = null;
    this.modalRef.close();
  };

  viewDocument = (payment: Payment, nbg: TemplateRef<any>) => {
    if (payment.voucher) {
      if (this.getFileExtension(payment.voucher) === "pdf") {
        this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(
          payment.file_url
        );
        this.step = 2;
      } else {
        this.openModal(nbg, payment.file_url);
      }
    } else {
      return showMessage(
        "",
        this.translate.instant("validators.noFile"),
        "warning",
        true
      );
    }
  };
}
