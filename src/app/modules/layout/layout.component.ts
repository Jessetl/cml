import { Component, OnInit, OnDestroy, TemplateRef } from "@angular/core";
import { Router } from "@angular/router";

import { TranslateService } from "@ngx-translate/core";

import { Message, Chat, User } from "@models";
import { LayoutService } from "@core/service";
import { CHAT, TierId } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as menu from "@actions";

import { on, emit, unsubscribeOf } from "jetemit";

@Component({
  selector: "cml-layout",
  templateUrl: "./layout.component.html",
  styles: [],
})
export class LayoutComponent implements OnInit, OnDestroy {
  public isLoading: boolean = true;
  public submitted: boolean = false;
  public messages: number = 0;
  public toggled: boolean;
  public errors: Array<any>;

  public user: User;

  constructor(
    private translate: TranslateService,
    private store: Store<fromRoot.State>,
    private router: Router,
    private layoutService: LayoutService
  ) {
    this.store
      .select(fromRoot.getToggled)
      .subscribe((isToggled: boolean) => (this.toggled = isToggled));

    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    const strLanguage = localStorage.getItem("language");

    this.translate.setDefaultLang(strLanguage);
    this.translate.use(strLanguage);

    on(CHAT.RECEIVE_MESSAGE, (message: Message) => {
      if (message.receiver_id === this.user.id) {
        this.numberOfMessages();
      }
    });

    on(CHAT.NEW_CHAT, (data: Chat) => {
      if (
        data.receiver_id === this.user.id ||
        data.transmitter_id === this.user.id
      ) {
        this.numberOfMessages();
      }
    });

    on(CHAT.UNREAD_MESSAGE, (data) => {
      this.messages = data;
    });
  }

  toggleSidebar() {
    this.store.dispatch(new menu.ToggleMenuAction(!this.toggled));
  }

  getSideBarState(): boolean {
    return this.toggled;
  }

  ngOnDestroy(): void {
    unsubscribeOf(CHAT.RECEIVE_MESSAGE);
    unsubscribeOf(CHAT.NEW_CHAT);
    unsubscribeOf(CHAT.UNREAD_MESSAGE);
  }

  goToChats = () => {
    emit(CHAT.TO_CHAT);
    this.messages = 0;

    if (this.router.url === "/dashboard/chat") {
      const redirect = this.layoutService
        .getPreviousUrl()
        .replace(";home=true", "");

      const forceRedirect =
        redirect === "/dashboard/chat" ? "/dashboard" : redirect;

      return this.router.navigate([forceRedirect]);
    }

    return this.router.navigate(["/dashboard/chat"]);
  };

  getIconByUrl = (): boolean => {
    return this.router.url === "/dashboard/chat";
  };

  numberOfMessages = () => {
    if (this.router.url !== "/dashboard/chat") {
      this.messages = this.messages + 1;
    }
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        !!this.user.role.assignments && this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === "chat")
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  toggleButtonByTypeId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        !!this.user.role.assignments && this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter(
                (assignment) =>
                  assignment.submodule.path === "agencies" ||
                  assignment.submodule.path === "lockers" ||
                  assignment.submodule.path === "/resellers/supervisors" ||
                  assignment.submodule.path === "/resellers/sellers"
              )
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };
}
