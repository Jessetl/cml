import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { FormGroup, FormBuilder } from "@angular/forms";
import { FormControl, Validators } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { Country, RestCountry, Content, User } from "@models";
import { CountryService, GooglemapsService } from "@core/service";
import { ModalComponent } from "@shared/components";
import { TypeContent, getCountryNameLang } from "@shared/utils";

import { rawError, handlerError, validationsForm } from "@shared/utils";
import { TYPE_COMERCIAL, TYPE_MIXED, COUNTRY_NAME } from "@shared/utils";
import { TYPE_REGULAR, TYPE_ELECTRONIC, COUNTRY_OBJECT } from "@shared/utils";
import { showMessage, showConfirmButton, TierId } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

const countryValidator = {
  id: new FormControl("", []),
  name: new FormControl("", [Validators.required, Validators.maxLength(30)]),
  name_en: new FormControl("", [Validators.required, Validators.maxLength(30)]),
  user_settings: new FormControl(1, [Validators.required]),
  alpha2Code: new FormControl(null, [Validators.required]),
  type_impost: new FormControl(null, [Validators.required]),
  impost: new FormControl("", [
    Validators.required,
    Validators.maxLength(7),
    Validators.min(0),
  ]),
  type_insurance: new FormControl(null, [Validators.required]),
  insurance: new FormControl("", [
    Validators.required,
    Validators.maxLength(7),
    Validators.min(0),
  ]),
  type_nationalization: new FormControl(null, [Validators.required]),
  nationalization: new FormControl("", [
    Validators.required,
    Validators.maxLength(7),
    Validators.min(0),
  ]),
  type_crossing: new FormControl(null, [Validators.required]),
  crossing: new FormControl("", [
    Validators.required,
    Validators.maxLength(7),
    Validators.min(0),
  ]),
  type_content: new FormControl(null, [Validators.required]),
  content: new FormControl(null, []),
  type_taxation: new FormControl(null, [Validators.required]),
  taxation: new FormControl(null, [Validators.required]),
  type_extra: new FormControl(null, [Validators.required]),
  extra: new FormControl(null, [Validators.required]),
  membership_agency: new FormControl(null, [Validators.required]),
  membership_corporate: new FormControl(null, [Validators.required]),
};

const initState = {
  type_impost: null,
  impost: null,
  type_crossing: null,
  crossing: null,
  type_insurance: null,
  insurance: null,
  type_nationalization: null,
  nationalization: null,
};

@Component({
  selector: "cml-country",
  templateUrl: "./country.component.html",
  styles: [],
})
export class CountryComponent implements OnInit {
  @ViewChild(ModalComponent) modal: ModalComponent;

  private modalRef: NgbModalRef;
  public isLoading: boolean = true;
  public isEditing: boolean = false;
  public countries: Country[];
  public restCountry: RestCountry[];
  public user: User;
  public country: FormGroup;
  public submitted: boolean;
  public errors: Array<any>;
  public paginate: number = 1;
  public country_select: number;
  public content_country: number = 1;
  public countryContent: any = {};

  constructor(
    private ngbModal: NgbModal,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private countryService: CountryService,
    private googleMaps: GooglemapsService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.country = this.formBuilder.group(countryValidator);
  }

  ngOnInit() {
    this.isLoading = true;

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.isLoading = false;
        this.countries = countries;

        const country = countries.find(({ alpha2Code }) => {
          return alpha2Code === "MX";
        });

        if (!!country) {
          this.country_select = country.id;
          this.countryContent = country;
        }
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.googleMaps.getCountries().subscribe(
      (countries: RestCountry[]) => {
        this.restCountry = countries.sort((a, b) => (a.name > b.name ? 1 : -1));
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  chooseContentsCountry = () => {
    switch (this.content_country) {
      case TYPE_REGULAR:
        this.changeType(this.countries, TYPE_REGULAR);
        break;
      case TYPE_ELECTRONIC:
        this.changeType(this.countries, TYPE_ELECTRONIC);
        break;
      case TYPE_MIXED:
        this.changeType(this.countries, TYPE_MIXED);
        break;
      case TYPE_COMERCIAL:
        this.changeType(this.countries, TYPE_COMERCIAL);
        break;
    }
  };

  changeType = (countries: Country[], TYPE_CONTENT: number) => {
    countries.map((country) => {
      let regular = country.content.find(({ type }) => type === TYPE_CONTENT);
      country.impost = 0;
      country.nationalization = 0;
      country.insurance = 0;
      country.crossing = 0;

      if (regular) {
        country.impost = regular.impost;
        country.type_impost = regular.type_impost === 1 ? true : false;
        country.nationalization = regular.nationalization;
        country.type_nationalization =
          regular.type_nationalization === 1 ? true : false;
        country.insurance = regular.insurance;
        country.type_insurance = regular.type_insurance === 1 ? true : false;
        country.crossing = regular.crossing;
        country.type_crossing = regular.type_crossing === 1 ? true : false;
      }
    });
  };

  changeContry = () => {
    let country = this.countries.find(({ id }) => {
      return id == this.country_select;
    });
    return (this.countryContent = country);
  };

  getNamePackage = (type: number): string => {
    return this.translate.instant(TypeContent.getDisplayName(type));
  };

  chooseTypeContent = () => {
    return this.country.get("type_content").value;
  };

  chooseContent = () => {
    const typeContent = this.country.get("type_content").value;
    const content = [...this.country.get("content").value];
    const type: Content = content.find(({ type }) => type === typeContent);

    this.country.patchValue(
      {
        ...initState,
      },
      { onlySelf: true }
    );

    if (type) {
      this.country.patchValue(
        {
          type_impost: type.type_impost,
          impost: type.impost,
          type_crossing: type.type_crossing,
          crossing: type.crossing,
          type_insurance: type.type_insurance,
          insurance: type.insurance,
          type_nationalization: type.type_nationalization,
          nationalization: type.nationalization,
        },
        { onlySelf: true }
      );
    }
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === "country")
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  shouldToggleCreate = (ngbCountry: TemplateRef<any>) => {
    this.isEditing = false;
    this.errors = null;

    this.country.reset();
    this.country.get("membership_agency").enable({ onlySelf: true });
    this.country.get("membership_corporate").enable({ onlySelf: true });
    this.country.patchValue({ status: 1 }, { onlySelf: true });

    this.modalRef = this.ngbModal.open(ngbCountry);
  };

  shouldToggleUpdate = (ngbCountry: TemplateRef<any>, countryId: number) => {
    this.isEditing = true;
    this.country.get("membership_corporate").disable({ onlySelf: true });
    this.country.get("membership_agency").disable({ onlySelf: true });
    const country: Country = this.countries.find(({ id }) => id === countryId);

    this.country.patchValue({
      ...country,
      type_impost: null,
      impost: null,
      type_crossing: null,
      crossing: null,
      type_insurance: null,
      insurance: null,
      type_nationalization: null,
      nationalization: null,
    });
    this.modalRef = this.ngbModal.open(ngbCountry);
  };

  displayCountry = (ngbDisplayCountry: TemplateRef<any>) => {
    this.modalRef = this.ngbModal.open(ngbDisplayCountry);
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
  };

  isFieldValid = (field: string): boolean => {
    return this.country.get(field).invalid && this.country.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  toggleButtonByTierAdmin = (): boolean => {
    if (!!this.user) {
      return this.user.tier === TierId.TIER_ADMIN;
    }
  };

  shouldToggleStatus = (countryId: number, status: number) => {
    showConfirmButton(
      this.translate.instant(
        status === 1 ? "ngbAlert.statusCountry" : "ngbAlert.actvCountry"
      ),
      this.translate.instant("ngbAlert.btnYes"),
      this.translate.instant("ngbAlert.btnNo"),
      () => {
        this.countryService.status({ id: countryId }).subscribe(
          (country: Country) => {
            this.submitted = false;

            this.shoulderUpdateCountry(country);

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.updateCountry"),
              "success",
              true
            );

            this.countryContent = country;

            this.country.reset();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
      }
    );
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  shouldMakeCountry = (data: Country) => {
    this.changeType(this.countries, this.content_country);
    this.countries = [...this.countries, data];
  };

  shoulderUpdateCountry = (data: Country) => {
    let keyCountry = this.countries.findIndex(({ id }) => id === data.id);
    let resCountry = [...this.countries];

    resCountry[keyCountry] = {
      ...data,
    };

    this.changeType(resCountry, this.content_country);
    this.countries = resCountry;
  };

  shoulderDestroyCountry = (countryId: number) => {
    this.countries = this.countries.filter(({ id }) => id !== countryId);
  };

  onSubmit = () => {
    const infoValid = this.country.valid;
    const isEditing = this.isEditing;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.country);
    }

    this.submitted = true;

    switch (isEditing) {
      case true:
        return this.countryService
          .update({ ...this.country.getRawValue() })
          .subscribe(
            (country: Country) => {
              this.submitted = false;

              this.shoulderUpdateCountry(country);

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.updateCountry"),
                "success",
                true
              );

              this.country.reset();
              this.countryContent = country;

              this.modalRef.close();
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
      case false:
        return this.countryService.store({ ...this.country.value }).subscribe(
          (country: Country) => {
            this.submitted = false;

            this.shouldMakeCountry(country);

            this.country.reset();

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.createCountry"),
              "success",
              true
            );

            this.modalRef.close();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  getCountry = (type: number): string | any => {
    let country = this.countries.find(
      ({ id }) => id === this.countryContent.id
    );
    switch (type) {
      case COUNTRY_NAME:
        return !!country
          ? country.name +
              " (" +
              this.translate.instant("cardText.lockers") +
              ")"
          : "";
      case COUNTRY_OBJECT:
        return !!country ? country : {};
    }
  };
}
