import { FormControl, Validators } from "@angular/forms";

export const AddressFormValidator = {
  address: new FormControl({ value: null, disabled: false }, [
    Validators.required,
  ]),
  zip_code: new FormControl({ value: null, disabled: false }, [
    Validators.required,
    Validators.maxLength(10),
  ]),
  country_id: new FormControl({ value: null, disabled: true }, [
    Validators.required,
  ]),
  state: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.maxLength(60),
  ]),
  city: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.maxLength(60),
  ]),
  policy: new FormControl({ value: null, disabled: false }, [
    Validators.required,
  ]),
};
