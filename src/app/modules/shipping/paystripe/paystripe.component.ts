import { Component, OnInit, ViewChild } from "@angular/core";
import { Input, Output, EventEmitter } from "@angular/core";

import { StripeService, StripeCardComponent } from "ngx-stripe";
import { StripeElementsOptions } from "@stripe/stripe-js";
import { StripeCardElementOptions } from "@stripe/stripe-js";
import { Token } from "@stripe/stripe-js";

import { TranslateService } from "@ngx-translate/core";
import { Quote } from "@models";

@Component({
  selector: "cml-paystripe",
  templateUrl: "./paystripe.component.html",
  styles: [],
})
export class PaystripeComponent implements OnInit {
  @ViewChild(StripeCardComponent)
  card: StripeCardComponent;

  @Input() shipping: Quote;
  @Output() onSubmitPayment = new EventEmitter<Token>();

  public submitted: boolean = false;
  public error: string;

  public settingsCard: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: "#666ee8",
        color: "#000",
        lineHeight: "40px",
        fontWeight: "300",
        fontSize: "20px",
      },
    },
  };

  public settingsElements: StripeElementsOptions = {
    locale: localStorage.getItem("language") === "es" ? "es" : "en",
  };

  constructor(
    private stripeService: StripeService
  ) // private translate: TranslateService
  {}

  ngOnInit() {}

  handlePayment = () => {
    const name = this.shipping.receiver.person.name;

    if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.stripeService.createToken(this.card.getCard(), { name }).subscribe(
      (response) => {
        this.onSubmitPayment.emit(response.token);
      },
      (error: Error) => {
        this.submitted = false;
        this.error = error.message;
      }
    );
  };
}
