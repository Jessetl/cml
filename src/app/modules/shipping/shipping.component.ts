import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { FormGroup, FormBuilder } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";

import { Quote, Country } from "@models";

import { TranslateService } from "@ngx-translate/core";
import { SignaturePad } from "angular2-signaturepad";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

import { showMessage } from "@shared/utils";
import { confirm, handlerError, rawError } from "@shared/utils";
import { countriesLang } from "@shared/utils";
import { getCountryNameLang, getOriginNameLang } from "@shared/utils";

import { LockerService, CountryService } from "@core/service";
import { GooglemapsService } from "@core/service";

import { StripeService, StripeCardComponent } from "ngx-stripe";
import { StripeElementsOptions } from "@stripe/stripe-js";
import { StripeCardElementOptions } from "@stripe/stripe-js";
import { Token, StripeElements } from "@stripe/stripe-js";

import { AddressFormValidator } from "./FormAddressValidator";
import { environment } from "environments/environment";

const language = localStorage.getItem("language") === "es" ? "es" : "en";

@Component({
  selector: "cml-shipping",
  templateUrl: "./shipping.component.html",
  styles: [],
})
export class ShippingComponent implements OnInit {
  @ViewChild(SignaturePad)
  signaturePad: SignaturePad;

  @ViewChild(StripeCardComponent)
  card: StripeCardComponent;

  private modalRef: NgbModalRef;

  public loading: boolean;
  public submitted: boolean = false;
  public searching: boolean = false;
  public form: boolean = false;
  public quotes: Quote[];
  public backupQuotes: Quote[];
  public countries: Country[];
  public shipping: Quote;
  public address: FormGroup;
  public signature: string = null;
  public signatureUrl: string = null;
  public elements: StripeElements;
  public token: Token;
  public latitude: string;
  public longitude: string;

  public urlFile: SafeResourceUrl;
  public step: number = 1;

  //search
  public alphanumeric: string;

  public shouldEnableStateAndCity: boolean = false;
  public errors: Array<any>;
  public error: string;
  public errorZipCode: boolean;

  public signatureConfig: Object = {
    minWidth: 2,
    canvasHeight: 150,
  };

  public settingsCard: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: "#666ee8",
        color: "#000",
        lineHeight: "40px",
        fontWeight: "300",
        fontSize: "20px",
      },
    },
  };

  public settingsElements: StripeElementsOptions = {
    locale: language,
  };

  constructor(
    public domSanitizer: DomSanitizer,
    private lockerService: LockerService,
    private formBuilder: FormBuilder,
    private ngbModal: NgbModal,
    private countryService: CountryService,
    private translate: TranslateService,
    private googleService: GooglemapsService
  ) {
    this.address = this.formBuilder.group(AddressFormValidator);
  }

  ngOnInit() {
    this.loading = true;

    this.getQuotesLoker();

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
    this.googleService
      .getLocation({ timeout: 15000 })
      .then(({ coords }) => {
        this.latitude = coords.latitude.toString();
        this.longitude = coords.longitude.toString();
      })
      .catch((error) => {
        console.log("error al obtener la ubicacion", error);
      });
  }

  getQuotesLoker = () => {
    this.lockerService.getQuotes().subscribe(
      (quotes: Quote[]) => {
        this.loading = false;
        this.quotes = quotes;
        this.backupQuotes = quotes;
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  ngAfterViewInit() {
    if (!!this.signaturePad) {
      this.signaturePad.set("width", "100%"); // set szimek/signature_pad options at runtime
    }
  }

  isFieldValid = (field: string): boolean => {
    return this.address.get(field).invalid && this.address.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getOriginName = (shipping: Quote): string => {
    return getOriginNameLang(shipping, this.translate.getDefaultLang());
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  onToggleForm = (shipping: Quote) => {
    this.shipping = shipping;
    this.form = true;
    this.address.patchValue({ ...this.shipping.receiver.person });
  };

  onDrawClear = (): void => {
    this.signatureUrl = null;
    this.signaturePad.clear();
  };

  onDrawComplete = (): void => {
    this.signatureUrl = this.signaturePad.toDataURL("image/png", 0.5);
  };

  onDefaultValuesStateAndCountry = () => {
    this.address.get("state").reset(null, { onlySelf: true });
    this.address.get("city").reset(null, { onlySelf: true });
  };

  shouldStateAndCity = () => {
    this.shouldEnableStateAndCity = !this.shouldEnableStateAndCity;

    if (this.shouldEnableStateAndCity) {
      this.address.get("state").enable({ onlySelf: true });
      this.address.get("city").enable({ onlySelf: true });
    } else {
      this.address.get("state").disable({ onlySelf: true });
      this.address.get("city").disable({ onlySelf: true });
    }
  };

  getLinkToPolicy = () => {
    window.open(environment.apiPublic + "policy.pdf");
  };

  onBlurZipCode = (nameField: string) => {
    const zipCode = this.address.get(nameField).value;
    const countryId = this.shipping.country_id;
    const countryAlphacode = this.countries.find(({ id }) => id === countryId);

    this.errorZipCode = false;

    if (!zipCode) {
      return;
    }

    this.onDefaultValuesStateAndCountry();
    this.searching = true;
    this.errorZipCode = false;

    this.googleService
      .getZipCode({
        postalCode: zipCode.toString(),
        country: !!countryAlphacode ? countryAlphacode.alpha2Code : "US",
      })
      .subscribe(
        async (res: any) => {
          const response = await this.googleService.getStateAndCity(res);

          this.address.patchValue(
            {
              state: response.state,
              city: response.city,
            },
            { onlySelf: true, emitEvent: false }
          );

          this.searching = false;
        },
        async () => {
          if (!this.shouldEnableStateAndCity) {
            this.searching = false;
            this.errorZipCode = true;

            const wantWrite = await confirm({
              text: this.translate.instant("ngbAlert.watWriteZipCode"),
              confirmButtonText: this.translate.instant(
                "buttonText.confirmButtonText"
              ),
              cancelButtonText: this.translate.instant(
                "buttonText.cancelButtonText"
              ),
            });

            if (wantWrite) {
              this.address.get("state").enable({ onlySelf: true });
              this.address.get("city").enable({ onlySelf: true });
            }
          }
        }
      );
  };

  getPaymentInvoice = (invoiceId: number) => {
    if (!!!this.submitted) {
      this.submitted = true;

      this.lockerService.getPaymentInvoice(invoiceId).subscribe(
        (url: string) => {
          this.submitted = false;

          this.step = 2;
          this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;

          if (typeof error.error === "string") {
            return showMessage("¡Error!", error.error, "error", true);
          }

          this.handleError(error);
        }
      );
    }
  };

  paymentShipping = (ngbPayment: TemplateRef<any>, quote: Quote) => {
    this.shipping = quote;

    this.modalRef = this.ngbModal.open(ngbPayment, {
      size: "lg",
    });
  };

  onCloseNgbModal = (): void => {
    this.modalRef.close();
  };

  onSubmitPayment = (response: Token) => {
    this.lockerService
      .payShipping({
        amount: parseFloat((this.shipping.total * 100).toFixed(2)),
        token: response.id,
        quoteId: this.shipping.id,
        latitude: this.latitude,
        longitude: this.longitude,
      })
      .subscribe(
        (quote: Quote) => {
          this.submitted = false;

          let copyQuotes = [...this.quotes];

          const key = this.quotes.findIndex(({ id }) => id === quote.id);

          copyQuotes[key] = {
            ...copyQuotes[key],
            invoiced: quote.invoiced,
          };

          this.quotes = [...copyQuotes];
          this.onCloseNgbModal();

          return showMessage(
            null,
            this.translate.instant("notifications.quoteInvoiced"),
            "success",
            true
          );
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.errors = error.error.message;
        }
      );
  };

  onSubmit = async () => {
    const signature = this.signatureUrl;

    if (!!!this.submitted) {
      const wantConfirm = await confirm({
        text: this.translate.instant("ngbAlert.confirmAddress"),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmButtonText"
        ),
        cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
      });

      if (!wantConfirm) {
        return;
      }

      if (!signature || !this.address.get("policy").value) {
        return showMessage(
          null,
          this.translate.instant("validations.policy"),
          "warning",
          true
        );
      }

      this.submitted = true;

      this.lockerService
        .shippingAddresss(
          {
            ...this.address.getRawValue(),
            signature: this.signatureUrl,
          },
          this.shipping.id
        )
        .subscribe(
          (quote: Quote) => {
            this.submitted = false;
            this.form = false;

            let copyQuotes = [...this.quotes];
            const key = this.quotes.findIndex(({ id }) => id === quote.id);

            copyQuotes[key] = quote;
            this.quotes = [...copyQuotes];

            return showMessage(
              null,
              this.translate.instant("notifications.updateAddress"),
              "success",
              true
            );
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;

            if (typeof error.error === "string") {
              return showMessage("¡Error!", error.error, "error", true);
            }

            this.handleError(error);
          }
        );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);
    this.errors = rawError(errorResponse);
  };

  onKey = () => {
    if (!this.alphanumeric) return;
    this.loading = true;

    this.lockerService.search({ alphanumeric: this.alphanumeric }).subscribe(
      (quotes: Quote[]) => {
        this.loading = false;
        this.quotes = quotes;
      },
      (error: HttpErrorResponse) => {
        this.loading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  clearAll = () => {
    this.alphanumeric = null;
    this.getQuotesLoker();
  };
}
