import { Component, OnInit, TemplateRef, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { User } from "@models";
import { UserService } from "@core/service";
import { handlerError, TIER_AGENCY, TierId } from "@shared/utils";
import { showMessage, formatingPhone, REAL_TIME } from "@shared/utils";
import { on, unsubscribeOf } from "jetemit";

@Component({
  selector: "cml-employees",
  templateUrl: "./employees.component.html",
  styles: [],
})
export class EmployeesComponent implements OnInit, OnDestroy {
  private modalRef: NgbModalRef;
  public readonly home: string;

  public isLoading: boolean = true;
  public backupUsers: User[];
  public users: User[];
  public displayEmployee: User;
  public paginate: number = 1;
  public filtered: boolean = false;

  public errors: Array<any>;

  public search = {
    name: null,
    email: null,
    phone: null,
    cellphone: null,
    id: null,
  };

  constructor(
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private userService: UserService,
    private route: ActivatedRoute
  ) {
    this.home = this.route.snapshot.paramMap.get("home");
  }

  ngOnInit() {
    this.getUsers();

    on(REAL_TIME.NEW_USER, (data) => {
      if (!this.filtered && data.tier !== TIER_AGENCY) {
        this.getUsers();
      }
    });
    on(REAL_TIME.NEW_EMPLOYEE, (data) => {
      if (!this.filtered) {
        this.getUsers();
      }
    });
  }
  ngOnDestroy(): void {
    unsubscribeOf(REAL_TIME.NEW_USER);
    unsubscribeOf(REAL_TIME.NEW_EMPLOYEE);
  }

  getUsers = () => {
    this.isLoading = true;

    this.userService.getEmployess().subscribe(
      (users: User[]) => {
        this.isLoading = false;

        const rawUsers = users.map((user: User) => {
          return {
            ...user,
            agency: user.agency[0] || null,
            agencyName: !!user.agency[0] ? user.agency[0].person.name : "",
          };
        });

        this.users = rawUsers;
        this.backupUsers = rawUsers;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  getAgencyName = (user: User): string => {
    const { agencyName, person, tier } = user;
    return tier === TierId.TIER_AGENCY ? person.name : agencyName;
  };

  formating = (phone: any) => {
    return phone ? formatingPhone(phone) : "";
  };

  clearInput = (name: string) => {
    for (var key in this.search) {
      if (key != name && this.search[key] != null) {
        this.search[key] = null;
      }
    }
    this.users = this.backupUsers;
  };

  searchButton = () => {
    let name;

    for (var key in this.search) {
      if (this.search[key] != null) name = key;
    }

    this.onKey(name);
  };

  clear = () => {
    this.emptySearch();
    this.getUsers();
    this.filtered = false;
  };

  onKey = (key: string) => {
    let name = key;
    let variable = this.search[key];

    if (!variable) {
      return;
    }

    this.filtered = true;

    if (this.isLoading) {
      return;
    }

    this.isLoading = true;

    this.userService
      .searchEmployees({ [name]: variable }, this.paginate)
      .subscribe(
        (users: User[]) => {
          this.isLoading = false;
          this.users = users;
          if (!name && !variable) this.backupUsers = users;
        },
        (error: HttpErrorResponse) => {
          this.isLoading = false;
          this.emptySearch();
          showMessage(
            "¡Error!",
            this.translate.instant(`errors${handlerError(error)}`),
            "error",
            true
          );
        }
      );
  };

  emptySearch = () => {
    this.search = {
      name: null,
      email: null,
      phone: null,
      cellphone: null,
      id: null,
    };
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
    this.displayEmployee = null;
  };

  isToggleEmployee = (ngbDisplayEmployee: TemplateRef<any>, key: number) => {
    this.displayEmployee = this.users[key];

    if (!!this.displayEmployee) {
      this.modalRef = this.ngbModal.open(ngbDisplayEmployee);
    }
  };
}
