import { Component, Input } from "@angular/core";

import { Quote, Charge } from "@models";

@Component({
  selector: "cml-timeline",
  templateUrl: "./timeline.component.html",
  styles: [],
})
export class TimelineComponent {
  @Input() tracking: Quote;
  @Input() locationVisibility: boolean;
  @Input() tier: number;

  public charge: Charge;
  public inAgency: any;
  public inTransit: any;
  public inBoarding: any;
  public inTransitDestination: any;
  public inDistribution: any;
  public inTransitRecipient: any;
  public inDelivered: any;

  constructor() {}

  setCharge = (packageId: number) => {
    const charge = this.tracking.charge.find(({ id }) => id === packageId);
    this.charge = charge;
  };

  setNullCharge = () => {
    this.charge = null;
  };

  getInfo = (numStatus: number): string => {
    const status = this.charge.location.find(
      ({ status }) => status == numStatus
    );

    if (!status) return " ";

    return this.locationVisibility
      ? "desde el " +
          `<b>${status.es_date}</b>` +
          " a las " +
          `<b>${status.es_hour}</b>` +
          " Actualizado por " +
          `<b>${status.user.person.name}</b>`
      : "desde el " +
          `<b>${status.es_date}</b>` +
          " a las " +
          `<b>${status.es_hour}</b>`;
  };
}
