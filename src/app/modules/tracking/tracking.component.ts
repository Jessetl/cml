import { Component, TemplateRef, ViewChild } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { Quote, User, Country } from "@models";
import { QuoteService } from "@core/service";
import { TimelineComponent } from "./timeline/timeline.component";
import { TierId } from "@shared/utils";
import { handlerError, showMessage } from "@shared/utils";
import { getOriginNameLang, getCountryNameLang } from "@shared/utils";

import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-tracking",
  templateUrl: "./tracking.component.html",
  styles: [],
})
export class TrackingComponent {
  @ViewChild(TimelineComponent)
  Timeline: TimelineComponent;

  private modalRef: NgbModalRef;
  public loading: boolean = false;
  public tracking: Quote[];
  public tracing: Quote;
  public user: User;
  public locationVisibility: boolean;
  public tier: number;
  //-----------------------
  public packageTracking = {
    alphanumeric: null,
    phone: null,
    identificationId: null,
  };

  constructor(
    private quoteService: QuoteService,
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    this.locationVisibility = this.toggleButtonByTierId(TierId.TIER_AGENCY);
  }

  getOriginName = (shipping: Quote): string => {
    return getOriginNameLang(shipping, this.translate.getDefaultLang());
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  getPackageTracking = (nameField: string) => {
    this.findByNameField(nameField);
  };

  searchButton = () => {
    let nameField;
    for (var key in this.packageTracking) {
      if (this.packageTracking[key] != null) nameField = key;
    }
    this.findByNameField(nameField);
  };

  findByNameField = (nameField: string) => {
    const nameValue = this.packageTracking[nameField];

    if (!nameValue) {
      this.tracking = [];
      return;
    }

    this.loading = true;
    this.tracking = [];

    this.quoteService.getPackageTracking(nameField, nameValue).subscribe(
      (quotes: Quote[]) => {
        this.tracking = quotes;
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      },
      () => {
        this.loading = false;
      }
    );
  };

  isToggleShowTracking = (ngbTracking: TemplateRef<any>, keyId: number) => {
    const tracing = this.tracking[keyId] || null;
    let agency = tracing.user.agency[0];
    this.tier = agency !== undefined ? agency.tier : tracing.user.tier;
    this.tracing = tracing;
    if (!!tracing) {
      this.modalRef = this.ngbModal.open(ngbTracking, {
        size: "lg",
      });
    }
  };

  onCloseNgbModal = (): void => {
    this.modalRef.close();
  };

  clearInput = (name: string) => {
    for (var key in this.packageTracking) {
      if (key != name && this.packageTracking[key] != null) {
        this.packageTracking[key] = null;
      }
    }
    this.tracking = [];
  };

  clearAll = () => {
    this.packageTracking["alphanumeric"] = null;
    this.packageTracking["identificationId"] = null;
    this.packageTracking["phone"] = null;
    this.getPackageTracking(null);
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter(
                (assignment) => assignment.submodule.path === "search-shipments"
              )
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };
}
