import { Component, OnInit } from "@angular/core";
import { EventEmitter, Input, Output } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { FormBuilder, FormGroup } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { FormAssignmentValidator } from "./FormAssignmentValidator";

import { User } from "@models";
import {
  handlerError,
  rawError,
  showMessage,
  validationsForm,
} from "@shared/utils";
import { LockerService } from "@core/service";

@Component({
  selector: "cml-assignments",
  templateUrl: "./assignments.component.html",
  styles: [],
})
export class AssignmentsComponent implements OnInit {
  @Input() resellers: User[];
  @Input() user: User;
  @Output() onAssignmentUser = new EventEmitter<User>();

  public submitted: boolean = false;
  public filterEnabled: boolean = false;
  public assignment: FormGroup;
  public users: User[] = [];
  public errors: Array<any>;

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private lockerService: LockerService
  ) {
    this.assignment = this.formBuilder.group(FormAssignmentValidator);
  }

  ngOnInit(): void {
    if (!!this.user) {
      this.assignment.patchValue(
        {
          type: null,
          user_id: this.user.id,
          name: null,
        },
        { onlySelf: true, emitEvent: false }
      );
    }
  }

  isFieldValid = (field: string): boolean => {
    return (
      this.assignment.get(field).invalid && this.assignment.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getTypeNotNull = (): boolean => {
    return !!this.assignment.get("type").value;
  };

  filterReseller = () => {
    const resellerName = this.assignment.get("name").value;
    const typeUser = this.assignment.get("type").value;

    if (!!resellerName) {
      return (this.users = this.resellers.filter((row) => {
        const { person } = row;
        const personName = person.name.toLocaleLowerCase();

        return (
          personName.indexOf(resellerName) !== -1 &&
          row.tier === parseInt(typeUser)
        );
      }));
    }
  };

  onChangeUser = (userId: number) => {
    if (!!userId) {
      const user = this.resellers.find(({ id }) => id === userId);

      if (!!user) {
        this.assignment.patchValue(
          {
            reseller_id: userId,
            name: user.person.name,
          },
          { onlySelf: true, emitEvent: false }
        );

        this.users = [];
      }
    }
  };

  onSubmit = () => {
    const infoValid = this.assignment.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.assignment);
    }

    if (!!!this.submitted) {
      this.submitted = true;

      return this.lockerService
        .assignment({ ...this.assignment.getRawValue() })
        .subscribe(
          (user: User) => {
            this.submitted = false;
            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.assignmentLocker"),
              "success",
              true
            );

            this.onAssignmentUser.emit(user);
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
