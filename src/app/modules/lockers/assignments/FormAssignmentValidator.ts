import { FormControl, Validators } from "@angular/forms";

export const FormAssignmentValidator = {
  user_id: new FormControl(null, [Validators.required]),
  reseller_id: new FormControl(null, [Validators.required]),
  name: new FormControl(null, []),
  type: new FormControl(null, []),
};
