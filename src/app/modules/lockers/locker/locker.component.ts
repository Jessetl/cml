import { Component, OnInit, TemplateRef, OnDestroy } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { ActivatedRoute } from "@angular/router";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { User, Users, Country, FormUser } from "@models";
import { UserService, LockerService } from "@core/service";
import { CountryService } from "@core/service";
import {
  rawError,
  handlerError,
  TierId,
  Socket,
  STATUS_ACTIVE,
  STATUS_INACTIVE,
} from "@shared/utils";
import { formatingPhone, countriesLang } from "@shared/utils";
import { confirm, showMessage } from "@shared/utils";
import { CREATE, EDIT, REAL_TIME } from "@shared/utils";
import { on, unsubscribeOf } from "jetemit";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-locker",
  templateUrl: "./locker.component.html",
  styles: [],
})
export class LockerComponent implements OnInit, OnDestroy {
  private modalRef: NgbModalRef;

  public isLoading: boolean = false;
  public submitted: boolean = false;
  public searching: boolean = false;
  public data: User;
  public user: User;
  public resellers: User[];
  public displayLocker: User;
  public currentUser: User;
  public users: User[];
  public backupUsers: User[];
  public countries: Country[];
  public crud: number = 0;
  public filtered: boolean = false;
  public status: number = STATUS_ACTIVE;

  public errors: Array<any>;

  // Password
  public validatePass: boolean = false;
  public minChar: boolean = false;
  public requireNumber: boolean = false;
  public requireCharEs: boolean = false;
  public requiteText: boolean = false;

  //routeHome
  public readonly home: string;

  // FormSearch
  public search = {
    name: null,
    email: null,
    phone: null,
    cellphone: null,
    id: null,
  };

  constructor(
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private userService: UserService,
    private lockerService: LockerService,
    private countryService: CountryService,
    private store: Store<fromRoot.State>,
    private route: ActivatedRoute
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.currentUser = user));

    this.home = this.route.snapshot.paramMap.get("home");
  }

  ngOnInit() {
    this.getResellers();
    this.getLockers();

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    on(REAL_TIME.NEW_LOCKER, (_: number) => {
      if (!this.filtered) {
        this.getLockers();
      }
    });
  }

  getLockers = () => {
    this.isLoading = true;

    this.userService.getLockers().subscribe(
      (users: Users[]) => {
        const rawData = users
          .map((user: User) => {
            return {
              ...user,
              person: {
                ...user.person,
                format_cellphone: formatingPhone(user.person.cellphone),
                format_phone: formatingPhone(user.person.phone),
              },
              role: { ...user.role[0] },
            };
          })
          .filter((user) => !!user.person);

        this.backupUsers = rawData;
        this.users = rawData.filter(({ status }) => status === this.status);

        this.isLoading = false;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  getResellers = () => {
    this.userService.getResellers().subscribe(
      (users: Users[]) => {
        const rawData = users
          .map((user: User) => {
            return {
              ...user,
              person: {
                ...user.person,
                format_cellphone: formatingPhone(user.person.cellphone),
                format_phone: formatingPhone(user.person.phone),
              },
              role: { ...user.role[0] },
            };
          })
          .filter((user) => !!user.person);

        this.resellers = rawData.filter(({ status }) => status === this.status);
        this.isLoading = false;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  changeStatus = () => {
    let users = [...this.backupUsers];

    this.status = this.status ? STATUS_INACTIVE : STATUS_ACTIVE;
    this.users = users.filter(({ status }) => status === this.status);
  };

  ngOnDestroy(): void {
    unsubscribeOf(REAL_TIME.NEW_LOCKER);
  }

  shouldToggleCreate = (ngbUserCreate: TemplateRef<any>) => {
    this.errors = null;
    this.crud = CREATE;

    this.modalRef = this.ngbModal.open(ngbUserCreate);
  };

  shouldTransferUser = (ngbUserTransfer: TemplateRef<any>, userId: number) => {
    const user: User = this.users.find(({ id }) => id === userId);

    if (!!user) {
      this.user = user;
      this.modalRef = this.ngbModal.open(ngbUserTransfer);
    }
  };

  updateUserBackup = (user: User, type: number) => {
    switch (type) {
      case CREATE:
        return (this.backupUsers = [...this.backupUsers, user]);
      case EDIT:
        let keyUser = this.backupUsers.findIndex(({ id }) => id === user.id);
        let resUser = [...this.backupUsers];

        resUser[keyUser] = {
          ...user,
          person: { ...user.person },
          id: user.id,
        };
        return (this.backupUsers = resUser);
      default:
        this.backupUsers;
    }
  };

  shouldToggleUpdate = (ngbUserUpdate: TemplateRef<any>, userId: number) => {
    this.errors = null;
    this.crud = EDIT;

    const user: User = this.users.find(({ id }) => id === userId);

    if (!!user) {
      this.data = user;
      this.modalRef = this.ngbModal.open(ngbUserUpdate);
    }
  };

  onAssignmentUser = (user: User) => {
    let keyUser = this.users.findIndex(({ id }) => id === user.id);
    let resUser = [...this.users];

    if (!!user) {
      resUser[keyUser] = {
        ...resUser[keyUser],
        reseller: [...user.reseller],
      };

      this.users = resUser;
      this.updateUserBackup(user, EDIT);
    }

    this.onCloseNgbModal();
  };

  onCloseNgbModal = () => {
    this.modalRef.close();

    this.user = null;
    this.displayLocker = null;
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.currentUser) {
      const module =
        this.currentUser.tier !== TierId.TIER_ADMIN
          ? this.currentUser.role.assignments
              .filter((assignment) => assignment.submodule.path === "lockers")
              .find((assignment) => assignment.type === type)
          : null;

      return this.currentUser.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  shouldMakeUser = (user: User) => {
    const resUser = {
      ...user,
      person: { ...user.person },
      role: { ...user.role[0] },
      id: user.id,
    };

    this.users = [...this.users, resUser];
    this.updateUserBackup(resUser, CREATE);
  };

  shoulderUpdateUser = (user: User) => {
    let keyUser = this.users.findIndex(({ id }) => id === user.id);
    let resUser = [...this.users];

    resUser[keyUser] = {
      ...user,
      person: { ...user.person },
      id: user.id,
    };

    this.users = resUser;
    this.updateUserBackup(user, EDIT);
  };

  shoulderUpdateUserStatus = (user: User) => {
    let resUser = [...this.users];
    this.users = resUser.filter(({ id }) => id !== user.id);
    this.updateUserBackup(user, EDIT);
  };

  shouldToggleStatus = async (userId: number) => {
    const wantStatus = await confirm({
      text: this.translate.instant("ngbAlert.wantToChangeStatus"),
      confirmButtonText: this.translate.instant("buttonText.confirmButtonText"),
      cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
    });

    if (wantStatus) {
      this.userService.changeStatus({ id: userId }).subscribe(
        (user: User) => {
          this.submitted = false;

          this.shoulderUpdateUserStatus(user);

          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.updateUser"),
            "success",
            true
          );
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;

          if (typeof error.error === "string") {
            return showMessage(null, error.error, "error", true);
          }

          this.handleError(error);
        }
      );
    }
  };

  emptySearch = () => {
    this.search = {
      name: null,
      email: null,
      phone: null,
      cellphone: null,
      id: null,
    };
  };

  clear = () => {
    this.emptySearch();
    this.onKey("");
    this.filtered = false;
  };

  searchButton = () => {
    let name;

    for (var key in this.search) {
      if (this.search[key] != null) {
        name = key;
      }
    }

    this.onKey(name);
  };

  onKey(key: string) {
    let name = key;
    let variable = this.search[key];
    this.filtered = true;

    if (!!!this.isLoading) {
      this.isLoading = true;

      this.userService
        .getLockersByFilters({ [name]: variable, status: this.status }, 1)
        .subscribe(
          (users: Users[]) => {
            this.isLoading = false;
            const rawData = users
              .map((user: User) => {
                return {
                  ...user,
                  person: { ...user.person },
                  role: { ...user.role[0] },
                };
              })
              .filter((user) => !!user.person);

            this.users = rawData;

            if (!name && !variable) {
              this.backupUsers = rawData;
            }
          },
          (error: HttpErrorResponse) => {
            this.isLoading = false;

            this.emptySearch();

            showMessage(
              "¡Error!",
              this.translate.instant(`errors${handlerError(error)}`),
              "error",
              true
            );
          }
        );
    }
  }

  clearInput = (name: string) => {
    for (var key in this.search) {
      if (key !== name && this.search[key] !== null) {
        this.search[key] = null;
      }
    }
    let backup = [...this.backupUsers];
    this.users = backup.filter(({ status }) => status === this.status);
  };

  onSubmit = (user: FormUser) => {
    const isEditing = this.crud;
    this.errors = null;
    const { agency } = this.currentUser;
    const agencyId = !!agency ? agency.id : this.currentUser.id;

    if (!!!this.submitted) {
      this.submitted = true;

      switch (isEditing) {
        case EDIT:
          return this.lockerService.update(user).subscribe(
            (user: User) => {
              this.submitted = false;

              this.shoulderUpdateUser(user);

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.updateUser"),
                "success",
                true
              );

              this.modalRef.close();
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
        case CREATE:
          return this.lockerService.store(user).subscribe(
            (user: User) => {
              this.submitted = false;

              this.shouldMakeUser(user);

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.createUser"),
                "success",
                true
              );

              this.modalRef.close();
              Socket.emit(REAL_TIME.LOCKER, agencyId);
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
      }
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  isToggleLocker = (ngbDisplayLocker: TemplateRef<any>, key: number) => {
    this.displayLocker = this.users[key];
    console.log("usuario ", this.displayLocker);

    if (!!this.displayLocker) {
      this.modalRef = this.ngbModal.open(ngbDisplayLocker);
    }
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
