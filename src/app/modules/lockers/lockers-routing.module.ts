import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { LockerComponent } from "./locker/locker.component";
import { AssignmentsComponent } from "./assignments/assignments.component";

const routes: Routes = [
  {
    path: "",
    component: LockerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LockersRoutingModule {}

export const routingComponents = [LockerComponent, AssignmentsComponent];
