import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { LockersRoutingModule } from "./lockers-routing.module";
import { routingComponents } from "./lockers-routing.module";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@shared";

@NgModule({
  declarations: [routingComponents],
  imports: [
    CommonModule,
    LockersRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class LockersModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
