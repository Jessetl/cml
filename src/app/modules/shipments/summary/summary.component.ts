import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";

import { TimelineComponent } from "./timeline/timeline.component";

import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";

import { ShipmentService } from "@core/service";
import { Quote, Charge, Country } from "@models";
import {
  showMessage,
  handlerError,
  formatingPhone,
  TypeContent,
  getOriginNameLang,
  getCountryNameLang,
} from "@shared/utils";

@Component({
  selector: "cml-summary",
  templateUrl: "./summary.component.html",
  styles: [],
})
export class SummaryComponent implements OnInit {
  @ViewChild(TimelineComponent)
  Timeline: TimelineComponent;

  private readonly alphanumeric: string;

  private modalRef: NgbModalRef;
  public shipment: Quote;
  public tracking: Charge;
  public locationVisibility: boolean;
  public isLoading: boolean = true;
  public tier: number;

  constructor(
    private shipmentService: ShipmentService,
    private translate: TranslateService,
    private ngbModal: NgbModal,
    private route: ActivatedRoute
  ) {
    this.alphanumeric = this.route.snapshot.paramMap.get("alphanumeric");
  }

  ngOnInit() {
    this.shipmentService.getShipmentByAlphanumeric(this.alphanumeric).subscribe(
      (shipment: Quote) => {
        const { user } = shipment;

        const tierId = !!user.agency[0] ? user.agency[0].tier : user.tier;

        this.shipment = shipment;
        this.tier = tierId;
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  getOriginName = (shipping: Quote): string => {
    return getOriginNameLang(shipping, this.translate.getDefaultLang());
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  keyIncrement = (key: number): number => {
    return key + 1;
  };

  isToggleTracking = (
    ngbTracking: TemplateRef<TimelineComponent>,
    keyId: number
  ) => {
    const tracking = this.shipment.charge[keyId] || null;
    this.tracking = tracking;

    if (!!tracking) {
      this.modalRef = this.ngbModal.open(ngbTracking, {
        size: "lg",
      });
    }
  };

  onCloseNgbModal = (): void => {
    this.modalRef.close();
  };

  formating = (phone: number) => {
    return phone ? formatingPhone(phone) : "";
  };

  getNamePackage = (name: string, typeConent: number): string => {
    return (
      name +
      ` (${this.translate.instant(TypeContent.getDisplayName(typeConent))})`
    );
  };
}
