import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@shared";

import { HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import {
  ShipmentsRoutingModule,
  routingComponents,
} from "./shipments-routing.module";

@NgModule({
  declarations: [routingComponents],
  imports: [
    CommonModule,
    ShipmentsRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    ReactiveFormsModule,
    SharedModule,
    NgbModule,
  ],
})
export class ShipmentsModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
