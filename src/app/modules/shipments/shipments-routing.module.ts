import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SummaryComponent } from "./summary/summary.component";
import { TimelineComponent } from "./summary/timeline/timeline.component";

const routes: Routes = [
  {
    path: "summary/:alphanumeric",
    component: SummaryComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShipmentsRoutingModule {}

export const routingComponents = [SummaryComponent, TimelineComponent];
