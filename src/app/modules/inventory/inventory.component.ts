import { Component, OnInit, TemplateRef } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { ActivatedRoute } from "@angular/router";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { FormGroup, FormBuilder } from "@angular/forms";
import { FormInventoryValidators } from "./FormInventoriesValidators";

import { Package, Inventory, Image, Uploaded } from "@models";
import { InventoryService } from "@core/service";
import { ENTRY_IN } from "@shared/utils";
import { showMessage, validationsForm } from "@shared/utils";
import { handlerError, rawError } from "@shared/utils";
import { getServiceDescriptionLang } from "@shared/utils";

@Component({
  selector: "cml-inventory",
  templateUrl: "./inventory.component.html",
  styles: [],
})
export class InventoryComponent implements OnInit {
  public readonly home: string;

  private modalRef: NgbModalRef;

  public isLoading: boolean = true;
  public submitted: boolean = false;
  public inventories: Package[];
  public packageId: number;
  public inventory: FormGroup;
  public images: Image[] = [];
  public error: any;
  public errors: Array<any>;

  constructor(
    private translate: TranslateService,
    private inventoryService: InventoryService,
    private ngbModal: NgbModal,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.inventory = this.formBuilder.group(FormInventoryValidators);
    this.home = this.route.snapshot.paramMap.get("home");
  }

  ngOnInit() {
    this.getInventories();
  }

  getInventories = () => {
    this.isLoading = true;

    this.inventoryService.getPackageForSales().subscribe(
      (inventories: Package[]) => {
        this.isLoading = false;

        let invetory_map = inventories.map((i) => {
          return {
            ...i,
            sale_price: i.sale_price || 0,
            stock: this.getLastMovement(i.last_movement),
          };
        });

        this.inventories = !!this.home
          ? invetory_map.filter((i) => {
              return !!i.last_movement && i.stock < 0;
            })
          : invetory_map;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  getLastMovement = (inventory: Inventory): number => {
    if (!!inventory) {
      return inventory.movement === ENTRY_IN
        ? inventory.current_stock + inventory.amount
        : inventory.current_stock - inventory.amount;
    }

    return 0;
  };

  isFieldValid = (field: string): boolean => {
    return (
      this.inventory.get(field).invalid && this.inventory.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  shouldToggleEdit = (
    ngbInventories: TemplateRef<any>,
    inventoryId: number
  ) => {
    const inventory = this.inventories.find(({ id }) => id === inventoryId);

    if (!!inventory) {
      this.inventory.patchValue(
        { id: inventoryId, sale_price: inventory.sale_price },
        { onlySelf: true }
      );

      this.openModal(ngbInventories);
    }
  };

  shouldToggleUpload = (ngbUpload: TemplateRef<any>, inventoryId: number) => {
    const inventory = this.inventories.find(({ id }) => id === inventoryId);

    if (!!inventory) {
      this.inventory.patchValue(
        { id: inventoryId, sale_price: inventory.sale_price },
        { onlySelf: true }
      );

      this.images = inventory.images.map((row, key) => {
        return {
          id: key + 1,
          url: row.image_url,
          original_url: row.url,
          created_at: false,
          deleted_at: false,
        };
      });

      if (!!inventory.url) {
        this.images = [
          {
            id: this.images.length + 1,
            url: inventory.image_url,
            original_url: inventory.url,
            created_at: false,
            deleted_at: false,
          },
          ...this.images,
        ];
      }

      this.packageId = inventoryId;
      return this.openModal(ngbUpload);
    }

    this.images = [];
  };

  shoulderUpdatePackage = (pack: Package) => {
    let keyAgency = this.inventories.findIndex(({ id }) => id === pack.id);
    let resPackage = [...this.inventories];

    resPackage[keyAgency] = {
      ...pack,
      stock: this.getLastMovement(pack.last_movement),
    };

    this.inventories = resPackage;
  };

  onCloseNgbModal = () => {
    this.packageId = null;
    this.images = [];

    this.inventory.reset();
    this.modalRef.close();
  };

  openModal = (ngbModal: TemplateRef<any>) => {
    this.modalRef = this.ngbModal.open(ngbModal, {
      size: "md",
    });
  };

  getServiceName = (inventory: Package) => {
    return getServiceDescriptionLang(
      inventory,
      this.translate.getDefaultLang()
    );
  };

  responseFiles = (data: Uploaded) => {
    if (!!!this.submitted) {
      this.submitted = true;

      this.inventoryService.upload(this.packageId, data).subscribe(
        (pack: Package) => {
          this.submitted = false;
          this.shoulderUpdatePackage(pack);

          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.uploadImages"),
            "success",
            true
          );

          this.onCloseNgbModal();
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
    }
  };

  onSubmit = () => {
    const infoValid = this.inventory.valid;
    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.inventory);
    }

    if (!!!this.submitted) {
      this.submitted = true;

      this.inventoryService.update(this.inventory.getRawValue()).subscribe(
        (pack: Package) => {
          this.inventory.reset();

          this.submitted = false;
          this.shoulderUpdatePackage(pack);

          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.updateInventory"),
            "success",
            true
          );

          this.onCloseNgbModal();
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
