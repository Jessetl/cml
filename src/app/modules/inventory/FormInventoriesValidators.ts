import { FormControl, Validators } from "@angular/forms";

export const FormInventoryValidators = {
  id: new FormControl(null, [Validators.required]),
  amount: new FormControl(null, []),
  sale_price: new FormControl(null, [Validators.required]),
};
