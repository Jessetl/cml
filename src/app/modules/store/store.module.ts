import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@shared";

import { NgxStripeModule } from "ngx-stripe";
import { environment } from "environments/environment";

import { StoreRoutingModule, routingComponents } from "./store-routing.module";

@NgModule({
  declarations: [routingComponents],
  imports: [
    CommonModule,
    StoreRoutingModule,
    NgxStripeModule.forRoot(environment.stripeKey),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class StoreModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
