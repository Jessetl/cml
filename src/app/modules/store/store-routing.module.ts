import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Components
import { StoreComponent } from "./store/store.component";
import { PayComponent } from "./pay/pay.component";
import { CheckoutComponent } from "./pay/checkout/checkout.component";

const routes: Routes = [
  {
    path: "",
    component: StoreComponent,
  },
  {
    path: "pay",
    component: PayComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StoreRoutingModule {}

export const routingComponents = [
  StoreComponent,
  PayComponent,
  CheckoutComponent,
];
