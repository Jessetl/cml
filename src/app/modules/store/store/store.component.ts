import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";

import { Package, Shop, Inventory, Country } from "@models";
import { User, PackageImage } from "@models";
import { CountryService, StoreService } from "@core/service";
import { showMessage, handlerError } from "@shared/utils";
import { getServiceDescriptionLang } from "@shared/utils";
import { TYPE_PERCENTAGE, ENTRY_IN } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as shop from "@actions";
import * as user from "@actions";

@Component({
  selector: "cml-store",
  templateUrl: "./store.component.html",
  styles: [],
})
export class StoreComponent implements OnInit {
  public error: any;
  public errors: Array<any>;
  public articles: Shop[];
  public shop: Shop[];
  public isLoading: boolean = true;
  public colorStock: string;
  public country: Country;
  public user: User;

  constructor(
    private router: Router,
    private translate: TranslateService,
    private countryService: CountryService,
    private store: Store<fromRoot.State>,
    private storeService: StoreService
  ) {
    this.store
      .select(fromRoot.getShop)
      .subscribe((shop: Shop[]) => (this.shop = shop));

    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    this.isLoading = true;

    this.countryService.getCountryUnitedStates().subscribe(
      (country: Country) => {
        this.country = country;
        this.getArticles();
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;

        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  getArticles = () => {
    this.storeService.getPackageWithStock().subscribe(
      (articles: Package[]) => {
        this.isLoading = false;

        this.articles = articles
          .map((a) => {
            return {
              id: a.id,
              name: this.getServiceName(a),
              sale_price: a.sale_price,
              quantity: 0,
              taxation: 0,
              stock: this.getLastMovement(a.last_movement),
              subtotal: 0,
              total: 0,
              inStore: !!this.shop.find(({ id }) => id === a.id),
              images: this.getImages(a.images),
            };
          })
          .filter((a) => a.stock > 0);
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;

        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  quantityMaximum = (stock: number): string => {
    return this.translate.instant("formBuilder.quantityMax", {
      value: stock,
    });
  };

  getLastMovement = (inventory: Inventory): number => {
    if (!!inventory) {
      return inventory.movement === ENTRY_IN
        ? inventory.current_stock + inventory.amount
        : inventory.current_stock - inventory.amount;
    }

    return 0;
  };

  getImages = (images: PackageImage[]) => {
    return images.map((i) => {
      return {
        url: i.image_url,
      };
    });
  };

  getServiceName = (article: Package): string => {
    return getServiceDescriptionLang(article, this.translate.getDefaultLang());
  };

  stockMessage = (stock: number): string => {
    if (stock <= 10) {
      return this.translate.instant("cardText.limited_existence");
    } else if (stock >= 25) {
      return this.translate.instant("cardText.sufficient_existence");
    } else if (stock >= 20 || stock <= 24) {
      return this.translate.instant("cardText.moderate_existence");
    }

    return "";
  };

  removeItemToCart = (key: number) => {
    const article = this.articles[key];

    if (!!this.articles[key]) {
      let copyArticles = [...this.articles];

      if (copyArticles[key].quantity > 0) {
        const quantity = copyArticles[key].quantity - 1;

        copyArticles[key] = {
          ...copyArticles[key],
          quantity: quantity,
          taxation: 0,
          total: parseFloat((quantity * article.sale_price).toFixed(2)),
        };

        this.articles = copyArticles;
      }
    }
  };

  addToCart = (key: number) => {
    const article = this.articles[key];
    if (article.quantity > article.stock) return;
    if (!!article) {
      let copyShop = [...this.shop];
      let copyArticles = [...this.articles];

      if (
        !!!this.shop.find(({ id }) => id === article.id) &&
        article.quantity > 0
      ) {
        const taxation =
          this.country.type_taxation === TYPE_PERCENTAGE
            ? this.country.taxation / 100
            : this.country.taxation;

        const subtotal = parseFloat(
          (article.quantity * article.sale_price).toFixed(2)
        );

        const amountTaxation =
          this.country.type_taxation === TYPE_PERCENTAGE
            ? this.multiplyPercentage(subtotal, taxation)
            : this.sumPercentage(subtotal, taxation);

        copyArticles[key] = {
          ...copyArticles[key],
          inStore: true,
          subtotal: subtotal,
          taxation: amountTaxation,
          total: parseFloat((subtotal + amountTaxation).toFixed(2)),
        };

        this.store.dispatch(
          new shop.SetShopAction([...copyShop, copyArticles[key]])
        );

        this.articles = copyArticles;
      }
    }
  };

  addItemToCart = (key: number) => {
    if (!!this.articles[key]) {
      let copyArticles = [...this.articles];

      if (copyArticles[key].quantity < copyArticles[key].stock) {
        const quantity = copyArticles[key].quantity + 1;

        copyArticles[key] = {
          ...copyArticles[key],
          quantity: quantity,
        };

        this.articles = copyArticles;
      }
    }
  };

  modifyToCart = (key: number) => {
    const article = this.articles[key];

    if (!!article) {
      let copyShop = [...this.shop];
      let copyArticles = [...this.articles];

      if (!!this.shop.find(({ id }) => id === article.id)) {
        copyArticles[key] = {
          ...copyArticles[key],
          inStore: false,
        };

        this.store.dispatch(
          new shop.SetShopAction(copyShop.filter(({ id }) => id !== article.id))
        );

        this.articles = copyArticles;
      }
    }
  };

  removeToCart = (key: number) => {
    const article = this.articles[key];

    if (!!article) {
      let copyShop = [...this.shop];
      let copyArticles = [...this.articles];

      if (!!this.shop.find(({ id }) => id === article.id)) {
        copyArticles[key] = {
          ...copyArticles[key],
          inStore: false,
          quantity: 0,
          subtotal: 0,
          taxation: 0,
          total: 0,
        };

        this.store.dispatch(
          new shop.SetShopAction(copyShop.filter(({ id }) => id !== article.id))
        );

        this.articles = copyArticles;
      }
    }
  };

  multiplyPercentage = (amount: number, taxation: number): number => {
    return parseFloat((amount * taxation).toFixed(2));
  };

  sumPercentage = (amount: number, taxation: number): number => {
    return parseFloat((amount + taxation).toFixed(2));
  };

  goToPay = () => {
    this.router.navigate(["/dashboard/store/pay"]);
  };
}
