import { Component, OnInit } from "@angular/core";
import { Input, Output, EventEmitter } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";

import { Token } from "@stripe/stripe-js";
import { Payment, Shop, User } from "@models";
import { DepositForm, TransferForm } from "@models";

import { StoreService } from "@core/service";
import { GooglemapsService } from "@core/service";
import { Socket, REAL_TIME } from "@shared/utils";
import { showMessage, handlerError } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as shop from "@actions";

@Component({
  selector: "cml-checkout",
  templateUrl: "./checkout.component.html",
  styles: [],
})
export class CheckoutComponent implements OnInit {
  @Input() shop: Shop[];
  @Output() onClose = new EventEmitter<null>();

  public errors: any;
  public submitted: boolean = false;
  public grandTotal: number = 0;
  public toggle: boolean = true;
  public paymentMethod: number = 0;
  public user: User;
  public latitude: string;
  public longitude: string;

  constructor(
    private translate: TranslateService,
    private storeService: StoreService,
    private googleService: GooglemapsService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit(): void {
    this.googleService
      .getLocation({ timeout: 15000 })
      .then(({ coords }) => {
        this.latitude = coords.latitude.toString();
        this.longitude = coords.longitude.toString();
      })
      .catch(handlerError);

    this.grandTotal = this.shop.reduce(
      (prevValue: number, currentValue: Shop) => {
        return prevValue + currentValue.total;
      },
      0
    );
  }

  eventLoadSubmitted = (submitted: boolean) => {
    this.submitted = submitted;
  };

  getToggleName = (): string => {
    return this.toggle
      ? "tableRender.hiddenDetails"
      : "tableRender.showDetails";
  };

  responseToken = (token: Token) => {
    this.storeService
      .payStripeStore({
        shop: this.shop,
        token: token.id,
        amount: parseFloat((this.grandTotal * 100).toFixed(2)),
        latitude: this.latitude,
        longitude: this.longitude,
      })
      .subscribe(
        (payment: Payment) => {
          this.submitted = false;

          this.store.dispatch(new shop.RemoveShopAction([]));

          showMessage(
            null,
            this.translate.instant("notifications.default"),
            "success",
            true
          );

          this.emitSocket();
          this.onClose.emit();
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.errors = error.error.message;
        }
      );
  };

  responseTransfer = (transfer: TransferForm) => {
    if (!!!this.submitted) {
      this.submitted = true;

      this.storeService
        .payTransferStore({
          ...transfer,
          shop: this.shop,
          latitude: this.latitude,
          longitude: this.longitude,
        })
        .subscribe(
          (payment: Payment) => {
            this.submitted = false;

            this.store.dispatch(new shop.RemoveShopAction([]));

            showMessage(
              null,
              this.translate.instant("notifications.default"),
              "success",
              true
            );

            this.emitSocket();
            this.onClose.emit();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.errors = error.error.message;
          }
        );
    }
  };

  responseDeposit = (deposit: DepositForm) => {
    if (!!!this.submitted) {
      this.submitted = true;

      this.storeService
        .payDepositStore({
          ...deposit,
          shop: this.shop,
          latitude: this.latitude,
          longitude: this.longitude,
        })
        .subscribe(
          (payment: Payment) => {
            this.submitted = false;

            this.store.dispatch(new shop.RemoveShopAction([]));

            showMessage(
              null,
              this.translate.instant("notifications.default"),
              "success",
              true
            );

            this.emitSocket();
            this.onClose.emit();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.errors = error.error.message;
          }
        );
    }
  };

  emitSocket = () => {
    if (!!this.user) {
      const { agency } = this.user;
      const agencyId = !!agency ? agency.id : this.user.id;

      Socket.emit(REAL_TIME.INVOICE, agencyId);
    }
  };
}
