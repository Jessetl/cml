import { Component, OnInit, TemplateRef } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

import { Shop } from "@models";
import { StoreService } from "@core/service";
import { showMessage, handlerError } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as shop from "@actions";

@Component({
  selector: "cml-pay",
  templateUrl: "./pay.component.html",
  styles: [],
})
export class PayComponent implements OnInit {
  private modalRef: NgbModalRef;

  public error: any;
  public errors: Array<any>;
  public shop: Shop[];
  public isLoading: boolean = true;

  constructor(
    private ngbModal: NgbModal,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getShop)
      .subscribe((shop: Shop[]) => (this.shop = shop));
  }

  ngOnInit(): void {
    this.isLoading = false;
  }

  onDestroyArticleInShop = (shopId: number) => {
    const filterShop = this.shop.filter(({ id }) => shopId !== id);

    this.store.dispatch(new shop.SetShopAction(filterShop));
  };

  payStoreItems = (ngbPayment: TemplateRef<any>) => {
    if (!!this.shop) {
      this.modalRef = this.ngbModal.open(ngbPayment, {
        size: "lg",
      });
    }
  };

  onCloseNgbModal = (): void => {
    this.modalRef.close();
  };
}
