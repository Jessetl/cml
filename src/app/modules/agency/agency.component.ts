import { Component, OnInit, TemplateRef, OnDestroy } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { Country, Role as Profile, User } from "@models";
import { User as Agency, Users as Agencies } from "@models";

import { ProfileService, GooglemapsService } from "@core/service";
import { AgencyService, CountryService } from "@core/service";

import { AgencyFormValidator } from "./FormAgencyValidator";

import {
  confirm,
  TierId,
  showMessage,
  validationsForm,
  STATUS_ACTIVE,
  STATUS_INACTIVE,
} from "@shared/utils";
import { rawError, handlerError, TIER_SUPERVISOR } from "@shared/utils";
import { showConfirmButton, formatingPhone } from "@shared/utils";
import { profilesLang, countriesLang, TIER_SELLER } from "@shared/utils";
import { REAL_TIME, CREATE, EDIT, DELETE, Socket } from "@shared/utils";

import { on, unsubscribeOf } from "jetemit";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-agency",
  templateUrl: "./agency.component.html",
  styles: [],
})
export class AgencyComponent implements OnInit, OnDestroy {
  private modalRef: NgbModalRef;
  public isLoading: boolean = true;
  public isEditing: boolean = false;
  public user: User;
  public displayAgency: User;
  public agencies: Agency[];
  public backupAgencies: Agency[];
  public countries: Country[];
  public agency: FormGroup;
  public submitted: boolean;
  public filtered: boolean = false;
  public searching: boolean = false;
  public paginate: number = 1;
  public agency_settings: [];
  public profiles: Profile[];
  public status: number = STATUS_ACTIVE;
  public shouldEnableStateAndCity: boolean = false;
  public errors: Array<any>;
  public errorZipCode: boolean;

  // Search
  public search = {
    name: null,
    contact: null,
    phone: null,
    cellphone: null,
    id: null,
  };

  //password
  public validatePass: boolean = false;
  public minChar: boolean = false;
  public requireNumber: boolean = false;
  public requireCharEs: boolean = false;
  public requireText: boolean = false;
  public lang: string;

  //routeHome
  public readonly home: string;

  constructor(
    private router: Router,
    private ngbModal: NgbModal,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private agencyService: AgencyService,
    private countryService: CountryService,
    private profileService: ProfileService,
    private googleService: GooglemapsService,
    private store: Store<fromRoot.State>,
    private route: ActivatedRoute
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.agency = this.formBuilder.group(AgencyFormValidator);
    this.home = this.route.snapshot.paramMap.get("home");
  }

  ngOnInit() {
    this.getAgencies();

    this.profileService.getProfiles().subscribe(
      (profiles: Profile[]) => {
        this.profiles = profilesLang(profiles, this.translate.getDefaultLang());
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    on(REAL_TIME.NEW_AGENCY, (_: number) => {
      if (!this.filtered) {
        this.getAgencies();
      }
    });
  }

  ngOnDestroy(): void {
    unsubscribeOf(REAL_TIME.NEW_AGENCY);
  }

  getAgencies = () => {
    this.isLoading = true;
    this.agencyService.getAgenciesByPage(this.paginate).subscribe(
      (agencies: Agencies[]) => {
        this.isLoading = false;
        const rawData = agencies
          .map((agency: Agency) => {
            return {
              ...agency,
              person: { ...agency.person },
              role: { ...agency.role[0] },
            };
          })
          .filter((user) => !!user.person);
        this.backupAgencies = rawData;
        this.agencies = rawData.filter(({ status }) => status === this.status);
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  changeStatus = () => {
    this.status =
      this.status === STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
    let agencies = [...this.backupAgencies];
    this.agencies = agencies.filter(({ status }) => status === this.status);
  };

  isFieldValid = (field: string): boolean => {
    return this.agency.get(field).invalid && this.agency.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  updateBackupAgencies = (agency: Agency, type: number) => {
    switch (type) {
      case CREATE:
        this.backupAgencies = [...this.backupAgencies, agency];
        return;
      case EDIT:
        let keyAgency = this.backupAgencies.findIndex(
          ({ id }) => id === agency.id
        );
        let resAgency = [...this.backupAgencies];

        resAgency[keyAgency] = {
          ...agency,
          person: { ...agency.person },
          role: { ...agency.role[0] },
          id: agency.id,
        };
        this.backupAgencies = resAgency;
        return;
      case DELETE:
        this.backupAgencies = this.backupAgencies.filter(
          ({ id }) => id !== agency.id
        );
        return;
      default:
        this.backupAgencies;
    }
  };

  toggleButtonByTierId = (type: number): Boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === "agencies")
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  shouldStateAndCity = () => {
    this.shouldEnableStateAndCity = !this.shouldEnableStateAndCity;

    if (this.shouldEnableStateAndCity) {
      this.agency.get("state").enable({ onlySelf: true });
      this.agency.get("city").enable({ onlySelf: true });
    } else {
      this.agency.get("state").disable({ onlySelf: true });
      this.agency.get("city").disable({ onlySelf: true });
    }
  };

  shouldToggleCreate = (ngbAgency: TemplateRef<any>) => {
    this.isEditing = false;
    this.errors = null;
    this.validatePass = false;
    this.clean();

    this.agency.reset();

    this.agency
      .get("password")
      .setValidators([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.pattern(
          "(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$"
        ),
      ]);

    const countryId = this.countries.find(
      ({ alpha2Code }) => alpha2Code === "US"
    )?.id;

    if (!!countryId) {
      this.agency.patchValue({ country_id: countryId }, { onlySelf: true });
    }

    if (this.user.tier === TIER_SELLER || this.user.tier === TIER_SUPERVISOR) {
      this.agency.patchValue({ agency_settings: 1 }, { onlySelf: true });
    }

    this.modalRef = this.ngbModal.open(ngbAgency);
  };

  shouldToggleUpdate = (ngbAgency: TemplateRef<any>, agencyId: number) => {
    this.isEditing = true;
    this.validatePass = false;
    this.clean();

    const agency: Agency = this.agencies.find(({ id }) => id === agencyId);

    this.agency.patchValue({
      ...agency,
      ...agency.person,
      role: agency.role.id,
      id: agency.id,
      password: null,
    });

    this.agency.get("password").setValidators([]);

    this.modalRef = this.ngbModal.open(ngbAgency);
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
    this.validatePass = false;
    this.clean();
  };

  shouldToggleDelete = (agencyId: number) => {
    showConfirmButton(
      this.translate.instant("ngbAlert.deleteUser"),
      this.translate.instant("ngbAlert.btnYes"),
      this.translate.instant("ngbAlert.btnNo"),
      (agency: Agency) => {
        this.agencyService.destroy(agencyId).subscribe(
          () => {
            this.shoulderDestroyAgency(agencyId);
            this.updateBackupAgencies(agency, DELETE);
            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.destroyAgency"),
              "success",
              true
            );

            this.modalRef.close();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
      }
    );
  };

  shouldMakeAgency = (agency: Agency) => {
    const resAgency = {
      ...agency,
      person: { ...agency.person },
      id: agency.id,
      role: { ...agency.role[0] },
    };

    this.agencies = [...this.agencies, resAgency];
    this.updateBackupAgencies(resAgency, CREATE);
  };

  shoulderUpdateAgency = (agency: Agency) => {
    let keyAgency = this.agencies.findIndex(({ id }) => id === agency.id);
    let resAgency = [...this.agencies];

    resAgency[keyAgency] = {
      ...agency,
      person: { ...agency.person },
      role: { ...agency.role[0] },
      id: agency.id,
    };

    this.agencies = resAgency;
    this.updateBackupAgencies(agency, EDIT);
  };

  shoulderUpdateAgencyStatus = (agency: Agency) => {
    let resAgency = [...this.agencies];
    this.agencies = resAgency.filter(({ id }) => id !== agency.id);

    this.updateBackupAgencies(agency, EDIT);
  };

  shoulderDestroyAgency = (agencyId: number) => {
    this.agencies = this.agencies.filter(({ id }) => id !== agencyId);
  };

  onSubmit = () => {
    const infoValid = this.agency.valid;
    const isEditing = this.isEditing;
    const { agency } = this.user;
    const agencyId = !!agency ? agency.id : this.user.id;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.agency);
    } else if (this.submitted) {
      return;
    }

    this.submitted = true;

    switch (isEditing) {
      case true:
        return this.agencyService.update(this.agency.getRawValue()).subscribe(
          (agency: Agency) => {
            this.submitted = false;

            this.shoulderUpdateAgency(agency);

            this.agency.reset();

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.updateAgency"),
              "success",
              true
            );

            this.modalRef.close();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
      case false:
        return this.agencyService.store(this.agency.getRawValue()).subscribe(
          (agency: Agency) => {
            this.submitted = false;

            this.shouldMakeAgency(agency);

            this.agency.reset();

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.createAgency"),
              "success",
              true
            );

            this.modalRef.close();
            Socket.emit(REAL_TIME.AGENCY, agencyId);
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  emptySearch = () => {
    this.search = {
      name: null,
      contact: null,
      phone: null,
      cellphone: null,
      id: null,
    };
  };

  clear = () => {
    this.emptySearch();
    this.getAgencies();
    this.displayAgency = null;
    this.filtered = false;
  };

  onDefaultValuesStateAndCountry = () => {
    this.agency.get("state").reset(null, { onlySelf: true });
    this.agency.get("city").reset(null, { onlySelf: true });
  };

  onBlurZipCode = (nameField: string) => {
    const zipCode = this.agency.get(nameField).value;
    const countryId = this.agency.get("country_id").value;
    const countryAlphacode = this.countries.find(({ id }) => id === countryId);

    this.errorZipCode = false;

    if (!zipCode) {
      return;
    }

    this.onDefaultValuesStateAndCountry();
    this.searching = true;
    this.errorZipCode = false;

    this.googleService
      .getZipCode({
        postalCode: zipCode.toString(),
        country: !!countryAlphacode ? countryAlphacode.alpha2Code : "US",
      })
      .subscribe(
        async (res: any) => {
          const response = await this.googleService.getStateAndCity(res);

          this.agency.patchValue(
            {
              state: response.state,
              city: response.city,
            },
            { onlySelf: true, emitEvent: false }
          );

          this.searching = false;
        },
        async () => {
          if (!this.shouldEnableStateAndCity) {
            this.searching = false;
            this.errorZipCode = true;

            const wantWrite = await confirm({
              text: this.translate.instant("ngbAlert.watWriteZipCode"),
              confirmButtonText: this.translate.instant(
                "buttonText.confirmButtonText"
              ),
              cancelButtonText: this.translate.instant(
                "buttonText.cancelButtonText"
              ),
            });

            if (wantWrite) {
              this.agency.get("state").enable({ onlySelf: true });
              this.agency.get("city").enable({ onlySelf: true });
            }
          }
        }
      );
  };

  searchButton = () => {
    let name;

    for (var key in this.search) {
      if (this.search[key] != null) name = key;
    }

    this.onKey(name);
  };

  onKey = (key: string) => {
    let name = key;
    let variable = this.search[key];

    if (!variable) {
      return;
    }

    this.filtered = true;
    if (this.isLoading) return;
    this.isLoading = true;

    this.agencyService
      .search({ [name]: variable, status: this.status }, this.paginate)
      .subscribe(
        (agencies: Agencies[]) => {
          this.isLoading = false;
          const rawData = agencies
            .map((agency: Agency) => {
              return {
                ...agency,
                person: { ...agency.person },
                role: { ...agency.role[0] },
              };
            })
            .filter((user) => !!user.person);

          this.agencies = rawData;
          if (!name && !variable) this.backupAgencies = rawData;
        },
        (error: HttpErrorResponse) => {
          this.isLoading = false;
          this.emptySearch();
          showMessage(
            "¡Error!",
            this.translate.instant(`errors${handlerError(error)}`),
            "error",
            true
          );
        }
      );
  };

  getValidationRegex = () => {
    const pass = this.agency.get("password").value;
    const regex = /(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$/;
    const isEditing = this.isEditing;

    if ((pass === null || !regex.test(pass)) && !isEditing) {
      return (this.validatePass = true);
    }

    return (this.validatePass = false);
  };

  validate = (variable: any) => {
    const regexMin = /(?=.{6,})/;
    const regexNumber = /(?=.*[0-9])/;
    const regexText = /(?=.*[A-Z])/;
    const regexChar = /(?=.*[@#$%^&*-/%+=]).*$/;

    this.requireText = false;
    if (regexText.test(variable)) {
      this.requireText = true;
    }

    this.requireNumber = false;
    if (regexNumber.test(variable)) {
      this.requireNumber = true;
    }

    this.requireCharEs = false;
    if (regexChar.test(variable)) {
      this.requireCharEs = true;
    }

    this.minChar = false;
    if (regexMin.test(variable)) {
      this.minChar = true;
    }
  };

  fPass = () => {
    this.validatePass = true;
  };

  clean = () => {
    this.requireText = false;
    this.requireNumber = false;
    this.requireCharEs = false;
    this.minChar = false;

    this.agency.get("state").disable({ onlySelf: true });
    this.agency.get("city").disable({ onlySelf: true });
    this.agency.patchValue({ agency_settings: null }, { onlySelf: true });
  };

  formating = (phone: any) => {
    return phone ? formatingPhone(phone) : "";
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  goToEmployees = (agency: Agency) => {
    return this.router.navigate([
      `/dashboard/employee/${agency.id}`,
      { status: agency.status, name_agency: agency.person.name },
    ]);
  };

  clearInput = (name: string) => {
    for (var key in this.search) {
      if (key != name && this.search[key] != null) {
        this.search[key] = null;
      }
    }

    let backup = [...this.backupAgencies];
    this.agencies = backup.filter(({ status }) => status === this.status);
  };

  toggleButtonByOnlyAdmin = (): boolean => {
    if (!!this.user) {
      const { agency } = this.user;
      const tier = !!agency ? agency.tier : this.user.tier;

      return tier === TierId.TIER_ADMIN;
    }
  };

  shouldToggleStatus = async (userId: number) => {
    const wantStatus = await confirm({
      text: this.translate.instant("ngbAlert.wantToChangeStatus"),
      confirmButtonText: this.translate.instant("buttonText.confirmButtonText"),
      cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
    });

    if (wantStatus) {
      this.agencyService.changeStatus({ id: userId }).subscribe(
        (agency: Agency) => {
          this.submitted = false;

          this.shoulderUpdateAgencyStatus(agency);

          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.updateUser"),
            "success",
            true
          );
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;

          if (typeof error.error === "string") {
            return showMessage(null, error.error, "error", true);
          }

          this.handleError(error);
        }
      );
    }
  };

  isToggleAgency = (ngbDisplayAgency: TemplateRef<any>, key: number) => {
    this.displayAgency = this.agencies[key];

    if (!!this.displayAgency) {
      this.modalRef = this.ngbModal.open(ngbDisplayAgency);
    }
  };
}
