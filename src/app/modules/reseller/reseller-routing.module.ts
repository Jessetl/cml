import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SupervisorComponent } from "./supervisors/supervisor.component";
import { SellersComponent } from "./sellers/sellers.component";

const routes: Routes = [
  {
    path: "supervisors",
    component: SupervisorComponent,
  },
  {
    path: "sellers",
    component: SellersComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResellerRoutingModule {}

export const routingComponents = [SupervisorComponent, SellersComponent];
