import { Component, OnInit, TemplateRef, OnDestroy } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { User, Users, Role as Profile, Country } from "@models";
import { Image, Uploaded, BanckingForm, Bancking } from "@models";
import { PropsValue, FormUser } from "@models";
import { SupervisorService, ProfileService } from "@core/service";
import { CountryService, InformationService } from "@core/service";
import {
  CREATE,
  EDIT,
  DELETE,
  REAL_TIME,
  STATUS_ACTIVE,
  STATUS_INACTIVE,
} from "@shared/utils";
import { confirm, showMessage, rawError, handlerError } from "@shared/utils";
import { TierId, countriesLang, Socket } from "@shared/utils";
import { formatingPhone, profilesLang } from "@shared/utils";
import { ActivatedRoute } from "@angular/router";

import { on, unsubscribeOf } from "jetemit";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-supervisor",
  templateUrl: "./supervisor.component.html",
  styles: [],
})
export class SupervisorComponent implements OnInit, OnDestroy {
  private modalRef: NgbModalRef;

  public isLoading: boolean = true;
  public filtered: boolean = false;
  public submitted: boolean = false;
  public crud: number;
  public user: User;
  public data: User;
  public tiers: PropsValue[];
  public users: User[];
  public backupUsers: User[];
  public profiles: Profile[];
  public countries: Country[];
  public images: Image[] = [];
  public status: number = STATUS_ACTIVE;

  public errors: Array<any>;
  //routeHome
  public readonly home: string;

  public search = {
    name: null,
    email: null,
    phone: null,
    cellphone: null,
    id: null,
  };

  constructor(
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private supervisorService: SupervisorService,
    private profileService: ProfileService,
    private infoService: InformationService,
    private countryService: CountryService,
    private store: Store<fromRoot.State>,
    private route: ActivatedRoute
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.home = this.route.snapshot.paramMap.get("home");
  }

  ngOnInit(): void {
    this.getSupervisors();

    this.profileService.getProfiles().subscribe(
      (profiles: Profile[]) => {
        this.profiles = profilesLang(profiles, this.translate.getDefaultLang());
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.tiers = [
      {
        value: TierId.TIER_SUPERVISOR,
        name: this.translate.instant("tierId.supervisor"),
      },
    ];

    on(REAL_TIME.NEW_SUPERVISOR, (_: number) => {
      if (!this.filtered) {
        this.getSupervisors();
      }
    });
  }

  ngOnDestroy(): void {
    unsubscribeOf(REAL_TIME.NEW_SUPERVISOR);
  }

  getSupervisors = () => {
    this.isLoading = true;

    this.supervisorService.getResellers().subscribe(
      (users: Users[]) => {
        this.isLoading = false;
        const rawData = users
          .map((user: User) => {
            const { person } = user;

            return {
              ...user,
              agency: user.agency[0] || null,
              person: {
                ...user.person,
                format_phone: formatingPhone(user.person.phone),
                format_cellphone: formatingPhone(user.person.cellphone),
              },
              role: { ...user.role[0] },
            };
          })
          .filter((user) => !!user.person);

        this.backupUsers = rawData;
        this.users = rawData.filter(({ status }) => status === this.status);
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;

        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  changeStatus = () => {
    this.status = this.status ? STATUS_INACTIVE : STATUS_ACTIVE;

    let users = [...this.backupUsers];
    this.users = users.filter(({ status }) => status === this.status);
  };

  resetSearchFields = () => {
    Object.keys(this.search).forEach((fieldName) => {
      return (this.search[fieldName] = null);
    });
  };

  clear = () => {
    this.filtered = false;

    this.resetSearchFields();
    this.getSupervisors();
  };

  clearInput = (name: string) => {
    for (var key in this.search) {
      if (key !== name && this.search[key] !== null) {
        this.search[key] = null;
      }
    }
    let backup = [...this.backupUsers];
    this.users = backup.filter(({ status }) => status === this.status);
  };

  onKey = (key: string) => {
    let name = key;
    let variable = this.search[key];

    if (!!variable) {
      if (!!!this.isLoading) {
        this.filtered = true;
        this.isLoading = true;

        this.supervisorService
          .getResellersBySearch({ [name]: variable, status: this.status })
          .subscribe(
            (users: Users[]) => {
              this.isLoading = false;
              const rawData = users
                .map((user: User) => {
                  return {
                    ...user,
                    person: { ...user.person },
                    role: { ...user.role[0] },
                  };
                })
                .filter((user) => !!user.person);

              this.users = rawData;
              if (!name && !variable) this.backupUsers = rawData;
            },
            (error: HttpErrorResponse) => {
              this.isLoading = false;
              this.resetSearchFields();

              showMessage(
                "¡Error!",
                this.translate.instant(`errors${handlerError(error)}`),
                "error",
                true
              );
            }
          );
      }
    }
  };

  searchButton = () => {
    let name;

    for (var key in this.search) {
      if (this.search[key] !== null) {
        name = key;
      }
    }

    this.onKey(name);
  };

  shouldToggleCreate = (ngbUserCreate: TemplateRef<any>) => {
    this.errors = null;
    this.crud = CREATE;

    this.modalRef = this.ngbModal.open(ngbUserCreate);
  };

  shouldToggleUpdate = (ngbUserUpdate: TemplateRef<any>, userId: number) => {
    this.errors = null;
    this.crud = EDIT;

    const user: User = this.users.find(({ id }) => id === userId);

    if (!!user) {
      this.data = user;

      this.modalRef = this.ngbModal.open(ngbUserUpdate);
    }
  };

  shouldToggleLink = async (userId: number) => {
    const user: User = this.users.find(({ id }) => id === userId);

    if (!!user) {
      const wantStatusLink = await confirm({
        text: this.translate.instant("ngbAlert.wantToChangeStatusLink"),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmButtonText"
        ),
        cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
      });

      if (wantStatusLink) {
        this.supervisorService.link({ id: userId }).subscribe(
          (user: User) => {
            this.shoulderUpdateUser(user);

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.updateUser"),
              "success",
              true
            );
          },
          (error: HttpErrorResponse) => {
            if (typeof error.error === "string") {
              return showMessage(null, error.error, "error", true);
            }

            this.handleError(error);
          }
        );
      }
    }
  };

  shouldToggleUpload = (ngbUploadImage: TemplateRef<any>, userId: number) => {
    this.errors = null;

    const user: User = this.users.find(({ id }) => id === userId);

    if (!!user) {
      this.data = user;

      this.modalRef = this.ngbModal.open(ngbUploadImage);
    }
  };

  shouldToggleUpdateBank = (
    ngbUserBancking: TemplateRef<any>,
    userId: number
  ) => {
    this.errors = null;

    const user: User = this.users.find(({ id }) => id === userId);

    if (!!user) {
      this.data = user;

      this.modalRef = this.ngbModal.open(ngbUserBancking);
    }
  };

  shouldToggleStatus = async (userId: number) => {
    const wantStatus = await confirm({
      text: this.translate.instant("ngbAlert.wantToChangeStatus"),
      confirmButtonText: this.translate.instant("buttonText.confirmButtonText"),
      cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
    });

    if (wantStatus) {
      this.supervisorService.status({ id: userId }).subscribe(
        (user: User) => {
          this.submitted = false;

          this.shoulderUpdateUserStatus(user);

          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.updateUser"),
            "success",
            true
          );
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;

          if (typeof error.error === "string") {
            return showMessage(null, error.error, "error", true);
          }

          this.handleError(error);
        }
      );
    }
  };

  isToggleUser = (ngbDisplayUser: TemplateRef<any>, key: number) => {
    this.data = this.users[key];

    if (!!this.data) {
      this.modalRef = this.ngbModal.open(ngbDisplayUser);
    }
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? this.user.role.assignments
              .filter(
                (assignment) =>
                  assignment.submodule.path === "resellers/supervisors"
              )
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  toggleButtonByOnlyAgency = (): boolean => {
    if (!!this.user) {
      return (
        this.user.tier === TierId.TIER_ADMIN ||
        this.user.tier === TierId.TIER_AGENCY
      );
    }
  };

  shouldMakeUser = (user: User) => {
    const resUser = {
      ...user,
      person: { ...user.person },
      role: { ...user.role[0] },
      id: user.id,
    };

    this.users = [...this.users, resUser];
    this.updateUserBackup(resUser, CREATE);
  };

  shoulderUpdateUser = (user: User) => {
    let keyUser = this.users.findIndex(({ id }) => id === user.id);
    let resUser = [...this.users];

    resUser[keyUser] = {
      ...user,
      person: { ...user.person },
      role: { ...user.role[0] },
      id: user.id,
    };

    this.users = resUser;
    this.updateUserBackup(user, EDIT);
  };

  shoulderUpdateUserStatus = (user: User) => {
    let resUser = [...this.users];
    this.users = resUser.filter(({ id }) => id !== user.id);
    this.updateUserBackup(user, EDIT);
  };

  shoulderUpdateBank = (bancking: Bancking) => {
    let keyUser = this.users.findIndex(({ id }) => id === bancking.user_id);
    let resUser = [...this.users];

    resUser[keyUser] = {
      ...resUser[keyUser],
      bancking: bancking,
    };

    this.users = resUser;
    this.updateUserBackup(resUser[keyUser], EDIT);
  };

  updateUserBackup = (user: User, type: number) => {
    switch (type) {
      case CREATE:
        return (this.backupUsers = [...this.backupUsers, user]);
      case EDIT:
        let resUser = [...this.backupUsers];
        const keyUser = this.backupUsers.findIndex(({ id }) => id === user.id);
        const roleUser = user.role[0] || user.role;

        resUser[keyUser] = {
          ...user,
          person: { ...user.person },
          role: { ...roleUser },
          id: user.id,
        };

        return (this.backupUsers = resUser);
      case DELETE:
        return (this.backupUsers = this.backupUsers.filter(
          ({ id }) => id !== user.id
        ));
      default:
        return this.backupUsers;
    }
  };

  responseFiles = (data: Uploaded) => {
    if (!!!this.submitted) {
      this.submitted = true;

      this.supervisorService.upload(this.data.id, data).subscribe(
        (user: User) => {
          this.submitted = false;
          this.shoulderUpdateUser(user);

          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.uploadImages"),
            "success",
            true
          );

          this.onCloseNgbModal();
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
    }
  };

  onSubmit = (user: FormUser) => {
    const isEditing = this.crud;

    if (!!!this.submitted) {
      this.submitted = true;

      switch (isEditing) {
        case EDIT:
          return this.supervisorService.update(user).subscribe(
            (user: User) => {
              this.submitted = false;

              this.shoulderUpdateUser(user);

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.updateUser"),
                "success",
                true
              );

              this.modalRef.close();
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
        case CREATE:
          return this.supervisorService.store(user).subscribe(
            (user: User) => {
              this.submitted = false;

              this.shouldMakeUser(user);

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.createUser"),
                "success",
                true
              );

              this.modalRef.close();
              Socket.emit(REAL_TIME.SUPERVISOR, user.user_id);
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
      }
    }
  };

  responseBancking = (bancking: BanckingForm) => {
    if (!!!this.submitted) {
      this.submitted = true;

      this.infoService
        .updateBancking({
          ...bancking,
          user_id: this.data.id,
        })
        .subscribe(
          (data) => {
            this.submitted = false;

            this.shoulderUpdateBank(data);

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.updateUser"),
              "success",
              true
            );

            this.modalRef.close();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;

            const errorResponse = handlerError(error);

            if (typeof errorResponse === "string") {
              return showMessage(
                "¡Error!",
                this.translate.instant(`errors${errorResponse}`),
                "error",
                true
              );
            }

            this.errors = rawError(errorResponse);
          }
        );
    }
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      return showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };
}
