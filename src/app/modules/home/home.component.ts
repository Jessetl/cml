import { Component, OnInit, TemplateRef, OnDestroy } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";

import { DashboardService } from "@core/service";

import { Dashboard, User, Inventory, Package, Country } from "@models";
import { Notification } from "@models";
import { showMessage, handlerError, TierId, CHAT } from "@shared/utils";
import { getCountryNameLang, TIER_AGENCY } from "@shared/utils";
import { getServiceDescriptionLang, ENTRY_IN } from "@shared/utils";
import { TIER_LOCKER, ImagesLocker } from "@shared/utils";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { REAL_TIME, langNotification } from "@shared/utils";

import { emit, on, unsubscribeOf } from "jetemit";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-home",
  templateUrl: "./home.component.html",
  styles: [],
})
export class HomeComponent implements OnInit, OnDestroy {
  private modalRef: NgbModalRef;
  public dashboard: Dashboard;
  public user: User;
  public messages: number = 0;
  public submitted: boolean = true;
  public inventory: Package[];
  public promotions: Package[];
  public images: any[];
  public notifications: Notification[] = [];

  constructor(
    private ngbModal: NgbModal,
    private dashboardService: DashboardService,
    private translate: TranslateService,
    private store: Store<fromRoot.State>,
    private router: Router
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    this.images = this.getImagesConsolidation();
    let lang = this.translate.getDefaultLang();
    this.dashboardService.getDashboard().subscribe(
      (dashboard: Dashboard) => {
        this.submitted = false;

        this.notifications = dashboard.notifications.map((notify) => {
          return {
            ...notify,
            content: langNotification(notify, lang),
          };
        });

        this.dashboard = dashboard;

        this.messages = dashboard.messages;
        this.promotions = dashboard.promotions;

        this.inventory = this.inventories_stock(dashboard.inventory);

        if (this.messages > 0) {
          emit(CHAT.UNREAD_MESSAGE, this.messages);
        }
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    on(REAL_TIME.NEW_NOTIFICATION, (notification: Notification) => {
      if (this.user.tier === notification.tier) {
        let notify = {
          ...notification,
          content: langNotification(notification, lang),
        };
        this.setNotifications(notify);
      }
    });
  }

  ngOnDestroy(): void {
    unsubscribeOf(REAL_TIME.NEW_NOTIFICATION);
  }

  setNotifications = (notification: Notification) => {
    let exist = this.notifications.find(({ id }) => id === notification.id);

    if (!!exist) {
      let filter = this.notifications.filter(
        ({ id }) => id !== notification.id
      );
      this.notifications = [notification, ...filter];
      return;
    }
    this.notifications.unshift(notification);
  };
  getNameByLanguage = (promotion: Package): string => {
    return (
      getCountryNameLang(promotion.country, this.translate.getDefaultLang()) +
      " - " +
      getServiceDescriptionLang(promotion, this.translate.getDefaultLang())
    );
  };

  toogleDifferentTo = (): boolean => {
    if (!!this.user) {
      return this.user.tier !== TierId.TIER_LOCKERS;
    }
  };

  toogleByTierWithPermission = (type: number, name: string): boolean => {
    if (!!this.user) {
      const { tier } = this.user;

      const module =
        tier !== TierId.TIER_ADMIN && tier !== TIER_LOCKER
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === name)
              .find((assignment) => assignment.type === type)
          : null;

      return tier === TierId.TIER_ADMIN || !!module;
    }
  };

  toggleButtonByTierId = (type: number, name_path: string): Boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN && this.user.tier !== TIER_LOCKER
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === name_path)
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  goTo = (ruta: string) => {
    this.router.navigate([`/dashboard/${ruta}`, { home: true }]);
  };

  inventories_stock = (inventories: Package[]): Package[] => {
    let lang = this.translate.getDefaultLang();
    return inventories
      .map((i) => {
        return {
          ...i,
          description: getServiceDescriptionLang(i, lang),
          stock: this.getLastMovement(i.last_movement),
        };
      })
      .filter((i) => {
        return !!i.last_movement && i.stock < 0;
      });
  };

  getLastMovement = (inventory: Inventory): number => {
    if (!!inventory) {
      return inventory.movement === ENTRY_IN
        ? inventory.current_stock + inventory.amount
        : inventory.current_stock - inventory.amount;
    }

    return 0;
  };

  userAgency = (): boolean => {
    let tier = this.user.tier;
    if (!!this.user.agency) {
      tier = !!this.user.agency[0]
        ? this.user.agency[0].tier
        : this.user.agency.tier;
    }
    return tier !== TIER_AGENCY;
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
  };

  openModal = async (ngbModal: TemplateRef<any>) => {
    this.modalRef = this.ngbModal.open(ngbModal, {
      size: "md",
    });
  };

  getImagesConsolidation = () => {
    let lang = this.translate.getDefaultLang();
    return ImagesLocker(lang);
  };

  getNameCountry = (country: Country) => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };
}
