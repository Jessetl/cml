import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { SignaturePadModule } from "angular2-signaturepad";
import { FileSaverModule } from "ngx-filesaver";
import { MomentModule } from "ngx-moment";
import { ChartsModule } from "ng2-charts";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ClipboardModule } from "ngx-clipboard";
import { SharedModule } from "@shared";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { NgxMaskModule, IConfig } from "ngx-mask";

import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { PERFECT_SCROLLBAR_CONFIG } from "ngx-perfect-scrollbar";
import { PerfectScrollbarConfigInterface } from "ngx-perfect-scrollbar";

import { BnNgIdleService } from "bn-ng-idle";

import { routingComponents } from "./dashboard-routing.module";
import { DashboardRoutingModule } from "./dashboard-routing.module";

import { NgxStripeModule } from "ngx-stripe";
import { AngularEditorModule } from "@kolkov/angular-editor";
import { environment } from "environments/environment";

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
};

const maskConfig: Partial<IConfig> = {
  validation: false,
};

const PerfectScroll = {
  provide: PERFECT_SCROLLBAR_CONFIG,
  useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
};

@NgModule({
  declarations: [routingComponents],
  providers: [BnNgIdleService, PerfectScroll],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SignaturePadModule,
    NgxStripeModule.forRoot(environment.stripeKey),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgxMaskModule.forRoot(maskConfig),
    BsDatepickerModule.forRoot(),
    PerfectScrollbarModule,
    FileSaverModule,
    ChartsModule,
    MomentModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    ClipboardModule,
    AngularEditorModule,
  ],
})
export class DashboardModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
