import { Component, OnInit, OnDestroy } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { trigger, state, style } from "@angular/animations";
import { transition, animate } from "@angular/animations";

import { emit, on, unsubscribeOf } from "jetemit";

import { TranslateService } from "@ngx-translate/core";
// import { BnNgIdleService } from "bn-ng-idle";
import { Router } from "@angular/router";

import { Socket, TIER_LOCKER, TIER_SELLER } from "@shared/utils";
import { User, Message, Chat, SubModule } from "@models";
import { Module, ModulesAndAssignments, Notification } from "@models";
import { ModuleService, AuthenticationService } from "@core/service";

import { TierId, CHAT, REAL_TIME, STATUS_USER } from "@shared/utils";
import { INCOMPLETE } from "@shared/utils";
import { handlerError, showMessage, TIER_SUPERVISOR } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as menu from "@actions";
import * as user from "@actions";
import * as token from "@actions";
import * as shop from "@actions";

var timeinterval = null;

@Component({
  selector: "cml-sidebar",
  templateUrl: "./sidebar.component.html",
  styles: [],
  animations: [
    trigger("slide", [
      state("up", style({ height: 0 })),
      state("down", style({ height: "*" })),
      transition("up <=> down", animate(200)),
    ]),
  ],
})
export class SidebarComponent implements OnInit, OnDestroy {
  public toggled: boolean;
  public submitted: boolean = false;
  public isLoading: boolean = true;
  public user: User;
  public modules: Module[];
  public _hasBackgroundImage: boolean = false;
  public path: string = null;
  public badge: boolean;
  public support: string;

  public sweetAlert = {
    isAlertVisible: false,
    icon: "error",
    title: "",
    message: "",
  };

  constructor(
    private router: Router,
    private moduleService: ModuleService,
    private authService: AuthenticationService,
    private translate: TranslateService,
    // private bnIdle: BnNgIdleService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.store
      .select(fromRoot.getToggled)
      .subscribe((isToggled: boolean) => (this.toggled = isToggled));
  }

  ngOnInit() {
    const { agency } = this.user;
    const agencyId = !!agency ? agency.id : this.user.id;
    const tier = !!agency ? agency.tier : this.user.tier;
    const userTier = this.user.tier;
    this.support = this.translate.instant("modules.support");

    if (this.user.completed === INCOMPLETE) {
      this.router.navigate(["/auth/complete"]);
      return;
    }

    if (this.user.tier === TIER_LOCKER && this.user.type_pay === null) {
      this.router.navigate(["/auth/subscription"]);
      return;
    }

    Socket.connect();

    this.badge = false;

    // this.bnIdle.startWatching(900).subscribe((isTimedOut: boolean) => {
    //   if (isTimedOut) {
    //     this.logout(isTimedOut);
    //   }
    // });

    this.moduleService.getModules().subscribe(
      (row: ModulesAndAssignments) => {
        const rawModules = row.modules
          .map((module) => ({
            ...module,
            name: this.translate.instant(`modules.${module.name}`),
            active: false,
            sub_module: module.sub_module
              .filter((sub_module) =>
                this.getFiltersByAssignments(row, sub_module)
              )
              .map((submodule) => ({
                ...submodule,
                name: this.translate.instant(`submodules.${submodule.name}`),
              })),
          }))
          .filter(({ sub_module }) => !!sub_module.length);

        this.isLoading = false;
        this.modules = rawModules;
      },
      (error: HttpErrorResponse) => {
        const errorResponse = handlerError(error);

        showMessage(
          "¡Error!",
          this.translate.instant(`errors${errorResponse}`),
          "error",
          true
        );
      }
    );

    if (!!this.user) {
      timeinterval = setInterval(() => {
        Socket.emit(STATUS_USER.CONNECTED, this.user.id);
      }, 5000);
    }

    Socket.on(CHAT.RECEIVE_MESSAGE, (data: Message) => {
      if (this.user.id == data.receiver_id) {
        this.badge = this.router.url == "/dashboard/chat" ? false : true;
      }
      emit(CHAT.RECEIVE_MESSAGE, data);
      emit(CHAT.NEW_MESSAGE, data);
    });

    Socket.on(CHAT.NEW_CHAT, (data: Chat) => {
      if (this.user.id == (data.receiver_id || data.transmitter_id)) {
        this.badge = this.router.url === "/dashboard/chat" ? false : true;
      }
      emit(CHAT.NEW_CHAT, data);
      emit(CHAT.CHAT_NEW, data);
    });

    Socket.on(REAL_TIME.SHIPPING, (id: number) => {
      if (id === agencyId || tier === TierId.TIER_ADMIN) {
        emit(REAL_TIME.SHIPPING, id);
      }
    });

    Socket.on(REAL_TIME.NEW_USER, (data: any) => {
      if (data.id === agencyId || tier === TierId.TIER_ADMIN) {
        emit(REAL_TIME.NEW_USER, data);
      }
    });

    Socket.on(REAL_TIME.NEW_AGENCY, (id: number) => {
      if (id === agencyId || tier === TierId.TIER_ADMIN) {
        emit(REAL_TIME.NEW_AGENCY, id);
      }
    });

    Socket.on(REAL_TIME.NEW_LOCKER, (id: number) => {
      if (id === agencyId || tier === TierId.TIER_ADMIN) {
        emit(REAL_TIME.NEW_LOCKER, id);
      }
    });

    Socket.on(REAL_TIME.NEW_SUPERVISOR, (id: number) => {
      if (
        tier === TierId.TIER_ADMIN &&
        userTier !== TIER_SUPERVISOR &&
        userTier !== TIER_SELLER
      ) {
        emit(REAL_TIME.NEW_SUPERVISOR, id);
      }
    });

    Socket.on(REAL_TIME.NEW_SELLER, (id: number) => {
      if (
        id === this.user.id ||
        (tier === TierId.TIER_ADMIN &&
          userTier !== TIER_SUPERVISOR &&
          userTier !== TIER_SELLER)
      ) {
        emit(REAL_TIME.NEW_SELLER, id);
      }
    });

    Socket.on(REAL_TIME.NEW_EMPLOYEE, (data: any) => {
      if (
        data.id === agencyId ||
        tier === TierId.TIER_ADMIN ||
        agencyId == data.agency
      ) {
        emit(REAL_TIME.NEW_EMPLOYEE, data);
      }
    });

    Socket.on(REAL_TIME.INVOICE, (id: number) => {
      if (id === agencyId || tier === TierId.TIER_ADMIN) {
        emit(REAL_TIME.INVOICE, id);
      }
    });

    Socket.on(REAL_TIME.NEW_NOTIFICATION, (notification: Notification) => {
      if (this.user.tier === notification.tier) {
        emit(REAL_TIME.NEW_NOTIFICATION, notification);
      }
    });

    on(CHAT.UNREAD_MESSAGE, (data) => {
      if (!!data && data > 0) {
        this.badge = this.router.url === "/dashboard/chat" ? false : true;
      }
    });

    on(CHAT.TO_CHAT, () => {
      this.badge = false;
    });
  }

  ngOnDestroy(): void {
    unsubscribeOf(CHAT.UNREAD_MESSAGE);
    unsubscribeOf(CHAT.TO_CHAT);

    if (timeinterval) {
      clearInterval(timeinterval);
    }

    Socket.removeAllListeners();
  }

  gerNameTier = (): string => {
    return this.translate.instant(
      `tierId.${TierId.getDisplayName(this.user.tier)}`
    );
  };

  toggle = (currentMenu: Module): void => {
    if (currentMenu.type === "dropdown") {
      this.modules.forEach((element) => {
        if (element === currentMenu) {
          currentMenu.active = !currentMenu.active;
        } else {
          element.active = false;
        }
      });
    }
  };

  collapse = (currentMenu: Module = null, pathRoute: string = null) => {
    this.store.dispatch(new menu.ToggleMenuAction(!this.toggled));
    this.path = pathRoute;

    if (pathRoute === "chat") {
      this.badge = false;
    }

    if (currentMenu && currentMenu.active) {
      this.toggle(currentMenu);
    }
  };

  getStateReverse = (currentMenu: Module): string => {
    if (currentMenu.active) {
      return "up";
    }
  };

  getState = (currentMenu: Module): string => {
    if (currentMenu.active) {
      return "down";
    } else {
      return "up";
    }
  };

  hasBackgroundImage() {
    return this.hasBackgroundImage;
  }

  getIgnoredModulesByTier = (tierId: number): boolean => {
    return tierId === TierId.TIER_ADMIN || tierId === TierId.TIER_LOCKERS;
  };

  getFiltersByAssignments = (
    row: ModulesAndAssignments,
    submodule: SubModule
  ): boolean => {
    if (!!this.user) {
      return (
        this.getIgnoredModulesByTier(this.user.tier) ||
        row.assignments.some(
          ({ submodule_id }) => submodule.id === submodule_id
        )
      );
    }
  };

  logout(isTimedOut: boolean = false): void {
    if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.authService.logout(this.user.id).subscribe(
      (response: string) => {
        this.submitted = false;

        Socket.disconnect();

        this.store.dispatch(new user.RemoveUserAction(null));
        this.store.dispatch(new token.RemoveTokenAction(null));
        this.store.dispatch(new shop.RemoveShopAction([]));

        const message = isTimedOut
          ? this.translate.instant("notifications.sessionExpired")
          : this.translate.instant(`notifications.${response[0]}`);

        this.router.navigate(["/auth/login"]);

        return showMessage(
          this.translate.instant("messages.congratulations"),
          message,
          "success",
          true
        );
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        const errorResponse = handlerError(error);

        showMessage(
          "¡Error!",
          this.translate.instant(`errors${errorResponse}`),
          "error",
          true
        );
      }
    );
  }
}
