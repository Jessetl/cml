import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Forms
import { FormRemitterComponent } from "../quote/FormRemitter/remitter.component";
import { FormReceiverComponent } from "../quote/FormReceiver/receiver.component";
import { SearchComponent } from "../quote/Search/search.component";
import { FormUserComponent } from "../quote/FormUser/user.component";
import { ChargesComponent } from "../quote/Charges/charges.component";
import { FormPackageComponent } from "../quote/Package/package.component";
import { WatchComponent } from "../quote/Watch/watch.component";
import { PreviewComponent } from "../quote/Preview/preview.component";
import { TagComponent } from "../quote/Tag/tag.component";
import { TimelineComponent } from "../tracking/timeline/timeline.component";
import { DeliveriesComponent } from "../quotes/deliveries/deliveries.component";
import { ProgressComponent } from "../quotes/progress/progress.component";
import { BanckingComponent } from "../banking/bancking.component";

// Components
import { SidebarComponent } from "./sidebar/sidebar.component";
import { LayoutComponent } from "../layout/layout.component";
import { HomeComponent } from "../home/home.component";
import { InformationComponent } from "../information/information.component";
import { CountryComponent } from "../country/country.component";
import { UserComponent } from "../user/user.component";
import { AgencyComponent } from "../agency/agency.component";
import { ProfileComponent } from "../profile/profile.component";
import { AssignmentComponent } from "../assignment/assignment.component";
import { QuoteComponent } from "../quote/quote.component";
import { QuotesComponent } from "../quotes/quotes.component";
import { TrackingComponent } from "../tracking/tracking.component";
import { ChatComponent } from "../chat/chat.component";
import { InvoicingComponent } from "../invoicing/invoicing.component";
import { ReportsComponent } from "../reports/reports.component";
import { ReportCommissionsComponent } from "../reports/commisions/commisions.component";
import { AdditionalsComponent } from "../reports/additionals/additionals.component";
import { LocationComponent } from "../reports/location/location.component";
import { GlobalprofitComponent } from "../reports/globalprofit/globalprofit.component";
import { PayComponent } from "../pay/pay.component";
import { CheckoutComponent } from "../pay/checkout/checkout.component";
import { EmployeeComponent } from "../employee/employee.component";
import { LockerComponent } from "../locker/locker.component";
import { ShippingComponent } from "../shipping/shipping.component";
import { PaystripeComponent } from "../shipping/paystripe/paystripe.component";
import { EmployeesComponent } from "../employees/employees.component";
import { PaymentsComponent } from "../invoicing/payments/payments.component";
import { InventoryComponent } from "../inventory/inventory.component";
import { PaymentSettingsComponent } from "../payment-settings/payment-settings.component";
import { PaypalVerificationComponent } from "../paypal-verification/paypal-verification.component";
import { EmailsComponent } from "../emails/emails.component";
import { AccountStatusComponent } from "../reports/account-status/account-status.component";
import { ProductivityComponent } from "../reports/productivity/productivity.component";
import { PayComponent as PayReportComponent } from "../reports/pay/pay.component";
import { CommodityComponent } from "../commodity/commodity.component";
import { NotificationComponent } from "../notification/notification.component";

const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "",
        component: HomeComponent,
      },
      {
        path: "information",
        component: InformationComponent,
      },
      {
        path: "bancking",
        component: BanckingComponent,
      },
      {
        path: "country",
        component: CountryComponent,
      },
      {
        path: "users",
        component: UserComponent,
      },
      {
        path: "agencies",
        component: AgencyComponent,
      },
      {
        path: "employee/:id",
        component: EmployeeComponent,
      },
      {
        path: "profile",
        component: ProfileComponent,
      },
      {
        path: "permissions",
        component: AssignmentComponent,
      },
      {
        path: "inventory",
        component: InventoryComponent,
      },
      {
        path: "payment/settings",
        component: PaymentSettingsComponent,
      },
      {
        path: "quote",
        component: QuoteComponent,
      },
      {
        path: "quote/search",
        component: TrackingComponent,
      },
      {
        path: "quote/consult",
        component: QuotesComponent,
      },
      {
        path: "locker",
        component: LockerComponent,
      },
      {
        path: "locker/pay",
        component: ShippingComponent,
      },
      {
        path: "invoicing",
        component: InvoicingComponent,
      },
      {
        path: "pay",
        component: PayComponent,
      },
      {
        path: "movements/report",
        component: ReportsComponent,
      },
      {
        path: "commissions/report",
        component: ReportCommissionsComponent,
      },
      {
        path: "additional/report",
        component: AdditionalsComponent,
      },
      {
        path: "location/report",
        component: LocationComponent,
      },
      {
        path: "utility/report",
        component: GlobalprofitComponent,
      },
      {
        path: "billing/report",
        component: AccountStatusComponent,
      },
      {
        path: "pay/report",
        component: PayReportComponent,
      },
      {
        path: "productivity/report",
        component: ProductivityComponent,
      },
      {
        path: "employees",
        component: EmployeesComponent,
      },
      {
        path: "payments/:id",
        component: PaymentsComponent,
      },
      {
        path: "chat",
        component: ChatComponent,
      },
      {
        path: "verification",
        component: PaypalVerificationComponent,
      },
      {
        path: "emails",
        component: EmailsComponent,
      },
      {
        path: "commodity",
        component: CommodityComponent,
      },
      {
        path: "notification",
        component: NotificationComponent,
      },
      {
        path: "services",
        loadChildren: () =>
          import("../services/services.module").then((m) => m.ServicesModule),
      },
      {
        path: "store",
        loadChildren: () =>
          import("../store/store.module").then((m) => m.StoreModule),
      },
      {
        path: "resellers",
        loadChildren: () =>
          import("../reseller/reseller.module").then((m) => m.ResellerModule),
      },
      {
        path: "lockers",
        loadChildren: () =>
          import("../lockers/lockers.module").then((m) => m.LockersModule),
      },
      {
        path: "productivity",
        loadChildren: () =>
          import("../productivity/productivity.module").then(
            (m) => m.ProductivityModule
          ),
      },
      {
        path: "documents",
        loadChildren: () =>
          import("../documents/documents.module").then(
            (m) => m.DocumentsModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}

export const routingComponents = [
  LayoutComponent,
  SidebarComponent,
  HomeComponent,
  InformationComponent,
  CountryComponent,
  UserComponent,
  AgencyComponent,
  ProfileComponent,
  AssignmentComponent,
  QuoteComponent,
  QuotesComponent,
  FormRemitterComponent,
  FormReceiverComponent,
  FormUserComponent,
  ChargesComponent,
  FormPackageComponent,
  WatchComponent,
  PreviewComponent,
  TagComponent,
  TrackingComponent,
  ChatComponent,
  TimelineComponent,
  DeliveriesComponent,
  ProgressComponent,
  InvoicingComponent,
  ReportsComponent,
  ReportCommissionsComponent,
  AdditionalsComponent,
  LocationComponent,
  PayComponent,
  GlobalprofitComponent,
  CheckoutComponent,
  EmployeeComponent,
  LockerComponent,
  ShippingComponent,
  PaystripeComponent,
  EmployeesComponent,
  PaymentsComponent,
  InventoryComponent,
  PaymentSettingsComponent,
  PaypalVerificationComponent,
  SearchComponent,
  EmailsComponent,
  BanckingComponent,
  AccountStatusComponent,
  PayReportComponent,
  ProductivityComponent,
  CommodityComponent,
  NotificationComponent,
];
