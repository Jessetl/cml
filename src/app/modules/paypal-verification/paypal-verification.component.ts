import { Component, OnInit } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { showMessage, handlerError } from "@shared/utils";
import { TranslateService } from "@ngx-translate/core";
import { AccessTokenPayPal, User } from "@models";
import { CustomerPaypal, PayPalVerification } from "@models";
import { PaypalService, SellerService } from "@core/service";
import { formCustomer } from "@shared/utils";
import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as user from "@actions";

@Component({
  selector: "cml-paypal-verification",
  templateUrl: "./paypal-verification.component.html",
  styles: [],
})
export class PaypalVerificationComponent implements OnInit {
  public user: User;
  public paypalCode: string = null;
  public customer: object = {};
  public submitted: boolean = false;
  public loading: boolean = false;
  public email: string = null;

  constructor(
    private translate: TranslateService,
    private store: Store<fromRoot.State>,
    private route: ActivatedRoute,
    private paypalService: PaypalService,
    private sellerService: SellerService,
    private router: Router
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
    this.route.queryParams.subscribe((params: Params) => {
      this.paypalCode = params["code"];
      console.log(this.paypalCode);
    });
  }

  ngOnInit(): void {
    if (!!this.paypalCode) {
      this.getAccessToken();
    }
  }

  getAccessToken = () => {
    this.loading = true;
    this.paypalService.getAccessToken(this.paypalCode).subscribe(
      (access: AccessTokenPayPal) => {
        this.getCustomer(access.access_token);
      },
      (error: any) => {
        this.paypalCode = null;
        this.loading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  getCustomer = (access_token: string) => {
    this.paypalService.getCustomerInfo(access_token).subscribe(
      (customer: CustomerPaypal) => {
        console.log("responde paypal customer ", customer);
        this.customer = formCustomer(customer);
        this.email = customer.emails[0].value;
        this.loading = false;
      },
      (error: any) => {
        this.paypalCode = null;
        this.loading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  verification = () => {
    this.submitted = true;
    this.sellerService.verificationPaypal(this.customer).subscribe(
      (verification: PayPalVerification) => {
        this.updateUser(verification);
        this.submitted = false;
        this.router.navigate(["/dashboard"]);
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  updateUser = (paypal: PayPalVerification) => {
    let userBefore = this.user;
    this.store.dispatch(
      new user.SetUserAction({
        ...userBefore,
        paypal: paypal,
      })
    );
  };
}
