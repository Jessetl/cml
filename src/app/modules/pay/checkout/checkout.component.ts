import { Component, OnInit, OnDestroy } from "@angular/core";
import { Input, Output, EventEmitter, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { BsLocaleService } from "ngx-bootstrap/datepicker";

import { User, Quote, Country } from "@models";
import { QuoteService } from "@core/service";

import { CheckValidators } from "../CheckoutValidators";
import { CheckoutValidators, TransferValidators } from "../CheckoutValidators";

import { TranslateService } from "@ngx-translate/core";

import { rawError, handlerError, TierId } from "@shared/utils";
import { validationsForm, showMessage } from "@shared/utils";
import { PAYMENT_METHOD_CHECK, SIZE_FILE } from "@shared/utils";
import { PAYMENT_METHOD_STRIPE, PAYMENT_METHOD_TRANSFER } from "@shared/utils";
import { getOriginNameLang, getCountryNameLang } from "@shared/utils";
import { Socket, REAL_TIME } from "@shared/utils";
import { getStripeLanguage } from "@shared/utils";

import { StripeService, StripeCardComponent } from "ngx-stripe";
import { StripeElementsOptions } from "@stripe/stripe-js";
import { StripeCardElementOptions } from "@stripe/stripe-js";
import { Token, StripeElements } from "@stripe/stripe-js";

import { defineLocale } from "ngx-bootstrap/chronos";
import { esLocale, enGbLocale } from "ngx-bootstrap/locale";

const language = localStorage.getItem("language") === "es" ? "es" : "en";

language === "es"
  ? defineLocale("es", esLocale)
  : defineLocale("engb", enGbLocale);

@Component({
  selector: "cml-checkout",
  templateUrl: "./checkout.component.html",
  styles: [],
})
export class CheckoutComponent implements OnInit, OnDestroy {
  @ViewChild(StripeCardComponent)
  card: StripeCardComponent;

  @Input() user: User;
  @Input() agencies: User[];
  @Input() quotes: Quote[];
  @Input() onlyQuote: boolean = false;
  @Input() payforCML: boolean;
  @Input() latitude: string;
  @Input() longitude: string;
  @Output() refreshAgencyId = new EventEmitter<number>();
  @Output() setInvoiceByCML = new EventEmitter<number[]>();
  @Output() setInvoiceByAgency = new EventEmitter<number[]>();
  @Output() setIvoiceQuote = new EventEmitter<number>();

  public checkout: FormGroup;
  public transfer: FormGroup;
  public check: FormGroup;
  public amountPayment: number;
  public submitted: boolean = false;
  public errors: any;
  public elements: StripeElements;
  public token: Token;
  public paymentMethod: number = 1;
  public maxDate: Date = new Date();
  public backupQuotes: Quote[];

  public settingsCard: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: "#666ee8",
        color: "#000",
        lineHeight: "40px",
        fontWeight: "300",
        fontSize: "20px",
      },
    },
  };

  public settingsElements: StripeElementsOptions = {
    locale: getStripeLanguage(this.translate.getDefaultLang()),
  };

  constructor(
    private quoteService: QuoteService,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private stripeService: StripeService,
    private localeService: BsLocaleService
  ) {
    this.checkout = this.formBuilder.group(CheckoutValidators);
    this.transfer = this.formBuilder.group(TransferValidators);
    this.check = this.formBuilder.group(CheckValidators);
  }

  ngOnInit() {
    const name = this.getNameTotal();

    this.amountPayment = this.quotes.reduce(function (amount, row) {
      return amount + row[name];
    }, 0);

    if (!!this.user) {
      const { agency } = this.user;
      const tier = !!agency ? agency.tier : this.user.tier;

      if (tier === TierId.TIER_AGENCY) {
        return this.checkout.patchValue({ typeInvoice: 1 }, { onlySelf: true });
      }
    }

    this.checkout.patchValue({ typeInvoice: 0 }, { onlySelf: true });
  }

  ngAfterViewInit() {
    this.localeService.use(language);
  }

  isFieldValid = (field: string): boolean => {
    switch (this.paymentMethod) {
      case PAYMENT_METHOD_STRIPE:
        return (
          this.checkout.get(field).invalid && this.checkout.get(field).touched
        );
      case PAYMENT_METHOD_TRANSFER:
        return (
          this.transfer.get(field).invalid && this.transfer.get(field).touched
        );
      case PAYMENT_METHOD_CHECK:
        return this.check.get(field).invalid && this.check.get(field).touched;
    }
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  setValueType = (type: number) => {
    this.checkout.patchValue({ typeInvoice: type }, { onlySelf: true });
  };

  ngOnDestroy(): void {
    this.checkout.reset();
    this.check.reset();
    this.transfer.reset();
  }

  onToggleOnlyByCML = (): boolean => {
    if (!!this.user) {
      const { agency } = this.user;
      const tier = !!agency ? agency.tier : this.user.tier;

      return tier === TierId.TIER_ADMIN;
    }
  };

  setValueMethod = (type: number) => {
    this.paymentMethod = type;

    switch (this.paymentMethod) {
      case PAYMENT_METHOD_TRANSFER:
        return this.transfer.patchValue(
          { amount: this.amountPayment.toFixed(2) },
          { onlySelf: true }
        );
      case PAYMENT_METHOD_CHECK:
        return this.check.patchValue(
          { amount: this.amountPayment.toFixed(2) },
          { onlySelf: true }
        );
    }
  };

  isAgencyChecked = (): boolean => {
    let defaultValue: boolean = false;

    if (this.checkout.get("typeInvoice").value) {
      defaultValue = true;
      this.checkout.get("agency").enable();
    } else {
      defaultValue = false;
      this.checkout.get("agency").disable({ onlySelf: true });
    }

    return defaultValue;
  };

  onSubmitCML = () => {
    const infoValid = this.checkout.valid;
    const agencyId = this.checkout.get("agency").value;
    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.checkout);
    } else if (this.submitted) {
      return;
    }
    this.submitted = true;

    this.quoteService.invoiceByCML({ ...this.checkout.value }).subscribe(
      (quotes: number[]) => {
        this.submitted = false;
        agencyId ? this.refreshAgencyId.emit(agencyId) : null;
        this.setInvoiceByCML.emit(quotes);
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;

        if (error.error) {
          return showMessage(
            null,
            this.translate.instant(`validators.${error.error}`),
            "warning",
            true
          );
        }

        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  handlePayment = async () => {
    const quoteIds = this.quotes.map(({ id }) => id);
    const name = this.user.person.name;
    this.errors = null;

    const isTransferValid = this.transfer.valid;
    const isCheckValid = this.check.valid;

    if (this.paymentMethod === PAYMENT_METHOD_TRANSFER && !isTransferValid) {
      return validationsForm(this.transfer);
    }

    if (this.paymentMethod === PAYMENT_METHOD_CHECK && !isCheckValid) {
      return validationsForm(this.check);
    }

    if (this.submitted) {
      return;
    }
    if (
      this.paymentMethod === PAYMENT_METHOD_CHECK ||
      this.paymentMethod === PAYMENT_METHOD_TRANSFER
    ) {
      const insufficientAmount = await this.insufficientAmount();
      if (insufficientAmount) {
        return;
      }
    }
    const agencyId = this.checkout.get("agency").value;
    this.submitted = true;

    switch (this.paymentMethod) {
      case PAYMENT_METHOD_STRIPE:
        return this.stripeService
          .createToken(this.card.getCard(), { name })
          .subscribe(
            (response) => {
              this.paymentRequest(response.token);
            },
            (error: Error) => {
              this.submitted = false;
              this.errors = error.message;
            }
          );
      case PAYMENT_METHOD_TRANSFER:
        return this.quoteService
          .payShippingTransfer({
            quoteIds,
            ...this.transfer.getRawValue(),
            latitude: this.latitude,
            longitude: this.longitude,
            agency: agencyId,
          })
          .subscribe(
            (quotes: number[]) => {
              this.submitted = false;
              this.transfer.reset();
              agencyId ? this.refreshAgencyId.emit(agencyId) : null;

              this.emitSocket();
              this.setInvoiceByAgency.emit(quotes);
              this.setInvoiceByCML.emit(quotes);
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;

              const errorResponse = handlerError(error);

              if (typeof errorResponse === "string") {
                return showMessage(
                  "¡Error!",
                  this.translate.instant(`errors${errorResponse}`),
                  "error",
                  true
                );
              }

              this.errors = rawError(errorResponse);
            }
          );
      case PAYMENT_METHOD_CHECK:
        return this.quoteService
          .payShippingCheck({
            quoteIds,
            ...this.check.getRawValue(),
            latitude: this.latitude,
            longitude: this.longitude,
            agency: agencyId,
          })
          .subscribe(
            (quotes: number[]) => {
              this.submitted = false;
              this.check.reset();
              agencyId ? this.refreshAgencyId.emit(agencyId) : null;

              this.emitSocket();
              this.setInvoiceByAgency.emit(quotes);
              this.setInvoiceByCML.emit(quotes);
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;

              const errorResponse = handlerError(error);

              if (typeof errorResponse === "string") {
                return showMessage(
                  "¡Error!",
                  this.translate.instant(`errors${errorResponse}`),
                  "error",
                  true
                );
              }

              this.errors = rawError(errorResponse);
            }
          );
    }
  };

  paymentRequest = (response: Token) => {
    const quoteIds = this.quotes.map(({ id }) => id);
    const agencyId = this.checkout.get("agency").value;

    this.quoteService
      .payShipping({
        amount: parseFloat((this.amountPayment * 100).toFixed(2)),
        token: response.id,
        quoteIds: quoteIds,
        latitude: this.latitude,
        longitude: this.longitude,
        agency: agencyId,
      })
      .subscribe(
        (quotes: number[]) => {
          this.submitted = false;
          agencyId ? this.refreshAgencyId.emit(agencyId) : null;

          this.emitSocket();
          this.setInvoiceByAgency.emit(quotes);
          this.setInvoiceByCML.emit(quotes);
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.errors = error.error.message;
        }
      );
  };

  insufficientAmount = () => {
    let isInvalid: boolean = false;
    const amount =
      this.paymentMethod == PAYMENT_METHOD_TRANSFER
        ? this.transfer.get("amount").value
        : this.check.get("amount").value;

    if (amount < this.amountPayment) {
      isInvalid = true;

      showMessage(
        null,
        this.translate.instant("validations.minimunTotal"),
        "warning",
        true
      );
      return new Promise<boolean>((resolve, _) => {
        resolve(isInvalid);
      });
    }
  };

  onFileChanged = (event: any) => {
    const readerFile = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];

      if (this.maxFileSize(file.size)) {
        this.paymentMethod == PAYMENT_METHOD_TRANSFER
          ? this.transfer.patchValue({ voucher: null }, { onlySelf: true })
          : this.check.patchValue({ voucher: null }, { onlySelf: true });

        return showMessage(
          "",
          this.translate.instant("validations.fileInvalid"),
          "warning",
          true
        );
      }

      if (this.formatTypeImage(file.type)) {
        this.paymentMethod == PAYMENT_METHOD_TRANSFER
          ? this.transfer.patchValue({ voucher: null }, { onlySelf: true })
          : this.check.patchValue({ voucher: null }, { onlySelf: true });

        return showMessage(
          "",
          this.translate.instant("validations.format"),
          "warning",
          true
        );
      }

      readerFile.readAsDataURL(file);

      readerFile.onload = () => {
        const strResult = readerFile.result as string;
        this.paymentMethod == PAYMENT_METHOD_TRANSFER
          ? this.transfer.patchValue({ voucher: strResult }, { onlySelf: true })
          : this.check.patchValue({ voucher: strResult }, { onlySelf: true });
      };
    }
  };

  formatTypeImage = (img: string): boolean => {
    const isTrue: boolean = true;

    switch (img) {
      case "image/jpeg":
        return !isTrue;
      case "image/png":
        return !isTrue;
      case "image/bmp":
        return !isTrue;
      case "application/pdf":
        return !isTrue;
      default:
        return isTrue;
    }
  };

  maxFileSize = (fileSize: number): boolean => {
    const isTrue: boolean = true;
    const sizeMegabytes = fileSize / 1024 / 1024;
    if (sizeMegabytes > SIZE_FILE) {
      return isTrue;
    } else {
      return !isTrue;
    }
  };

  validations = (value: string): string => {
    let name = this.translate.instant(value).toLowerCase();
    return this.translate.instant("validations.required", { name: name });
  };

  payAgency = () => {
    const agency = this.checkout.get("agency").value;
    this.backupQuotes = this.quotes;

    this.quotes = this.quotes.filter((quote) => {
      return (
        quote.user.id == agency ||
        (!!quote.user.agency[0] && quote.user.agency[0].id == agency)
      );
    });

    this.amountPayment = this.quotes.reduce(function (amount, row) {
      return amount + row.total_user;
    }, 0);

    this.payforCML = agency !== null ? true : false;
  };

  goToAgencies = () => {
    this.quotes = this.backupQuotes;
    this.checkout.patchValue({ agency: null }, { onlySelf: true });

    return (this.payforCML = false);
  };

  totalByUser = (quote: Quote) => {
    return quote.user.tier === 5 ||
      (!!quote.user.agency[0] && quote.user.agency[0].tier === 5)
      ? quote.total_user
      : quote.total;
  };

  emitSocket = () => {
    if (!!this.user) {
      const { agency } = this.user;
      const agencyId = !!agency ? agency.id : this.user.id;

      Socket.emit(REAL_TIME.INVOICE, agencyId);
    }
  };

  maxlenghtNumber = (event: Event) => {
    if ((<HTMLInputElement>event.target).value.length >= 4) {
      return false;
    }
  };

  getTotalAmount = (shipping: Quote): number => {
    return shipping[this.getNameTotal()];
  };

  getNameTotal = (): string => {
    return this.onlyQuote ? "total" : "total_user";
  };

  getOriginName = (shipping: Quote): string => {
    return getOriginNameLang(shipping, this.translate.getDefaultLang());
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };
}
