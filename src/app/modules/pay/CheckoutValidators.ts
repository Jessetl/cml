import { FormControl, Validators } from "@angular/forms";

export const CheckoutValidators = {
  typeInvoice: new FormControl(0, [Validators.required]),
  agency: new FormControl({ value: null, disabled: true }),
};

export const TransferValidators = {
  issueDate: new FormControl(new Date(), [Validators.required]),
  origin: new FormControl(null, [Validators.required]),
  lastDigitsOrigin: new FormControl(null, [Validators.required]),
  destiny: new FormControl(null, [Validators.required]),
  lastDigitsDestiny: new FormControl(null, [Validators.required]),
  amount: new FormControl({ value: null, disabled: true }, [
    Validators.required,
  ]),
  reference: new FormControl(null, [Validators.required]),
  voucher: new FormControl(null, [Validators.required]),
};

export const CheckValidators = {
  issueDate: new FormControl(new Date(), [Validators.required]),
  origin: new FormControl(null, [Validators.required]),
  reference: new FormControl(null, [Validators.required]),
  amount: new FormControl({ value: null, disabled: true }, [
    Validators.required,
  ]),
  voucher: new FormControl(null, [Validators.required]),
};

export const StripePaymentValidators = {
  name: [null, Validators.required],
};
