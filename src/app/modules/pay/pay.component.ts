import { Component, OnInit, TemplateRef } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

import { Quote, User, Country } from "@models";
import { QuoteService } from "@core/service";
import { GooglemapsService } from "@core/service";

import { handlerError, showMessage, TierId } from "@shared/utils";
import { getOriginNameLang, getCountryNameLang } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-pay",
  templateUrl: "./pay.component.html",
  styles: [],
})
export class PayComponent implements OnInit {
  private modalRef: NgbModalRef;
  public submitted: boolean = false;
  public errors: any;
  public quotes: Quote[];
  public user: User;
  public agencies: User[];
  public latitude: string;
  public longitude: string;
  public isLoading: boolean;

  constructor(
    private quoteService: QuoteService,
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private store: Store<fromRoot.State>,
    private googleService: GooglemapsService
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    this.isLoading = true;

    this.quoteService.getPackagesToBeInvoiced().subscribe(
      (quotes: Quote[]) => {
        this.isLoading = false;
        this.quotes = quotes;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    if (this.user.tier !== TierId.TIER_AGENCY) {
      this.quoteService.getAgencyWithInvoice().subscribe(
        (agencies: User[]) => {
          this.agencies = agencies;
        },
        (error: HttpErrorResponse) => {
          showMessage(
            "¡Error!",
            this.translate.instant(`errors${handlerError(error)}`),
            "error",
            true
          );
        }
      );
    }

    this.googleService
      .getLocation({ timeout: 15000 })
      .then(({ coords }) => {
        this.latitude = coords.latitude.toString();
        this.longitude = coords.longitude.toString();
      })
      .catch((error) => {
        console.log("error al obtener la ubicacion", error);
      });
  }

  getOriginName = (shipping: Quote): string => {
    return getOriginNameLang(shipping, this.translate.getDefaultLang());
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  refreshAgencyId = (agencyId: number) => {
    this.agencies = this.agencies.filter(({ id }) => id != agencyId);
  };

  setInvoiceByCML = (quotes: number[]) => {
    showMessage(
      null,
      this.translate.instant("notifications.updateQuotes"),
      "success",
      true
    );

    const rawQuotes = this.quotes.filter(({ id }) => {
      return !quotes.includes(id);
    });

    this.quotes = rawQuotes;

    this.onCloseNgbModal();
  };

  setInvoiceByAgency = (quotes: number[]) => {
    showMessage(
      null,
      this.translate.instant("notifications.updateQuotes"),
      "success",
      true
    );

    const rawQuotes = this.quotes.filter(({ id }) => {
      return !quotes.includes(id);
    });

    this.quotes = rawQuotes;

    this.onCloseNgbModal();
  };

  setInvoiceQuotes = async (ngbPayment: TemplateRef<any>) => {
    if (!!this.user) {
      const { agency } = this.user;
      const tier = !!agency ? agency.tier : this.user.tier;

      this.modalRef = this.ngbModal.open(ngbPayment, {
        size: tier === TierId.TIER_AGENCY ? "lg" : "md",
      });
    }
  };

  onCloseNgbModal = (): void => {
    this.modalRef.close();
  };

  totalByUser = (quote: Quote) => {
    return quote.user.tier === 5 ||
      (!!quote.user.agency[0] &&
        quote.user.agency[0].tier === TierId.TIER_AGENCY)
      ? quote.total_user
      : quote.total;
  };

  nameAgency = (user: User) => {
    const agency = user.agency[0];
    return !!agency ? agency.person.name : user.person.name;
  };
}
