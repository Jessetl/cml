import { Component, OnInit, TemplateRef, OnDestroy } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { CommodityFormValidator } from "./FormCommodityValidators";
import { Commodity, User } from "@models";
import { ConmodityService } from "@core/service";
import { UNPROCESSABLE_ENTITY } from "@shared/utils";
import { showMessage, handlerError, TIER_LOCKER } from "@shared/utils";
import { TierId, validationsForm } from "@shared/utils";
import { maxFileSize, formatTypeImage, confirm } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-commodity",
  templateUrl: "./commodity.component.html",
  styles: [],
})
export class CommodityComponent implements OnInit {
  private modalRef: NgbModalRef;

  public user: User;
  public isLoading: boolean = true;
  public filtered: boolean = false;
  public submitted: boolean = false;
  public urlFile: SafeResourceUrl;
  public users: User[];
  public backupUsers: User[];
  public commoditys: Commodity[];
  public commodity: FormGroup;
  public isEditing: boolean = false;
  public changeStatus: boolean = false;
  public image: string = null;
  public imageSrc: string = null;
  public step: number = 1;

  public wantMinInsurance: boolean = false;

  //routeHome
  public readonly home: string;

  public errors: Array<any>;

  public search = {
    name: null,
    email: null,
    phone: null,
    cellphone: null,
    id: null,
  };

  constructor(
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private commodityService: ConmodityService,
    private store: Store<fromRoot.State>,
    private route: ActivatedRoute,
    public domSanitizer: DomSanitizer,
    private formBuilder: FormBuilder
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.home = this.route.snapshot.paramMap.get("home");

    this.commodity = this.formBuilder.group(CommodityFormValidator);
  }

  ngOnInit(): void {
    this.getCommoditys();
  }

  getCommoditys = () => {
    this.isLoading = true;

    this.commodityService.getCommoditys().subscribe(
      (commoditys: Commodity[]) => {
        this.isLoading = false;
        this.commoditys = commoditys;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;

        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  onSubmit = async () => {
    const infoValid = this.commodity.valid;
    const isEditing = this.isEditing;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.commodity);
    } else if (this.submitted) {
      return;
    }

    const requiredDeclared = await this.getValidateDeclared();

    if (requiredDeclared) {
      return;
    }

    const insuranceZero = await this.getValidateInsuranceZero();

    if (insuranceZero) {
      return;
    }
    const isInvalidInsurancePackage = await this.getValidationInsurancePackage();

    if (isInvalidInsurancePackage) {
      return;
    }

    this.submitted = true;

    switch (isEditing) {
      case true:
        return this.commodityService
          .update(this.commodity.getRawValue())
          .subscribe(
            (commodity: Commodity) => {
              this.submitted = false;
              this.commodity.reset();
              this.imageSrc = null;

              this.shoulderUpdateCommodity(commodity);

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.updateCommodity"),
                "success",
                true
              );

              this.modalRef.close();
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              if (error.status === UNPROCESSABLE_ENTITY) {
                return showMessage(
                  null,
                  this.translate.instant("notifications.emptyQuote"),
                  "warning",
                  true
                );
              }
              this.handleError(error);
            }
          );
      case false:
        return this.commodityService
          .create(this.commodity.getRawValue())
          .subscribe(
            (commodity: Commodity) => {
              this.submitted = false;
              this.imageSrc = null;
              this.commodity.reset();
              this.shouldMakeCommodity(commodity);

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.createCommodity"),
                "success",
                true
              );

              this.modalRef.close();
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }
  };

  isFieldValid = (field: string): boolean => {
    return (
      this.commodity.get(field).invalid && this.commodity.get(field).touched
    );
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
    this.commodity.reset();
    this.commodity.get("file").setValidators([]);
    this.commodity.get("alphanumeric").setValidators([]);
    this.imageSrc = null;
    this.changeStatus = false;
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  shouldMakeCommodity = (commodity: Commodity) => {
    this.commoditys.unshift(commodity);
  };

  shoulderUpdateCommodity = (commodity: Commodity) => {
    let keyCommodity = this.commoditys.findIndex(
      ({ id }) => id === commodity.id
    );
    let resCommoditys = [...this.commoditys];

    resCommoditys[keyCommodity] = { ...commodity };

    this.commoditys = resCommoditys;
  };

  shouldToggleUpdate = (
    ngbConmmodityUpdate: TemplateRef<any>,
    idCommodity: number,
    status: boolean
  ) => {
    this.errors = null;

    const commodity: Commodity = this.commoditys.find(
      ({ id }) => id === idCommodity
    );
    if (!!status) {
      this.commodity.get("alphanumeric").setValidators([Validators.required]);
    }
    if (!!commodity) {
      this.imageSrc = !!status ? null : commodity.file_url;
      commodity.amount < 100 && commodity.amount > 0
        ? this.commodity.get("insured_value").disable()
        : this.commodity.get("insured_value").enable();
      this.commodity.patchValue({ ...commodity, file: null });
      this.commodity.get("file").setValidators([]);
      this.changeStatus = status;
      this.openModal(ngbConmmodityUpdate);
      this.isEditing = true;
    }
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.user) {
      const module =
        this.user.tier !== TierId.TIER_ADMIN && this.user.tier !== TIER_LOCKER
          ? this.user.role.assignments
              .filter((assignment) => assignment.submodule.path === "commodity")
              .find((assignment) => assignment.type === type)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  shouldToggleCreate = (ngbCommodityCreate: TemplateRef<any>) => {
    this.errors = null;
    this.isEditing = false;
    this.imageSrc = null;
    this.commodity.reset();
    this.commodity.get("file").setValidators([Validators.required]);
    this.commodity.get("alphanumeric").setValidators([]);
    this.commodity.get("insured_value").enable();

    this.openModal(ngbCommodityCreate);
  };

  shouldToggleView = (
    ngbCommodityView: TemplateRef<any>,
    commodityId: number
  ) => {
    this.errors = null;
    const commodity: Commodity = this.commoditys.find(
      ({ id }) => id === commodityId
    );

    if (!!commodity) {
      this.image = commodity.file_url;

      if (this.getFileExtension(commodity.file_url) === "pdf") {
        this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(
          commodity.file_url
        );
        this.step = 2;
      } else {
        this.openModal(ngbCommodityView);
      }
    }
  };

  openModal = async (ngbModal: TemplateRef<any>) => {
    this.modalRef = this.ngbModal.open(ngbModal, {
      size: "md",
    });
  };

  onFileChanged = (event: any) => {
    const readerFile = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];

      if (maxFileSize(file.size)) {
        this.commodity.patchValue({ file: null });

        return showMessage(
          "",
          this.translate.instant("validations.fileInvalid"),
          "warning",
          true
        );
      }

      if (formatTypeImage(file.type)) {
        this.commodity.patchValue({ file: null });

        return showMessage(
          "",
          this.translate.instant("validations.format"),
          "warning",
          true
        );
      }

      readerFile.readAsDataURL(file);
      readerFile.onload = () => {
        const strResult = readerFile.result as string;
        this.imageSrc = strResult;
        this.commodity.patchValue({ file: strResult }, { onlySelf: true });
      };
    }
  };

  getFileExtension = (filename) => {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  };

  maxInsuredValue = () => {
    const amount = this.commodity.get("amount").value;
    const insuredValue = this.commodity.get("insured_value").value;

    if (insuredValue > 0 && insuredValue < 100) {
      showMessage(
        "",
        this.translate.instant("validations.minInsuranceAmount", {
          min: 100,
        }),
        "warning",
        true
      );
      return this.commodity.patchValue(
        { insured_value: 100 },
        { onlySelf: true }
      );
    } else if (insuredValue > amount && amount >= 100) {
      showMessage(
        "",
        this.translate.instant("validations.maxInsuredValue"),
        "warning",
        true
      );
      return this.commodity.patchValue(
        { insured_value: amount },
        { onlySelf: true }
      );
    } else if (insuredValue > amount && amount > 1 && amount < 100) {
      showMessage(
        "",
        this.translate.instant("validations.minInsurance", {
          min: 100,
        }),
        "warning",
        true
      );
      this.commodity.get("insured_value").disable();
      return this.commodity.patchValue(
        { insured_value: 100 },
        { onlySelf: true }
      );
    }
  };

  minInsured = async () => {
    const amount = this.commodity.get("amount").value;

    if (amount < 1) {
      return showMessage(
        "",
        this.translate.instant("validations.negativeDeclared"),
        "warning",
        true
      );
    }
    if (amount < 100 && amount > 0) {
      const wantMinInsurancePackage = await confirm({
        text: this.translate.instant("validations.minInsurance", {
          min: 100,
        }),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmButtonText"
        ),
        cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
      });

      if (wantMinInsurancePackage) {
        this.wantMinInsurance = true;
        this.commodity.get("insured_value").disable();
        return this.commodity.patchValue(
          { insured_value: 100 },
          { onlySelf: true }
        );
      }
    }

    this.wantMinInsurance = false;
    this.commodity.get("insured_value").enable();
    this.commodity.patchValue({ insured_value: null }, { onlySelf: true });
  };

  getValidationInsurancePackage = async () => {
    let isInvalid: boolean = false;
    const insurance: boolean = this.commodity.get("insured_value").value;

    if (!insurance) {
      showMessage(
        null,
        this.translate.instant("validations.insuranceRequired"),
        "warning",
        true
      );

      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidateDeclared = () => {
    let isInvalid: boolean = false;
    const declared = this.commodity.get("amount").value;

    let name = this.translate.instant("formBuilder.amount");

    if (declared === 0) {
      showMessage(
        null,
        this.translate.instant("validations.required", { name: name }),
        "warning",
        true
      );

      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidateInsuranceZero = () => {
    let isInvalid: boolean = false;
    const insuredValue = this.commodity.get("insured_value").value;

    if (insuredValue === 0) {
      showMessage(
        null,
        this.translate.instant("validations.insuranceValueZero"),
        "warning",
        true
      );

      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };
}
