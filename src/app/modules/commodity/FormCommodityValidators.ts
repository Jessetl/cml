import { FormControl, Validators } from "@angular/forms";

export const CommodityFormValidator = {
  id: new FormControl(null),
  description: new FormControl(null, [Validators.required]),
  amount: new FormControl(null, [Validators.required]),
  file: new FormControl(null, []),
  insured_value: new FormControl(null, [Validators.required]),
  alphanumeric: new FormControl(null),
};
