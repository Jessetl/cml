import { Component, TemplateRef } from "@angular/core";
import { OnInit, OnDestroy, ViewChild } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { FormGroup, FormBuilder } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { SignaturePad } from "angular2-signaturepad";

import { Country, User, Package, ChargeForm, Content } from "@models";
import { RemitterReceiverForm, Quote, Person } from "@models";

import { TierId, rawError, handlerError } from "@shared/utils";
import { confirm, showMessage, TypeContent, ShippingType } from "@shared/utils";
import { PACKAGE_MARITIME, PACKAGE_LAND, PACKAGE_AERIAL } from "@shared/utils";
import { REAL_TIME, STATUS_ACTIVE } from "@shared/utils";
import { Socket, countriesLang } from "@shared/utils";
import { getCountryNameLang, getChargeNameLang } from "@shared/utils";

// Forms
import { FormRemitterComponent } from "./FormRemitter/remitter.component";
import { FormReceiverComponent } from "./FormReceiver/receiver.component";
import { ChargesComponent } from "./Charges/charges.component";
import { FormPackageComponent } from "./Package/package.component";
import { WatchComponent } from "./Watch/watch.component";
import { PreviewComponent } from "./Preview/preview.component";
import { FormUserComponent } from "./FormUser/user.component";
import { TagComponent } from "./Tag/tag.component";
import { SearchComponent } from "./Search/search.component";

import { CountryService, GooglemapsService } from "@core/service";
import { QuoteService, AgencyService, PackageService } from "@core/service";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

import { FormPackageValidator } from "./FormQuoteValidator";

interface Type {
  id: number;
  name: string;
}

@Component({
  selector: "cml-quote",
  templateUrl: "./quote.component.html",
  styles: [],
})
export class QuoteComponent implements OnInit, OnDestroy {
  @ViewChild(FormRemitterComponent)
  FormRemitter: FormRemitterComponent;

  @ViewChild(FormReceiverComponent)
  FormReceiver: FormReceiverComponent;

  @ViewChild(FormUserComponent)
  FormUser: FormUserComponent;

  @ViewChild(ChargesComponent)
  Charges: ChargesComponent;

  @ViewChild(FormPackageComponent)
  Package: FormPackageComponent;

  @ViewChild(WatchComponent)
  Watch: WatchComponent;

  @ViewChild(SignaturePad)
  signaturePad: SignaturePad;

  @ViewChild(PreviewComponent)
  Preview: PreviewComponent;

  @ViewChild(TagComponent)
  Tag: TagComponent;

  private modalRef: NgbModalRef;
  public isLoading: boolean = true;
  public isEditing: boolean = false;
  public isLocker: boolean = false;
  public step: number = 1;
  public membershipType: number;
  public onStepForm: number = 1;
  public signature: string = null;
  public signatureUrl: string = null;
  public chargeKey: number;
  public currentUser: User;
  public countries: Country[];
  public agencies: User[];
  public charges: ChargeForm[];
  public charge: ChargeForm;
  public remitter: RemitterReceiverForm;
  public receiver: RemitterReceiverForm;
  public user: RemitterReceiverForm;
  public package: FormGroup;
  public types: Type[];
  public objTypes: [][];
  public packages: Type[];
  public objPackages: {};
  public descriptions: Package[];
  public contents: Content[];
  public submitted: boolean;
  public errors: Array<any>;
  public countryId: number = null;
  public userTier: number = null;
  public userTierUpdate: boolean = false;
  public imageSrc: string = null;
  public quote: Quote;
  public latitude: string;
  public longitude: string;

  public signatureConfig: Object = {
    minWidth: 2,
    canvasHeight: 150,
  };

  constructor(
    private formBuilder: FormBuilder,
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private countryService: CountryService,
    private agencyService: AgencyService,
    private quoteService: QuoteService,
    private packageService: PackageService,
    private store: Store<fromRoot.State>,
    private googleService: GooglemapsService
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.currentUser = user));
  }

  ngOnInit() {
    this.createPackageForm();

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        let countriesLanguage = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );

        this.countries = countriesLanguage.filter(
          ({ status, content }) =>
            status === STATUS_ACTIVE && content.length > 0
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.googleService
      .getLocation({ timeout: 15000 })
      .then(({ coords }) => {
        this.latitude = coords.latitude.toString();
        this.longitude = coords.longitude.toString();
      })
      .catch((error) => {
        console.log("error al obtener la ubicacion", error);
      });

    this.isLoading = false;
  }

  ngAfterViewInit() {
    if (this.step === 4) {
      this.signaturePad.set("minWidth", 2); // set szimek/signature_pad options at runtime
    }
  }

  ngOnDestroy(): void {
    this.package.reset();
  }

  getCountriesWithPackages = (countries: Country[]): Country[] => {
    return (
      !!countries &&
      countries.filter((c) => !!c.packages && c.packages.length > 0)
    );
  };

  setValuesDefault = () => {
    this.countryId = null;
    this.charges = [];
    this.receiver = null;
    this.remitter = null;
    this.signatureUrl = null;
    this.signature = null;
    this.step = 1;
  };

  createPackageForm = () => {
    this.package = this.formBuilder.group(FormPackageValidator);
  };

  getTypeName = (type: number): string => {
    switch (type) {
      case PACKAGE_MARITIME:
        return this.translate.instant("select.maritime");
      case PACKAGE_LAND:
        return this.translate.instant("select.land");
      case PACKAGE_AERIAL:
        return this.translate.instant("select.aerial");
    }
  };

  shippingTypeName = (): string => {
    switch (this.userTier) {
      case ShippingType.REMITTER:
        return this.translate.instant("cardContent.remitter");
      case ShippingType.RECEIVER:
        return this.translate.instant("cardContent.receiver");
    }
  };

  getNamePackage = (): string => {
    return (
      getChargeNameLang(this.charge, this.translate.getDefaultLang()) +
      ` (${this.translate.instant(
        TypeContent.getDisplayName(this.charge.type_content)
      )})`
    );
  };

  getCountryFullName = (country: Country, person: Person): string => {
    return `${getCountryNameLang(country, this.translate.getDefaultLang())} ${
      person.state
    } ${person.city} ${person.zip_code}`;
  };

  setValuesField = (user: User): RemitterReceiverForm => {
    const { person } = user;

    return {
      id: user.id,
      alphanumeric: user.alphanumeric,
      name: user.person.name,
      full_address: this.getCountryFullName(person.country, person),
      identification_id: person.identification_id,
      phone: person.phone,
      cellphone: person.cellphone,
      email: user.email,
      zip_code: person.zip_code,
      country_id: person.country_id,
      state: person.state,
      city: person.city,
      address: person.address,
      special_instructions: person.special_instructions,
      person: person,
      type_pay: user.type_pay,
    };
  };

  setUser = (user: User) => {
    switch (this.userTier) {
      case ShippingType.REMITTER:
        this.remitter = this.setValuesField(user);
        return (this.FormRemitter.step = 2);
      case ShippingType.RECEIVER:
        this.receiver = this.setValuesField(user);
        return (this.FormReceiver.step = 2);
    }
  };

  setRemitter = (remitter: RemitterReceiverForm) => {
    this.remitter = { ...remitter };
    this.FormRemitter.step = 2;
  };

  setNullRemitter() {
    this.remitter = null;
  }

  setReceiver = (receiver: RemitterReceiverForm) => {
    this.receiver = { ...receiver };
    this.membershipType = receiver.type_pay;
    !!this.FormReceiver ? (this.FormReceiver.step = 2) : null;

    if (receiver.country_id !== this.countryId) {
      return showMessage(
        null,
        this.translate.instant("validations.countryDoesNotMatch"),
        "warning",
        true
      );
    }
  };

  onToggleByType = (): boolean => {
    if (!!this.currentUser) {
      const { agency } = this.currentUser;

      return (
        (!!this.currentUser && !!agency && agency.tier === TierId.TIER_ADMIN) ||
        (!!this.currentUser && this.currentUser.tier === TierId.TIER_ADMIN)
      );
    }
  };

  onChangeLocker = async () => {
    if (!!this.charges && this.charges.length) {
      const wantSwitchLocker = await confirm({
        text: this.translate.instant("validations.switchPackage"),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmButtonText"
        ),
        cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
      });

      if (wantSwitchLocker) {
        this.receiver = null;
        this.membershipType = 0;
        return (this.charges = []);
      }

      this.isLocker = !this.isLocker;
    }
  };

  onSearchLocker = (ngbSearch: TemplateRef<SearchComponent>) => {
    this.modalRef = this.ngbModal.open(ngbSearch, {
      size: "md",
    });
  };

  setNullReceiver() {
    this.receiver = null;
  }

  setCharge = (charge: ChargeForm) => {
    const isEditing = this.isEditing;
    const rawCharges = this.charges;
    const chargeKey = this.chargeKey;

    if (isEditing) {
      rawCharges[chargeKey] = {
        ...charge,
      };

      this.charges = rawCharges;
      this.isEditing = false;
      this.chargeKey = null;

      return;
    }

    this.charges = this.charges ? [charge, ...this.charges] : [charge];
  };

  setImageSrc = (imageSrc: string): void => {
    this.imageSrc = imageSrc;
  };

  isToggleCreateRemitter = (ngbUser: TemplateRef<FormUserComponent>) => {
    this.userTier = ShippingType.REMITTER;
    this.userTierUpdate = false;

    this.modalRef = this.ngbModal.open(ngbUser, {
      size: "md",
    });
  };

  isToggleUpdateRemitter = (ngbUser: TemplateRef<FormUserComponent>) => {
    this.userTier = ShippingType.REMITTER;
    this.userTierUpdate = true;
    this.user = { ...this.remitter };

    this.modalRef = this.ngbModal.open(ngbUser, {
      size: "md",
    });
  };

  isToggleCreateReceiver = (ngbUser: TemplateRef<FormUserComponent>) => {
    this.userTier = ShippingType.RECEIVER;
    this.userTierUpdate = false;

    this.modalRef = this.ngbModal.open(ngbUser, {
      size: "md",
    });
  };

  isToggleUpdateReceiver = (ngbUser: TemplateRef<FormUserComponent>) => {
    this.userTier = ShippingType.RECEIVER;
    this.userTierUpdate = true;
    this.user = { ...this.receiver };

    this.modalRef = this.ngbModal.open(ngbUser, {
      size: "md",
    });
  };

  isTogglePackage = (togglePackage: TemplateRef<FormPackageComponent>) => {
    this.isEditing = false;
    this.errors = null;
    this.imageSrc = null;

    const countryId = this.countryId;
    const charges = this.charges;

    if (!countryId) {
      return showMessage(
        null,
        this.translate.instant("validations.countryRequired"),
        "warning",
        true
      );
    }

    if (!!charges && charges.length >= 9) {
      return showMessage(
        null,
        this.translate.instant("validations.maxCharge"),
        "warning",
        true
      );
    }

    this.package.reset();
    this.package.enable();

    this.modalRef = this.ngbModal.open(togglePackage, {
      size: "lg",
    });
  };

  onWatchCharge = (ngbCharge: TemplateRef<WatchComponent>, key: number) => {
    const packageByKey = this.charges[key];

    this.charge = {
      ...packageByKey,
    };

    this.modalRef = this.ngbModal.open(ngbCharge, {
      size: "lg",
    });
  };

  onUpdateCharge = (
    togglePackage: TemplateRef<FormPackageComponent>,
    chargeKey: number
  ) => {
    const packageByKey = this.charges[chargeKey];
    const hasInsurance = packageByKey.insurance > 0;
    const hasImpost = packageByKey.impost > 0;

    this.package.patchValue({
      ...packageByKey,
      file: null,
      insurance: hasInsurance,
      impost: hasImpost,
    });

    this.isEditing = true;
    this.chargeKey = chargeKey;
    this.imageSrc = packageByKey.file_url;

    this.modalRef = this.ngbModal.open(togglePackage, {
      size: "lg",
    });
  };

  onDeleteChargeByKey = (chargeKey: number) => {
    this.charges = this.charges.filter((_, keyId) => keyId !== chargeKey);
  };

  getPackagesByCountry = (countryId: number) => {
    const country = this.countries.find(({ id }) => id === countryId);
    this.contents = [...country.content];

    this.packageService.getPackagesByType(countryId).subscribe(
      (packages: [][]) => {
        this.types = Object.keys(packages).map(
          (type) => ({
            id: parseInt(type),
            name: this.getTypeName(parseInt(type)),
          }),
          this
        );

        this.objTypes = packages;
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  getAgencies = () => {
    this.agencyService.getAgencies().subscribe(
      (agencies: User[]) => {
        const rawAgencies = agencies
          .map((agency: User) => {
            return {
              ...agency,
              person: { ...agency.person[0] },
            };
          })
          .filter((user) => !!user.person);

        this.agencies = rawAgencies;
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  onValidForm = () => {
    const charges = this.charges;
    const invalidCharges = !charges || charges.length === 0;

    if (invalidCharges) {
      return showMessage(
        null,
        this.translate.instant("validations.chargeRequired"),
        "warning",
        true
      );
    }
  };

  onSubmit = () => {
    const { agency } = this.currentUser;
    const signature = this.signatureUrl;
    const signatureName = this.signature;

    if (!signatureName && !this.isLocker) {
      return showMessage(
        null,
        this.translate.instant("validations.signatureName"),
        "warning",
        true
      );
    }

    if (!signature && !this.isLocker) {
      return showMessage(
        null,
        this.translate.instant("validations.signatureRequired"),
        "warning",
        true
      );
    }

    if (!this.longitude && !this.latitude) {
      return showMessage(
        null,
        this.translate.instant("validations.noCoords"),
        "warning",
        true
      );
    }

    if (!!!this.submitted) {
      this.submitted = true;

      this.quoteService
        .store({
          remitter: this.remitter,
          receiver: this.receiver,
          country_id: this.countryId,
          signature: signature,
          signatureName: signatureName,
          charges: this.charges,
          latitude: this.latitude,
          longitude: this.longitude,
          isLocker: this.isLocker,
        })
        .subscribe(
          (quote: Quote) => {
            showMessage(
              null,
              this.translate.instant("notifications.createQuote"),
              "success",
              true
            );

            this.step = 5;
            this.quote = quote;
            this.submitted = false;

            const agencyId = !!agency ? agency.id : this.currentUser.id;
            Socket.emit(REAL_TIME.SHIPPING, agencyId);
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
    }
  };

  onStep = (step: number): void => {
    if (step === 4 && this.isLocker) {
      return this.onSubmit();
    }

    this.step = step;
  };

  onDrawComplete = (): void => {
    this.signatureUrl = this.signaturePad.toDataURL("image/png", 0.5);
  };

  onDrawClear = (): void => {
    this.signatureUrl = null;
    this.signaturePad.clear();
  };

  onCloseNgbModal = (): void => {
    this.modalRef.close();
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      return showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };
}
