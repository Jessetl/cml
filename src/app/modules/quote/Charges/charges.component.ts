import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import { ChargeForm } from "@models";
import { showConfirmButton } from "@shared/utils";

import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "cml-charges",
  templateUrl: "./charges.component.html",
  styles: [],
})
export class ChargesComponent implements OnInit {
  @Input() charges: ChargeForm[];
  @Output() onWatchCharge = new EventEmitter<number>();
  @Output() onDeleteChargeByKey = new EventEmitter<number>();
  @Output() onUpdateCharge = new EventEmitter<number>();

  constructor(private translate: TranslateService) {}

  ngOnInit() {}

  shouldToggleDelete = (chargeKey: number) => {
    showConfirmButton(
      this.translate.instant("ngbAlert.deleteCharge"),
      this.translate.instant("ngbAlert.btnYes"),
      this.translate.instant("ngbAlert.btnNo"),
      () => {
        this.onDeleteChargeByKey.emit(chargeKey);
      }
    );
  };

  shouldToggleUpdate = (chargeKey: number) => {
    this.onUpdateCharge.emit(chargeKey);
  };

  shouldToggleWatch = (chargeKey: number) => {
    this.onWatchCharge.emit(chargeKey);
  };

  havePackages = (): boolean => {
    return !this.charges;
  };
}
