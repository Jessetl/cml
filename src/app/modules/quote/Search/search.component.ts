import { Component, OnInit, Output, Input } from "@angular/core";
import { EventEmitter } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";

import { Country, RemitterReceiverForm } from "@models";
import { UserService } from "@core/service";
import { showMessage, handlerError, rawError } from "@shared/utils";

@Component({
  selector: "cml-search",
  templateUrl: "./search.component.html",
  styles: [],
})
export class SearchComponent implements OnInit {
  @Input() countryId: number;
  @Output() setReceiver = new EventEmitter<RemitterReceiverForm>();
  @Output() onCloseNgbModal = new EventEmitter<null>();

  public submitted: boolean = false;
  public filter: string = null;
  public filterBy: number = 1;
  public errors: any;
  public isLocker: boolean = true;
  public country: Country;

  constructor(
    private userService: UserService,
    private translate: TranslateService
  ) {}

  ngOnInit(): void {}

  searchByFillable = () => {
    const strFilter = this.filter;
    const filterBy = this.filterBy;

    if (!strFilter) {
      return;
    }

    if (!!!this.submitted) {
      this.submitted = true;

      this.userService
        .getByFillable({
          filter: strFilter,
          filterBy: filterBy,
          locker: this.isLocker,
        })
        .subscribe(
          (user: RemitterReceiverForm) => {
            this.submitted = false;

            if (this.countryId !== user.country_id) {
              return showMessage(
                null,
                this.translate.instant(
                  `notifications.countryDoesNotCorrespond`
                ),
                "warning",
                true
              );
            } else if (user.type_pay === null) {
              return showMessage(
                null,
                this.translate.instant(`notifications.nullMembership`),
                "warning",
                true
              );
            }

            this.setReceiver.emit(user);
            this.onCloseNgbModal.emit();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;

            if (error.error === "receiverNotFound") {
              return showMessage(
                null,
                this.translate.instant(`notifications.${error.error}`),
                "warning",
                true
              );
            } else if (error.error === "INACTIVE") {
              return showMessage(
                null,
                this.translate.instant(`errors.INACTIVE`),
                "warning",
                true
              );
            }

            this.handleError(error);
          }
        );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      return showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };
}
