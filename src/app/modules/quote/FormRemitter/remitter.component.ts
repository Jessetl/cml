import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import {
  User as Agency,
  Country,
  RemitterReceiverForm,
  User,
  Person,
} from "@models";
import { TranslateService } from "@ngx-translate/core";

import { AgencyService } from "@core/service";
import {
  confirm,
  showMessage,
  handlerError,
  rawError,
  TIER_ADMIN,
} from "@shared/utils";
import { AgencyComponent } from "app/modules/agency/agency.component";

@Component({
  selector: "cml-remitter",
  templateUrl: "./remitter.component.html",
  styles: [],
})
export class FormRemitterComponent implements OnInit {
  @Input() countryId: number;
  @Input() agencies: Agency[];
  @Input() countries: Country[];
  @Input() user: User;
  @Input() remitter: RemitterReceiverForm;
  @Output() isToggleCreateRemitter = new EventEmitter<null>();
  @Output() setRemitter = new EventEmitter<RemitterReceiverForm>();
  @Output() setNullRemitter = new EventEmitter<null>();
  @Output() isToggleUpdateRemitter = new EventEmitter<null>();

  public submitted: boolean = false;
  public step: number = 1;
  public filter: string = null;
  public filterBy: number = 1;
  public errors: any;

  constructor(
    private agencyService: AgencyService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    const remitterId = this.remitter;

    if (remitterId) {
      this.step = 2;
    }
  }

  isToggleGenerateAgency = () => {
    this.isToggleCreateRemitter.emit();
  };

  searchByFillable = () => {
    const strFilter = this.filter;
    const filterBy = this.filterBy;

    if (!strFilter) {
      return;
    }

    if (!!!this.submitted) {
      this.submitted = true;

      this.agencyService
        .getByFillable({ filter: strFilter, filterBy: filterBy })
        .subscribe(
          (agency: RemitterReceiverForm) => {
            this.submitted = false;

            let remitter = !!agency.consolidation
              ? {
                  ...agency,
                  address: agency.consolidation.address,
                  city: agency.consolidation.city,
                  state: agency.consolidation.state,
                  zip_code: agency.consolidation.city,
                }
              : agency;
            this.setRemitter.emit(remitter);
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;

            if (error.error === "remitterNotFound") {
              return showMessage(
                null,
                this.translate.instant(`notifications.${error.error}`),
                "warning",
                true
              );
            } else if (error.error === "INACTIVE") {
              return showMessage(
                null,
                this.translate.instant(`errors.INACTIVE`),
                "warning",
                true
              );
            }

            this.handleError(error);
          }
        );
    }
  };

  setStep = async (step: number) => {
    const wantToGoBack = await confirm({
      text: this.translate.instant("ngbAlert.wantToGoBackSearch"),
      confirmButtonText: this.translate.instant("buttonText.confirmButtonText"),
      cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
    });

    if (wantToGoBack) {
      this.step = step;
      this.filter = null;
      this.setNullRemitter.emit();
    }
  };

  autocompleteRemitter = () => {
    if (!!this.user.agency) {
      const { agency } = this.user;
      let newAgency = this.newUser(agency);
      const user: RemitterReceiverForm = {
        ...this.getRemitterData(newAgency),
      };

      return this.setRemitter.emit(user);
    }
    let user = this.newUser(this.user);
    return this.setRemitter.emit(this.getRemitterData(user));
  };
  newUser = (user: User): User => {
    return user.tier === TIER_ADMIN
      ? { ...user, person: this.consolidation(user) }
      : user;
  };

  consolidation = (user: User): Person => {
    return {
      ...user.person,
      address: user.consolidation.address,
      city: user.consolidation.city,
      state: user.consolidation.state,
      zip_code: user.consolidation.city,
    };
  };

  getRemitterData = (agency: User): RemitterReceiverForm => {
    return {
      ...agency,
      name: agency.person.name,
      alphanumeric: agency.alphanumeric,
      full_address: agency.person.full_address,
      special_instructions: agency.person.address,
      identification_id: agency.person.identification_id,
      phone: agency.person.phone,
      cellphone: agency.person.cellphone,
      zip_code: agency.person.zip_code,
      country_id: agency.person.country_id,
      state: agency.person.state,
      city: agency.person.city,
      address: agency.person.address,
    };
  };

  onToggleByTier = (): boolean => {
    return !this.user.agency;
  };

  onUpdateRemitter = () => {
    this.isToggleUpdateRemitter.emit();
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      return showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };
}
