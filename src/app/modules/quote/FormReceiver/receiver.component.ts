import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";

import { Country, RemitterReceiverForm } from "@models";
import { UserService } from "@core/service";
import { confirm, showMessage, handlerError, rawError } from "@shared/utils";

@Component({
  selector: "cml-receiver",
  templateUrl: "./receiver.component.html",
  styles: [],
})
export class FormReceiverComponent implements OnInit {
  @Input() countryId: number;
  @Input() countries: Country[];
  @Input() receiver: RemitterReceiverForm;
  @Input() isLocker: boolean;
  @Output() isToggleCreateReceiver = new EventEmitter<null>();
  @Output() setReceiver = new EventEmitter<RemitterReceiverForm>();
  @Output() setNullReceiver = new EventEmitter<null>();
  @Output() isToggleUpdateReceiver = new EventEmitter<null>();

  public submitted: boolean = false;
  public step: number = 1;
  public filter: string = null;
  public filterBy: number = 1;
  public errors: any;

  constructor(
    private userService: UserService,
    private translate: TranslateService
  ) {}

  ngOnInit() {
    const receiverId = this.receiver;

    if (receiverId) {
      this.step = 2;
    }
  }

  isToggleGenerateUser = () => {
    this.isToggleCreateReceiver.emit();
  };

  searchByFillable = () => {
    const strFilter = this.filter;
    const filterBy = this.filterBy;

    if (!strFilter) {
      return;
    }

    if (!!!this.submitted) {
      this.submitted = true;

      this.userService
        .getByFillable({
          filter: strFilter,
          filterBy: filterBy,
          locker: this.isLocker,
        })
        .subscribe(
          (user: RemitterReceiverForm) => {
            this.submitted = false;
            this.setReceiver.emit(user);
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;

            if (error.error === "receiverNotFound") {
              return showMessage(
                null,
                this.translate.instant(`notifications.${error.error}`),
                "warning",
                true
              );
            } else if (error.error === "INACTIVE") {
              return showMessage(
                null,
                this.translate.instant(`errors.INACTIVE`),
                "warning",
                true
              );
            }

            this.handleError(error);
          }
        );
    }
  };

  setStep = async (step: number) => {
    const wantToGoBack = await confirm({
      text: this.translate.instant("ngbAlert.wantToGoBackSearch"),
      confirmButtonText: this.translate.instant("buttonText.confirmButtonText"),
      cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
    });

    if (wantToGoBack) {
      this.step = step;
      this.filter = null;
      this.setNullReceiver.emit();
    }
  };

  onUpdateReceiver = () => {
    this.isToggleUpdateReceiver.emit();
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      return showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };
}
