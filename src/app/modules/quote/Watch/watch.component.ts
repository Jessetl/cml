import { Component, Input } from "@angular/core";

import { ChargeForm } from "@models";
import {
  FORMULA_ELECTRONICS,
  FORMULA_INCHLINEAR,
  FORMULA_TELEVISIONS,
  FORMULA_MESSENGERSERVICE,
  FORMULA_PACKAGEPOUND,
} from "@shared/utils";

import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "cml-watch",
  templateUrl: "./watch.component.html",
  styles: [],
})
export class WatchComponent {
  @Input() charge: ChargeForm;

  constructor(private translate: TranslateService) {}

  getMeasureType = (type: number): string => {
    switch (type) {
      case FORMULA_INCHLINEAR:
      case FORMULA_TELEVISIONS:
        return this.translate.instant("cardForm.inch");
      default:
        return this.translate.instant("cardForm.physical_weight");
    }
  };

  getSelectMessengerService = (type: number): boolean => {
    return type !== FORMULA_MESSENGERSERVICE;
  };

  getInchType = (type: number): number => {
    switch (type) {
      case FORMULA_INCHLINEAR:
      case FORMULA_TELEVISIONS:
        return this.charge.inch;
      default:
        return this.charge.weight;
    }
  };

  getTypeExtraPound = (type: number): boolean => {
    return type === FORMULA_PACKAGEPOUND;
  };
}
