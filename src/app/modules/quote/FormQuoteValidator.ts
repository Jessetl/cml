import { FormControl, Validators } from "@angular/forms";

export const FormUserValidator = {
  id: new FormControl(null, []),
  name: new FormControl(null, [Validators.required, Validators.maxLength(90)]),
  phone: new FormControl(null, [Validators.maxLength(15)]),
  cellphone: new FormControl(null, [
    Validators.required,
    Validators.maxLength(15),
  ]),
  email: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
  identification_id: new FormControl(null, [
    Validators.required,
    Validators.maxLength(30),
  ]),
  zip_code: new FormControl(null, [
    Validators.required,
    Validators.maxLength(10),
  ]),
  country_id: new FormControl(null, [Validators.required]),
  state: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.maxLength(50),
  ]),
  city: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.maxLength(50),
  ]),
  address: new FormControl(null, [Validators.required]),
  special_instructions: new FormControl(null, []),
};

export const FormPackageValidator = {
  id: new FormControl(null, []),
  type_content: new FormControl(null, [Validators.required]),
  shipping_type: new FormControl(null, [Validators.required]),
  type: new FormControl(null, [Validators.required]),
  name: new FormControl(null, [Validators.required]),
  description: new FormControl(null, [Validators.required]),
  declared: new FormControl(null, [Validators.required, Validators.min(1)]),
  insured_value: new FormControl(null, [Validators.required]),
  volumetric_weight: new FormControl(null, [Validators.min(0)]),
  cubic_foot: new FormControl(null, [Validators.min(0)]),
  cubic_inches: new FormControl(null, [Validators.min(0)]),
  cubic_meter: new FormControl(null, [Validators.min(0)]),
  linear_weight: new FormControl(null, [Validators.min(0)]),
  cost: new FormControl(null, [Validators.min(0)]),
  other_measures: new FormControl(null, [Validators.min(0)]),
  weight: new FormControl(null, [Validators.required, Validators.min(0)]),
  inch: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.min(0),
  ]),
  long: new FormControl(null, [Validators.required, Validators.min(0)]),
  width: new FormControl(null, [Validators.required, Validators.min(0)]),
  high: new FormControl(null, [Validators.required, Validators.min(0)]),
  price: new FormControl(null, [Validators.min(0)]),
  insurance: new FormControl(null, [Validators.min(0)]),
  impost: new FormControl(null, [Validators.min(0)]),
  content: new FormControl(null, [Validators.required]),
  file: new FormControl(null, []),
};
