import { Component, Input, EventEmitter, Output } from "@angular/core";

import { ChargeForm, RemitterReceiverForm, Country } from "@models";
import {
  formatingPhone,
  getCountryNameLang,
  getChargeNameLang,
} from "@shared/utils";

import { TranslateService } from "@ngx-translate/core";
import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import { User } from "@models";

@Component({
  selector: "cml-preview",
  templateUrl: "./preview.component.html",
  styles: [],
})
export class PreviewComponent {
  public user: User;
  @Input() charges: ChargeForm[];
  @Input() isLocker: boolean;
  @Input() submitted: boolean;
  @Input() remitter: RemitterReceiverForm;
  @Input() receiver: RemitterReceiverForm;
  @Output() onStep = new EventEmitter<number>();

  constructor(
    private translate: TranslateService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  getServiceName = (charge: ChargeForm): string => {
    return getChargeNameLang(charge, this.translate.getDefaultLang());
  };

  getReduceAmount = (name: string): number => {
    return this.charges.reduce((amount, charge) => amount + charge[name], 0);
  };

  goRoute = (step: number): void => {
    this.onStep.emit(step);
  };

  formating = (phone: number) => {
    return phone ? formatingPhone(phone) : "";
  };
}
