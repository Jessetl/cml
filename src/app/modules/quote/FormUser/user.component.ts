import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
} from "@angular/core";

import { FormUserValidator } from "../FormQuoteValidator";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Country, User, RemitterReceiverForm } from "@models";
import { PackageService, GooglemapsService } from "@core/service";

import { TranslateService } from "@ngx-translate/core";
import { HttpErrorResponse } from "@angular/common/http";

import { rawError, validationsForm, handlerError } from "@shared/utils";
import { confirm, showMessage, ShippingType } from "@shared/utils";

@Component({
  selector: "cml-tier",
  templateUrl: "./user.component.html",
  styles: [],
})
export class FormUserComponent implements OnInit, OnDestroy {
  @Input() userTier: number;
  @Input() userTierUpdate: boolean;
  @Input() countryId: number;
  @Input() isLocker: boolean;
  @Input() userForm: RemitterReceiverForm;
  @Input() countries: Country[];
  @Output() setUser = new EventEmitter<User>();
  @Output() onClose = new EventEmitter<any>();

  public submitted: boolean;
  public searching: boolean = false;
  public user: FormGroup;

  public errors: Array<any>;
  public errorZipCode: boolean = false;
  public shouldEnableStateAndCity: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private packageService: PackageService,
    private translate: TranslateService,
    private googleService: GooglemapsService
  ) {
    this.user = this.formBuilder.group(FormUserValidator);
  }

  ngOnInit() {
    this.user.reset();

    if (this.userTierUpdate) {
      this.user.patchValue({
        ...this.userForm,
      });
    }
    let country = this.user.get("country_id").value;
    this.user.get("state").disable();
    this.user.get("city").disable();

    if (country !== this.countryId) {
      this.user.patchValue(
        { zip_code: null, state: null, city: null },
        { onlySelf: true }
      );
    }
    if (this.userTier === ShippingType.RECEIVER) {
      this.user.get("country_id").disable();
      return this.user.patchValue(
        { country_id: this.countryId },
        { onlySelf: true }
      );
    }

    this.user.get("country_id").enable();
    this.user.patchValue({ country_id: 1 }, { onlySelf: true });
  }

  ngOnDestroy(): void {
    this.user.reset();
  }

  isFieldValid = (field: string): boolean => {
    return this.user.get(field).invalid && this.user.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  shouldStateAndCity = () => {
    this.shouldEnableStateAndCity = !this.shouldEnableStateAndCity;

    if (this.shouldEnableStateAndCity) {
      this.user.get("state").enable({ onlySelf: true });
      this.user.get("city").enable({ onlySelf: true });
    } else {
      this.user.get("state").disable({ onlySelf: true });
      this.user.get("city").disable({ onlySelf: true });
    }
  };

  onDefaultValuesStateAndCountry = () => {
    this.user.get("state").reset(null, { onlySelf: true });
    this.user.get("city").reset(null, { onlySelf: true });
  };

  onBlurZipCode = (nameField: string) => {
    const zipCode = this.user.get(nameField).value;
    const countryId = this.user.get("country_id").value;
    const countryAlphacode = this.countries.find(({ id }) => id === countryId);

    if (!zipCode) {
      return;
    }

    this.onDefaultValuesStateAndCountry();
    this.searching = true;
    this.errorZipCode = false;

    this.googleService
      .getZipCode({
        postalCode: zipCode.toString(),
        country: !!countryAlphacode ? countryAlphacode.alpha2Code : "US",
      })
      .subscribe(
        async (res: any) => {
          const response = await this.googleService.getStateAndCity(res);

          this.user.patchValue(
            {
              state: response.state,
              city: response.city,
            },
            { onlySelf: true, emitEvent: false }
          );

          this.searching = false;
        },
        async () => {
          if (!this.shouldEnableStateAndCity) {
            this.searching = false;
            this.errorZipCode = true;

            const wantWrite = await confirm({
              text: this.translate.instant("ngbAlert.watWriteZipCodeShipment"),
              confirmButtonText: this.translate.instant(
                "buttonText.confirmButtonText"
              ),
              cancelButtonText: this.translate.instant(
                "buttonText.cancelButtonText"
              ),
            });

            if (wantWrite) {
              this.user.get("state").enable({ onlySelf: true });
              this.user.get("city").enable({ onlySelf: true });
            }
          }
        }
      );
  };

  onSubmit = () => {
    const infoValid = this.user.valid;
    const updateForm = this.userTierUpdate;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.user);
    }

    if (!!!this.submitted) {
      this.submitted = true;

      switch (updateForm) {
        case true:
          return this.packageService
            .updateUser(this.user.getRawValue())
            .subscribe(
              (user: User) => {
                this.submitted = false;
                this.setUser.emit(user);

                showMessage(
                  "",
                  this.translate.instant("notifications.createUser"),
                  "success",
                  true
                );

                this.onClose.emit();
              },
              (error: HttpErrorResponse) => {
                this.submitted = false;
                this.handleError(error);
              }
            );
        case false:
          return this.packageService
            .storeUser(this.user.getRawValue(), this.isLocker)
            .subscribe(
              (user: User) => {
                this.submitted = false;
                this.setUser.emit(user);

                showMessage(
                  "",
                  this.translate.instant("notifications.updateUser"),
                  "success",
                  true
                );

                this.onClose.emit();
              },
              (error: HttpErrorResponse) => {
                this.submitted = false;
                this.handleError(error);
              }
            );
      }
    }
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      return showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };
}
