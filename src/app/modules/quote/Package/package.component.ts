import { Component, EventEmitter, ElementRef } from "@angular/core";
import { OnInit, OnDestroy } from "@angular/core";
import { Input, Output, ViewChild } from "@angular/core";
import { pairwise, startWith } from "rxjs/operators";
import { HttpErrorResponse } from "@angular/common/http";
import { FormGroup } from "@angular/forms";

import { Package, ChargeForm, Content, TypeContent } from "@models";

import {
  confirm,
  showMessage,
  validationsForm,
  handlerError,
  rawError,
  MAXIMUM_WEIGHT_PACKAGE,
  TYPECONTENT_ELECTRONIC,
  TYPECONTENT_REGULAR,
  TYPECONTENT_COMMERCIAL,
  TYPECONTENT_MIXED,
  TYPE_MARITIME,
  FORMULA_PACKAGEPOUND,
  FORMULA_CUBICFOOT,
  FORMULA_POUND,
  FORMULA_PACKAGE,
  FORMULA_TELEVISIONS,
  FORMULA_INCHLINEAR,
  FORMULA_MESSENGERSERVICE,
  FORMULA_ELECTRONICS,
  VOLUMETRIC_WEIGHT,
  CUBIC_FOOT,
  ZERO,
} from "@shared/utils";

import {
  formatTypeImage as formatImage,
  maxFileSize as maxSize,
} from "@shared/utils";

import { TranslateService } from "@ngx-translate/core";
import { PackageService } from "@core/service";

interface Type {
  id: number;
  name: string;
}

interface TypeDescription {
  description: string;
  formula: number;
  high: number;
  id: number;
  long: number;
  maximum_insurance: number;
  maximum_weight: number;
  minimum_insurance: number;
  minimum_weight: number;
  commercial_surplus: number;
  type: number;
  type_content: TypeContent[];
  width: number;
}

interface Measurements {
  weight: number;
  inch: number;
  long: number;
  high: number;
  width: number;
}

const initialState = {
  description: null,
  type_content: null,
  shipping_type: null,
  weight: null,
  inch: null,
  declared: null,
  insured_value: null,
  long: null,
  high: null,
  width: null,
  content: null,
};

@Component({
  selector: "cml-package",
  templateUrl: "./package.component.html",
  styles: [],
})
export class FormPackageComponent implements OnInit, OnDestroy {
  @ViewChild("insuredValue") insuredField: ElementRef;
  @ViewChild("declaredValue") declaredField: ElementRef;

  @Input() contents: Content[];
  @Input() package: FormGroup;
  @Input() types: Type[];
  @Input() objTypes: [][];
  @Input() isEditing: boolean;
  @Input() imageSrc: string;
  @Input() countryId: number;
  @Input() isLocker: boolean;
  @Input() membershipType: number;
  @Output() setCharge = new EventEmitter<ChargeForm>();
  @Output() setImageSrc = new EventEmitter<string>();
  @Output() onClose = new EventEmitter<any>();

  public showInformation: boolean = false;
  public packages: Type[];
  public objPackages: {};
  public descriptions: Package[];
  public typeContent: Type[];
  public submitted: boolean;
  public errors: Array<any>;
  public wantMinWeight: boolean = false;
  public wantMinInsurance: boolean = false;
  public wantMaxTypeCommercialInsurance: boolean = false;
  public typeValidation: number = null;
  public typeFile: string = null;

  public prevName: string = null;

  constructor(
    private translate: TranslateService,
    private packageService: PackageService
  ) {}

  ngOnInit() {
    const isEditing = this.isEditing;
    !isEditing ? this.patchValue("insurance", true) : null;
    !isEditing ? this.patchValue("impost", true) : null;

    if (isEditing) {
      const packageId = this.package.get("description").value;

      this.getProductsByType();
      this.getProductsByName();
      this.lockInputs();

      const {
        type_content: typeContent,
        formula: typeValidation,
      } = this.findById(packageId);

      this.typeValidation = typeValidation;

      this.typeContent = this.setTypeContent(typeContent).sort(function (a, b) {
        if (a.name.toUpperCase() > b.name.toUpperCase()) return 1;
        else if (a.name.toUpperCase() < b.name.toUpperCase()) return -1;
        else return 0;
      });

      this.rulesMessenger(typeValidation);
    }
  }

  ngAfterViewInit() {
    this.package
      .get("name")
      .valueChanges.pipe(startWith(1), pairwise())
      .subscribe(([prevValue, _]) => {
        this.prevName = prevValue;
      });
  }

  ngOnDestroy() {
    this.showInformation = false;
    this.typeFile = null;
  }

  isFieldValid = (field: string): boolean => {
    return this.package.get(field).invalid && this.package.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  lockInputs = () => {
    const packageId = this.package.get("description").value;
    const { formula: typeFormula } = this.findById(packageId);

    this.package.get("type").disable({ onlySelf: true });
    this.package.get("name").disable({ onlySelf: true });
    this.package.get("description").disable({ onlySelf: true });
    this.package.get("type_content").disable({ onlySelf: true });
    this.package.get("shipping_type").disable({ onlySelf: true });
    if (
      typeFormula !== FORMULA_INCHLINEAR &&
      typeFormula !== FORMULA_TELEVISIONS
    ) {
      this.package.get("inch").disable({ onlySelf: true });
    }
  };

  inputFieldDisabled = (names: string[]): void => {
    names.forEach((name) => this.package.get(name).disable());
  };

  inputFieldEnabled = (names: string[]): void => {
    names.forEach((name) => this.package.get(name).enable());
  };

  rulesMessenger = (typeValidation: number): void => {
    this.package.get("declared").enable();
    const amount = this.package.get("declared").value;
    const packageId = this.package.get("id").value;
    const { minimum_insurance: minInsurance } = this.findById(packageId);

    if (typeValidation === FORMULA_MESSENGERSERVICE) {
      return this.inputFieldDisabled([
        "declared",
        "insured_value",
        "long",
        "high",
        "width",
      ]);
    }

    this.inputFieldEnabled([
      "declared",
      "insured_value",
      "long",
      "high",
      "width",
    ]);

    if (amount < minInsurance && amount) {
      this.package.get("insured_value").disable();
    }
  };

  patchValue = (name: string, value: string | number | boolean) => {
    this.package.patchValue(
      {
        [name]: value,
      },
      { onlySelf: true, emitEvent: false }
    );
  };

  setInitialState = (): void => {
    Object.keys(initialState).forEach((fieldName) => {
      return this.package.get(fieldName).reset(null, { onlySelf: true });
    });
  };

  getTypeName = (type: number): string => {
    switch (type) {
      case TYPECONTENT_REGULAR:
        return this.translate.instant("select.regular");
      case TYPECONTENT_ELECTRONIC:
        return this.translate.instant("select.electronic");
      case TYPECONTENT_COMMERCIAL:
        return this.translate.instant("select.commercial");
      case TYPECONTENT_MIXED:
        return this.translate.instant("select.mixed");
    }
  };

  getProductsByType = () => {
    const isEditing = this.isEditing;

    let groupByName = {};

    const typeId = this.package.get("type").value;
    const lang = this.translate.getDefaultLang();

    if (typeId) {
      this.objTypes[typeId].forEach(function (packages: Package) {
        const name = lang === "es" ? packages["name"] : packages["name_en"];
        const description =
          lang === "es" ? packages["description"] : packages["description_en"];

        groupByName[name] = groupByName[name] || [];
        groupByName[name].push({
          id: packages["id"],
          type: packages["type"],
          type_content: packages["type_content"],
          formula: packages["formula"],
          description: description,
          long: packages["long"],
          width: packages["width"],
          high: packages["high"],
          minimum_weight: packages["minimum_weight"],
          maximum_weight: packages["maximum_weight"],
          minimum_insurance: packages["minimum_insurance"],
          maximum_insurance: packages["maximum_insurance"],
          commercial_surplus: packages["commercial_surplus"],
          volumetric_weight: packages["volumetric_weight"],
        });
      }, lang);
    }

    this.packages = Object.keys(groupByName).map((name) => ({
      id: typeId,
      name: name,
    }));

    this.objPackages = groupByName;

    if (!isEditing) {
      this.descriptions = [];
      this.package.get("name").reset(null, { onlySelf: true });
      this.showInformation = false;

      this.setInitialState();
    }
  };

  getProductsByName = async () => {
    const isEditing = this.isEditing;
    const name = this.package.get("name").value;
    const packageId = this.package.get("description").value;

    if (packageId && !isEditing) {
      const wantSwitchPackage = await confirm({
        text: this.translate.instant("validations.switchPackage"),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmButtonText"
        ),
        cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
      });

      if (!wantSwitchPackage) {
        return this.package.patchValue(
          { name: this.prevName },
          { emitEvent: true }
        );
      }

      this.descriptions = this.objPackages[name];
      this.showInformation = false;

      this.setInitialState();
    }

    this.descriptions = this.objPackages[name].sort(
      (soFar: TypeDescription, p: TypeDescription) => {
        if (soFar.description < p.description) {
          return -1;
        } else {
          return 1;
        }
      },
      []
    );
  };

  setPackageId = () => {
    const packageId = this.package.get("description").value;

    const {
      type_content: typeContent,
      formula: typeValidation,
    } = this.findById(packageId);

    typeValidation !== FORMULA_TELEVISIONS &&
    typeValidation !== FORMULA_INCHLINEAR
      ? this.package.get("inch").disable()
      : this.package.get("inch").enable();

    this.patchValue("id", packageId);
    this.showInformation = true;
    this.typeValidation = typeValidation;

    this.typeContent = this.setTypeContent(typeContent).sort(function (a, b) {
      if (a.name.toUpperCase() > b.name.toUpperCase()) return 1;
      else if (a.name.toUpperCase() < b.name.toUpperCase()) return -1;
      else return 0;
    });

    this.rulesMessenger(typeValidation);
  };

  getTypeMesseger = () => {
    return this.typeValidation !== FORMULA_MESSENGERSERVICE;
  };

  getNameTypeContent = (contentType: number): string => {
    switch (contentType) {
      case TYPECONTENT_REGULAR:
        return this.translate.instant("select.regular");
      case TYPECONTENT_ELECTRONIC:
        return this.translate.instant("select.electronic");
      case TYPECONTENT_MIXED:
        return this.translate.instant("select.mixed");
      case TYPECONTENT_COMMERCIAL:
        return this.translate.instant("select.commercial");
    }
  };

  getTypeContents = (): Type[] => {
    const contents = this.contents;
    let typeContent: Type[] = [];

    for (let content = 1; content <= 4; content++) {
      typeContent = [
        ...typeContent,
        {
          id: content,
          name: this.getNameTypeContent(content),
        },
      ];
    }

    return typeContent.filter(({ id }) => {
      return contents.some(({ type }) => type === id);
    });
  };

  setTypeContent = (typeContent: TypeContent[]): Type[] => {
    const contents = this.contents;
    const typeContentLength = typeContent.length > 0;

    if (!typeContentLength) {
      return this.getTypeContents();
    }

    return typeContent
      .map(({ content }) => ({
        id: content,
        name: this.getNameTypeContent(content),
      }))
      .filter(({ id }) => {
        return contents.some(({ type }) => type === id);
      });
  };

  findById = (packageId: number) => {
    return this.descriptions.find(({ id }) => id === packageId);
  };

  getTypeMessage = (): string => {
    const isEditing = this.isEditing;

    return isEditing ? "notifications.updateCharge" : "notifications.addCharge";
  };

  toggleTypeInch = (): boolean => {
    const packageId = this.package.get("description").value;

    const { formula: typeValidation } = this.findById(packageId);

    return (
      typeValidation === FORMULA_TELEVISIONS ||
      typeValidation === FORMULA_INCHLINEAR
    );
  };

  toggleApplyPackage = (): boolean => {
    const packageId = this.package.get("description").value;

    const { formula: typeValidation } = this.findById(packageId);

    return typeValidation !== FORMULA_MESSENGERSERVICE;
  };

  toggleFileTypeCommercial = (): boolean => {
    const typeContent = this.package.get("type_content").value;

    return typeContent === TYPECONTENT_COMMERCIAL;
  };

  packageMeasurements = (): Measurements => {
    const weight = this.package.get("weight").value;
    const inch = this.package.get("inch").value;
    const long = this.package.get("long").value;
    const high = this.package.get("high").value;
    const width = this.package.get("width").value;

    return {
      weight: weight,
      inch: inch,
      long: long,
      high: high,
      width: width,
    };
  };

  getMeasurementsByVolumetricWeight = (measurements: number): number => {
    return Math.ceil(measurements / VOLUMETRIC_WEIGHT);
  };

  getMeasurementsByCubitFoot = (measurements: number): number => {
    return Math.ceil(measurements / CUBIC_FOOT);
  };

  getTranslateType = (type: number): string => {
    return type === TYPE_MARITIME
      ? this.translate.instant("typePackage.cubic_foot")
      : this.translate.instant("typePackage.volumetric_weight");
  };

  getInsuranceChecked = (): boolean => {
    return this.package.get("insurance").value;
  };

  getImpostChecked = (): boolean => {
    return this.package.get("impost").value;
  };

  maxWeight = () => {
    const weight: number = this.package.get("weight").value;
    const packageId: number = this.package.get("id").value;

    const { maximum_weight: maxWeight, formula } = this.findById(packageId);

    const allowedWeight =
      formula === FORMULA_PACKAGEPOUND ? MAXIMUM_WEIGHT_PACKAGE : maxWeight;

    if (formula === FORMULA_TELEVISIONS || formula === FORMULA_INCHLINEAR) {
      return;
    }

    if (
      (formula !== FORMULA_PACKAGEPOUND && weight > maxWeight) ||
      (formula === FORMULA_PACKAGEPOUND && weight > MAXIMUM_WEIGHT_PACKAGE)
    ) {
      showMessage(
        "",
        this.translate.instant("validations.maxWeight", {
          name: this.translate.instant("typePackage.weight"),
        }),
        "warning",
        true
      );

      return this.patchValue("weight", allowedWeight);
    }
  };

  formatTypeImage = (img: string): boolean => {
    return formatImage(img);
  };

  maxFileSize = (fileSize: number): boolean => {
    return maxSize(fileSize);
  };

  getFileExtension = (filename) => {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : null;
  };

  onFileChanged = (event: any) => {
    const readerFile = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];

      if (this.maxFileSize(file.size)) {
        this.package.patchValue({ file: null });

        return showMessage(
          "",
          this.translate.instant("validations.fileInvalid"),
          "warning",
          true
        );
      }

      if (this.formatTypeImage(file.type)) {
        this.package.patchValue({ file: null });

        return showMessage(
          "",
          this.translate.instant("validations.format"),
          "warning",
          true
        );
      }

      readerFile.readAsDataURL(file);

      this.typeFile = file.type;
      console.log(" gil ", this.typeFile);
      readerFile.onload = () => {
        const strResult = readerFile.result as string;
        this.setImageSrc.emit(strResult);
      };
    }
  };

  openUrlLink = (url: string) => {
    window.open(url, "_blank", "fullscreen");
  };
  changeInsurance = (): void => {
    const checkInsured = this.package.get("insurance").value;

    this.package.patchValue(
      {
        insurance: !checkInsured,
      },
      { onlySelf: true, emitEvent: false }
    );
  };

  changeImpost = (): void => {
    const checkImpost = this.package.get("impost").value;

    this.package.patchValue(
      {
        impost: !checkImpost,
      },
      { onlySelf: true, emitEvent: false }
    );
  };

  minInsured = async () => {
    const amount = this.package.get("declared").value;
    const checkInsured: boolean = this.package.get("insurance").value;

    const packageId = this.package.get("id").value;
    const {
      maximum_insurance: maxInsurance,
      minimum_insurance: minInsurance,
    } = this.findById(packageId);

    if (amount < 1) {
      return showMessage(
        "",
        this.translate.instant("validations.negativeDeclared"),
        "warning",
        true
      );
    }
    if (
      checkInsured &&
      amount < minInsurance &&
      (amount !== null || amount === 0)
    ) {
      const wantMinInsurancePackage = await confirm({
        text: this.translate.instant("validations.minInsurance", {
          min: minInsurance,
        }),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmButtonText"
        ),
        cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
      });

      if (wantMinInsurancePackage) {
        this.wantMinInsurance = true;
        this.package.get("insured_value").disable();
        return this.patchValue("insured_value", minInsurance);
      }
    }

    this.wantMinInsurance = false;
    this.package.get("insured_value").enable();
    this.patchValue("insured_value", null);
  };

  maxInsuredValue = () => {
    const amount = this.package.get("declared").value;
    const insuredValue = this.package.get("insured_value").value;
    const packageId = this.package.get("id").value;

    const { minimum_insurance: min, maximum_insurance: max } = this.findById(
      packageId
    );

    const typeContent = this.package.get("type_content").value;

    if (insuredValue < min && insuredValue !== null) {
      showMessage(
        "",
        this.translate.instant("validations.minInsuranceAmount", {
          min: min,
        }),
        "warning",
        true
      );
      return this.patchValue("insured_value", min);
    } else if (insuredValue > amount && amount >= min) {
      showMessage(
        "",
        this.translate.instant("validations.maxInsuredValue"),
        "warning",
        true
      );
      return this.patchValue("insured_value", amount);
    } else if (
      amount >= max &&
      insuredValue > max &&
      typeContent !== TYPECONTENT_COMMERCIAL
    ) {
      showMessage(
        "",
        this.translate.instant("validations.maxInsurance", {
          amount: max,
        }),
        "warning",
        true
      );

      return this.patchValue("insured_value", max);
    }
  };

  maxInch = () => {
    const inch = this.package.get("inch").value;
    const packageId = this.package.get("id").value;

    const { maximum_weight: maxWeight } = this.findById(packageId);

    if (inch > maxWeight) {
      showMessage(
        "",
        this.translate.instant("validations.maxWeight", {
          name: this.translate.instant("typePackage.linearInch"),
        }),
        "warning",
        true
      );
      return this.patchValue("inch", maxWeight);
    }
  };

  getMaxWeightPackage = (): number => {
    const { weight, long, high, width } = this.packageMeasurements();

    const measurements = long * high * width;
    const calc = this.getMeasurementsByVolumetricWeight(measurements);

    return calc > weight ? calc : weight;
  };

  getValidationMeasurementPackage = () => {
    let isInvalid: boolean = false;
    const packageId = this.package.get("id").value;
    const { long, high, width } = this.packageMeasurements();

    const { formula: typeFormula, long: L, high: H, width: W } = this.findById(
      packageId
    );

    const unconditional = long > L || width > W || high > H;
    const linearInch =
      typeFormula === FORMULA_TELEVISIONS ||
      typeFormula === FORMULA_INCHLINEAR ||
      typeFormula === FORMULA_ELECTRONICS;

    const strUnconditional = linearInch
      ? this.translate.instant("validations.unconditionalTv", {
          long: L,
          high: H,
          width: W,
        })
      : this.translate.instant("validations.unconditional");

    if (unconditional) {
      showMessage(null, strUnconditional, "warning", true);
      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationMinimumWeight = async () => {
    let isInvalid: boolean;
    const wantMinWeight = this.wantMinWeight;
    const packageId = this.package.get("id").value;

    const { minimum_weight: minWeight } = this.findById(packageId);

    // Obtengo el peso mayor resultante del ingresado o del volumetrico.
    const higherWeight = this.getMaxWeightPackage();

    if (!wantMinWeight && higherWeight < minWeight) {
      isInvalid = true;

      const wantMinWeightPackage = await confirm({
        text: this.translate.instant("validations.minWeight", {
          min: minWeight,
        }),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmButtonText"
        ),
        cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
      });

      if (wantMinWeightPackage) {
        this.wantMinWeight = true;
        isInvalid = false;
      }
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationMaximumWeight = () => {
    let isInvalid: boolean = false;
    const packageId = this.package.get("id").value;

    const { type, formula, maximum_weight: maxWeight } = this.findById(
      packageId
    );

    const higherWeight = this.getMaxWeightPackage();
    const name: string = this.getTranslateType(type);

    const topWeight =
      formula === FORMULA_PACKAGEPOUND ? MAXIMUM_WEIGHT_PACKAGE : maxWeight;

    if (higherWeight > topWeight) {
      isInvalid = true;

      showMessage(
        null,
        this.translate.instant("validations.maxWeight", {
          name: name,
        }),
        "warning",
        true
      );
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationMinimumInch = async () => {
    let isInvalid: boolean;
    const wantMinWeight = this.wantMinWeight;
    const { inch } = this.packageMeasurements();

    const packageId = this.package.get("id").value;

    const { minimum_weight: minWeight } = this.findById(packageId);

    if (!wantMinWeight && inch < minWeight) {
      isInvalid = true;

      const wantMinInchPackage = await confirm({
        text: this.translate.instant("validations.minInch", {
          min: minWeight,
        }),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmButtonText"
        ),
        cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
      });

      if (wantMinInchPackage) {
        this.wantMinWeight = true;
        isInvalid = false;
      }
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationMaximumInch = () => {
    let isInvalid: boolean = false;
    const { inch } = this.packageMeasurements();
    const packageId = this.package.get("id").value;

    const { maximum_weight: maxInch } = this.findById(packageId);

    if (inch > maxInch) {
      showMessage(
        null,
        this.translate.instant("validations.maxInch", {
          max: maxInch,
        }),
        "warning",
        true
      );

      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationMinimumFoot = async () => {
    let isInvalid: boolean = false;
    const { long, high, width } = this.packageMeasurements();
    const packageId = this.package.get("id").value;

    const { minimum_weight: minFoot } = this.findById(packageId);

    const measurements = long * high * width;
    const calc = this.getMeasurementsByCubitFoot(measurements);
    console.log(calc);
    if (calc < minFoot) {
      isInvalid = true;

      const wantMinFootPackage = await confirm({
        text: this.translate.instant("validations.minFoot", {
          min: minFoot,
        }),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmButtonText"
        ),
        cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
      });

      if (wantMinFootPackage) {
        this.wantMinWeight = true;
        isInvalid = false;
      }
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationMaximumFoot = () => {
    let isInvalid: boolean = false;
    const { long, high, width } = this.packageMeasurements();
    const packageId = this.package.get("id").value;

    const { maximum_weight: maxFoot } = this.findById(packageId);
    const name: string = this.translate.instant("typePackage.cubic_foot");

    const measurements = long * high * width;
    const calc = this.getMeasurementsByVolumetricWeight(measurements);

    if (calc > maxFoot) {
      showMessage(
        null,
        this.translate.instant("validations.maxWeight", {
          name: name,
        }),
        "warning",
        true
      );

      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationMinInsurancePackage = async () => {
    let isInvalid: boolean = false;
    const amount: number = this.package.get("insured_value").value;
    const insurance: boolean = this.package.get("insurance").value;
    const packageId = this.package.get("id").value;
    const { minimum_insurance: minInsurance } = this.findById(packageId);

    if (insurance && amount < minInsurance) {
      isInvalid = true;

      const wantMinInsurancePackage = await confirm({
        text: this.translate.instant("validations.minInsurance", {
          min: minInsurance,
        }),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmButtonText"
        ),
        cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
      });

      if (wantMinInsurancePackage) {
        this.wantMinInsurance = true;
        this.patchValue("insured_value", minInsurance);
        isInvalid = false;
      }
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationMaxInsurancePackage = () => {
    let isInvalid: boolean = false;
    const amount: number = this.package.get("insured_value").value;
    const insurance: boolean = this.package.get("insurance").value;

    const typeContent: number = this.package.get("type_content").value;
    const packageId = this.package.get("id").value;

    const { maximum_insurance: maxInsurance } = this.findById(packageId);
    const typeContentAllowed = typeContent !== TYPECONTENT_COMMERCIAL;

    if (insurance && typeContentAllowed && amount > maxInsurance) {
      showMessage(
        null,
        this.translate.instant("validations.maxInsurance", {
          amount: maxInsurance,
        }),
        "warning",
        true
      );

      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationMaxInsuranceTypeCommercialPackage = async () => {
    let isInvalid: boolean = false;
    const amount: number = this.package.get("declared").value;
    const insurance: boolean = this.package.get("insurance").value;
    const typeContent: number = this.package.get("type_content").value;
    const packageId = this.package.get("id").value;
    const file = this.package.get("file").value;
    const amountInsurace: number = this.package.get("insured_value").value;

    const {
      maximum_insurance: maxInsurance,
      commercial_surplus: surplus,
    } = this.findById(packageId);
    const typeContentAllowed = typeContent === TYPECONTENT_COMMERCIAL;

    if (
      insurance &&
      typeContentAllowed &&
      amount > maxInsurance &&
      amountInsurace > maxInsurance &&
      (!surplus || amountInsurace <= surplus)
    ) {
      isInvalid = true;

      const wantMaxTypeCommercialInsurance = await confirm({
        text: this.translate.instant("validations.maxTypeCommercialInsurance"),
        confirmButtonText: this.translate.instant(
          "buttonText.confirmCoverageButtonText"
        ),
        cancelButtonText: this.translate.instant(
          "buttonText.cancelCoverageButtonText"
        ),
      });

      if (!wantMaxTypeCommercialInsurance) {
        isInvalid = false;
        this.wantMaxTypeCommercialInsurance = true;
      } else if (wantMaxTypeCommercialInsurance && file) {
        isInvalid = false;
      }
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationSurplus = async () => {
    let isInvalid: boolean = false;
    const amount: number = this.package.get("declared").value;
    const insurance: boolean = this.package.get("insurance").value;
    const amountInsurace: number = this.package.get("insured_value").value;

    const typeContent: number = this.package.get("type_content").value;
    const packageId = this.package.get("id").value;
    const {
      maximum_insurance: maxInsurance,
      commercial_surplus: surplus,
    } = this.findById(packageId);
    const typeContentAllowed = typeContent === TYPECONTENT_COMMERCIAL;
    if (
      surplus &&
      insurance &&
      typeContentAllowed &&
      amount > maxInsurance &&
      amountInsurace > surplus
    ) {
      showMessage(
        null,
        this.translate.instant("validations.maxInsuranceSurplus", {
          surplus: surplus,
          amount: maxInsurance,
        }),
        "warning",
        true
      );

      isInvalid = true;
    }
    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationInsurancePackage = async () => {
    let isInvalid: boolean = false;
    const insurance: boolean = this.package.get("insurance").value;

    if (!insurance) {
      showMessage(
        null,
        this.translate.instant("validations.insuranceRequired"),
        "warning",
        true
      );

      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidationImpostPackage = () => {
    let isInvalid: boolean = false;
    const impost: boolean = this.package.get("impost").value;

    if (!impost) {
      showMessage(
        null,
        this.translate.instant("validations.impostRequired"),
        "warning",
        true
      );

      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidateDeclared = () => {
    let isInvalid: boolean = false;
    const declared = this.package.get("declared").value;

    if (declared === ZERO) {
      showMessage(
        null,
        this.translate.instant("validations.requiredDeclared"),
        "warning",
        true
      );

      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  getValidateInsuranceZero = () => {
    let isInvalid: boolean = false;
    const insuredValue = this.package.get("insured_value").value;

    if (insuredValue === ZERO) {
      showMessage(
        null,
        this.translate.instant("validations.insuranceValueZero"),
        "warning",
        true
      );

      isInvalid = true;
    }

    return new Promise<boolean>((resolve, _) => {
      resolve(isInvalid);
    });
  };

  onSubmit = async () => {
    const infoValid = this.package.valid;
    const isEditing = this.isEditing;
    const submitted = this.submitted;

    this.errors = null;

    if (!infoValid) {
      showMessage(
        null,
        this.translate.instant("validations.ErrorValue"),
        "warning",
        true
      );
      return validationsForm(this.package);
    } else if (submitted) {
      return;
    }

    const styType: string = this.getTypeMessage();
    const packageId = this.package.get("id").value;

    const { formula: type } = this.findById(packageId);

    // Validación de medidas de los paquetes
    if (
      type === FORMULA_PACKAGE ||
      type === FORMULA_PACKAGEPOUND ||
      type === FORMULA_TELEVISIONS ||
      type === FORMULA_ELECTRONICS ||
      type === FORMULA_INCHLINEAR
    ) {
      const isInvalidMeasuresPackage = await this.getValidationMeasurementPackage();

      if (isInvalidMeasuresPackage) {
        return;
      }
    }

    // Validación de peso minimo
    if (
      type === FORMULA_PACKAGE ||
      type === FORMULA_PACKAGEPOUND ||
      type === FORMULA_POUND ||
      type === FORMULA_MESSENGERSERVICE
    ) {
      const isInvalidMinWeightPackage = await this.getValidationMinimumWeight();

      if (isInvalidMinWeightPackage) {
        return;
      }
    }

    if (
      type === FORMULA_PACKAGE ||
      type === FORMULA_PACKAGEPOUND ||
      type === FORMULA_POUND ||
      type === FORMULA_MESSENGERSERVICE ||
      type === FORMULA_CUBICFOOT
    ) {
      const isInvalidMaxWeightPackage = await this.getValidationMaximumWeight();

      if (isInvalidMaxWeightPackage) {
        return;
      }
    }

    if (type === FORMULA_TELEVISIONS || type === FORMULA_INCHLINEAR) {
      const isInvalidMinInchPackage = await this.getValidationMinimumInch();

      if (isInvalidMinInchPackage) {
        return;
      }
    }

    if (type === FORMULA_TELEVISIONS || type === FORMULA_INCHLINEAR) {
      const isInvalidMaxInchPackage = await this.getValidationMaximumInch();

      if (isInvalidMaxInchPackage) {
        return;
      }
    }

    if (type === FORMULA_CUBICFOOT) {
      const isInvalidMinFootPackage = await this.getValidationMinimumFoot();

      if (isInvalidMinFootPackage) {
        return;
      }
    }

    if (type === FORMULA_CUBICFOOT) {
      const isInvalidMaxFootPackage = await this.getValidationMaximumFoot();

      if (isInvalidMaxFootPackage) {
        return;
      }
    }

    if (type !== FORMULA_MESSENGERSERVICE) {
      const requiredDeclared = await this.getValidateDeclared();

      if (requiredDeclared) {
        return;
      }

      const insuranceZero = await this.getValidateInsuranceZero();

      if (insuranceZero) {
        return;
      }

      const isInvalidImpostPackage = await this.getValidationImpostPackage();

      if (isInvalidImpostPackage) {
        return;
      }

      const isInvalidInsurancePackage = await this.getValidationInsurancePackage();

      if (isInvalidInsurancePackage) {
        return;
      }

      const isInvalidMaxInsurancePackage = await this.getValidationMaxInsurancePackage();

      if (isInvalidMaxInsurancePackage) {
        return;
      }

      const isValidSurplus = await this.getValidationSurplus();
      if (isValidSurplus) {
        return;
      }
      const isInvalidMaxInsuranceTypeCommercialPackage = await this.getValidationMaxInsuranceTypeCommercialPackage();

      if (isInvalidMaxInsuranceTypeCommercialPackage) {
        return;
      }
    }

    this.submitted = true;

    this.packageService
      .getCharge(
        {
          ...this.package.getRawValue(),
          country_id: this.countryId,
          file: this.imageSrc,
          minInsurance: this.wantMinInsurance,
          maxTypeCommercial: this.wantMaxTypeCommercialInsurance,
          membershipType: this.membershipType,
        },
        this.isLocker
      )
      .subscribe(
        (charge: ChargeForm) => {
          showMessage("", this.translate.instant(styType), "success", true);

          this.setCharge.emit(charge);
          this.onClose.emit();
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;

          if (isEditing) {
            this.lockInputs();
          }

          if ((error.error || {}).validator) {
            //
            showMessage(
              "",
              this.translate.instant(`validations.${error.error.validator}`),
              "warning",
              true
            );

            return;
          }

          this.handleError(error);
        }
      );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );

      return;
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    let name = this.translate.instant(value).toLowerCase();
    return this.translate.instant("validations.required", { name: name });
  };
}
