import { Component, Input, Output, EventEmitter } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";

import { TranslateService } from "@ngx-translate/core";
import { HttpErrorResponse } from "@angular/common/http";

import { Quote, User, Package } from "@models";
import { Store } from "@ngrx/store";

import { QuoteService } from "@core/service";
import { rawError, handlerError } from "@shared/utils";
import { showMessage, TierId, getServiceDescriptionLang } from "@shared/utils";

import * as fromRoot from "@reducers";

@Component({
  selector: "cml-tag",
  templateUrl: "./tag.component.html",
  styles: [],
})
export class TagComponent {
  @Input() quote: Quote;
  @Input() isLocker: boolean;
  @Output() setValuesDefault = new EventEmitter<null>();

  public submitted: boolean = false;
  public errors: any;
  public user: User;
  public tagStep: number = 1;
  public urlFile: SafeResourceUrl;

  constructor(
    private translate: TranslateService,
    private quoteService: QuoteService,
    public domSanitizer: DomSanitizer,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  getServiceDescription = (service: Package): string => {
    return getServiceDescriptionLang(service, this.translate.getDefaultLang());
  };

  setDefaultValues = () => {
    this.setValuesDefault.emit();
  };

  onBeforeStep = (step: number) => {
    this.tagStep = step;
  };

  showByAgency = (): boolean => {
    if (!!this.user) {
      const { agency } = this.user;
      const tierId = !!agency ? agency.tier : this.user.tier;

      return tierId === TierId.TIER_AGENCY;
    }
  };

  printPackging = (quoteId: number) => {
    if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.quoteService.printPackging({ quoteId }).subscribe(
      (url: string) => {
        this.submitted = false;
        this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
        this.tagStep = 2;
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  printPackgingTag = (quoteId: number, chargeId: number) => {
    if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.quoteService.printPackgingTag({ quoteId, chargeId }).subscribe(
      (url: string) => {
        this.submitted = false;
        this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
        this.tagStep = 2;
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };

  printInternalTraffic = (quoteId: number, chargeId: number, key: number) => {
    const quantityCharge = this.quote.charge.length;
    const trueKey = key + 1;
    const namePackage = `(${trueKey}/${quantityCharge})`;

    if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.quoteService
      .printInternalTraffic({ quoteId, chargeId, namePackage })
      .subscribe(
        (url: string) => {
          this.submitted = false;
          this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
          this.tagStep = 2;
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
  };

  printBoarding = (quoteId: number, chargeId: number, key: number) => {
    const quantityCharge = this.quote.charge.length;
    const trueKey = key + 1;
    const namePackage = `(${trueKey}/${quantityCharge})`;

    if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.quoteService
      .printBoarding({ quoteId, chargeId, namePackage })
      .subscribe(
        (url: string) => {
          this.submitted = false;
          this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
          this.tagStep = 2;
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };
}
