import { FormControl, Validators } from "@angular/forms";

export const PaymentSettingFormValidator = {
  id: new FormControl("", []),
  country_id: new FormControl(1, [Validators.required]),
  name: new FormControl({ value: null, disabled: true }, []),
  user_settings: new FormControl(null, [Validators.required]),
  payment: new FormControl(null, [Validators.required]),
  type: new FormControl(null, [Validators.required]),
  type_amount: new FormControl(null, [Validators.required]),
  amount: new FormControl(null, [
    Validators.required,
    Validators.maxLength(15),
    Validators.min(0),
  ]),
};
