import { Component, OnInit, TemplateRef } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { FormGroup, FormBuilder } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { PaymentSettingFormValidator } from "./FormPaymentSettingValidator";
import { Country, PaymentSetting, User } from "@models";
import { CountryService, PaymentSettingService } from "@core/service";
import { showMessage } from "@shared/utils";
import { TierId, getCountryNameLang } from "@shared/utils";
import { TypePayment, TypeSetting, TypeUser } from "@shared/utils";
import { rawError, handlerError, validationsForm } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-payment-settings",
  templateUrl: "./payment-settings.component.html",
  styles: [],
})
export class PaymentSettingsComponent implements OnInit {
  private modalRef: NgbModalRef;

  public isLoading: boolean = true;
  public submitted: boolean = false;
  public user: User;
  public countries: Country[];
  public countryId: number;
  public paymentSettings: PaymentSetting[] = [];
  public country: FormGroup;
  public errors: Array<any>;

  constructor(
    private ngbModal: NgbModal,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private countryService: CountryService,
    private paymentSetting: PaymentSettingService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.country = this.formBuilder.group(PaymentSettingFormValidator);
  }

  ngOnInit(): void {
    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.isLoading = false;
        this.countries = countries;

        const country = countries.find(({ alpha2Code }) => {
          return alpha2Code === "MX";
        });

        if (!!country) {
          this.countryId = country.id;
          this.paymentSettings = country.payment_setting;
        }
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  isFieldValid = (field: string): boolean => {
    return this.country.get(field).invalid && this.country.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  getPaymentName = (type: number): string => {
    return this.translate.instant(TypePayment.getDisplayName(type));
  };

  getTypeSettingName = (setting: number): string => {
    return this.translate.instant(TypeSetting.getDisplayName(setting));
  };

  getTypeUserName = (type: number): string => {
    return this.translate.instant(TypeUser.getDisplayName(type));
  };

  setPatchValueNull = () => {
    this.country.patchValue(
      {
        id: null,
        type_amount: null,
        amount: null,
      },
      { onlySelf: true }
    );
  };

  toggleButtonByTierId = (typeId: number): boolean => {
    if (!!this.user) {
      const { role } = this.user;

      const module =
        this.user.tier !== TierId.TIER_ADMIN
          ? role.assignments
              .filter(({ submodule }) => submodule.path === "commissions")
              .find(({ type }) => type === typeId)
          : null;

      return this.user.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  onChangeCountryId = () => {
    const country = this.countries.find(({ id }) => {
      return id == this.countryId;
    });

    return (this.paymentSettings = country.payment_setting);
  };

  getFormValue = (name: string) => {
    return this.country.get(name).value;
  };

  hiddenByValue = (name: string) => {
    return !!this.country.get(name).value;
  };

  onChangeTypeSetting = () => {
    return this.country.patchValue(
      {
        id: null,
        payment: null,
        type: null,
        type_amount: null,
        amount: null,
      },
      { onlySelf: true }
    );
  };

  onChangeTypePayment = () => {
    return this.country.patchValue(
      {
        id: null,
        type: null,
        type_amount: null,
        amount: null,
      },
      { onlySelf: true }
    );
  };

  onChangeTypeUser = () => {
    const payment = this.paymentSettings.find(
      ({ user_settings, payment, type }) => {
        return (
          user_settings === this.getFormValue("user_settings") &&
          payment === this.getFormValue("payment") &&
          type === this.getFormValue("type")
        );
      }
    );

    if (!!payment) {
      return this.country.patchValue(
        {
          id: payment.id,
          type_amount: payment.type_amount,
          amount: payment.amount,
        },
        { onlySelf: true }
      );
    }

    this.setPatchValueNull();
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
  };

  shouldToggleUpdate = (ngbCountry: TemplateRef<any>, countryId: number) => {
    const country: Country = this.countries.find(({ id }) => id === countryId);

    this.country.reset();

    if (!!country) {
      this.country.patchValue(
        {
          country_id: countryId,
          name: this.getCountryName(country),
          user_settings: null,
          payment: null,
          type: null,
          type_amount: null,
          amount: null,
        },
        { onlySelf: true }
      );

      this.modalRef = this.ngbModal.open(ngbCountry);
    }
  };

  shoulderUpdateCountry = (data: PaymentSetting) => {
    let keyCountry = this.countries.findIndex(
      ({ id }) => id === this.countryId
    );

    let keyPayment = this.paymentSettings.findIndex(({ id }) => id === data.id);

    let resCountry = [...this.countries];
    let resPayment = [...this.paymentSettings];

    if (!!resPayment[keyPayment]) {
      resPayment[keyPayment] = {
        ...data,
      };
    } else {
      resPayment = [...resPayment, data];
    }

    if (!!resCountry[keyCountry]) {
      resCountry[keyCountry] = {
        ...resCountry[keyCountry],
        payment_setting: resPayment,
      };

      this.paymentSettings = resPayment;
      this.countries = resCountry;
    }
  };

  onSubmit = () => {
    const infoValid = this.country.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.country);
    }

    if (!!!this.submitted) {
      this.submitted = true;

      return this.paymentSetting
        .storeOrUpdate({ ...this.country.value })
        .subscribe(
          (payment: PaymentSetting) => {
            this.submitted = false;

            this.shoulderUpdateCountry(payment);

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.default"),
              "success",
              true
            );

            this.country.reset();
            this.modalRef.close();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
