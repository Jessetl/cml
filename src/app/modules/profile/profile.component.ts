import { Component, OnInit, TemplateRef } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { Role as Profile, Roles as Profiles, User } from "@models";
import { ProfileService } from "@core/service";

import {
  showMessage,
  handlerError,
  validationsForm,
  rawError,
  showConfirmButton,
  profilesLang,
  getNameProfileLang,
} from "@shared/utils";

import * as fromRoot from "@reducers";
import { Store } from "@ngrx/store";

const profileValidator = {
  id: new FormControl("", []),
  user_id: new FormControl(0, []),
  name: new FormControl("", [Validators.required, Validators.maxLength(60)]),
  name_en: new FormControl("", [Validators.required, Validators.maxLength(60)]),
};

@Component({
  selector: "cml-profile",
  templateUrl: "./profile.component.html",
  styles: [],
})
export class ProfileComponent implements OnInit {
  private modalRef: NgbModalRef;
  public isLoading: boolean = true;
  public isEditing: boolean = false;
  public user: User;
  public profiles: Profile[];
  public profile: FormGroup;
  public submitted: boolean;
  public errors: Array<any>;
  public paginate: number = 1;

  constructor(
    private ngbModal: NgbModal,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private profileService: ProfileService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));

    this.profile = this.formBuilder.group(profileValidator);
  }

  ngOnInit() {
    const { id } = this.user;

    if (!!this.user) {
      this.profile.patchValue({ user_id: id }, { onlySelf: true });
    }
    this.isLoading = true;
    this.profileService.getProfiles().subscribe(
      (profiles: Profile[]) => {
        this.isLoading = false;
        this.profiles = profilesLang(profiles, this.translate.getDefaultLang());
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  isFieldValid = (field: string): boolean => {
    return this.profile.get(field).invalid && this.profile.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  shouldToggleCreate = (ngbProfile: TemplateRef<any>) => {
    this.isEditing = false;

    this.profile.reset();

    this.modalRef = this.ngbModal.open(ngbProfile);
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
  };

  shouldToggleDelete = (profileId: number) => {
    showConfirmButton(
      this.translate.instant("ngbAlert.deleteProfile"),
      this.translate.instant("ngbAlert.btnYes"),
      this.translate.instant("ngbAlert.btnNo"),
      () => {
        this.profileService.destroy(profileId).subscribe(
          () => {
            this.shoulderDestroyProfile(profileId);

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.destroyProfile"),
              "success",
              true
            );

            this.onCloseNgbModal();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
      }
    );
  };

  shouldToggleUpdate = (ngbProfile: TemplateRef<any>, profileId: number) => {
    this.isEditing = true;
    const profile: Profile = this.profiles.find(({ id }) => id === profileId);

    this.profile.patchValue({
      ...profile,
    });

    this.modalRef = this.ngbModal.open(ngbProfile);
  };

  shouldMakeProfile = (prof: Profile) => {
    let profile = getNameProfileLang(prof, this.translate.getDefaultLang());
    this.profiles = [...this.profiles, profile];
  };

  shoulderUpdateProfile = (prof: Profile) => {
    let profile = getNameProfileLang(prof, this.translate.getDefaultLang());
    let keyProfile = this.profiles.findIndex(({ id }) => id === profile.id);
    let resProfile = [...this.profiles];

    resProfile[keyProfile] = {
      ...profile,
    };

    this.profiles = resProfile;
  };

  shoulderDestroyProfile = (profileId: number) => {
    this.profiles = this.profiles.filter(({ id }) => id !== profileId);
  };

  onSubmit = () => {
    const infoValid = this.profile.valid;
    const isEditing = this.isEditing;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.profile);
    }
    this.submitted = true;
    let profile = getNameProfileLang(
      this.profile.value,
      this.translate.getDefaultLang()
    );

    switch (isEditing) {
      case true:
        return this.profileService.update(profile).subscribe(
          (profile: Profile) => {
            this.submitted = false;

            this.shoulderUpdateProfile(profile);

            this.profile.reset();

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.updateProfile"),
              "success",
              true
            );

            this.onCloseNgbModal();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
      case false:
        return this.profileService.store(profile).subscribe(
          (profile: Profile) => {
            this.submitted = false;

            this.shouldMakeProfile(profile);

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.createProfile"),
              "success",
              true
            );

            this.onCloseNgbModal();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
    }
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
      return;
    }

    this.errors = rawError(errorResponse);
  };

  validations = (value: string): string => {
    let name = this.translate.instant(value).toLowerCase();
    return this.translate.instant("validations.required", { name: name });
  };
}
