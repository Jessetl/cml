import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { FilesComponent } from "./files/files.component";

const routes: Routes = [
  {
    path: "files",
    component: FilesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DocumentsRoutingModule {}

export const routingComponents = [FilesComponent];
