import { Component, OnInit, TemplateRef } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { Document } from "@models";
import { FilesService } from "@core/service";
import { showMessage, handlerError } from "@shared/utils";

@Component({
  selector: "cml-files",
  templateUrl: "./files.component.html",
  styles: [],
})
export class FilesComponent implements OnInit {
  private modalRef: NgbModalRef;

  public isLoading: boolean = true;
  public files: Document[];
  public urlFile: SafeResourceUrl;
  public step: number = 1;
  public imageSrc: string;

  constructor(
    private filesService: FilesService,
    private translate: TranslateService,
    public domSanitizer: DomSanitizer,
    private ngbModal: NgbModal
  ) {}

  ngOnInit(): void {
    this.filesService.getUserFiles().subscribe(
      (files: Document[]) => {
        this.files = files;
        this.isLoading = false;
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  }

  onToggleModal = (ngbModal: TemplateRef<any>, image: string) => {
    this.imageSrc = image;

    this.modalRef = this.ngbModal.open(ngbModal, {
      size: "md",
    });
  };

  getFileExtension = (filename) => {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  };

  bypassSecurityTrustResourceUrl = (file: string) => {
    this.step = 2;
    return this.domSanitizer.bypassSecurityTrustResourceUrl(file);
  };

  onToggleDocument = (nbg: TemplateRef<any>, file: Document) => {
    if (!!file.url) {
      if (this.getFileExtension(file.url) === "pdf") {
        this.urlFile = this.bypassSecurityTrustResourceUrl(file.image_url);
      } else {
        return this.onToggleModal(nbg, file.image_url);
      }
    } else {
      return showMessage(
        "",
        this.translate.instant("validators.noFile"),
        "warning",
        true
      );
    }
  };

  onCloseNgbModal = (): void => {
    this.imageSrc = null;
    this.step = 1;
    this.modalRef.close();
  };
}
