import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@shared";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { DocumentsRoutingModule } from "./documents-routing.module";
import { routingComponents } from "./documents-routing.module";

@NgModule({
  declarations: [routingComponents],
  imports: [
    CommonModule,
    DocumentsRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class DocumentsModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
