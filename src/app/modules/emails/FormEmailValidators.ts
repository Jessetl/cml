import { FormControl, FormGroup, Validator, Validators } from "@angular/forms";

export const EmailValidators = {
  id: new FormControl(null, [Validators.required]),
  name: new FormControl(null, []),
  html: new FormControl("", [Validators.required]),
  type: new FormControl(null, [Validators.required]),
};
