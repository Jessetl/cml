import { Component, OnInit } from "@angular/core";
import { AngularEditorConfig } from "@kolkov/angular-editor";
import { HttpErrorResponse } from "@angular/common/http";
import { TranslateService } from "@ngx-translate/core";
import { EmailService } from "@core/service";
import { Email } from "@models";
import { showMessage, handlerError, validationsForm } from "@shared/utils";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { EmailValidators } from "./FormEmailValidators";
@Component({
  selector: "cml-emails",
  templateUrl: "./emails.component.html",
  styles: [],
})
export class EmailsComponent implements OnInit {
  public htmlContent: any;
  public emails: Email[];
  public email: FormGroup;
  public errors: Array<any>;
  public submitted: boolean = false;
  public step: number = 1;
  public typeEmail: number;
  public isLoading: boolean = true;

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: "auto",
    minHeight: "0",
    maxHeight: "auto",
    width: "100%",
    minWidth: "0",
    translate: "yes",
    enableToolbar: true,
    showToolbar: true,
    placeholder: "Enter text here...",
    defaultParagraphSeparator: "",
    defaultFontName: "",
    defaultFontSize: "",
    fonts: [
      { class: "arial", name: "Arial" },
      { class: "times-new-roman", name: "Times New Roman" },
      { class: "calibri", name: "Calibri" },
      { class: "comic-sans-ms", name: "Comic Sans MS" },
    ],
    customClasses: [],
    uploadUrl: "v1/image",
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: "top",
    toolbarHiddenButtons: [
      [
        "justifyLeft",
        "justifyCenter",
        "justifyRight",
        "justifyFull",
        "strikeThrough",
        "subscript",
        "superscript",
        "outdent",
        "insertUnorderedList",
        "insertOrderedList",
        "fontName",
      ],
      [
        "backgroundColor",
        "customClasses",
        "link",
        "unlink",
        "insertImage",
        "insertVideo",
      ],
    ],
  };
  constructor(
    private translate: TranslateService,
    private emailService: EmailService,
    private formBuilder: FormBuilder
  ) {
    this.email = this.formBuilder.group(EmailValidators);
  }

  ngOnInit(): void {
    this.getEmails();
  }

  getEmails = () => {
    this.emailService.getEmails().subscribe(
      (emails: Email[]) => {
        this.emails = emails;
        this.isLoading = false;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  isFieldValid = (field: string): boolean => {
    return this.email.get(field).invalid && this.email.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };
  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };

  onSubmit = () => {
    const infoValid = this.email.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.email);
    } else if (this.submitted) {
      return;
    }

    this.submitted = true;

    this.emailService.updateEmail(this.email.getRawValue()).subscribe(
      (email: Email) => {
        this.submitted = false;
        this.email.patchValue({
          ...email,
        });
        const copyEmails = [...this.emails];
        const key = this.emails.findIndex(({ id }) => id === email.id);
        copyEmails[key] = {
          ...email,
        };
        this.emails = copyEmails;

        showMessage(
          "¡Enhorabuena!",
          this.translate.instant("notifications.updateEmail"),
          "success",
          true
        );
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  shouldToggleUpdate = (emailId: number) => {
    const email: Email = this.emails.find(({ id }) => id === emailId);

    this.email.patchValue({
      ...email,
    });

    this.typeEmail = email.type;

    this.step = 2;
  };

  back = () => {
    this.step = 1;
    this.typeEmail = null;
    this.email.reset();
  };
  gethtml = () => {
    return this.email.get("html").value;
  };
}
