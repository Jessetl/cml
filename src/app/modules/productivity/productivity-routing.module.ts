import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Components
import { ProductivityComponent } from "./productivity/productivity.component";
import { TransactionComponent } from "./transaction/transaction.component";
import { PaymentsComponent } from "./payments/payments.component";
import { PayComponent } from "./pay/pay.component";

const routes: Routes = [
  {
    path: "",
    component: ProductivityComponent,
  },
  {
    path: "payments",
    component: PaymentsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductivityRoutingModule {}

export const routingComponents = [
  ProductivityComponent,
  PaymentsComponent,
  PayComponent,
  TransactionComponent,
];
