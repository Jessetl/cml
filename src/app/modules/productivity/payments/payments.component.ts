import { Component, OnInit, TemplateRef } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { Country, Quote, User, UserProductivity } from "@models";
import { ProductivityService } from "@core/service";
import { handlerError, STATUS_ACTIVE } from "@shared/utils";
import { showMessage, TierId } from "@shared/utils";
import { getCountryNameLang, getOriginNameLang } from "@shared/utils";
import { TYPE_SUPERVISOR, TYPE_SELLER } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-payments",
  templateUrl: "./payments.component.html",
  styles: [],
})
export class PaymentsComponent implements OnInit {
  private modalRef: NgbModalRef;

  public isLoading: boolean = true;
  public submitted: boolean = false;
  public productivity: UserProductivity[] = [];
  public data: UserProductivity[] = [];
  public user: UserProductivity;
  public quotes: Quote[];
  public me: User;
  public urlFile: SafeResourceUrl;

  public typeUser: number = 0;

  constructor(
    private ngbModal: NgbModal,
    private translate: TranslateService,
    private productivityService: ProductivityService,
    public domSanitizer: DomSanitizer,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.me = user));
  }

  ngOnInit(): void {
    this.getShippingPaymentsByUsers();
  }

  getShippingPaymentsByUsers = () => {
    this.isLoading = true;
    this.productivity = this.data = [];

    this.productivityService.getShippingPaymentsByUsers().subscribe(
      (productivity: UserProductivity[]) => {
        this.productivity = this.data = productivity
          .map((user) => {
            return {
              ...user,
              tier: this.translate.instant(
                `tierId.${TierId.getDisplayName(user.user.tier)}`
              ),
            };
          })
          .filter((row) => {
            return row.payment_income || row.payment_shipping;
          });

        this.isLoading = false;
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  getOriginName = (shipping: Quote): string => {
    return getOriginNameLang(shipping, this.translate.getDefaultLang());
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  getStatusName = (status: number): string => {
    return this.translate.instant(
      status === STATUS_ACTIVE ? "select.confirmed" : "select.unconfirmed"
    );
  };

  getPaymentList = (ngbPayment: TemplateRef<any>, userId: number) => {
    const productivity = this.productivity.find(
      ({ user_id }) => user_id === userId
    );

    if (!!productivity) {
      this.quotes = productivity.quotes.map((quote) => {
        return {
          ...quote,
          invoicing: quote.invoicing[0],
        };
      });

      this.modalRef = this.ngbModal.open(ngbPayment, {
        size: "lg",
      });
    }
  };

  getProductivityPay = (ngbPay: TemplateRef<any>, userId: number) => {
    const productivity = this.productivity.find(
      ({ user_id }) => user_id === userId
    );

    if (!!productivity) {
      this.user = productivity;

      this.modalRef = this.ngbModal.open(ngbPay, {
        size: "lg",
      });
    }
  };

  getTypeTierByRole = (tierId: number): number => {
    switch (tierId) {
      case TierId.TIER_SELLER:
        return TYPE_SELLER;
      case TierId.TIER_SUPERVISOR:
        return TYPE_SUPERVISOR;
    }
  };

  filterTypeUserData = () => {
    if (!!this.typeUser) {
      return (this.data = this.productivity.filter((row) => {
        return this.getTypeTierByRole(row.user.tier) === this.typeUser;
      }));
    }

    this.data = this.productivity;
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.me) {
      const module =
        this.me.tier !== TierId.TIER_ADMIN
          ? this.me.role.assignments
              .filter(
                (assignment) =>
                  assignment.submodule.path === "productivity/payments"
              )
              .find((assignment) => assignment.type === type)
          : null;

      return this.me.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  onCloseNgbModal = (url: string): void => {
    if (!!this.user && !!url) {
      this.data = this.productivity = this.productivity.filter((row) => {
        return row.user_id !== this.user.user_id;
      });

      this.user = null;
      this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(url);
    }

    this.modalRef.close();
  };
}
