import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { SignaturePadModule } from "angular2-signaturepad";

import { ProductivityRoutingModule } from "./productivity-routing.module";
import { routingComponents } from "./productivity-routing.module";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SharedModule } from "@shared";

@NgModule({
  declarations: [routingComponents],
  imports: [
    CommonModule,
    ProductivityRoutingModule,
    SignaturePadModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class ProductivityModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
