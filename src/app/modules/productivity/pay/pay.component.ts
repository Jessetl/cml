import { Component, OnInit } from "@angular/core";
import { Input, Output, EventEmitter } from "@angular/core";

import { TranslateService } from "@ngx-translate/core";

import { Country, Quote, User } from "@models";
import { UserProductivity } from "@models";
import { showMessage, TierId } from "@shared/utils";
import { getCountryNameLang, getOriginNameLang } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";
import * as moment from "moment";

@Component({
  selector: "cml-pay",
  templateUrl: "./pay.component.html",
  styles: [],
})
export class PayComponent implements OnInit {
  @Input() productivity: UserProductivity;
  @Output() onClose = new EventEmitter<string>();

  private format: string = "MM-DD-YYYY-hh:mm";

  public step: number = 1;
  public quotes: Quote[] = [];
  public users: User[] = [];
  public user: User;
  public hasProductivity: boolean = false;

  public totalQuotes: number = 0;
  public totalUsers: number = 0;
  public total: number = 0;

  constructor(
    private translate: TranslateService,
    private store: Store<fromRoot.State>
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit(): void {
    if (!!this.productivity) {
      this.quotes = this.productivity.quotes.map((quote) => {
        return {
          ...quote,
          invoicing: quote.invoicing[0],
        };
      });

      this.users = this.productivity.users.map((user) => user);

      this.totalUsers = parseFloat(
        this.users
          .reduce(function (previusValue: number, currentValue: User) {
            return (
              previusValue + parseFloat(currentValue.commission.toFixed(2))
            );
          }, 0)
          .toFixed(2)
      );

      this.totalQuotes = parseFloat(
        this.quotes
          .reduce(function (previusValue: number, currentValue: Quote) {
            return (
              previusValue + parseFloat(currentValue.commission.toFixed(2))
            );
          }, 0)
          .toFixed(2)
      );

      this.total = this.totalUsers + this.totalQuotes;
      this.hasProductivity = !!this.productivity.user.productivity_payment
        .length;
    }
  }

  onChangeStep = (step: number) => {
    const { user } = this.productivity;

    if (!!!user.paypal && !!!user.bancking) {
      return showMessage(
        null,
        this.translate.instant("notifications.configuredPayments"),
        "warning",
        true
      );
    }

    this.step = step;
  };

  getOriginName = (shipping: Quote): string => {
    return getOriginNameLang(shipping, this.translate.getDefaultLang());
  };

  getCountryName = (country: Country): string => {
    return getCountryNameLang(country, this.translate.getDefaultLang());
  };

  getTypeUser = (tierId: number): string => {
    return this.translate.instant(`tierId.${TierId.getDisplayName(tierId)}`);
  };

  getPriceByUser = (shipping: Quote): number => {
    return shipping.locker ? shipping.subtotal : shipping.subtotal_user;
  };

  getFileName = (): string => {
    return moment().format(this.format).toString();
  };

  onCloseNgbModal = (url: string): void => {
    this.onClose.emit(url);
  };
}
