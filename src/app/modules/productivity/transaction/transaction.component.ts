import { Component, OnInit } from "@angular/core";
import { Input, Output, EventEmitter } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";

import { UserProductivity, Productivity, Bancking } from "@models";
import { DepositForm, PaypalForm, TransferForm } from "@models";
import { GooglemapsService, ProductivityService } from "@core/service";
import { showMessage, handlerError, rawError } from "@shared/utils";

@Component({
  selector: "cml-transaction",
  templateUrl: "./transaction.component.html",
  styles: [],
})
export class TransactionComponent implements OnInit {
  @Input() productivity: UserProductivity;
  @Input() grandTotal: number;
  @Output() onClose = new EventEmitter<string>();

  public errors: Array<any> = [];
  public paymentMethod: number = 0;
  public submitted: boolean = false;
  public data: Bancking;
  public latitude: string;
  public longitude: string;

  constructor(
    private translate: TranslateService,
    private productivityService: ProductivityService,
    private googleService: GooglemapsService
  ) {}

  ngOnInit(): void {
    if (!!this.productivity.user.bancking) {
      this.data = this.productivity.user.bancking;
    }

    this.googleService
      .getLocation({ timeout: 15000 })
      .then(({ coords }) => {
        this.latitude = coords.latitude.toString();
        this.longitude = coords.longitude.toString();
      })
      .catch(handlerError);
  }

  responseTransfer = (transfer: TransferForm) => {
    if (!!!this.submitted) {
      this.submitted = true;
      console.log(transfer);
      this.productivityService
        .payTransferStore(this.productivity, {
          ...transfer,
          latitude: this.latitude,
          longitude: this.longitude,
        })
        .subscribe(
          (url: string) => {
            this.submitted = false;

            this.onClose.emit(url);

            return showMessage(
              null,
              this.translate.instant("notifications.default"),
              "success",
              true
            );
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.errors = rawError(handlerError(error));
          }
        );
    }
  };

  responseDeposit = (deposit: DepositForm) => {
    if (!!!this.submitted) {
      this.submitted = true;

      this.productivityService
        .payDepositStore(this.productivity, {
          ...deposit,
          latitude: this.latitude,
          longitude: this.longitude,
        })
        .subscribe(
          (url: string) => {
            this.submitted = false;

            this.onClose.emit(url);

            return showMessage(
              null,
              this.translate.instant("notifications.default"),
              "success",
              true
            );
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.errors = rawError(handlerError(error));
          }
        );
    }
  };

  responsePaypal = (paypal: PaypalForm) => {
    if (!!!this.submitted) {
      this.submitted = true;

      this.productivityService
        .payPaypalStore(this.productivity, {
          ...paypal,
          latitude: this.latitude,
          longitude: this.longitude,
        })
        .subscribe(
          (url: string) => {
            this.submitted = false;

            this.onClose.emit(url);

            return showMessage(
              null,
              this.translate.instant("notifications.default"),
              "success",
              true
            );
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.errors = rawError(handlerError(error));
          }
        );
    }
  };
}
