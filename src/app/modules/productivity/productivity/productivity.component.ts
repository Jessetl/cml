import { Component, OnInit } from "@angular/core";
import { ViewChild, TemplateRef } from "@angular/core";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { HttpErrorResponse } from "@angular/common/http";

import { TranslateService } from "@ngx-translate/core";
import { SignaturePad } from "angular2-signaturepad";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

import { Productivity, ProductivityPay, User } from "@models";
import { ProductivityService } from "@core/service";
import { showMessage, TierId } from "@shared/utils";
import { rawError, handlerError } from "@shared/utils";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-productivity",
  templateUrl: "./productivity.component.html",
  styles: [],
})
export class ProductivityComponent implements OnInit {
  @ViewChild(SignaturePad)
  signaturePad: SignaturePad;

  private modalRef: NgbModalRef;

  public submitted: boolean = false;
  public filtered: boolean = false;
  public isLoading: boolean;
  public user: User;
  public errors: any;
  public productivity: Productivity;
  public productivities: Productivity[];
  public backupProductivities: Productivity[];

  public step: number = 1;
  public typeTier: number = null;

  public urlFile: SafeResourceUrl;
  public imageSrc: string;

  public signature: string = null;
  public signatureUrl: string = null;

  public signatureConfig: Object = {
    minWidth: 2,
    canvasHeight: 150,
  };

  //Search
  public search = {
    alphanumeric: null,
    date: null,
  };

  constructor(
    public domSanitizer: DomSanitizer,
    private translate: TranslateService,
    private productivityService: ProductivityService,
    private store: Store<fromRoot.State>,
    private ngbModal: NgbModal
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.user = user));
  }

  ngOnInit() {
    this.submitted = true;

    this.getProductivity();
  }

  ngAfterViewInit() {
    if (this.step === 2) {
      this.signaturePad.set("minWidth", 2); // set szimek/signature_pad options at runtime
    }
  }

  getProductivity = () => {
    this.productivityService.getProductivity().subscribe(
      (productivity: Productivity[]) => {
        this.submitted = false;

        this.productivities = productivity;
        this.backupProductivities = productivity;
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  printProductivity = (urlFile: string) => {
    if (!!!this.submitted) {
      this.step = 3;
      this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(urlFile);
    }
  };

  clearAll = () => {
    this.search = {
      alphanumeric: null,
      date: null,
    };

    this.typeTier = null;

    if (!!!this.submitted) {
      this.submitted = true;
      this.filtered = false;

      this.getProductivity();
    }
  };

  searchButton = () => {
    let name;

    for (var key in this.search) {
      if (this.search[key] != null) name = key;
    }

    this.onKey(name);
  };

  clearInput = (name: string) => {
    for (var key in this.search) {
      if (key != name && this.search[key] != null) {
        this.search[key] = null;
      }
    }
    if (this.typeTier !== null) {
      this.typeTier = null;
    }

    this.productivities = this.backupProductivities;
  };

  onKey = (key: string) => {
    let name = key;
    let variable = this.search[key];

    if (!variable) {
      return;
    }

    if (!!!this.submitted) {
      this.submitted = true;
      this.filtered = true;

      this.productivityService.search({ [name]: variable }).subscribe(
        (productivity: Productivity[]) => {
          this.submitted = false;
          this.productivities = [...productivity];
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          showMessage(
            "¡Error!",
            this.translate.instant(`errors${handlerError(error)}`),
            "error",
            true
          );
        }
      );
    }
  };

  changeTypeTier = () => {
    this.search = {
      alphanumeric: null,
      date: null,
    };

    let productivities = [...this.backupProductivities];

    switch (this.typeTier) {
      case TierId.TIER_SUPERVISOR:
        return (this.productivities = productivities.filter(
          ({ reseller }) => reseller.tier === TierId.TIER_SUPERVISOR
        ));
      case TierId.TIER_SELLER:
        return (this.productivities = productivities.filter(
          ({ reseller }) => reseller.tier === TierId.TIER_SELLER
        ));
      default:
        return (this.productivities = this.backupProductivities);
    }
  };

  getFileExtension = (filename) => {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  };

  shouldUpdateProductivity = (productivity: Productivity) => {
    const productivityKey = this.productivities.findIndex(
      ({ id }) => id === productivity.id
    );
    const productivityFind = this.productivities.find(
      ({ id }) => id === productivity.id
    );

    if (!!productivityFind) {
      let copyProductivities = [...this.productivities];

      copyProductivities[productivityKey] = {
        ...productivityFind,
        url: productivity.url,
        url_file: productivity.url_file,
        signaturename: productivity.signaturename,
        signaturepad: productivity.signaturepad,
      };

      this.productivity = null;
      this.productivities = copyProductivities;
    }
  };

  onToggleDocument = (
    payment: ProductivityPay,
    ngbPreview: TemplateRef<any>
  ) => {
    if (payment.voucher) {
      if (this.getFileExtension(payment.voucher) === "pdf") {
        this.urlFile = this.domSanitizer.bypassSecurityTrustResourceUrl(
          payment.file_url
        );
        this.step = 3;
      } else {
        this.onToggleModal(ngbPreview, payment.file_url);
      }
    } else {
      showMessage(
        "",
        this.translate.instant("validators.noFile"),
        "warning",
        true
      );
    }
  };

  onToggleSignaturepad = (productivity: Productivity) => {
    this.productivity = productivity;

    if (!!this.productivity) {
      this.step = 2;
    }
  };

  onToggleModal = async (ngbModal: TemplateRef<any>, image: string) => {
    this.imageSrc = image;

    this.modalRef = this.ngbModal.open(ngbModal, {
      size: "md",
    });
  };

  onDrawComplete = (): void => {
    this.signatureUrl = this.signaturePad.toDataURL("image/png", 0.5);
  };

  onDrawClear = (): void => {
    this.signatureUrl = null;
    this.signaturePad.clear();
  };

  onSubmit = () => {
    const signature = this.signatureUrl;
    const signatureName = this.signature;

    if (!signatureName) {
      return showMessage(
        null,
        this.translate.instant("validations.signatureProductivityName"),
        "warning",
        true
      );
    }

    if (!signature) {
      return showMessage(
        null,
        this.translate.instant("validations.signatureProductivityRequired"),
        "warning",
        true
      );
    }

    if (!!!this.submitted) {
      this.submitted = true;

      this.productivityService
        .signature({
          id: this.productivity.id,
          signaturename: this.signature,
          signature: this.signatureUrl,
        })
        .subscribe(
          (productivity: Productivity) => {
            this.shouldUpdateProductivity(productivity);

            showMessage(
              null,
              this.translate.instant("notifications.default"),
              "success",
              true
            );

            this.step = 1;
            this.submitted = false;
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
    }
  };

  onCloseNgbModal = (): void => {
    this.imageSrc = null;

    this.modalRef.close();
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      return showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };
}
