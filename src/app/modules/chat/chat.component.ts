import { ElementRef, ViewChild } from "@angular/core";
import { Component, OnInit, OnDestroy, TemplateRef } from "@angular/core";

import { HttpErrorResponse } from "@angular/common/http";
import { ChatService } from "@core/service";

import { FormGroup, FormControl } from "@angular/forms";
import { FormBuilder, Validators } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

import { User, Chat, Message } from "@models";

import { Socket, showMessage, handlerError, STATUS_SEND } from "@shared/utils";
import { TierId, CHAT, formatTypeImage, existChat } from "@shared/utils";
import { messageChatArchived, chatName, statusUnread } from "@shared/utils";
import { notForMe, sendMessage, fileMessage } from "@shared/utils";
import { confirm, maxFileSize, formArchived } from "@shared/utils";
import { getLevelUser } from "@shared/utils";

import { on, emit, unsubscribeOf } from "jetemit";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

const sendMessge = {
  message: new FormControl(null, Validators.required),
  file: new FormControl(null),
};
@Component({
  selector: "cml-chat",
  templateUrl: "./chat.component.html",
  styles: [],
})
export class ChatComponent implements OnInit, OnDestroy {
  @ViewChild("scrollMe")
  private myScrollContainer: ElementRef;
  @ViewChild("myMessage", { static: false }) inputEl: ElementRef;

  private modalRef: NgbModalRef;

  public isLoading: boolean;

  public message: FormGroup;
  public submitted: boolean;
  public me: User;
  public errors: Array<any>;
  public usersOnline: User[];
  public chats: Chat[] = [];
  public chatsArchived: Chat[] = [];
  public chatOpen: Chat;
  public unconfirmedChat: any = null;
  public load: boolean;
  public imageSrc: string;
  public namePdf: string;
  public disableScrollDown = false;
  public loadArchived: boolean;

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private store: Store<fromRoot.State>,
    private chatService: ChatService,
    private ngbModal: NgbModal
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.me = user));

    this.message = this.formBuilder.group(sendMessge);
  }

  ngOnInit() {
    this.isLoading = true;
    this.chatService.getChats().subscribe(
      (chat: Chat[]) => {
        this.isLoading = false;
        if (chat.length > 0) {
          const chatList = chat.map((chat) =>
            chatName(chat, this.me, this.translate)
          );
          this.chats = chatList;
        }
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    on(CHAT.NEW_MESSAGE, (msg) => {
      if (msg) {
        this.receiverNewMessage(msg);
      }
    });

    on(CHAT.CHAT_NEW, (data: Chat) => {
      if (!data) return;
      notForMe(data, this.chats, this.me) ? null : this.getChat(data.id);
    });
  }

  receiverNewMessage = (msg: Message) => {
    let chat = this.chats.find(({ id }) => id == msg.chat_id);
    if (chat !== undefined) {
      let message = chat.chat_user.find(({ id }) => id == msg.id);
      !!this.chatOpen && chat.id === this.chatOpen.id
        ? this.statusMessage(msg)
        : (chat.badge = chat.badge + 1);
      if (!message) chat.chat_user.push(msg);
    }

    messageChatArchived(msg, this.chats, this.me)
      ? this.getChat(msg.chat_id)
      : null;
  };

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  unread = () => {
    let unread = 0;
    this.chats.map((chat) => {
      unread = chat.badge > 0 ? unread + chat.badge : unread;
    });
    emit(CHAT.UNREAD_MESSAGE, unread);
  };

  messageWrite = (): string => {
    return this.message.get("message").value;
  };

  clearInput = () => {
    this.message.patchValue({ message: null }, { onlySelf: true });
  };

  ngOnDestroy(): void {
    this.chatOpen = null;
    this.unread();
    unsubscribeOf(CHAT.NEW_MESSAGE);
    unsubscribeOf(CHAT.CHAT_NEW);
  }

  onScroll = () => {
    const native = this.myScrollContainer.nativeElement;
    const bottom = this.scrollHeight(native);

    if (this.disableScrollDown && !!bottom) {
      this.disableScrollDown = false;
    } else {
      this.disableScrollDown = true;
    }
  };

  scrollHeight = (native: any): boolean => {
    return native.scrollHeight - native.scrollTop === native.clientHeight;
  };

  scrollToBottom = (): void => {
    if (!this.disableScrollDown) {
      try {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
      } catch (err) {}
    }
  };

  getChat = (id: number) => {
    this.chatService.getChat(id).subscribe(
      (chat: Chat) => {
        if (!chat) return;

        let chatUser = {
          ...chatName(chat, this.me, this.translate),
          to: chat.chat_user.length <= 1 ? null : 1,
        };
        this.chats.unshift(chatUser);
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      }
    );
  };

  getUsersOnline = () => {
    this.chatService.getUsersOnline().subscribe(
      (users: User[]) => {
        this.load = false;
        this.usersOnline = users;
      },
      (error: HttpErrorResponse) => {
        this.load = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  generatedUncorfimedChat = (user: User) => {
    let chatConfirmed = existChat(user.id, this.chats);
    let chat: Chat;
    if (chatConfirmed !== undefined) {
      chat = chatConfirmed;
    } else {
      if (this.unconfirmedChat) {
        this.chats = this.chats.filter((chat) => {
          return chat.id !== 0;
        });
      }
      this.unconfirmedChat = {
        id: 0,
        name: user.person.name,
        role: this.getNameTier(user),
        receiver_id: user.id,
        transmitter_id: this.me.id,
        status: 0,
        chat_user: [],
      };
      chat = this.unconfirmedChat;
      this.chats.unshift(chat);
    }
    this.onCloseNgbModal();
    this.chatOpen = chat;
  };

  onSubmit = () => {
    const infoValid = this.message.valid;
    if (!infoValid) return;

    if (this.unconfirmedChat) {
      let message = {
        message: this.messageWrite(),
        receiver_id: this.unconfirmedChat.receiver_id,
      };
      return this.generateChat(message);
    }

    const send = sendMessage(this.chatOpen, this.me, this.messageWrite());
    Socket.emit(CHAT.SEND_MESSAGE, send);
    this.clearInput();
    this.disableScrollDown = false;
  };

  setUsers = (ngbChat: TemplateRef<any>) => {
    this.openModal(ngbChat);
    this.usersOnline = [];
    this.load = true;
    this.getUsersOnline();
  };

  openModal = async (ngbModal: TemplateRef<any>) => {
    this.modalRef = this.ngbModal.open(ngbModal, {
      size: "md",
    });
  };

  onCloseNgbModal = (): void => {
    this.modalRef.close();
    this.message.patchValue({ file: null }, { onlySelf: true });
    this.imageSrc = null;
    this.namePdf = null;
    this.chatsArchived = [];
  };

  openChat = (chatId: number) => {
    this.disableScrollDown = false;

    if (this.unconfirmedChat) {
      this.chats = this.chats.filter((chat) => {
        return chat.id !== 0;
      });
      this.unconfirmedChat = null;
    }

    this.chatOpen = this.chats.find(({ id }) => id == chatId);
    this.chatOpen.badge = 0;
    let messages = this.chatOpen.chat_user;
    let message = messages.find((message) => message.status === STATUS_SEND);

    this.statusMessage(message);
  };

  closeChat = () => {
    this.chatOpen = null;
  };

  generateChat = (message: any) => {
    if (this.submitted) {
      return;
    }
    this.submitted = true;
    this.clearInput();
    this.chatService.createChat(message).subscribe(
      (chat: Chat) => {
        this.submitted = false;
        this.unconfirmedChat = null;
        let messages = chat.chat_user;

        let newChat = { ...chatName(chat, this.me, this.translate), to: null };
        let confirmedChat = this.chats.filter((chat) => {
          return chat.id != 0;
        });

        this.chats = [newChat, ...confirmedChat];
        this.openChat(this.chats[0].id);

        messages.length > 1
          ? Socket.emit(CHAT.SEND_MESSAGE, messages[messages.length - 1])
          : Socket.emit(CHAT.CREATE_CHAT, { ...newChat, chat_user: [] });
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  sendFile = () => {
    if (this.unconfirmedChat) {
      let message = {
        message: this.messageWrite(),
        file: this.message.get("file").value,
        receiver_id: this.unconfirmedChat.receiver_id,
      };
      this.generateChat(message);
      this.onCloseNgbModal();
      return;
    }
    let message = this.message.get("file").value;
    let file = fileMessage(this.chatOpen, this.me, message);
    this.uploadfile(file);

    this.onCloseNgbModal();
  };

  statusMessage = (message: Message) => {
    if (statusUnread(message, this.me)) {
      Socket.emit(CHAT.CHANGE_STATUS, message.chat_id);
    }
  };

  onFileChanged = (event: any, ngbModal: TemplateRef<any>) => {
    const readerFile = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];

      if (maxFileSize(file.size)) {
        return showMessage(
          "",
          this.translate.instant("validations.fileInvalid"),
          "warning",
          true
        );
      }

      if (formatTypeImage(file.type)) {
        return showMessage(
          "",
          this.translate.instant("validations.format"),
          "warning",
          true
        );
      }

      readerFile.readAsDataURL(file);

      readerFile.onload = () => {
        const strResult = readerFile.result as string;
        this.openModal(ngbModal);

        file.type == "application/pdf"
          ? (this.namePdf = file.name)
          : (this.imageSrc = strResult);
        this.message.patchValue({ file: strResult }, { onlySelf: true });
      };
    }
  };

  uploadfile = (message: any) => {
    if (this.submitted) return;
    this.submitted = true;
    this.clearInput();

    this.chatService.sendFile(message).subscribe(
      (message: Message) => {
        this.submitted = false;

        Socket.emit(CHAT.SEND_FILE, message);
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  archiveChat = async (archive: boolean, chat: Chat) => {
    let text = archive ? "ngbAlert.archivedChat" : "ngbAlert.archivedChatOut";
    const wantStatus = await confirm({
      text: this.translate.instant(text),
      confirmButtonText: this.translate.instant("buttonText.confirmButtonText"),
      cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
    });

    if (wantStatus) {
      this.archive(archive, chat);
    }
  };

  archive = (archive: boolean, chat: Chat) => {
    if (this.submitted) {
      return;
    }
    this.submitted = true;
    this.loadArchived = true;
    let id = chat.id;
    let form = formArchived(archive, chat, this.me);

    this.chatService.archiveChat(form).subscribe(
      (chat: Chat) => {
        this.submitted = false;
        this.loadArchived = false;
        if (archive) {
          this.chatOpen =
            !!this.chatOpen && chat.id == this.chatOpen.id
              ? null
              : this.chatOpen;
          this.chats = this.chats.filter((chat) => chat.id != id);
          return;
        }
        this.onCloseNgbModal();

        let newChat = chatName(chat, this.me, this.translate);
        if (this.unconfirmedChat) {
          this.chats = this.chats.filter(
            ({ id }) => id != this.unconfirmedChat.id
          );
          this.unconfirmedChat = null;
        }
        this.chats.unshift(newChat);
        this.chatOpen = newChat;
      },
      (error: HttpErrorResponse) => {
        this.loadArchived = false;
        this.submitted = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  getArchivedChats = () => {
    if (this.load) return;
    this.load = true;
    this.chatService.getArchivedChats().subscribe(
      (chat: Chat[]) => {
        this.load = false;
        if (chat.length > 0) {
          this.chatsArchived = chat.map((chat) =>
            chatName(chat, this.me, this.translate)
          );
        }
      },
      (error: HttpErrorResponse) => {
        this.load = false;
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  openChatsArchived = (nbgModal: TemplateRef<any>) => {
    this.openModal(nbgModal);
    this.getArchivedChats();
  };

  getNameTier = (user: User) => {
    return !!user.agency && user.agency.tier === 5
      ? this.translate.instant("cardText.employee")
      : this.translate.instant("tierId." + getLevelUser(user.tier));
  };
}
