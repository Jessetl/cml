import { Component, OnInit, TemplateRef } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";
import { NgbModalRef, NgbModal } from "@ng-bootstrap/ng-bootstrap";

import { User, Users, Role as Profile, Country } from "@models";

import { UserService, ProfileService } from "@core/service";
import { CountryService, GooglemapsService } from "@core/service";

import { FormUserValidator } from "./FormUserValidator";

import { ActivatedRoute } from "@angular/router";

import {
  showMessage,
  showConfirmButton,
  STATUS_ACTIVE,
  STATUS_INACTIVE,
} from "@shared/utils";
import { confirm, rawError, handlerError } from "@shared/utils";
import { TierId, countriesLang, Socket } from "@shared/utils";

import { validationsForm, formatingPhone, profilesLang } from "@shared/utils";
import { REAL_TIME } from "@shared/utils";
import { CREATE, EDIT, DELETE } from "@shared/utils";

import { on, unsubscribeOf } from "jetemit";

import { Store } from "@ngrx/store";
import * as fromRoot from "@reducers";

@Component({
  selector: "cml-user",
  templateUrl: "./user.component.html",
  styles: [],
})
export class UserComponent implements OnInit {
  private modalRef: NgbModalRef;

  public isLoading: boolean = true;
  public isEditing: boolean = false;
  public currentUser: User;
  public backupUsers: User[];
  public users: User[];
  public profiles: Profile[];
  public countries: Country[];
  public user: FormGroup;
  public displayUser: User;
  public submitted: boolean;
  public filtered: boolean = false;
  public searching: boolean = false;
  public paginate: number = 1;
  public status: number = STATUS_ACTIVE;

  public errors: Array<any>;
  public errorZipCode: boolean;
  public shouldEnableStateAndCity: boolean = false;

  // Password
  public validatePass: boolean = false;
  public minChar: boolean = false;
  public requireNumber: boolean = false;
  public requireCharEs: boolean = false;
  public requiteText: boolean = false;

  // FormSearch
  public search = {
    name: null,
    email: null,
    phone: null,
    cellphone: null,
    id: null,
  };

  //routeHome
  public readonly home: string;

  constructor(
    private ngbModal: NgbModal,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private userService: UserService,
    private profileService: ProfileService,
    private countryService: CountryService,
    private googleService: GooglemapsService,
    private store: Store<fromRoot.State>,
    private route: ActivatedRoute
  ) {
    this.store
      .select(fromRoot.getUser)
      .subscribe((user: User) => (this.currentUser = user));

    this.user = this.formBuilder.group(FormUserValidator);
    this.home = this.route.snapshot.paramMap.get("home");
  }

  ngOnInit() {
    const { agency } = this.currentUser;
    const tier = !!agency ? agency.tier : this.currentUser.tier;
    const agencyId = !!agency ? agency.id : this.currentUser.id;
    this.getUsers();

    this.profileService.getProfiles().subscribe(
      (profiles: Profile[]) => {
        this.profiles = profilesLang(profiles, this.translate.getDefaultLang());
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    this.countryService.getCountries().subscribe(
      (countries: Country[]) => {
        this.countries = countriesLang(
          countries,
          this.translate.getDefaultLang()
        );
      },
      (error: HttpErrorResponse) => {
        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );

    on(REAL_TIME.NEW_USER, (data) => {
      if (!this.filtered) {
        this.getUsers();
      }
    });
    on(REAL_TIME.NEW_EMPLOYEE, (data) => {
      if (
        !this.filtered &&
        tier === TierId.TIER_AGENCY &&
        agencyId == data.agency
      ) {
        this.getUsers();
      }
    });
  }

  ngOnDestroy(): void {
    unsubscribeOf(REAL_TIME.NEW_USER);
    unsubscribeOf(REAL_TIME.NEW_EMPLOYEE);
  }

  isFieldValid = (field: string): boolean => {
    return this.user.get(field).invalid && this.user.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  getUsers = () => {
    this.isLoading = true;

    this.userService.getUsersByPage(this.paginate).subscribe(
      (users: Users[]) => {
        this.isLoading = false;
        const rawData = users
          .map((user: User) => {
            return {
              ...user,
              agency: user.agency[0] || null,
              person: { ...user.person },
              role: { ...user.role[0] },
            };
          })
          .filter((user) => !!user.person);
        this.backupUsers = rawData;
        this.users = rawData.filter(({ status }) => status === this.status);
      },
      (error: HttpErrorResponse) => {
        this.isLoading = false;

        showMessage(
          "¡Error!",
          this.translate.instant(`errors${handlerError(error)}`),
          "error",
          true
        );
      }
    );
  };

  changeStatus = () => {
    this.status =
      this.status === STATUS_ACTIVE ? STATUS_INACTIVE : STATUS_ACTIVE;
    let users = [...this.backupUsers];
    this.users = users.filter(({ status }) => status === this.status);
  };

  updateBackupUsers = (user: User, type: number) => {
    switch (type) {
      case CREATE:
        return (this.backupUsers = [...this.backupUsers, user]);
      case EDIT:
        let resUser = [...this.backupUsers];
        const keyUser = this.backupUsers.findIndex(({ id }) => id === user.id);

        resUser[keyUser] = {
          ...user,
          person: { ...user.person },
          role: { ...user.role[0] },
          id: user.id,
        };

        return (this.backupUsers = resUser);
      case DELETE:
        return (this.backupUsers = this.backupUsers.filter(
          ({ id }) => id !== user.id
        ));
      default:
        return this.backupUsers;
    }
  };

  toggleButtonByTierId = (type: number): boolean => {
    if (!!this.currentUser) {
      const module =
        this.currentUser.tier !== TierId.TIER_ADMIN
          ? this.currentUser.role.assignments
              .filter((assignment) => assignment.submodule.path === "users")
              .find((assignment) => assignment.type === type)
          : null;

      return this.currentUser.tier === TierId.TIER_ADMIN || !!module;
    }
  };

  toggleButtonByOnlyAgency = (): boolean => {
    if (!!this.currentUser) {
      return (
        this.currentUser.tier === TierId.TIER_ADMIN ||
        this.currentUser.tier === TierId.TIER_AGENCY
      );
    }
  };

  shouldStateAndCity = () => {
    this.shouldEnableStateAndCity = !this.shouldEnableStateAndCity;

    if (this.shouldEnableStateAndCity) {
      this.user.get("state").enable({ onlySelf: true });
      this.user.get("city").enable({ onlySelf: true });
    } else {
      this.user.get("state").disable({ onlySelf: true });
      this.user.get("city").disable({ onlySelf: true });
    }
  };

  shouldToggleCreate = (ngbUser: TemplateRef<any>) => {
    this.isEditing = false;
    this.errors = null;
    this.validatePass = false;
    this.clean();

    this.user.reset();

    this.user
      .get("password")
      .setValidators([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.pattern(
          "(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$"
        ),
      ]);

    const countryId = this.countries.find(
      ({ alpha2Code }) => alpha2Code === "US"
    )?.id;

    if (!!countryId) {
      this.user.patchValue({ country_id: countryId }, { onlySelf: true });
    }

    this.modalRef = this.ngbModal.open(ngbUser);
  };

  shouldToggleUpdate = (ngbUser, userId: number) => {
    this.isEditing = true;
    this.validatePass = false;

    this.clean();

    const user: User = this.users.find(({ id }) => id === userId);

    this.user.patchValue({
      ...user,
      ...user.person,
      role: user.role.id,
      id: user.id,
      password: null,
    });

    this.user.get("password").setValidators([]);

    this.modalRef = this.ngbModal.open(ngbUser);
  };

  shouldToggleDelete = (userId: number) => {
    showConfirmButton(
      this.translate.instant("ngbAlert.deleteUser"),
      this.translate.instant("ngbAlert.btnYes"),
      this.translate.instant("ngbAlert.btnNo"),
      () => {
        this.userService.destroy(userId).subscribe(
          (use: User) => {
            this.shoulderDestroyUser(userId);
            this.updateBackupUsers(use, DELETE);

            showMessage(
              "¡Enhorabuena!",
              this.translate.instant("notifications.destroyUser"),
              "success",
              true
            );

            this.modalRef.close();
          },
          (error: HttpErrorResponse) => {
            this.submitted = false;
            this.handleError(error);
          }
        );
      }
    );
  };

  shouldMakeUser = (user: User) => {
    const resUser = {
      ...user,
      person: { ...user.person },
      role: { ...user.role[0] },
      id: user.id,
    };

    this.users = [...this.users, resUser];
    this.updateBackupUsers(resUser, CREATE);
  };

  shoulderUpdateUser = (user: User) => {
    let keyUser = this.users.findIndex(({ id }) => id === user.id);
    let resUser = [...this.users];

    resUser[keyUser] = {
      ...user,
      person: { ...user.person },
      role: { ...user.role[0] },
      id: user.id,
    };

    this.users = resUser;
    this.updateBackupUsers(user, EDIT);
  };

  shoulderUpdateUserStatus = (user: User) => {
    let resUser = [...this.users];
    this.users = resUser.filter(({ id }) => id !== user.id);
    this.updateBackupUsers(user, EDIT);
  };

  shoulderDestroyUser = (userId: number) => {
    this.users = this.users.filter(({ id }) => id !== userId);
  };

  onSubmit = () => {
    const infoValid = this.user.valid;
    const isEditing = this.isEditing;
    const { agency } = this.currentUser;
    const agencyId = !!agency ? agency.id : this.currentUser.id;
    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.user);
    } else if (!!!this.submitted) {
      this.submitted = true;

      switch (isEditing) {
        case true:
          return this.userService.update(this.user.getRawValue()).subscribe(
            (user: User) => {
              this.submitted = false;

              this.shoulderUpdateUser(user);

              this.user.reset();

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.updateUser"),
                "success",
                true
              );

              this.modalRef.close();
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
        case false:
          return this.userService.store(this.user.getRawValue()).subscribe(
            (user: User) => {
              this.submitted = false;

              this.shouldMakeUser(user);

              this.user.reset();

              showMessage(
                "¡Enhorabuena!",
                this.translate.instant("notifications.createUser"),
                "success",
                true
              );

              const agencyTier = !!user.agency ? user.agency.tier : user.tier;

              this.modalRef.close();
              Socket.emit(REAL_TIME.USER, { id: agencyId, tier: agencyTier });
            },
            (error: HttpErrorResponse) => {
              this.submitted = false;
              this.handleError(error);
            }
          );
      }
    }
  };

  onCloseNgbModal = () => {
    this.modalRef.close();
    this.validatePass = false;
    this.clean();
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (typeof errorResponse === "string") {
      return showMessage(
        "¡Error!",
        this.translate.instant(`errors${errorResponse}`),
        "error",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };

  emptySearch = () => {
    this.search = {
      name: null,
      email: null,
      phone: null,
      cellphone: null,
      id: null,
    };
  };

  clear = () => {
    this.emptySearch();
    this.getUsers();
    this.displayUser = null;
    this.filtered = false;
  };

  onDefaultValuesStateAndCountry = () => {
    this.user.get("state").reset(null, { onlySelf: true });
    this.user.get("city").reset(null, { onlySelf: true });
  };

  onBlurZipCode = (nameField: string) => {
    const zipCode = this.user.get(nameField).value;
    const countryId = this.user.get("country_id").value;
    const countryAlphacode = this.countries.find(({ id }) => id === countryId);

    if (!zipCode) {
      return;
    }

    this.onDefaultValuesStateAndCountry();
    this.searching = true;
    this.errorZipCode = false;

    this.googleService
      .getZipCode({
        postalCode: zipCode.toString(),
        country: !!countryAlphacode ? countryAlphacode.alpha2Code : "US",
      })
      .subscribe(
        async (res: any) => {
          const response = await this.googleService.getStateAndCity(res);

          this.user.patchValue(
            {
              state: response.state,
              city: response.city,
            },
            { onlySelf: true, emitEvent: false }
          );

          this.searching = false;
        },
        async () => {
          if (!this.shouldEnableStateAndCity) {
            this.searching = false;
            this.errorZipCode = true;

            const wantWrite = await confirm({
              text: this.translate.instant("ngbAlert.watWriteZipCode"),
              confirmButtonText: this.translate.instant(
                "buttonText.confirmButtonText"
              ),
              cancelButtonText: this.translate.instant(
                "buttonText.cancelButtonText"
              ),
            });

            if (wantWrite) {
              this.user.get("state").enable({ onlySelf: true });
              this.user.get("city").enable({ onlySelf: true });
            }
          }
        }
      );
  };

  searchButton = () => {
    let name;
    for (var key in this.search) {
      if (this.search[key] != null) name = key;
    }

    this.onKey(name);
  };

  onKey(key: string) {
    let name = key;
    let variable = this.search[key];

    if (!!!variable) {
      return;
    }

    if (!!!this.isLoading) {
      this.filtered = true;
      this.isLoading = true;

      this.userService
        .search({ [name]: variable, status: this.status }, 0)
        .subscribe(
          (users: Users[]) => {
            this.isLoading = false;
            const rawData = users
              .map((user: User) => {
                return {
                  ...user,
                  person: { ...user.person },
                  role: { ...user.role[0] },
                };
              })
              .filter((user) => !!user.person);

            this.users = rawData;
            if (!name && !variable) this.backupUsers = rawData;
          },
          (error: HttpErrorResponse) => {
            this.isLoading = false;
            this.emptySearch();
            showMessage(
              "¡Error!",
              this.translate.instant(`errors${handlerError(error)}`),
              "error",
              true
            );
          }
        );
    }
  }

  getValidationRegex = () => {
    const pass = this.user.get("password").value;
    const regex = /(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$/;
    const isEditing = this.isEditing;

    if ((pass === null || !regex.test(pass)) && !isEditing) {
      return (this.validatePass = true);
    }

    return (this.validatePass = false);
  };

  validate = (variable: any) => {
    const regexMin = /(?=.{6,})/;
    const regexNumber = /(?=.*[0-9])/;
    const regexText = /(?=.*[A-Z])/;
    const regexChar = /(?=.*[@#$%^&*-/%+=]).*$/;
    this.requiteText = false;
    if (regexText.test(variable)) {
      this.requiteText = true;
    }
    this.requireNumber = false;
    if (regexNumber.test(variable)) {
      this.requireNumber = true;
    }
    this.requireCharEs = false;
    if (regexChar.test(variable)) {
      this.requireCharEs = true;
    }
    this.minChar = false;
    if (regexMin.test(variable)) {
      this.minChar = true;
    }
  };

  fPass = () => {
    this.validatePass = true;
  };

  clean = () => {
    this.requiteText = false;
    this.requireNumber = false;
    this.requireCharEs = false;
    this.minChar = false;

    this.user.get("state").disable({ onlySelf: true });
    this.user.get("city").disable({ onlySelf: true });
  };

  formating = (phone: any) => {
    return phone ? formatingPhone(phone) : "";
  };

  shouldToggleStatus = async (userId: number) => {
    const wantStatus = await confirm({
      text: this.translate.instant("ngbAlert.wantToChangeStatus"),
      confirmButtonText: this.translate.instant("buttonText.confirmButtonText"),
      cancelButtonText: this.translate.instant("buttonText.cancelButtonText"),
    });

    if (wantStatus) {
      this.userService.changeStatus({ id: userId }).subscribe(
        (user: User) => {
          this.submitted = false;

          this.shoulderUpdateUserStatus(user);

          showMessage(
            "¡Enhorabuena!",
            this.translate.instant("notifications.updateUser"),
            "success",
            true
          );
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;

          if (typeof error.error === "string") {
            return showMessage(null, error.error, "error", true);
          }

          this.handleError(error);
        }
      );
    }
  };

  clearInput = (name: string) => {
    for (var key in this.search) {
      if (key != name && this.search[key] != null) {
        this.search[key] = null;
      }
    }
    let backup = [...this.backupUsers];
    this.users = backup.filter(({ status }) => status === this.status);
  };

  isToggleUser = (ngbDisplayUser: TemplateRef<any>, key: number) => {
    this.displayUser = this.users[key];

    if (!!this.displayUser) {
      this.modalRef = this.ngbModal.open(ngbDisplayUser);
    }
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
