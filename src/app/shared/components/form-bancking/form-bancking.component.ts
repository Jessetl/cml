import { Component, OnInit } from "@angular/core";
import { Input, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";

import { FormBanckingValidator } from "./FormBanckingValidator";
import { BanckingForm, User } from "@models";
import { validationsForm } from "@shared/utils";

@Component({
  selector: "cml-form-bancking",
  templateUrl: "./form-bancking.component.html",
  styles: [],
})
export class FormBanckingComponent implements OnInit {
  @Input() submitted: boolean;
  @Input() data: User;
  @Input() errors: Array<any>;
  @Output() responseBancking = new EventEmitter<BanckingForm>();

  public bancking: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService
  ) {
    this.bancking = this.formBuilder.group(FormBanckingValidator);
  }

  ngOnInit(): void {
    const { bancking } = this.data;

    if (!!bancking) {
      this.bancking.patchValue({
        ...bancking,
        id: bancking.id,
      });
    }
  }

  isFieldValid = (field: string): boolean => {
    return this.bancking.get(field).invalid && this.bancking.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  onSubmit = () => {
    const infoValid = this.bancking.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.bancking);
    }

    if (!!!this.submitted) {
      this.responseBancking.emit(this.bancking.getRawValue());
    }
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
