import { FormControl, Validators } from "@angular/forms";

export const FormBanckingValidator = {
  id: new FormControl({ value: null, disabled: false }),
  user_id: new FormControl({ value: null, disabled: false }),
  headline: new FormControl({ value: null, disabled: false }, [
    Validators.required,
    Validators.maxLength(120),
  ]),
  account_number: new FormControl({ value: null, disabled: false }, [
    Validators.required,
    Validators.maxLength(90),
  ]),
  interbank: new FormControl({ value: null, disabled: false }, [
    Validators.maxLength(50),
  ]),
  institution: new FormControl({ value: null, disabled: false }, [
    Validators.required,
    Validators.maxLength(70),
  ]),
  tax_identification: new FormControl({ value: null, disabled: false }, [
    Validators.required,
    Validators.maxLength(50),
  ]),
};
