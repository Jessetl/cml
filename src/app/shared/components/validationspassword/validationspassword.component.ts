import { Component, Input } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "cml-password",
  templateUrl: "./validationspassword.component.html",
  styles: [],
})
export class ValidationspasswordComponent {
  @Input() requireText: boolean;
  @Input() requireNumber: boolean;
  @Input() requireCharEs: boolean;
  @Input() minChar: boolean;
  @Input() msgText: string;
  @Input() msgNumber: string;
  @Input() msgCharEs: string;
  @Input() msgminChar: string;

  constructor(private translate: TranslateService) {}
}
