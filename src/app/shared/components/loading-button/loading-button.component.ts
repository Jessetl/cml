import { Component, Input } from "@angular/core";

@Component({
  selector: "cml-loading-button",
  templateUrl: "./loading-button.component.html",
  styles: [],
})
export class LoadingButtonComponent {
  @Input() loading: boolean;

  constructor() {}
}
