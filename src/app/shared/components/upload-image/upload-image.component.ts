import { Component, OnInit, Input, Output } from "@angular/core";
import { EventEmitter } from "@angular/core";

import { TranslateService } from "@ngx-translate/core";

import { Image, Uploaded } from "@models";
import { showMessage } from "@shared/utils";
import { TYPE_FILE_UPLOAD, TYPE_IMAGE, SIZE_FILE } from "@shared/utils";

@Component({
  selector: "cml-upload-image",
  templateUrl: "./upload-image.component.html",
  styles: [],
})
export class UploadImageComponent implements OnInit {
  @Input() data?: Image[] = [];
  @Input() isSubmitted?: boolean = false;
  @Input() onToogleType?: boolean = false;
  @Output() responseFiles = new EventEmitter<Uploaded>();

  public typeFiles: number = TYPE_IMAGE;
  public files: Image[] = [];
  public filesDeleted: string[] = [];
  public file: File;

  constructor(private translate: TranslateService) {}

  ngOnInit(): void {
    this.files = this.data;
  }

  onFileChanged = (event: any) => {
    const readerFile = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];

      if (this.maxFileSize(file.size)) {
        return showMessage(
          "",
          this.translate.instant("validations.fileInvalid"),
          "warning",
          true
        );
      }

      if (this.formatTypeImage(file.type)) {
        return showMessage(
          "",
          this.translate.instant("validations.format"),
          "warning",
          true
        );
      }

      readerFile.readAsDataURL(file);

      readerFile.onload = () => {
        const strResult = readerFile.result as string;

        const uploadedImage: Image = {
          id: this.getLengthIdImages(),
          url: strResult,
          original_url: null,
          created_at: true,
          deleted_at: false,
        };

        this.files = [...this.files, uploadedImage];
        this.file = null;
      };
    }
  };

  shouldUploadImages = () => {
    if (!!this.files.length && !!!this.isSubmitted) {
      this.responseFiles.emit({
        images: this.files,
        images_deleted: this.filesDeleted,
      });
    }
  };

  shouldDelete = (imageId: number) => {
    const file = this.files.find(({ id }) => id === imageId);
    this.files = this.files.filter(({ id }) => id !== imageId);

    if (!!file) {
      this.filesDeleted = [...this.filesDeleted, file.url];
    }
  };

  maxFileSize = (fileSize: number): boolean => {
    const isTrue: boolean = true;
    const sizeMegabytes = fileSize / 1024 / 1024;

    if (sizeMegabytes > SIZE_FILE) {
      return isTrue;
    }

    return !isTrue;
  };

  getTypeImage = (file: string): boolean => {
    const isTrue: boolean = true;

    switch (file) {
      case "image/jpeg":
        return !isTrue;
      case "image/png":
        return !isTrue;
      case "image/bmp":
        return !isTrue;
      default:
        return isTrue;
    }
  };

  getTypeFile = (file: string): boolean => {
    const isTrue: boolean = true;

    switch (file) {
      case "application/pdf":
        return !isTrue;
      case "application/msword":
        return !isTrue;
      default:
        return isTrue;
    }
  };

  formatTypeImage = (file: string): boolean => {
    if (this.typeFiles === TYPE_IMAGE) {
      return this.getTypeImage(file);
    } else if (this.typeFiles === TYPE_FILE_UPLOAD) {
      return this.getTypeFile(file);
    }
  };

  getLengthIdImages = (): number => {
    return this.files.length + 1;
  };
}
