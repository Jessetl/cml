import { Component, Input, Output } from "@angular/core";
import { EventEmitter, ViewChild } from "@angular/core";

import { TranslateService } from "@ngx-translate/core";

import { StripeCardComponent, StripeService } from "ngx-stripe";
import { StripeElementsOptions, PaymentMethod } from "@stripe/stripe-js";
import { StripeCardElementOptions } from "@stripe/stripe-js";

import { getStripeLanguage } from "@shared/utils";

@Component({
  selector: "cml-stripe-subscriptions",
  templateUrl: "./stripe-subscriptions.component.html",
  styles: [],
})
export class StripeSubscriptionsComponent {
  @ViewChild(StripeCardComponent) stripe: StripeCardComponent;

  @Input() submitted: boolean;
  @Input() email: string;
  @Input() grandTotal: number;
  @Output() responsePaymentMethod = new EventEmitter<PaymentMethod>();
  @Output() responseError = new EventEmitter<any>();
  @Output() eventLoadSubmitted = new EventEmitter<boolean>();

  public isLoading: boolean = false;
  public language: string;
  public cardErrors: string;

  public settingsCard: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: "#87BBFD",
        color: "#000",
        lineHeight: "30px",
        fontWeight: "300",
        fontSize: "18px",
        "::placeholder": {
          color: "#87BBFD",
        },
      },
    },
  };

  public settingsElements: StripeElementsOptions = {
    locale: getStripeLanguage(this.translate.getDefaultLang()),
  };
  constructor(
    private translate: TranslateService,
    private stripeService: StripeService
  ) {}

  handleSubmit = async () => {
    this.cardErrors = null;
    if (!!!this.submitted) {
      this.eventLoadSubmitted.emit(true);
      const { paymentMethod, error } = await this.stripeService
        .createPaymentMethod({
          type: "card",
          card: this.stripe.element,
          billing_details: {
            name: this.email,
          },
        })
        .toPromise();
      if (error) {
        this.eventLoadSubmitted.emit(false);
        this.cardErrors = error.message;
      } else {
        this.responsePaymentMethod.emit(paymentMethod);
      }
    }
  };
}
