import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "cml-floating-home",
  templateUrl: "./floating-home.component.html",
  styles: [],
})
export class FloatingHomeComponent {
  @Input() show: string;
  constructor(private router: Router) {}

  goToHome = () => {
    this.router.navigate(["/dashboard"]);
  };
}
