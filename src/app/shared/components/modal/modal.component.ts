import {
  Component,
  ViewChild,
  Input,
  ElementRef,
  Output,
  EventEmitter,
} from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "cml-modal",
  templateUrl: "./modal.component.html",
  styles: [],
})
export class ModalComponent {
  @ViewChild("myModal") myModal: ElementRef;

  @Input() title: string = null;
  @Input() subtitle: string = null;
  @Input() size: boolean = false;
  @Output() onClose = new EventEmitter<null>();

  private modalRef: NgbModalRef;

  constructor(private modalService: NgbModal) {}

  open() {
    this.modalRef = this.modalService.open(this.myModal, {
      ariaLabelledBy: "modal-basic-title",
      size: this.size ? "lg" : "",
    });
  }

  close() {
    this.modalRef.close();
  }

  onCloseNgbModal() {
    this.onClose.emit(null);
  }
}
