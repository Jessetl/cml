import { Component, Input } from "@angular/core";
import { Country } from "@models";
import { TranslateService } from "@ngx-translate/core";
import { userSetting } from "@shared/utils";
@Component({
  selector: "cml-display-country",
  templateUrl: "./display-country.component.html",
  styles: [],
})
export class DisplayCountryComponent {
  @Input() country: Country;
  constructor(private translate: TranslateService) {}

  getExtra = (): string => {
    if (!!this.country.extra) {
      return this.country.type_extra !== 0
        ? "$" + this.country.extra
        : this.country.extra + "%";
    }
    return "";
  };

  userSettingsOptios = () => {
    return userSetting(this.country.user_settings, this.translate);
  };
}
