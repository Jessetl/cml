import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";

import { TranslateService } from "@ngx-translate/core";

import { FormUserValidator, PasswordValidator } from "./FormUserValidator";
import { Country, Role as Profile, PropsValue, FormUser, User } from "@models";
import { GooglemapsService } from "@core/service";
import { confirm, TierId, validationsForm } from "@shared/utils";
import { CREATE, EDIT } from "@shared/utils";

@Component({
  selector: "cml-form-user",
  templateUrl: "./form-user.component.html",
  styles: [],
})
export class FormUserComponent implements OnInit {
  @Input() type: number;
  @Input() errors: Array<any>;
  @Input() submitted: boolean;
  @Input() countries: Country[];
  @Input() profiles?: Profile[] = [];
  @Input() tiers?: PropsValue[] = [];
  @Input() data?: User;
  @Output() responseUser = new EventEmitter<FormUser>();

  public user: FormGroup;
  public searching: boolean = false;
  public errorZipCode: boolean;
  public shouldEnableStateAndCity: boolean = false;

  public passwordValidation: boolean = false;
  public minCharacters: boolean = false;
  public requiredNumber: boolean = false;
  public requiredCharacter: boolean = false;
  public requiredText: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private googleService: GooglemapsService
  ) {
    this.user = this.formBuilder.group(FormUserValidator);
  }

  ngOnInit(): void {
    if (!!!this.profiles.length) {
      this.user.get("role").setValidators([]);
    }

    if (!!!this.tiers.length) {
      this.user.get("tier").setValidators([]);
    }

    if (this.type === CREATE) {
      const country = this.countries.find(
        ({ alpha2Code }) => alpha2Code === "US"
      );

      if (!!country) {
        this.user.patchValue({ country_id: country.id }, { onlySelf: true });
      }

      return this.user.get("password").setValidators(PasswordValidator);
    }

    if (!!this.data) {
      const { person } = this.data;

      this.user.patchValue({
        id: this.data.id,
        name: person.name,
        identification_id: person.identification_id,
        email: this.data.email,
        address: person.address,
        phone: person.phone,
        cellphone: person.cellphone,
        zip_code: person.zip_code,
        country_id: person.country_id,
        state: person.state,
        city: person.city,
        password: null,
        tier: this.data.tier,
        role: this.data.role.id,
      });

      this.user.get("password").setValidators([]);
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.user.reset();
  }

  isFieldValid = (field: string): boolean => {
    return this.user.get(field).invalid && this.user.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  validate = (variable: string) => {
    const regexMin = /(?=.{6,})/;
    const regexNumber = /(?=.*[0-9])/;
    const regexText = /(?=.*[A-Z])/;
    const regexChar = /(?=.*[@#$%^&*-/%+=]).*$/;

    this.requiredText = false;

    if (regexText.test(variable)) {
      this.requiredText = true;
    }

    this.requiredNumber = false;

    if (regexNumber.test(variable)) {
      this.requiredNumber = true;
    }

    this.requiredCharacter = false;

    if (regexChar.test(variable)) {
      this.requiredCharacter = true;
    }

    this.minCharacters = false;

    if (regexMin.test(variable)) {
      this.minCharacters = true;
    }
  };

  getPasswordValidation = () => {
    const password = this.user.get("password").value;
    const regex = /(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$/;

    if ((!!!password || !regex.test(password)) && this.type !== EDIT) {
      return (this.passwordValidation = true);
    }

    return (this.passwordValidation = false);
  };

  getPasswordPlaceholder = (): string => {
    return this.type === CREATE ? "" : "••••••••••";
  };

  shouldStateAndCity = () => {
    this.shouldEnableStateAndCity = !this.shouldEnableStateAndCity;

    if (this.shouldEnableStateAndCity) {
      this.user.get("state").enable({ onlySelf: true });
      this.user.get("city").enable({ onlySelf: true });
    } else {
      this.user.get("state").disable({ onlySelf: true });
      this.user.get("city").disable({ onlySelf: true });
    }
  };

  onToggleRoleByTierId = () => {
    if (parseInt(this.user.get("tier").value) !== TierId.TIER_LOCKERS) {
      return this.user.get("role").enable({ onlySelf: true });
    }

    this.user.get("role").reset(null, { onlySelf: true });
    this.user.get("role").disable({ onlySelf: true });
  };

  onDefaultValuesStateAndCountry = () => {
    this.user.get("state").reset(null, { onlySelf: true });
    this.user.get("city").reset(null, { onlySelf: true });
  };

  onBlurZipCode = (nameField: string) => {
    const zipCode = this.user.get(nameField).value;
    const countryId = this.user.get("country_id").value;
    const countryAlphacode = this.countries.find(({ id }) => id === countryId);

    if (!!zipCode) {
      this.onDefaultValuesStateAndCountry();

      this.searching = true;
      this.errorZipCode = false;

      this.googleService
        .getZipCode({
          postalCode: zipCode.toString(),
          country: !!countryAlphacode ? countryAlphacode.alpha2Code : "US",
        })
        .subscribe(
          async (res: any) => {
            const response = await this.googleService.getStateAndCity(res);

            this.user.patchValue(
              {
                state: response.state,
                city: response.city,
              },
              { onlySelf: true, emitEvent: false }
            );

            this.searching = false;
          },
          async () => {
            if (!this.shouldEnableStateAndCity) {
              this.searching = false;
              this.errorZipCode = true;

              const wantWrite = await confirm({
                text: this.translate.instant("ngbAlert.watWriteZipCode"),
                confirmButtonText: this.translate.instant(
                  "buttonText.confirmButtonText"
                ),
                cancelButtonText: this.translate.instant(
                  "buttonText.cancelButtonText"
                ),
              });

              if (wantWrite) {
                this.user.get("state").enable({ onlySelf: true });
                this.user.get("city").enable({ onlySelf: true });
              }
            }
          }
        );
    }
  };

  onSubmit = () => {
    const infoValid = this.user.valid;

    this.errors = null;

    if (!infoValid) {
      return validationsForm(this.user);
    }

    this.responseUser.emit(this.user.getRawValue());
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
