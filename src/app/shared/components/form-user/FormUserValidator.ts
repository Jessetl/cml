import { FormControl, Validators } from "@angular/forms";

export const FormUserValidator = {
  id: new FormControl(null, []),
  name: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
  identification_id: new FormControl(null, [
    Validators.required,
    Validators.maxLength(60),
  ]),
  email: new FormControl(null, [Validators.required, Validators.maxLength(60)]),
  address: new FormControl(null, [Validators.required]),
  phone: new FormControl(null, [Validators.maxLength(20)]),
  cellphone: new FormControl(null, [
    Validators.required,
    Validators.maxLength(20),
  ]),
  zip_code: new FormControl(null, [
    Validators.required,
    Validators.maxLength(10),
  ]),
  country_id: new FormControl(null, [Validators.required]),
  state: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.maxLength(30),
  ]),
  city: new FormControl({ value: null, disabled: true }, [
    Validators.required,
    Validators.maxLength(30),
  ]),
  password: new FormControl(null, [
    Validators.required,
    Validators.minLength(6),
    Validators.maxLength(30),
    Validators.pattern(
      "(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$"
    ),
  ]),
  tier: new FormControl(null, [Validators.required]),
  role: new FormControl(null, [Validators.required]),
};

export const PasswordValidator = [
  Validators.required,
  Validators.minLength(6),
  Validators.maxLength(30),
  Validators.pattern("(?=.{6,})(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&*-/%+=]).*$"),
];
