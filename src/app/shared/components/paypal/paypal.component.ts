import { Component, OnInit } from "@angular/core";
import { Input, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";

import { BsLocaleService } from "ngx-bootstrap/datepicker";
import { TranslateService } from "@ngx-translate/core";

import { PaypalValidations } from "./PaypalValidations";

import { defineLocale } from "ngx-bootstrap/chronos";
import { esLocale, enGbLocale } from "ngx-bootstrap/locale";

import { PaypalForm } from "@models";
import { showMessage, validationsForm } from "@shared/utils";
import { getAppLanguage } from "@shared/utils";
import { SIZE_FILE } from "@shared/utils";

@Component({
  selector: "cml-paypal",
  templateUrl: "./paypal.component.html",
  styles: [],
})
export class PaypalComponent implements OnInit {
  @Input() submitted: boolean;
  @Input() grandTotal: number;
  @Input() proprietary: string;
  @Output() responsePaypal = new EventEmitter<PaypalForm>();

  public language: string;
  public paypal: FormGroup;
  public maxDate: Date = new Date();

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private localeService: BsLocaleService
  ) {
    this.paypal = this.formBuilder.group(PaypalValidations);
  }

  ngOnInit(): void {
    this.language = getAppLanguage(this.translate.getDefaultLang());

    this.paypal.patchValue(
      {
        issueDate: new Date(),
        destiny: this.proprietary,
        amount: this.grandTotal.toFixed(2),
      },
      { onlySelf: true }
    );

    switch (this.language) {
      case "es":
        defineLocale("es", esLocale);
      case "engb":
        defineLocale("engb", enGbLocale);
    }
  }

  ngAfterViewInit() {
    this.localeService.use(this.language);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.paypal.reset();
  }

  isFieldValid = (field: string): boolean => {
    return this.paypal.get(field).invalid && this.paypal.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  onFileChanged = (event: any) => {
    const readerFile = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];

      if (this.maxFileSize(file.size)) {
        this.paypal.patchValue({ voucher: null }, { onlySelf: true });

        return showMessage(
          "",
          this.translate.instant("validations.fileInvalid"),
          "warning",
          true
        );
      }

      if (this.formatTypeImage(file.type)) {
        this.paypal.patchValue({ voucher: null }, { onlySelf: true });

        return showMessage(
          "",
          this.translate.instant("validations.format"),
          "warning",
          true
        );
      }

      readerFile.readAsDataURL(file);

      readerFile.onload = () => {
        const strResult = readerFile.result as string;
        this.paypal.patchValue({ voucher: strResult }, { onlySelf: true });
      };
    }
  };

  maxFileSize = (fileSize: number): boolean => {
    const isTrue: boolean = true;
    const sizeMegabytes = fileSize / 1024 / 1024;

    if (sizeMegabytes > SIZE_FILE) {
      return isTrue;
    } else {
      return !isTrue;
    }
  };

  formatTypeImage = (img: string): boolean => {
    const isTrue: boolean = true;

    switch (img) {
      case "image/jpeg":
        return !isTrue;
      case "image/png":
        return !isTrue;
      case "image/bmp":
        return !isTrue;
      case "application/pdf":
        return !isTrue;
      default:
        return isTrue;
    }
  };

  onSubmit = () => {
    const isValid = this.paypal.valid;

    if (!isValid) {
      return validationsForm(this.paypal);
    }

    return this.responsePaypal.emit(this.paypal.getRawValue());
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
