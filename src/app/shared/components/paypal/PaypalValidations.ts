import { FormControl, Validators } from "@angular/forms";

export const PaypalValidations = {
  issueDate: new FormControl(new Date(), [Validators.required]),
  destiny: new FormControl({ value: null, disabled: true }, [
    Validators.required,
  ]),
  reference: new FormControl(null, [Validators.required]),
  amount: new FormControl({ value: null, disabled: true }, [
    Validators.required,
  ]),
  voucher: new FormControl(null, [Validators.required]),
};
