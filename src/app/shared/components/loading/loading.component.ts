import { Component, Input } from "@angular/core";

@Component({
  selector: "cml-loading",
  templateUrl: "./loading.component.html",
  styles: [],
})
export class LoadingComponent {
  @Input() loading: boolean;
  @Input() color: string;
  constructor() {}
}
