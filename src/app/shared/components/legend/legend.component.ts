import { Component, OnInit, Input } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Email } from "@models";

@Component({
  selector: "cml-legend",
  templateUrl: "./legend.component.html",
  styles: [],
})
export class LegendComponent implements OnInit {
  @Input() typeEmail: number;
  constructor(private translate: TranslateService) {}

  ngOnInit(): void {}
}
