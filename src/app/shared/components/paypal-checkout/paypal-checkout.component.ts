import { Component, OnInit } from "@angular/core";
import { environment } from "environments/environment";

@Component({
  selector: "cml-paypal-checkout",
  templateUrl: "./paypal-checkout.component.html",
  styles: [],
})
export class PaypalCheckoutComponent implements OnInit {
  public imgButton: string =
    "https://www.paypalobjects.com/webstatic/en_US/developer/docs/login/connectwithpaypalbutton.png";

  constructor() {}

  ngOnInit(): void {}

  openLinkPayPal = () => {
    window.open(
      `${environment.baseUrlPayPal}?flowEntry=static&client_id=${environment.paypal_client_id}&response_type=code&scope=openid profile email address&redirect_uri=${environment.paypal_redirect}&state=123456`,
      "_self",
      "fullscreen"
    );
  };
}
