import { Component, Input, Output } from "@angular/core";
import { EventEmitter, ViewChild } from "@angular/core";

import { TranslateService } from "@ngx-translate/core";

import { StripeCardComponent, StripeService } from "ngx-stripe";
import { Token, StripeElementsOptions } from "@stripe/stripe-js";
import { StripeCardElementOptions } from "@stripe/stripe-js";

import { getStripeLanguage } from "@shared/utils";

@Component({
  selector: "cml-stripe",
  templateUrl: "./stripe.component.html",
  styles: [],
})
export class StripeComponent {
  @ViewChild(StripeCardComponent) stripe: StripeCardComponent;

  @Input() submitted: boolean;
  @Input() grandTotal: number;
  @Input() proprietary: string;
  @Output() responseToken = new EventEmitter<Token>();
  @Output() eventLoadSubmitted = new EventEmitter<boolean>();

  public isLoading: boolean = false;
  public language: string;
  public cardErrors: string;

  public settingsCard: StripeCardElementOptions = {
    style: {
      base: {
        iconColor: "#666ee8",
        color: "#000",
        lineHeight: "40px",
        fontWeight: "300",
        fontSize: "20px",
      },
    },
  };

  public settingsElements: StripeElementsOptions = {
    locale: getStripeLanguage(this.translate.getDefaultLang()),
  };

  constructor(
    private translate: TranslateService,
    private stripeService: StripeService
  ) {}

  handleSubmit = async () => {
    this.cardErrors = null;

    if (!!!this.submitted) {
      this.eventLoadSubmitted.emit(true);

      const { token, error } = await this.stripeService
        .createToken(this.stripe.element, { name })
        .toPromise();

      if (error) {
        // Inform the customer that there was an error.
        this.cardErrors = error.message;
      } else {
        // Send the token to your server.
        this.responseToken.emit(token);
      }
    }
  };
}
