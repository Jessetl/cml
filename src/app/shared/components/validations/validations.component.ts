import { Component, Input } from "@angular/core";

@Component({
  selector: "cml-validations",
  templateUrl: "./validations.component.html",
  styles: [],
})
export class ValidationsComponent {
  @Input() errorMsg: string;
  @Input() displayError: boolean;
}
