import { Component, Input } from "@angular/core";

import { TranslateService } from "@ngx-translate/core";

import { typeCharge, getServiceDescriptionLang } from "@shared/utils";
import { getNameMaxByFormula, getCost } from "@shared/utils";
import { getNameMinByFormula, getNameTypeContent } from "@shared/utils";

import { Package } from "@models";

@Component({
  selector: "cml-display-service",
  templateUrl: "./display-service.component.html",
  styles: [],
})
export class DisplayServiceComponent {
  @Input() service: Package;

  constructor(private translate: TranslateService) {}

  typeOfCharge = (type: number) => {
    return typeCharge(type, this.translate);
  };

  serviceDescription = (service: Package) => {
    return getServiceDescriptionLang(service, this.translate.getDefaultLang());
  };

  typeContent = (type: number) => {
    return getNameTypeContent(type, this.translate);
  };

  getNameMin = (formula: number): string => {
    return getNameMinByFormula(formula, this.translate);
  };

  getNameMax = (formula: number): string => {
    return getNameMaxByFormula(formula, this.translate);
  };

  getNameCost = (formula: number): string => {
    return getCost(formula, this.translate);
  };
}
