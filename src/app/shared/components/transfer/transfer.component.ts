import { Component, Input, Output, EventEmitter, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";

import { BsLocaleService } from "ngx-bootstrap/datepicker";
import { TranslateService } from "@ngx-translate/core";

import { TransferValidators } from "./TransferValidators";

import { defineLocale } from "ngx-bootstrap/chronos";
import { esLocale, enGbLocale } from "ngx-bootstrap/locale";

import { TransferForm, Bancking } from "@models";
import { SIZE_FILE } from "@shared/utils";
import { showMessage, validationsForm } from "@shared/utils";
import { getAppLanguage } from "@shared/utils";

@Component({
  selector: "cml-transfer",
  templateUrl: "./transfer.component.html",
  styles: [],
})
export class TransferComponent implements OnInit {
  @Input() submitted: boolean;
  @Input() data?: Bancking;
  @Input() grandTotal: number;
  @Input() proprietary: string;
  @Input() formType: number = 0; // 0: Pago, 1: Productividad
  @Output() responseTransfer = new EventEmitter<TransferForm>();

  public language: string;
  public transfer: FormGroup;
  public maxDate: Date = new Date();

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private localeService: BsLocaleService
  ) {
    this.transfer = this.formBuilder.group(TransferValidators);
  }

  ngOnInit(): void {
    this.language = getAppLanguage(this.translate.getDefaultLang());
    this.transfer.patchValue(
      { issueDate: new Date(), amount: this.grandTotal.toFixed(2) },
      { onlySelf: true }
    );

    if (!!this.data) {
      this.transfer.patchValue(
        {
          destiny: this.data.institution,
          lastDigitsDestiny: this.data.interbank,
          bank_id: this.data.id,
        },
        { onlySelf: true }
      );

      this.transfer.get("destiny").disable();
      this.transfer.get("lastDigitsDestiny").disable();
    }

    switch (this.language) {
      case "es":
        defineLocale("es", esLocale);
      case "engb":
        defineLocale("engb", enGbLocale);
    }
  }

  ngAfterViewInit() {
    this.localeService.use(this.language);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.transfer.reset();
  }

  isFieldValid = (field: string): boolean => {
    return this.transfer.get(field).invalid && this.transfer.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  maxlenghtNumber = (event: Event) => {
    if ((<HTMLInputElement>event.target).value.length >= 4) {
      return false;
    }
  };

  onFileChanged = (event: any) => {
    const readerFile = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];

      if (this.maxFileSize(file.size)) {
        this.transfer.patchValue({ voucher: null }, { onlySelf: true });

        return showMessage(
          "",
          this.translate.instant("validations.fileInvalid"),
          "warning",
          true
        );
      }

      if (this.formatTypeImage(file.type)) {
        this.transfer.patchValue({ voucher: null }, { onlySelf: true });

        return showMessage(
          "",
          this.translate.instant("validations.format"),
          "warning",
          true
        );
      }

      readerFile.readAsDataURL(file);

      readerFile.onload = () => {
        const strResult = readerFile.result as string;
        this.transfer.patchValue({ voucher: strResult }, { onlySelf: true });
      };
    }
  };

  maxFileSize = (fileSize: number): boolean => {
    const isTrue: boolean = true;
    const sizeMegabytes = fileSize / 1024 / 1024;

    if (sizeMegabytes > SIZE_FILE) {
      return isTrue;
    } else {
      return !isTrue;
    }
  };

  formatTypeImage = (img: string): boolean => {
    const isTrue: boolean = true;

    switch (img) {
      case "image/jpeg":
        return !isTrue;
      case "image/png":
        return !isTrue;
      case "image/bmp":
        return !isTrue;
      case "application/pdf":
        return !isTrue;
      default:
        return isTrue;
    }
  };

  setPaymentAccount = () => {
    if (!!this.data) {
      switch (parseInt(this.transfer.get("option").value)) {
        case 0:
          return this.transfer.patchValue(
            { lastDigitsDestiny: this.data.interbank },
            { onlySelf: true }
          );
        case 1:
          return this.transfer.patchValue(
            { lastDigitsDestiny: parseInt(this.data.account_number) },
            { onlySelf: true }
          );
      }
    }
  };

  onSubmit = () => {
    const isValid = this.transfer.valid;

    if (!isValid) {
      return validationsForm(this.transfer);
    }

    return this.responseTransfer.emit(this.transfer.getRawValue());
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
