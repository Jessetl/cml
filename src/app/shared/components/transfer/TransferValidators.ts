import { FormControl, Validators } from "@angular/forms";

export const TransferValidators = {
  issueDate: new FormControl(new Date(), [Validators.required]),
  bank_id: new FormControl(null, []),
  origin: new FormControl(null, [Validators.required]),
  lastDigitsOrigin: new FormControl(null, [Validators.required]),
  destiny: new FormControl(null, [Validators.required]),
  lastDigitsDestiny: new FormControl(null, [Validators.required]),
  amount: new FormControl({ value: null, disabled: true }, [
    Validators.required,
  ]),
  reference: new FormControl(null, [Validators.required]),
  voucher: new FormControl(null, [Validators.required]),
  option: new FormControl({ value: 0, disabled: false }, [Validators.required]),
};
