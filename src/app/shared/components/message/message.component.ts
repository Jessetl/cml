import { Component, OnInit, Input, TemplateRef } from "@angular/core";
import { User, Chat, Message, Msg } from "@models";
import { ChatService } from "@core/service";
import { HttpErrorResponse } from "@angular/common/http";
import { rawError, handlerError, UNPROCESSABLE_ENTITY } from "@shared/utils";
import { showMessage } from "@shared/utils";
import { DomSanitizer, SafeResourceUrl } from "@angular/platform-browser";
import { FileSaverService } from "ngx-filesaver";
import { TranslateService } from "@ngx-translate/core";
import * as moment from "moment";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "cml-message",
  templateUrl: "./message.component.html",
  styles: [],
})
export class MessageComponent {
  @Input() me: User;
  @Input() chatOpen: Chat;
  private modalRef: NgbModalRef;

  private format: string = "MM-DD-YYYY-hh:mm";
  public formatMessage: string = "MM/DD/YY hh:mm a";

  public errors: Array<any>;
  public submitted: boolean;
  public urlFile: SafeResourceUrl;
  public fileDownload: any;

  public imageSrc: string;
  public page: number;

  constructor(
    private chatService: ChatService,
    private translate: TranslateService,
    public domSanitizer: DomSanitizer,
    private fileSaverService: FileSaverService,
    private ngbModal: NgbModal
  ) {}

  getFileExtension = (filename) => {
    return /[.]/.exec(filename) ? /[^.]+$/.exec(filename)[0] : undefined;
  };

  downloadFile = (message: Message) => {
    this.modalRef.close();
    if (this.submitted) {
      return;
    }
    this.submitted = true;
    this.chatService
      .downloadFile({
        type: this.getFileExtension(message.file),
        file: message.file,
      })
      .subscribe(
        (file: Blob) => {
          this.submitted = false;
          this.fileSaverService.save(file, this.getFileName());
        },
        (error: HttpErrorResponse) => {
          this.submitted = false;
          this.handleError(error);
        }
      );
  };

  getDateMessage = (date: string) => {
    return moment(date).format(this.formatMessage).toString();
  };

  getFileName = (): string => {
    return moment().format(this.format).toString();
  };

  handleError = (error: HttpErrorResponse) => {
    const errorResponse = handlerError(error);

    if (error.status === UNPROCESSABLE_ENTITY) {
      return showMessage(
        null,
        this.translate.instant("validators.emptyDataDisplay"),
        "warning",
        true
      );
    }

    this.errors = rawError(errorResponse);
  };

  openModal = async (ngbModal: TemplateRef<any>) => {
    this.modalRef = this.ngbModal.open(ngbModal, {
      size: "md",
    });
  };

  openPreview = (message: Message, modal: TemplateRef<any>) => {
    this.imageSrc = message.file_url;
    this.fileDownload = message;
    this.openModal(modal);
  };

  onCloseNgbModal = (): void => {
    this.fileDownload = {};
    this.modalRef.close();
  };

  openUrlLink = (message: Message) => {
    window.open(message.file_url, "_blank", "fullscreen");
  };

  getPreviousMessages = (id: number) => {
    this.page = this.chatOpen.current_page;
    this.chatService.message(id, this.page).subscribe(
      (pagination: Msg) => {
        let messages = pagination.data;
        this.page = this.page + 1;
        for (var key in messages) {
          this.chatOpen.chat_user = [messages[key], ...this.chatOpen.chat_user];
        }
        this.chatOpen.current_page = this.page;
        this.chatOpen.to = pagination.to;
      },
      (error: HttpErrorResponse) => {
        this.submitted = false;
        this.handleError(error);
      }
    );
  };
}
