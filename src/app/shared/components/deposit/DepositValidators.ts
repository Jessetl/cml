import { FormControl, Validators } from "@angular/forms";

export const DepositValidators = {
  issueDate: new FormControl(new Date(), [Validators.required]),
  bank_id: new FormControl(null, []),
  origin: new FormControl(null, [Validators.required]),
  destiny: new FormControl({ value: null, disabled: true }, []),
  reference: new FormControl(null, [Validators.required]),
  amount: new FormControl({ value: null, disabled: true }, [
    Validators.required,
  ]),
  voucher: new FormControl(null, [Validators.required]),
  option: new FormControl({ value: 1, disabled: false }, [Validators.required]),
};
