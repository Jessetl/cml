import { Component, OnInit } from "@angular/core";
import { Input, Output, EventEmitter } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";

import { BsLocaleService } from "ngx-bootstrap/datepicker";
import { TranslateService } from "@ngx-translate/core";

import { DepositValidators } from "./DepositValidators";

import { defineLocale } from "ngx-bootstrap/chronos";
import { esLocale, enGbLocale } from "ngx-bootstrap/locale";

import { Bancking, DepositForm } from "@models";
import { showMessage, validationsForm } from "@shared/utils";
import { getAppLanguage } from "@shared/utils";
import { SIZE_FILE } from "@shared/utils";

@Component({
  selector: "cml-deposit",
  templateUrl: "./deposit.component.html",
  styles: [],
})
export class DepositComponent implements OnInit {
  @Input() submitted: boolean;
  @Input() data?: Bancking;
  @Input() grandTotal: number;
  @Input() proprietary: string;
  @Input() transaction: number = 0;
  @Input() formType: number = 0; // 0: Pago, 1: Productividad
  @Output() responseDeposit = new EventEmitter<DepositForm>();

  public language: string;
  public deposit: FormGroup;
  public maxDate: Date = new Date();

  constructor(
    private translate: TranslateService,
    private formBuilder: FormBuilder,
    private localeService: BsLocaleService
  ) {
    this.deposit = this.formBuilder.group(DepositValidators);
  }

  ngOnInit(): void {
    this.language = getAppLanguage(this.translate.getDefaultLang());

    this.deposit.patchValue(
      { issueDate: new Date(), amount: this.grandTotal.toFixed(2) },
      { onlySelf: true }
    );

    if (!!this.data) {
      this.deposit.patchValue(
        {
          origin: this.data.institution,
          destiny: this.data.account_number,
          bank_id: this.data.id,
        },
        { onlySelf: true }
      );

      this.deposit.get("origin").disable();
    }

    switch (this.language) {
      case "es":
        defineLocale("es", esLocale);
      case "engb":
        defineLocale("engb", enGbLocale);
    }
  }

  ngAfterViewInit() {
    this.localeService.use(this.language);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.deposit.reset();
  }

  isFieldValid = (field: string): boolean => {
    return this.deposit.get(field).invalid && this.deposit.get(field).touched;
  };

  displayFieldCss = (field: string) => {
    return {
      "is-invalid": this.isFieldValid(field),
    };
  };

  onFileChanged = (event: any) => {
    const readerFile = new FileReader();

    if (event.target.files && event.target.files.length) {
      const file = event.target.files[0];

      if (this.maxFileSize(file.size)) {
        this.deposit.patchValue({ voucher: null }, { onlySelf: true });

        return showMessage(
          "",
          this.translate.instant("validations.fileInvalid"),
          "warning",
          true
        );
      }

      if (this.formatTypeImage(file.type)) {
        this.deposit.patchValue({ voucher: null }, { onlySelf: true });

        return showMessage(
          "",
          this.translate.instant("validations.format"),
          "warning",
          true
        );
      }

      readerFile.readAsDataURL(file);

      readerFile.onload = () => {
        const strResult = readerFile.result as string;
        this.deposit.patchValue({ voucher: strResult }, { onlySelf: true });
      };
    }
  };

  maxFileSize = (fileSize: number): boolean => {
    const isTrue: boolean = true;
    const sizeMegabytes = fileSize / 1024 / 1024;

    if (sizeMegabytes > SIZE_FILE) {
      return isTrue;
    } else {
      return !isTrue;
    }
  };

  formatTypeImage = (img: string): boolean => {
    const isTrue: boolean = true;

    switch (img) {
      case "image/jpeg":
        return !isTrue;
      case "image/png":
        return !isTrue;
      case "image/bmp":
        return !isTrue;
      case "application/pdf":
        return !isTrue;
      default:
        return isTrue;
    }
  };

  setPaymentAccount = () => {
    if (!!this.data) {
      switch (parseInt(this.deposit.get("option").value)) {
        case 0:
          return this.deposit.patchValue(
            { destiny: this.data.interbank },
            { onlySelf: true }
          );
        case 1:
          return this.deposit.patchValue(
            { destiny: parseInt(this.data.account_number) },
            { onlySelf: true }
          );
      }
    }
  };

  onSubmit = () => {
    const isValid = this.deposit.valid;

    if (!isValid) {
      return validationsForm(this.deposit);
    }

    return this.responseDeposit.emit(this.deposit.getRawValue());
  };

  validations = (value: string): string => {
    return this.translate.instant("validations.required", {
      name: this.translate.instant(value).toLowerCase(),
    });
  };
}
