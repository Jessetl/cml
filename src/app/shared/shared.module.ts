import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { HttpClient } from "@angular/common/http";
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";
import { NgxStripeModule } from "ngx-stripe";
import { environment } from "environments/environment";

import { ValidationsComponent } from "./components/validations/validations.component";
import { SpinnersComponent } from "./components/spinners/spinners.component";
import { ErrorsComponent } from "./components/errors/errors.component";
import { ModalComponent } from "./components/modal/modal.component";
import { ValidationspasswordComponent } from "./components/validationspassword/validationspassword.component";
import { ResaltarDirective } from "./directives/resaltar.directive";
import { OnlyNumberDirective } from "./directives/only-number.directive";
import { MessageComponent } from "./components/message/message.component";
import { LoadingComponent } from "./components/loading/loading.component";
import { LoadingButtonComponent } from "./components/loading-button/loading-button.component";
import { FloatingHomeComponent } from "./components/floating-home/floating-home.component";
import { DisplayUserComponent } from "./components/display-user/display-user.component";
import { DisplayServiceComponent } from "./components/display-service/display-service.component";
import { StripeComponent } from "./components/stripe/stripe.component";
import { TransferComponent } from "./components/transfer/transfer.component";
import { DepositComponent } from "./components/deposit/deposit.component";
import { FormUserComponent } from "./components/form-user/form-user.component";
import { DisplayCountryComponent } from "./components/display-country/display-country.component";
import { PaypalCheckoutComponent } from "./components/paypal-checkout/paypal-checkout.component";
import { UploadImageComponent } from "./components/upload-image/upload-image.component";
import { StripeSubscriptionsComponent } from "./components/stripe-subscriptions/stripe-subscriptions.component";
import { PaypalComponent } from "./components/paypal/paypal.component";
import { LegendComponent } from "./components/legend/legend.component";
import { FormBanckingComponent } from "./components/form-bancking/form-bancking.component";

@NgModule({
  imports: [
    CommonModule,
    BsDatepickerModule.forRoot(),
    NgxStripeModule.forChild(environment.stripeKey),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ValidationsComponent,
    SpinnersComponent,
    ErrorsComponent,
    ModalComponent,
    ValidationspasswordComponent,
    ResaltarDirective,
    OnlyNumberDirective,
    MessageComponent,
    LoadingComponent,
    LoadingButtonComponent,
    FloatingHomeComponent,
    DisplayUserComponent,
    DisplayServiceComponent,
    StripeComponent,
    TransferComponent,
    DepositComponent,
    FormUserComponent,
    DisplayCountryComponent,
    PaypalCheckoutComponent,
    UploadImageComponent,
    StripeSubscriptionsComponent,
    PaypalComponent,
    LegendComponent,
    FormBanckingComponent,
  ],
  exports: [
    ValidationsComponent,
    SpinnersComponent,
    ErrorsComponent,
    ModalComponent,
    ValidationspasswordComponent,
    ResaltarDirective,
    OnlyNumberDirective,
    MessageComponent,
    LoadingComponent,
    LoadingButtonComponent,
    FloatingHomeComponent,
    DisplayUserComponent,
    DisplayServiceComponent,
    StripeComponent,
    TransferComponent,
    DepositComponent,
    FormUserComponent,
    DisplayCountryComponent,
    PaypalCheckoutComponent,
    UploadImageComponent,
    StripeSubscriptionsComponent,
    PaypalComponent,
    LegendComponent,
    FormBanckingComponent,
  ],
})
export class SharedModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
