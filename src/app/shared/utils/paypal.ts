import { CustomerPaypal } from "@models";

export const formCustomer = (customer: CustomerPaypal): object => {
  return {
    user_id: customer.user_id,
    name: customer.name,
    email: customer.emails[0].value,
    country: customer.address.country,
    zip_code: customer.address.postal_code,
    confirmed: !!customer.emails[0].confirmed ? 1 : 0,
  };
};
