import { AGENCY_PRICE, CORPORATE_PRICE, PUBLIC_PRICE } from "./constants";

export const userSetting = (type: number, translate: any) => {
  switch (type) {
    case AGENCY_PRICE:
      return translate.instant("select.agency");
    case CORPORATE_PRICE:
      return translate.instant("select.corporate");
    case PUBLIC_PRICE:
      return translate.instant("select.public");
    default:
      return "";
  }
};
