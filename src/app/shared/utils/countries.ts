import { Country, Package, ChargeForm, Quote, Role } from "@models";

export const countriesLang = (
  countres: Country[],
  language: string
): Country[] => {
  return countres.map((country) => {
    return getNameCountryLang(country, language);
  });
};

export const getNameCountryLang = (
  country: Country,
  language: string
): Country => {
  return {
    ...country,
    name: language === "es" ? country.name : country.name_en,
    name_en: language === "es" ? country.name_en : country.name,
  };
};

export const getCountryNameLang = (
  country: Country,
  language: string
): string => {
  return language === "es" ? country.name : country.name_en;
};

export const getServiceNameLang = (
  service: Package,
  language: string
): string => {
  return language === "es" ? service.name : service.name_en;
};

export const getServiceDescriptionLang = (
  service: Package,
  language: string
): string => {
  return language === "es" ? service.description : service.description_en;
};

export const getChargeNameLang = (
  charge: ChargeForm,
  language: string
): string => {
  return language === "es" ? charge.package_name : charge.package_name_en;
};

export const getOriginNameLang = (
  shipping: Quote,
  language: string
): string => {
  return language === "es" ? shipping.origin : shipping.origin_en;
};

export const getRoleName = (role: Role, trasnlate: any): string => {
  return trasnlate == "es" ? role.name : role.name_en;
};
