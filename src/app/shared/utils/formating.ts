export const formatingPhone = (number: number): string => {
  if (!!number) {
    const phone = number.toString();
  
    switch (phone.length) {
      case 10:
        return phone.replace(/^(\d{3})(\d{3})(\d{4})$/, "($1) $2 $3");
      case 11:
        return phone.replace(/^(\d{1})(\d{3})(\d{3})(\d{4})$/, "$1 ($2) $3 $4");
      case 12:
        return phone.replace(/^(\d{2})(\d{3})(\d{3})(\d{4})$/, "$1 ($2) $3 $4");
      case 13:
        return phone.replace(/^(\d{3})(\d{3})(\d{3})(\d{4})$/, "($1) $2 $3 $4");
      case 14:
        return phone.replace(/^(\d{3})(\d{3})(\d{4})(\d{4})$/, "($1) $2 $3 $4");
      case 15:
        return phone.replace(
          /^(\d{1})(\d{3})(\d{3})(\d{4})(\d{4})$/,
          "($1) $2 $3 $4 $5"
        );
    }
  }
};
