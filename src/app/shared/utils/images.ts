export const ImagesLocker = (lang: string): any[] => {
  let images = [
    { id: 1, file: "../../../assets/img/amazones.png", laguage: "es" },
    { id: 2, file: "../../../assets/img/amazonen.png", laguage: "en" },
    { id: 3, file: "../../../assets/img/wishes.png", laguage: "es" },
    { id: 4, file: "../../../assets/img/wishen.png", laguage: "en" },
    { id: 5, file: "../../../assets/img/bestbuyes.png", laguage: "es" },
    { id: 6, file: "../../../assets/img/bestbuyen.png", laguage: "en" },
  ];
  return images.filter(({ laguage }) => laguage === lang);
};
