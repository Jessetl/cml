import {
  PAYMENT_METHOD_STRIPE,
  PAYMENT_METHOD_TRANSFER,
  PAYMENT_METHOD_CHECK,
} from "./constants";
export const ShippingType = Object.freeze({
  REMITTER: 1,
  RECEIVER: 2,
});

export const typePayments = (type: number, translate: any): string => {
  switch (type) {
    case PAYMENT_METHOD_STRIPE:
      return translate.instant("payment.stripe");
    case PAYMENT_METHOD_CHECK:
      return translate.instant("payment.check");
    case PAYMENT_METHOD_TRANSFER:
      return translate.instant("payment.transfer");
  }
};
