export const TierId = Object.freeze({
  TIER_ADMIN: 1,
  TIER_MANAGEMENT: 2,
  TIER_ACCOUNTING: 3,
  TIER_SUPPORT: 4,
  TIER_AGENCY: 5,
  TIER_STORER: 6,
  TIER_ASSISTANT: 7,
  TIER_DRIVER: 8,
  TIER_DISPATCHER: 9,
  TIER_RRHH: 10,
  TIER_LOCKERS: 11,
  TIER_SUPERVISOR: 12,
  TIER_SELLER: 13,

  getDisplayName(tier: number) {
    switch (tier) {
      case TierId.TIER_ADMIN:
        return "administrator";
      case TierId.TIER_MANAGEMENT:
        return "management";
      case TierId.TIER_ACCOUNTING:
        return "accounting";
      case TierId.TIER_SUPPORT:
        return "support";
      case TierId.TIER_AGENCY:
        return "agency";
      case TierId.TIER_STORER:
        return "storer";
      case TierId.TIER_ASSISTANT:
        return "assistant";
      case TierId.TIER_DRIVER:
        return "driver";
      case TierId.TIER_DISPATCHER:
        return "dispatcher";
      case TierId.TIER_RRHH:
        return "humanResources";
      case TierId.TIER_LOCKERS:
        return "lockers";
      case TierId.TIER_SUPERVISOR:
        return "supervisor";
      case TierId.TIER_SELLER:
        return "seller";
      default:
        return "";
    }
  },
});

export const getLevelUser = (tier: number) => {
  switch (tier) {
    case TierId.TIER_ADMIN:
      return "administrator";
    case TierId.TIER_MANAGEMENT:
      return "management";
    case TierId.TIER_ACCOUNTING:
      return "accounting";
    case TierId.TIER_SUPPORT:
      return "support";
    case TierId.TIER_AGENCY:
      return "agency";
    case TierId.TIER_STORER:
      return "storer";
    case TierId.TIER_ASSISTANT:
      return "assistant";
    case TierId.TIER_DRIVER:
      return "driver";
    case TierId.TIER_DISPATCHER:
      return "dispatcher";
    case TierId.TIER_RRHH:
      return "humanResources";
    case TierId.TIER_LOCKERS:
      return "lockers";
    case TierId.TIER_SUPERVISOR:
      return "supervisor";
    case TierId.TIER_SELLER:
      return "seller";
    default:
      return "";
  }
};
