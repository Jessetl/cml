import { HttpErrorResponse } from "@angular/common/http";

const INTERNAL_SERVER_ERROR: number = 500;
const NOT_FOUND_ERROR: number = 404;
const UNAUTHORIZED: number = 401;
const UNPROCESSABLE_ENTITY: number = 422;
const FORBIDDEN = 403;
const UNKNOWN_ERROR: number = 0;
const METHOD_NOT_ALLOWED: number = 405;

export const handlerError = (error: HttpErrorResponse) => {
  const { status, error: errorResponse } = error;

  console.log(error, "ERROR");

  switch (status) {
    case UNKNOWN_ERROR:
      return ".UNKNOWN_ERROR";
    case NOT_FOUND_ERROR:
      return ".NOT_FOUND_ERROR";
    case INTERNAL_SERVER_ERROR:
      return ".INTERNAL_SERVER_ERROR";
    case UNAUTHORIZED:
      return ".UNAUTHORIZED";
    case FORBIDDEN:
      return ".FORBIDDEN";
    case METHOD_NOT_ALLOWED:
      return ".METHOD_NOT_ALLOWED";
    case UNPROCESSABLE_ENTITY:
      return errorResponse.errors;
  }
};

export const rawError = (errors: any) => {
  let errorsResponse = [];

  for (const property in errors) {
    if (errors.hasOwnProperty(property)) {
      const propertyErrors: Array<string> = errors[property];
      propertyErrors.forEach(
        (error) => (errorsResponse = [...errorsResponse, error])
      );
    }
  }

  return errorsResponse;
};

export {
  INTERNAL_SERVER_ERROR,
  NOT_FOUND_ERROR,
  UNAUTHORIZED,
  UNPROCESSABLE_ENTITY,
  FORBIDDEN,
  METHOD_NOT_ALLOWED,
};
