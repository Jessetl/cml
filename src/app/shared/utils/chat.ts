import { Chat, User, Message } from "@models";
import { TIER_ADMIN, STATUS_READ } from "./constants";
import { getRoleName } from "./countries";
import { getLevelUser } from "./roles";

const role = (user: any, translate: string): string => {
  return user.role.length > 0 ? getRoleName(user.role[0], translate) : "";
};

const badgeChat = (chat: Chat, me: User): number => {
  return chat.chat_user.filter((message) => {
    return message.status != STATUS_READ && message.receiver_id == me.id;
  }).length;
};

export const userNameChat = (chat: Chat, me: User): User => {
  return userChat(chat, me);
};

export const statusUnread = (message: Message, me: User): boolean => {
  return !!message && message.receiver_id === me.id && message.status === 0;
};

export const existChat = (id: number, chats: Chat[]) => {
  return chats.find((chat) => {
    return chat.transmitter_id == id || chat.receiver_id == id;
  });
};

const userChat = (chat: Chat, me: User): User => {
  return chat.transmitter_id == me.id ? chat.receiver : chat.transmitter;
};

export const chatName = (chat: Chat, me: User, translate: any) => {
  let user = userNameChat(chat, me);
  let nameLevel =
    !!user.agency && user.agency.tier === 5
      ? translate.instant("cardText.employee")
      : translate.instant("tierId." + getLevelUser(user.tier));
  return {
    ...chat,
    chat_user: chat.chat_user.reverse(),
    name: user.person.name,
    role: nameLevel,
    badge: badgeChat(chat, me),
    current_page: 2,
    to: 1,
  };
};

export const sendMessage = (chat: Chat, me: User, message: string): object => {
  return {
    user_id: me.id,
    chat_id: chat.id,
    receiver_id: userChat(chat, me).id,
    message: message,
    file: null,
  };
};

export const fileMessage = (chat: Chat, me: User, file: any): object => {
  return {
    message: null,
    file: file,
    chat_id: chat.id,
    receiver_id: userChat(chat, me).id,
  };
};

export const formArchived = (archive: boolean, chat: Chat, me: User) => {
  return {
    id: chat.id,
    user_id: !archive ? userChat(chat, me).id : null,
  };
};

export const notForMe = (
  chatReceiver: Chat,
  chats: Chat[],
  user: User
): boolean => {
  let existList = chats.find((chat) => chat.id == chatReceiver.id);
  let transmitter = chatReceiver.transmitter_id;
  let receiver = chatReceiver.receiver_id;
  let me = user.id;
  const chat_me = transmitter == me || receiver == me ? true : false;
  return existList != undefined || !chat_me;
};

export const messageChatArchived = (
  message: Message,
  chats: Chat[],
  user: User
): boolean => {
  const existList = chats.find((chat) => chat.id == message.chat_id);
  let me = user.id;
  let receiver = message.receiver_id;
  let transmitter = message.user_id;
  const chat_me = receiver == me || transmitter == me ? true : false;
  return existList === undefined && chat_me;
};
