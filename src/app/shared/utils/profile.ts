import { Role as Profile } from "@models";

export const profilesLang = (
  profiles: Profile[],
  language: string
): Profile[] => {
  return profiles.map((profile) => {
    return getNameProfileLang(profile, language);
  });
};

export const getNameProfileLang = (
  profile: Profile,
  language: string
): Profile => {
  return {
    ...profile,
    name: language == "es" ? profile.name : profile.name_en,
    name_en: language == "es" ? profile.name_en : profile.name,
  };
};
