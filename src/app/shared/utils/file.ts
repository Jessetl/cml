import { SIZE_FILE } from "./constants";

export const formatTypeImage = (img: string): boolean => {
  switch (img) {
    case "image/jpeg":
    case "image/png":
    case "image/bmp":
    case "application/pdf":
      return false;
    default:
      return true;
  }
};

export const maxFileSize = (fileSize: number): boolean => {
  const sizeMegabytes = fileSize / 1024 / 1024;
  return sizeMegabytes > SIZE_FILE;
};
