import * as io from "socket.io-client";
import { environment } from "environments/environment";

const Socket_ = io(environment.socketUrl, {
  autoConnect: false,
  reconnection: true,
  reconnectionDelay: 1000,
  reconnectionDelayMax: 5000,
  reconnectionAttempts: Infinity,
  forceNew: false,
  transports: ["websocket"],
  upgrade: false,
  secure: true,
});

export const Socket = Socket_;
