import Swal, { SweetAlertResult, SweetAlertOptions } from "sweetalert2";

export const showMessage = (
  title: string,
  message: string,
  icon: "error" | "warning" | "success",
  showConfirmButton: boolean
) => {
  Swal.fire({
    title: title,
    text: message,
    icon: icon,
    showConfirmButton: showConfirmButton,
  });
};

export const showMessageConfirm = (
  title: string,
  message: string,
  icon: "error" | "warning" | "success",
  yes: string,
  callback?: any
) => {
  Swal.fire({
    title: title,
    text: message,
    icon: icon,
    confirmButtonText: yes,
    showCancelButton: false,
  }).then((confirm: SweetAlertResult) => {
    if (confirm.value) {
      callback();
    }
  });
};

export const showConfirmButton = (
  message: string,
  yes: string,
  no: string,
  callback?: any
) => {
  Swal.fire({
    title: "",
    text: message,
    icon: "warning",
    showCancelButton: true,
    confirmButtonText: yes,
    cancelButtonText: no,
  }).then((confirm: SweetAlertResult) => {
    if (confirm.value) {
      callback();
    }
  });
};

export const confirm = ({
  title = "",
  text,
  icon = "warning",
  confirmButtonText,
  cancelButtonText,
}: SweetAlertOptions) => {
  return new Promise((resolve, _) => {
    Swal.fire({
      title,
      text,
      icon,
      showCancelButton: true,
      confirmButtonText,
      cancelButtonText,
      focusConfirm: false,
      focusCancel: false,
      cancelButtonColor: "#dc3545",
      allowEscapeKey: false,
      allowEnterKey: false,
    }).then(({ value }) => {
      resolve(!!value);
    });
  });
};
