export const TypePayment = Object.freeze({
  TYPE_INGRESS: 1,
  TYPE_COMMISSION: 2,

  getDisplayName(type: number) {
    switch (type) {
      case TypePayment.TYPE_INGRESS:
        return "select.ingress";
      case TypePayment.TYPE_COMMISSION:
        return "select.commission";
      default:
        return "";
    }
  },
});

export const TypeSetting = Object.freeze({
  TYPE_SUPERVISOR: 1,
  TYPE_SELLER: 2,

  getDisplayName(type: number) {
    switch (type) {
      case TypeSetting.TYPE_SUPERVISOR:
        return "select.supervisor";
      case TypeSetting.TYPE_SELLER:
        return "select.seller";
      default:
        return "";
    }
  },
});

export const TypeUser = Object.freeze({
  TYPE_SUPERVISOR: 1,
  TYPE_SELLER: 2,
  TYPE_AGENCY: 3,
  TYPE_LOCKER: 4,

  getDisplayName(type: number) {
    switch (type) {
      case TypeUser.TYPE_SUPERVISOR:
        return "select.supervisor";
      case TypeUser.TYPE_SELLER:
        return "select.seller";
      case TypeUser.TYPE_AGENCY:
        return "select.agency";
      case TypeUser.TYPE_LOCKER:
        return "select.locker";
      default:
        return "";
    }
  },
});
