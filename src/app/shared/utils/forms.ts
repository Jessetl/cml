import { FormGroup, FormControl } from "@angular/forms";

export const validationsForm = (formGroup: FormGroup) => {
	Object.keys(formGroup.controls).forEach(field => {
		const control = formGroup.get(field);
		if (control instanceof FormControl) {
			control.markAsTouched({ onlySelf: true });
		} else if (control instanceof FormGroup) {
			this.validationsForm(control);
		}
	});
};

export const generateForm = (formGroup: FormGroup) => {
	let ObjectForm = {};

	Object.keys(formGroup.controls).forEach(field => {
		ObjectForm = formGroup.get(field).value;
	});

	return ObjectForm;
};
