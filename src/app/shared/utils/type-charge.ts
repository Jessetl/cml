import {
  PACKAGE_AERIAL,
  PACKAGE_LAND,
  PACKAGE_MARITIME,
  FORMULA_PACKAGEPOUND,
} from "./constants";
import { FORMULA_CUBICFOOT, FORMULA_INCHLINEAR } from "./constants";
import { FORMULA_TELEVISIONS, TYPE_COMERCIAL } from "./constants";
import { TYPE_ELECTRONIC, TYPE_REGULAR, TYPE_MIXED } from "./constants";

export const typeCharge = (type: number, translate: any): string => {
  switch (type) {
    case PACKAGE_MARITIME:
      return translate.instant("select.maritime");
    case PACKAGE_LAND:
      return translate.instant("select.land");
    case PACKAGE_AERIAL:
      return translate.instant("select.aerial");
    default:
      return "";
  }
};

export const getNameMaxByFormula = (
  formula: number,
  translate: any
): string => {
  switch (formula) {
    case FORMULA_TELEVISIONS:
    case FORMULA_INCHLINEAR:
      return translate.instant("formBuilder.maximumInch");
    default:
      return translate.instant("formBuilder.maximumWeight");
  }
};

export const getNameMinByFormula = (
  formula: number,
  translate: any
): string => {
  switch (formula) {
    case FORMULA_TELEVISIONS:
    case FORMULA_INCHLINEAR:
      return translate.instant("formBuilder.minimumInch");
    case FORMULA_CUBICFOOT:
      return translate.instant("formBuilder.minmumFoot");
    default:
      return translate.instant("formBuilder.minimumWeight");
  }
};

export const getNameTypeContent = (type: number, translate: any) => {
  switch (type) {
    case TYPE_REGULAR:
      return translate.instant("select.regular");
    case TYPE_ELECTRONIC:
      return translate.instant("select.electronic");
    case TYPE_COMERCIAL:
      return translate.instant("select.commercial");
    case TYPE_MIXED:
      return translate.instant("select.mixed");
    default:
      return "";
  }
};

export const getCost = (formula: number, translate: any): string => {
  switch (formula) {
    case FORMULA_TELEVISIONS:
    case FORMULA_INCHLINEAR:
      return translate.instant("formBuilder.cost_inch");
    case FORMULA_CUBICFOOT:
      return translate.instant("formBuilder.cost_foot");
    case FORMULA_PACKAGEPOUND:
      return translate.instant("formBuilder.cost_pound_additional");
    default:
      return translate.instant("formBuilder.cost_pound");
  }
};
