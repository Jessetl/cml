export const TypeContent = Object.freeze({
  TYPE_REGULAR: 1,
  TYPE_ELECTRONIC: 2,
  TYPE_MIXED: 3,
  TYPE_COMMERCIAL: 4,

  getDisplayName(type: number) {
    switch (type) {
      case TypeContent.TYPE_REGULAR:
        return "select.regular";
      case TypeContent.TYPE_ELECTRONIC:
        return "select.electronic";
      case TypeContent.TYPE_COMMERCIAL:
        return "select.commercial";
      case TypeContent.TYPE_MIXED:
        return "select.mixed";
      default:
        return "";
    }
  },
});
