import { StripeElementLocale } from "@stripe/stripe-js";
import { Notification } from "@models";

export const getAppLanguage = (language: string): string => {
  return language === "es" ? "es" : "engb";
};

export const getStripeLanguage = (language: string): StripeElementLocale => {
  return language === "es" ? "es" : "en";
};

export const langNotification = (
  notification: Notification,
  language: string
) => {
  return language === "es" ? notification.content : notification.content_en;
};
