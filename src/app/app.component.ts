import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "cml-root",
  template: `
    <div id="cml-wrapper" class="container-full">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [],
})
export class AppComponent implements OnInit {
  constructor(private translate: TranslateService) {}

  ngOnInit() {
    const language = localStorage.getItem("language");

    if (!language) {
      localStorage.setItem("language", "es");

      this.translate.setDefaultLang("es");
      this.translate.use("es");

      return;
    }

    this.translate.setDefaultLang(language);
    this.translate.use(language);
  }
}
