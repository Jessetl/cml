#!/bin/sh
USER=${USER:-root}
HOST=${HOST:-162.254.253.89}
PORT=${PORT:-467}

SOURCE=${SOURCE:-build}
TARGET=${TARGET:-/home/cmlapps/cmlexportsweb}

echo "Removing existing files..."
ssh $USER@$HOST -p $PORT "rm -ifr $TARGET"
echo ""
echo "Uploading new files..."
echo ""
scp -P $PORT -r "$SOURCE" "$USER@$HOST:$TARGET"
echo ""
echo "Done"